#!/bin/sh

async () {
	 sleep $1;
	 shift;
	 "${@}";
}

cp ./.env.preprod ./.env
php composer.phar install --no-dev
chmod -R 777 .
php composer.phar dump-autoload
php artisan l5-swagger:generate
service supervisor start
service cron start
chmod -R 777 .

async 10 php artisan opcache:clear && php artisan opcache:invalidate && php artisan opcache:optimize &
php-fpm
