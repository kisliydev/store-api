<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Tests\Traits\ApiEnv;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations, ApiEnv;

    /**
     * @var bool
     */
    protected $useTestDBConnection = false;

    /**
     * @var array
     */
    protected $SQLiteTests = [];

    public $start;
    /**
     * @return void
     */
    public function setUp()
    {
//        $this->start = microtime(true);

        parent::setUp();

        if ($this->_useTestDB()) {
            Config::set('database.default', 'test');
            Artisan::call('migrate:refresh');
            $this->seed('CountriesTableSeeder');
        } else {
            Artisan::call('migrate');
        }
    }

    /**
     * @return void
     */
    public function tearDown()
    {
//        $time = (microtime(true) - $this->start);
//        Log::info(explode('::',$this->toString())[0] . " - {$this->getName()} - {$time} - {$this->hasFailed()}");

        parent::tearDown();

        if ($this->_useTestDB()) {
            Config::set('database.default', 'mysql');
        }
    }

    /**
     * @return bool
     */
    private function _useTestDB()
    {
        return $this->useTestDBConnection && (empty($this->SQLiteTests) || !in_array($this->getName(), $this->SQLiteTests));
    }
}
