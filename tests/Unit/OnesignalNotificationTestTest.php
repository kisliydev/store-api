<?php

namespace Tests\Unit;

use App\Models\PlayerId;
use App\Models\UserChannelStatus;
use Tests\TestCase;
use App\Jobs\OneSignalPushNotifications;
use Illuminate\Notifications\Notification;
use App\Models\User;
use App\Models\Channel;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use Faker\Factory as Faker;

use NotificationChannels\OneSignal\OneSignalChannel;
use App\Classes\OnesignalClientDecorator;

class OnesignalNotificationTestTest extends TestCase
{
    private $_users;

    private $_pushMessage;

    /**
     * @return void
     */
    public function testSendToDifferentAppsSuccess()
    {
        Config::set('mail.intercept', false);

        $this->_createEnvorinment();

        Log::spy();

        app()->bind(OneSignalChannel::class, function() {
            /**
             * @see OnesignalChannelMock::send() to discover which exactly asserts were performed
             */
            return new OnesignalChannelMock(new OnesignalClientDecorator('','',''), $this);
        });

        $job = new OneSignalPushNotifications($this->_users, $this->_pushMessage, get_class($this));
        $job->handle(new User(), new Channel());

        Config::set('mail.intercept', true);
    }

    /**
     * @return void
     */
    private function _createEnvorinment()
    {
        $channelID = $this->setupChannel();
        $faker = Faker::create();
        $apps = array_merge([null], config('onesignal.universal_apps'));
        $i = 0;

        $this->_users = factory(User::class, count($apps))
            ->create(['verified' => true])
            ->each(function($user) use ($faker, $apps, $channelID, &$i) {
                PlayerId::create([
                    'status_id' => $user->bindToChannel($channelID, 'active')->id,
                    'player_id' => $faker->uuid,
                    'app_name'  => $apps[$i],
                ]);

                $i ++;
            })->pluck('id')->toArray();

        $this->_pushMessage = [
            'channel_id' => $channelID,
            'subject'    => 'Test Message',
            'body'       => 'Test Message Content',
        ];
    }
}

/**
 * Class OnesignalChannelMock
 *
 * @package Tests\Unit
 */
class OnesignalChannelMock extends OneSignalChannel {

    /**
     * @var \Tests\Unit\OnesignalNotificationTestTest
     */
    protected $test;

    /**
     * OnesignalChannelMock constructor.
     *
     * @param \Berkayk\OneSignal\OneSignalClient $oneSignal
     * @param \Tests\Unit\OnesignalNotificationTestTest $test
     */
    public function __construct(OnesignalClientDecorator $oneSignal, OnesignalNotificationTestTest $test)
    {
        $this->oneSignal = $oneSignal;
        $this->test = $test;
    }

    /**
     * @param mixed $notifiable
     * @param \Illuminate\Notifications\Notification $notification
     * @return \Psr\Http\Message\ResponseInterface|void
     */
    public function send($notifiable, Notification $notification)
    {
        $data = PlayerId::where('player_id', $notifiable->routeNotificationFor('OneSignal')[0])->first();
        $config = config('services.onesignal');

        if (! $data->app_name) {
            $settings = Channel::find(UserChannelStatus::find($data->status_id)->channel_id)->countryBrand->brand->settings;
            $this->test->assertTrue($config['app_id'] == $settings['onesignal_app_id']);
            $this->test->assertTrue($config['rest_api_key'] ==  $settings['onesignal_rest_api_key']);
        } else {
            $this->test->assertTrue($config['app_id'] == env("ONESIGNAL_APP_ID_" . strtoupper($data->app_name)));
            $this->test->assertTrue($config['rest_api_key'] == env("ONESIGNAL_REST_API_KEY_" . strtoupper($data->app_name)));
        }
    }
}
