<?php
namespace Tests\Unit;

use App\Models\User;
use App\Models\ManualPoint;
use App\Jobs\OneSignalPushNotifications;
use Tests\TestCase;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Log;
use App\Jobs\ManualPointsUpdateStatistics;
use Spatie\Permission\Models\Role;

class ManualPointsStatisticsTest extends TestCase
{
    /**
     * @var int
     */
    private $_channel_id;

    /**
     * @var int
     */
    private $_dealers_ids;

    /**
     * @var int
     */
    private $_district_id;

    /**
     * @var \App\Models\ManualPoint
     */
    private $_point;

    /**
     * @var array
     */
    private $_users = [];

    /**
     * @var array
     */
    private $_setup_data = [
        'title' => 'Lorem Ipsum',
        'description' => 'Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.',
        'points' => 100,
        'recipients' => 'all:0',
    ];

    /**
     * @return void
     */
    public function testPointStatisticsJob()
    {
        app()['cache']->forget('spatie.permission.cache');
        Role::firstOrCreate(['name' => 'app_user']);
        Event::fake();
        Log::spy();

        $this->_channel_id = $this->setupChannel();
        $this->_setup_data['channel_id'] = $this->_channel_id;
        $this->_point = factory(ManualPoint::class)->create($this->_setup_data);
        $this->_district_id = $this->setupDistrict($this->_channel_id);
        $this->_dealers_ids = $this->setupDealer($this->_channel_id, 2, ['district_id' => $this->_district_id]);
        for ($i=0; $i < 6; $i++) {
            $user = factory(User::class)->create(['verified' => 1]);
            $user->assignRole('app_user');
            $user->bindToChannel($this->_channel_id, 'active');
            if ($i < 4) {
                $user->bindToDealer($this->_dealers_ids[$i%2]);
            }
            $this->_users[] = $user;
        }

        $this->createEnvironment(['view_points']);

        app()->bind(ManualPointsUpdateStatistics::class, function() {
            return new ManualPointsUpdateStatistics($this->_point, 'test', $this->user->id);
        });

        Queue::fake();
        app()->call(ManualPointsUpdateStatistics::class . '@handle');
        $this->assertDatabaseHas('threads', ['author_id' => $this->user->id]);
        foreach($this->_users as $user) {
            $this->assertDatabaseHas('thread_send_statistics', ['sent_to' => $user->id]);
        }
        Queue::assertPushed(OneSignalPushNotifications::class, 1);
    }
}
