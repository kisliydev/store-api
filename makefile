include .env
export

docker_name = $(DOCKER_PREFIX)-php-fpm
docker_image = $(DOCKER_PREFIX)-php-fpm
mysql_container = $(DOCKER_PREFIX)-mysql

help: #prints list of commands
	@cat ./makefile | grep : | grep -v "grep"

install: #start docker container #
	@cp ./.env.local ./.env && sudo docker-compose up -d && sudo docker exec -it $(docker_name) bash -c 'php composer.phar install && chmod -R 777 . && php composer.phar dump-autoload && php artisan migrate:refresh --seed'

start: #start docker container #
	@sudo docker-compose up -d

start_local: #start all containers and setup local environment
	@sudo docker-compose -f docker-compose-local.yml up -d

stop: #stop docker container
	@sudo docker-compose down

restart: #restart docker container
	@sudo docker-compose down && docker-compose up -d

remove: #remove docker image
	@sudo docker-compose down; sudo docker rmi -f $(docker_image)

remove_test_db: #remove testing DB volume
	@sudo docker volume rm backend_db_test_data

commit: #commit with tests name=[commitName]
	@sudo make test && git commit -am '$(name)'

push: #push all changes m=[commitMessage] b=[branchName]
	@sudo git commit -am '$(m)' && git push origin $(b)

composer_update: #update vendors
	@sudo docker exec -it $(docker_name) bash -c 'php composer.phar update && chmod -R 777 . && php composer.phar dump-autoload'

composer_dump: #update vendors
	@sudo docker exec -it $(docker_name) bash -c 'php composer.phar dump-autoload'

type=local
set_env: #set default environments
	@cp ./.env.$(type) ./.env

create_link: #creates symbolic link to make images accessible
	@ln -s ../storage/app ./public/storage

create_controller: #create controller name=[controllerName]
	@sudo docker exec -it $(docker_name) bash -c 'php artisan make:controller $(name) && chmod -R 777 .'

create_model: #create model name=[modelName]
	@sudo docker exec -it $(docker_name) bash -c 'php artisan make:model "Models\$(name)" -m && chmod -R 777 .'

create_seeder: #create seeder name=[seederName]
	@sudo docker exec -it $(docker_name) bash -c 'php artisan make:seeder $(name) && chmod -R 777 .'

create_test: #create test name=[Name]
	@sudo docker exec -it $(docker_name) bash -c 'php artisan make:test $(name)Test && chmod -R 777 .'

create_email: #create email name=[Name]
	@sudo docker exec -it $(docker_name) bash -c 'php artisan make:mail $(name) && chmod -R 777 .'

create_middleware: #create middleware name=[Name]
	@sudo docker exec -it $(docker_name) bash -c 'php artisan make:middleware $(name) && chmod -R 777 .'

create_resource: #create resource name=[Name]
	@sudo docker exec -it $(docker_name) bash -c 'php artisan make:resource $(name) && chmod -R 777 .'

create_request: #create request name=[Name]
	@sudo docker exec -it $(docker_name) bash -c 'php artisan make:request $(name) && chmod -R 777 .'

create_command: #create console command name=[Name]
	@sudo docker exec -it $(docker_name) bash -c 'php artisan make:command $(name) && chmod -R 777 .'

level=1
load_images=0
load_points: #Load Points Statistics
	@sudo docker exec -it $(docker_name) bash -c 'php artisan points:load $(level) $(load_images)'

create_queue_job: #create queue job name=[Name]
	@sudo docker exec -it $(docker_name) bash -c 'php artisan make:job $(name) && chmod -R 777 .'

create_rule: #create validation rule name=[Name]
	@sudo docker exec -it $(docker_name) bash -c 'php artisan make:rule $(name) && chmod -R 777 .'

create_provider: #create provider name=[Name]
	@sudo docker exec -it $(docker_name) bash -c 'php artisan make:provider $(name) && chmod -R 777 .'

create_notification: #create notification name=[Name]
	@sudo docker exec -it $(docker_name) bash -c 'php artisan make:notification $(name) && chmod -R 777 .'

create_event: #create event name=[Name]
	@sudo docker exec -it $(docker_name) bash -c 'php artisan event:generate $(name) && chmod -R 777 .'

create_migration: #create new migration
	@sudo docker exec $(docker_name) bash -c 'php artisan make:migration $(name)'

migration: #run migration
	@sudo docker exec $(docker_name) bash -c 'php composer.phar dump-autoload && php artisan migrate --force'

seed: #run migration
	@sudo docker exec -it $(docker_name) bash -c 'php composer.phar dump-autoload && php artisan db:seed'

seed_class: #run migration specific class name="$(name)"
	@sudo docker exec -it $(docker_name) bash -c 'php composer.phar dump-autoload && php artisan db:seed --class=$(name)'

refresh: #Refresh the database and run all database seeds
	@sudo docker exec -it $(docker_name) bash -c 'php composer.phar dump-autoload && php artisan migrate:refresh --seed'

chmod: #allow RW to all
	@sudo docker exec -it $(docker_name) bash -c 'chmod -R 777 .'

route: #show all routes
	@sudo docker exec -it $(docker_name) bash -c 'php artisan route:list'

conf: #refresh config cache
	@sudo docker exec -it $(docker_name) bash -c 'php artisan config:cache'

test: #run test cases
	@sudo docker exec -it $(docker_name) bash -c 'php artisan config:clear && vendor/bin/phpunit'

test_class: #test specific class name="$(name)"
	@sudo docker exec -it $(docker_name) bash -c 'vendor/bin/phpunit --filter $(name)'

clear_cache: #clear laravel cache php artisan optimize --force php artisan config:cache php artisan route:cache
	@sudo docker exec $(docker_name) bash -c 'php artisan cache:clear && php artisan view:clear && php artisan route:clear && php artisan config:clear'

socket: #start socket message service
	@sudo docker exec -it $(docker_name) bash -c 'php artisan socket_messages_server:serve'

remove_tmp_files: #remove files from tmp folder
	@sudo docker exec -it $(docker_name) bash -c 'php artisan tmp_files:remove'

connect: #connect to container bash
	@sudo docker exec -it $(docker_name) bash

connect_mysql: #connect to container bash
		@sudo docker exec -it $(mysql_container) bash

mysql_backup: #database backup
	@sudo docker exec $(mysql_container) /usr/bin/mysqldump -u root --password=12345 forge > backup.sql

mysql_import: #load mysql backup file=[path to SQL file]
	@sudo docker exec -i $(mysql_container) /usr/bin/mysql -u root --password=12345 forge < $(file)

version: #laravel version
	@sudo docker exec -it $(docker_name) bash -c 'php artisan --version'

volumes: #docker volumes list
	@sudo docker volume ls

rm_volume: #remove docker volume name=[volumeName]
	@sudo docker-compose down && sudo docker volume rm $(name)

cache_route: #cache route
	@sudo docker exec $(docker_name) bash -c 'php artisan route:cache'

start_queue: #start queue worker
	@sudo docker exec -it $(docker_name) bash -c 'php artisan queue:work'

prod: #optimizations for production server
	@sudo docker exec -it $(docker_name) bash -c 'php artisan route:clear && php artisan route:cache && php artisan optimize'

swagger_publish: #publish swagger conf
	@sudo docker exec -it $(docker_name) bash -c 'php artisan l5-swagger:publish'

swagger: #generate dock
	@sudo docker exec -it $(docker_name) bash -c 'php artisan l5-swagger:generate'

populate_vendors: #generate dock
	@sudo docker exec -it $(docker_name) bash -c 'cp -R ./vendor ./ven && chmod -R 777 .' && sudo sh -c 'rm -R ./vendor; mv ./ven ./vendor'

clear_log: #clear log file
	@sudo cat /dev/null > storage/logs/laravel.log && cat /dev/null > storage/logs/queue-worker.log && cat /dev/null > storage/logs/socket-messages-worker.log

