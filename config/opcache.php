<?php

return [
    /**
     * Directories list for OPCache optimization
     */
    'directories' => [
        'app',
        'bootstrap',
        'resources',
        'vendor',
    ],
];