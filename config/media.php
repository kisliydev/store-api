<?php
return [

    'force_media_url' => false,
    /***** Items that are available for Super and Internal admins only *****/

    /*
     * Brands' Logos
     */
    'brand_logos' => [
        'thumbnails' => [
            'media_list' => ['width' => 146, 'height' => 112],
            'web' => ['width' => 48, 'height' => 48],
        ],
        'folder' => 'brand-logos',
        'prefix' => 'logo_',
    ],

    /*
     * Users' avatars
     */
    'avatars' => [
        'thumbnails' => [
            'media_list' => ['width' => 146, 'height' => 112],
            'web' => ['width' => 32, 'height' => 32],
            'profile' => ['width' => 48, 'height' => 48],
            'statistics' => ['width' => 64, 'height' => 64],
        ],
        'folder' => 'user-avatars',
        'prefix' => 'avatar_',
    ],

    /***** Items that are available for Brand manager, Brand Admin and Global Manager *****/

    /*
     * Images that are used in mobile app settings
     */
    'app_layout' => [
        'thumbnails' => [
            'media_list' => ['width' => 146, 'height' => 112],
        ],
        'folder' => 'layout-images',
        'prefix' => 'image_',
        'available_mime_types' => ['image/jpeg', 'image/png', 'image/gif'],
    ],

    /*
     * Messages' attachments
     */
    'attachments' => [
        'thumbnails' => [
            'media_list' => ['width' => 146, 'height' => 112],
        ],
        'folder' => 'messages-attachments',
        'prefix' => 'attachment_',
    ],

    /*
     * Quizzes' and Surveys' images
     */
    'brand_items' => [
        'thumbnails' => [
            'web' => ['width' => 312, 'height' => 176],
            'media_list' => ['width' => 146, 'height' => 112],
        ],
        'folder' => 'academy-thumbnails',
        'prefix' => 'thumb_',
    ],

    'brand_items_background' => [
        'type' => 'brand_items',
        'thumbnails' => [
            'web' => ['width' => 312, 'height' => 468],
            'media_list' => ['width' => 146, 'height' => 112],
        ],
        'folder' => 'academy-backgrounds',
        'prefix' => 'image_',
    ],

    'question_images' => [
        'type' => 'brand_items',
        'thumbnails' => [
            'web' => ['width' => 128, 'height' => 96],
            'media_list' => ['width' => 146, 'height' => 112],
        ],
        'folder' => 'academy-question-images',
        'prefix' => 'image_',
    ],

    'answer_images' => [
        'type' => 'brand_items',
        'thumbnails' => [
            'web' => ['width' => 86, 'height' => 64],
            'media_list' => ['width' => 146, 'height' => 112],
        ],
        'folder' => 'academy-answer-images',
        'prefix' => 'image_',
    ],

    /*
     * Dealers' and Districts' logos
     */
    'logos' => [
        'thumbnails' => [
            'media_list' => ['width' => 146, 'height' => 112],
            'web' => ['width' => 32, 'height' => 32],
        ],
        'folder' => 'channel-logos',
        'prefix' => 'logo_',
    ],

    /*
     * Images that were loaded to Uploads folder
     */
    'uploads' => [
        'thumbnails' => [
            'media_list' => ['width' => 146, 'height' => 112],
        ],
        'folder' => 'uploads',
        'prefix' => 'image_',
    ],

    /*
     * Products Assortment
     */
    'products' => [
        'thumbnails' => [
            'media_list' => ['width' => 146, 'height' => 112],
        ],
        'folder' => 'product-assortment',
        'prefix' => 'thumb_',
    ],

    /***** Allowed File types list *****/
    'mimes' => [
        'video' => ['mpeg', 'mpg', 'mp4', 'webm', 'ogg', 'ogv', 'odt', 'qt', 'flv', '3gp', '3g2', 'avi', 'mov'],
        'document' => ['pdf', 'doc', 'docx', 'potx', 'ppsx', 'ppt', 'ppt', 'pptx', 'rtf', 'txt', 'xls', 'xlsx'],
        'image' => ['gif', 'jpeg', 'jpg', 'png']
    ]
];
