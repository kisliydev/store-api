<?php
return [
    'universal_apps' => ['store', 'dekk', 'moller'],
    'credentials' => [
        'dekk' => [
            'app_id' => env('ONESIGNAL_APP_ID_DEKK'),
            'api_key' => env('ONESIGNAL_REST_API_KEY_DEKK'),
        ],
        'store' => [
            'app_id' => env('ONESIGNAL_APP_ID_STORE'),
            'api_key' => env('ONESIGNAL_REST_API_KEY_STORE'),
        ],
        'moller' => [
            'app_id' => env('ONESIGNAL_APP_ID_MOLLER'),
            'api_key' => env('ONESIGNAL_REST_API_KEY_MOLLER'),
        ],
    ],
];
