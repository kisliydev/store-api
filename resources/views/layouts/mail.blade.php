<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>
    </title>
    <style>
        div.header {
            margin-bottom: 10pt ! important;
        }

        table.textAtm {
            margin-top: 10pt ! important;
            margin-bottom: 10pt ! important;
        }

        tr.textAtmH,
        tr.textAtmB,
        tr.rfc822B {
            font-weight: normal ! important;
        }

        tr.signInProgressH,
        tr.rfc822H,
        tr.encrH,
        tr.signOkKeyOkH,
        tr.signOkKeyBadH,
        tr.signWarnH,
        tr.signErrH {
            font-weight: bold ! important;
        }

        tr.textAtmH td,
        tr.textAtmB td {
            padding: 3px ! important;
        }

        table.rfc822 {
            width: 100% ! important;
            border: solid 1px black ! important;
            margin-top: 10pt ! important;
            margin-bottom: 10pt ! important;
        }

        table.textAtm,
        table.encr,
        table.signWarn,
        table.signErr,
        table.signOkKeyBad,
        table.signOkKeyOk,
        table.signInProgress,
        div.fancy.header table {
            width: 100% ! important;
            border-width: 0px ! important;
            line-height: normal;
        }

        div.htmlWarn {
            margin: 0px 5% ! important;
            padding: 10px ! important;
            text-align: left ! important;
            line-height: normal;
        }

        div.fancy.header > div {
            font-weight: bold ! important;
            padding: 4px ! important;
            line-height: normal;
        }

        div.fancy.header table {
            padding: 2px ! important;
            text-align: left ! important;
            border-collapse: separate ! important;
        }

        div.fancy.header table th {
            font-family: "Noto Sans" ! important;
            font-size: 16px ! important;
            padding: 0px ! important;
            white-space: nowrap ! important;
            border-spacing: 0px ! important;
            text-align: left ! important;
            vertical-align: top ! important;
            background-color: #eff0f1 ! important;
            color: #31363b ! important;
            border: 1px ! important;
        }

        div.fancy.header table td {
            font-family: "Noto Sans" ! important;
            font-size: 16px ! important;
            padding: 0px ! important;
            border-spacing: 0px ! important;
            text-align: left ! important;
            vertical-align: top ! important;
            width: 100% ! important;
            background-color: #eff0f1 ! important;
            color: #31363b ! important;
            border: 1px ! important;
        }

        div.fancy.header table a:hover {
            background-color: transparent ! important;
        }

        span.pimsmileytext {
            position: absolute;
            top: 0px;
            left: 0px;
            visibility: hidden;
        }

        img.pimsmileyimg {
        }

        div.quotelevelmark {
            position: absolute;
            margin-left: -10px;
        }

        @media screen {
            body {
                font-family: "Noto Sans" ! important;
                font-size: 16px ! important;
                color: #31363b ! important;
                background-color: #fcfcfc ! important;
            }

            a {
                color: #2980b9 ! important;
                text-decoration: none ! important;
            }

            a.white {
                color: white ! important;
            }

            a.black {
                color: black ! important;
            }

            table.textAtm {
                background-color: #31363b ! important;
            }

            tr.textAtmH {
                background-color: #fcfcfc ! important;
                font-family: "Noto Sans" ! important;
                font-size: 16px ! important;
            }

            tr.textAtmB {
                background-color: #fcfcfc ! important;
            }

            table.signInProgress,
            table.rfc822 {
                background-color: #fcfcfc ! important;
            }

            tr.signInProgressH,
            tr.rfc822H {
                font-family: "Noto Sans" ! important;
                font-size: 16px ! important;
            }

            table.encr {
                background-color: #0069cc ! important;
            }

            tr.encrH {
                background-color: #0080ff ! important;
                color: #ffffff ! important;
                font-family: "Noto Sans" ! important;
                font-size: 16px ! important;
            }

            tr.encrB {
                background-color: #e0f0ff ! important;
            }

            table.signOkKeyOk {
                background-color: #aac8b5 ! important;
            }

            tr.signOkKeyOkH {
                background-color: #d5fae2 ! important;
                color: #27ae60 ! important;
                font-family: "Noto Sans" ! important;
                font-size: 16px ! important;
            }

            tr.signOkKeyOkB {
                background-color: #f7faf8 ! important;
            }

            table.signOkKeyBad {
                background-color: #cbbfb8 ! important;
            }

            tr.signOkKeyBadH {
                background-color: #feeee6 ! important;
                color: #f67400 ! important;
                font-family: "Noto Sans" ! important;
                font-size: 16px ! important;
            }

            tr.signOkKeyBadB {
                background-color: #fefdfb ! important;
            }

            table.signWarn {
                background-color: #cbbfb8 ! important;
            }

            tr.signWarnH {
                background-color: #feeee6 ! important;
                color: #f67400 ! important;
                font-family: "Noto Sans" ! important;
                font-size: 16px ! important;
            }

            tr.signWarnB {
                background-color: #fefdfb ! important;
            }

            table.signErr {
                background-color: #c8bcbd ! important;
            }

            tr.signErrH {
                background-color: #fae9eb ! important;
                color: #da4453 ! important;
                font-family: "Noto Sans" ! important;
                font-size: 16px ! important;
            }

            tr.signErrB {
                background-color: #faf9f9 ! important;
            }

            div.htmlWarn {
                border: 2px solid #ff4040 ! important;
                line-height: normal;
            }

            div.header {
                font-family: "Noto Sans" ! important;
                font-size: 16px ! important;
            }

            div.fancy.header > div {
                background-color: #3daee9 ! important;
                color: #eff0f1 ! important;
                border: solid #31363b 1px ! important;
                line-height: normal;
            }

            div.fancy.header > div a[href] {
                color: #eff0f1 ! important;
            }

            div.fancy.header > div a[href]:hover {
                text-decoration: underline ! important;
            }

            div.fancy.header > div.spamheader {
                background-color: #cdcdcd ! important;
                border-top: 0px ! important;
                padding: 3px ! important;
                color: black ! important;
                font-weight: bold ! important;
                font-size: smaller ! important;
            }

            div.fancy.header > table.outer {
                background-color: #eff0f1 ! important;
                color: #31363b ! important;
                border-bottom: solid #31363b 1px ! important;
                border-left: solid #31363b 1px ! important;
                border-right: solid #31363b 1px ! important;
            }

            div.senderpic {
                padding: 0px ! important;
                font-size: 0.8em ! important;
                border: 1px solid #c4c9cd ! important;
                background-color: #eff0f1 ! important;
            }

            div.senderstatus {
                text-align: center ! important;
            }

            div.quotelevel1 {
                color: #209150 ! important;
                font-style: italic ! important;
            }

            div.quotelevel2 {
                color: #1a7440 ! important;
                font-style: italic ! important;
            }

            div.quotelevel3 {
                color: #135730 ! important;
                font-style: italic ! important;
            }

            div.deepquotelevel1 {
                color: #209150 ! important;
                font-style: italic ! important;
            }

            div.deepquotelevel2 {
                color: #1a7440 ! important;
                font-style: italic ! important;
            }

            div.deepquotelevel3 {
                color: #135730 ! important;
                font-style: italic ! important;
            }
        }

        @media print {
            body {
                font-family: "Noto Sans" ! important;
                font-size: 12pt ! important;
                color: #000000 ! important;
                background-color: #ffffff ! important
            }

            tr.textAtmH,
            tr.signInProgressH,
            tr.rfc822H,
            tr.encrH,
            tr.signOkKeyOkH,
            tr.signOkKeyBadH,
            tr.signWarnH,
            tr.signErrH,
            div.header {
                font-family: "Noto Sans" ! important;
                font-size: 12pt ! important;
            }

            div.fancy.header > div {
                background-color: #eff0f1 ! important;
                color: #31363b ! important;
                padding: 4px ! important;
                border: solid #31363b 1px ! important;
                line-height: normal;
            }

            div.fancy.header > div a[href] {
                color: #31363b ! important;
            }

            div.fancy.header > table.outer {
                background-color: #eff0f1 ! important;
                color: #31363b ! important;
                border-bottom: solid #31363b 1px ! important;
                border-left: solid #31363b 1px ! important;
                border-right: solid #31363b 1px ! important;
            }

            div.spamheader {
                display: none ! important;
            }

            div.htmlWarn {
                border: 2px solid #ffffff ! important;
                line-height: normal;
            }

            div.senderpic {
                font-size: 0.8em ! important;
                border: 1px solid black ! important;
                background-color: #eff0f1 ! important;
            }

            div.senderstatus {
                text-align: center ! important;
            }

            div.noprint {
                display: none ! important;
            }
    </style>
</head>
<body>
<div style="position: relative; word-wrap: break-word">
    <div id="attachmentDiv">
        <div id="attachmentDiv2">
            <div style="position: relative">

                <style>
                    @media only screen and (max-width: 600px) {
                        .inner-body {
                            width: 100% !important;
                        }

                        .footer {
                            width: 100% !important;
                        }
                    }

                    @media only screen and (max-width: 500px) {
                        .button {
                            width: 100% !important;
                        }
                    }

                </style>
                <table class="wrapper" width="100%" cellpadding="0" cellspacing="0"
                       style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #f5f8fa; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
                    <tbody>
                    <tr>
                        <td align="center" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">

                            <table class="content" width="100%" cellpadding="0" cellspacing="0"
                                   style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
                                <tbody>
                                <tr>
                                    <td class="header"
                                        style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 25px 0; text-align: center;">

                                        <a href="{{ config('app.spa_url') }}"
                                           style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #bbbfc3; font-size: 19px; font-weight: bold; text-decoration: none; text-shadow: 0 1px 0 white;">
                                            {{ empty($brand) ? config('app.name') : $brand }}
                                        </a>

                                    </td>
                                </tr>
                                <!-- Email Body -->
                                <tr>
                                    <td class="body" width="100%" cellpadding="0" cellspacing="0"
                                        style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #FFFFFF; border-bottom: 1px solid #EDEFF2; border-top: 1px solid #EDEFF2; margin: 0; padding: 0; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">

                                        <table class="inner-body" align="center" width="570" cellpadding="0"
                                               cellspacing="0"
                                               style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; background-color: #FFFFFF; margin: 0 auto; padding: 0; width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 570px;">
                                            <!-- Body content -->
                                            <tbody>
                                            <tr>
                                                @yield('content')
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>

                                </tr>
                                <tr>
                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">

                                        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0"
                                               style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 0 auto; padding: 0; text-align: center; width: 570px; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 570px;">
                                            <tbody>
                                            <tr>
                                                <td class="content-cell" align="center"
                                                    style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 35px;">

                                                    <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; line-height: 1.5em; margin-top: 0; color: #AEAEAE; font-size: 12px; text-align: center;">
                                                        &copy; {{ date('Y') }} {{ config('app.name') }}. All rights reserved.
                                                    </p>

                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</body>
</html>
