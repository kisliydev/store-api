<p>Dealer: <strong>{{ $dealer->name }}</strong></p>
<ul>
    @foreach($users as $user)
        <li>{{ $user->full_name }} ({{ $user->email }})</li>
    @endforeach
</ul>
<p><strong>Reason:</strong></p>
<p>{{ $reason }}</p>
