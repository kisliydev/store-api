@extends('layouts.mail')

@section('content')
<td class="content-cell" style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; padding: 35px;">
    <h1 style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #2F3133; font-size: 19px; font-weight: bold; margin-top: 0; text-align: left;">
        Hello {{ $user->first_name }},
    </h1>
    <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">
        Enter the following code into reset password form to confirm password reset
    </p>
    <table class="action" align="center" width="100%" cellpadding="0"
           cellspacing="0"
           style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; margin: 30px auto; padding: 0; text-align: center; width: 100%; -premailer-cellpadding: 0; -premailer-cellspacing: 0; -premailer-width: 100%;">
        <tbody>
        <tr>
            <td align="center"
                style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">

                <table width="100%" border="0" cellpadding="0"
                       cellspacing="0"
                       style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
                    <tbody>
                    <tr>
                        <td align="center"
                            style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">

                            <table border="0" cellpadding="0"
                                   cellspacing="0"
                                   style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;">
                                <tbody>
                                <tr>
                                    <td style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box;font-weight: bold;font-size: 19px;">
                                        {{ $user->forgot_password_token }}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>

                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    <p style="font-family: Avenir, Helvetica, sans-serif; box-sizing: border-box; color: #74787E; font-size: 16px; line-height: 1.5em; margin-top: 0; text-align: left;">
        Regards,
        <br>
        {{ empty($brand) ? config('app.name') : $brand }}
    </p>
</td>
@endsection
