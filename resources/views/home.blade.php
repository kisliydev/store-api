<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>STORE REST API</title>

    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Raleway', sans-serif;
            font-weight: 100;
            height: 100vh;
            margin: 0;
        }
        .full-height {
            height: 100vh;
        }
        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }
        .position-ref {
            position: relative;
        }
        .content {
            text-align: center;
        }
        .title {
            font-size: 84px;
        }
        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 12px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }
        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    <div class="content">
        <h1 class="title m-b-md">STORE REST API</h1>
    </div>
</div>
<script type="application/javascript">
    sendSocketMessage = (messageObject) => {
        this.socketConnection.send(JSON.stringify(messageObject));
    };

    initSocketConnection = () => {
        this.socketConnection = new WebSocket('ws://' + window.location.hostname + ':9090');

        this.socketConnection.onclose = e => {
            console.log("Connection closed: " + e.code);
        };

        this.socketConnection.onopen = () => {
            console.log("Connection established!");
        };

        this.socketConnection.onmessage = e => {
            e = JSON.parse(e.data);
            console.log(e);
        };
    };

</script>
</body>
</html>
