Hello {{ $user->first_name }}, you've been invited to the channel "{{ $data['channel_name'] }}".
Your invite code is {{ $data['invite_code'] }}.
You can download the mobile application by one of the following links:
{{ $data['app_url_apple'] }}
{{ $data['app_url_google'] }}