<?php

/**
 * Endpoints that can come from unauthorized accounts
 */
Route::group(['prefix' => 'auth', 'as' => 'auth.'], function () {

    Route::post('/login', ['as' => 'login', 'uses' => 'Auth\EmailPasswordController@login']);
    Route::post('/mobile/login', ['as' => 'ms.login', 'uses' => 'Auth\EmailPasswordController@mobileLogin']);

    Route::post('/mobile/channel/join/login', ['as' => 'ms.channel.join.login', 'uses' => 'Auth\EmailPasswordController@mobileJoinChannelAndLogin']);
    Route::post('/mobile/social/login', ['as' => 'ms.login', 'uses' => 'Auth\MobileSocialController@login']);

    Route::post('/mobile/password/forgot', ['as' => 'forgot.email.mobile', 'uses' => 'Auth\ForgotPasswordController@setResetTokenMobile']);
    Route::post('/password/forgot', ['as' => 'forgot.email', 'uses' => 'Auth\ForgotPasswordController@setResetToken']);
    Route::get('/password/forgot', ['as' => 'forgot.email.check.token', 'uses' => 'Auth\ForgotPasswordController@checkForgotTokens']);
    Route::put('/password/forgot', ['as' => 'forgot.reset', 'uses' => 'Auth\ForgotPasswordController@reset']);

    Route::get('/channel/id', ['as' => 'channel.id','uses' => 'Auth\RegisterController@getChannelId']);
    Route::get('/dealers', ['as' => 'channel.id','uses' => 'Auth\RegisterController@getDealersList']);
    Route::get('/positions', ['as' => 'channel.id','uses' => 'Auth\RegisterController@getPositionsList']);
    Route::post('/register', ['as' => 'ms.register', 'uses' => 'Auth\RegisterController@register']);
    Route::post('/register/verify/resend', 'Auth\RegisterController@verifyResend')->name('auth.verify.resend');
    Route::get('/register/verify/{token}', ['as' => 'auth.verify', 'uses' => 'Auth\RegisterController@verify']);
    Route::get('/refresh/token', ['as'=> 'refresh.token', 'uses' => 'Auth\EmailPasswordController@refreshToken']);
});


/**
 * Channel Meta data
 */
Route::get('/channel/meta/mobile', ['as' => 'channel.meta.get.mobile','uses' => 'ChannelController@showMetaMobile']);
Route::get('/user/check', ['as' => 'user.email.check', 'uses' => 'User\UserController@checkEmail']);
/**
 * Authorized accounts' endpoints
 */
Route::group(['before' => 'jwt-auth', 'middleware' => ['jwt']], function () {

    /**
     * Sockets management
     */
    Route::post('/socket/broadcast', ['as' => 'socket.broadcast', 'uses' => 'Messages\SocketController@broadcast']);

    /**
     * Auth endpoints
     */
    Route::get('/auth/logout', ['as'=> 'logout', 'uses' => 'Auth\LogoutController@logout']);
    Route::get('/auth/mobile/logout', ['as'=> 'mobile.logout', 'uses' => 'Auth\LogoutController@mobileLogout']);
    Route::get('/auth/mobile/channel/switch', ['as'=> 'mobile.channel.switch', 'uses' => 'Auth\LogoutController@switchChannel']);
    Route::post('/auth/mobile/channel/join', ['as'=> 'mobile.channel.join', 'uses' => 'Auth\LogoutController@joinChannel']);

    /**
     * ACL
     */
    Route::get('/acl', ['middleware' => ['permission:view_permissions'], 'as'=> 'acl.get', 'uses' => 'ACLController@getAll']);
    Route::get('/acl/role', ['middleware' => ['permission:edit_users'], 'as'=> 'acl.role.get', 'uses' => 'ACLController@show']);
    Route::get('/acl/restore', ['middleware' => ['permission:edit_permissions'], 'as'=> 'acl.restore', 'uses' => 'ACLController@restore']);
    Route::put('/acl', ['middleware' => ['permission:edit_permissions'], 'as'=> 'acl.update', 'uses' => 'ACLController@update']);
    Route::put('/acl/user', ['middleware' => ['permission:edit_permissions'], 'as'=> 'acl.user.update', 'uses' => 'ACLController@updateUser']);
    Route::get('/acl/user/restore', ['middleware' => ['permission:edit_permissions'], 'as'=> 'acl.user.restore', 'uses' => 'ACLController@restoreUser']);
    Route::get('/acl/user/has/access', ['as'=> 'acl.user.check.access', 'uses' => 'ACLController@checkAccess']);

    /**
     * Users lists: get the list of users
     */
    Route::get('/users', ['middleware' => ['permission:view_all_users'], 'as' => 'users.get','uses' => 'User\UserController@getAll']);
    Route::get('/users/brand', ['middleware' => ['permission:view_users'], 'as' => 'users.brand.get','uses' => 'User\UserController@getInBrand']);
    Route::get('/users/country_brand', ['middleware' => ['permission:view_users'], 'as' => 'users.country_brand.get','uses' => 'User\UserController@getInCountryBrand']);

    /**
     * Temporary commented due to changes
     * that will be provided into the channel statistics
     * and users page
     * @todo uncomment this string. Probably will need to make some changes in the
     *       functionality of fetching users list in the channel
     */
    //Route::get('/users/channel', ['middleware' => ['permission:view_users'], 'as' => 'users.country_brand.get','uses' => 'User\UserController@getInChannel']);
    Route::get('/users/channel', ['middleware' => ['permission:view_users'], 'as' => 'channel.statistics.users','uses' => 'ChannelStatistics\UserStatisticsController@getUsersListFroUnits']);

    /**
     * Users lists: get the list of users form within the app
     */
    Route::get('/users/dealer', ['middleware' => ['permission:app_view_app_user_list'], 'as' => 'users.dealer.get','uses' => 'User\UserController@getInDealer']);

    /**
     * Fetch user's data endpoints
     */
    Route::get('/user', ['as' => 'user', 'uses' => 'User\UserController@index']);
    Route::get('/user/mobile', ['as' => 'user.get.mobile', 'uses' => 'User\UserController@getForMobile']);
    Route::get('/user/mobile/totals', ['as' => 'user.get.mobile.total', 'uses' => 'User\UserController@getTotals']);
    Route::get('/user/bindings', ['middleware' => ['user_bindings'], 'as' => 'user.get.bindings', 'uses' => 'User\UserController@getBindings']);
    Route::get('/user/channels', ['middleware' => ['permission:app_view_profile'], 'as' => 'user.get.bindings', 'uses' => 'User\UserController@getBoundChannels']);
    Route::get('/user/get', ['middleware' => ['user_view_permission'], 'as' => 'user.get', 'uses' => 'User\UserController@show']);

    /**
     * Manage User from within the admin panel
     */
    Route::post('/user', ['middleware' => ['user_edit_permission'], 'as' => 'user.add', 'uses' => 'User\UserController@store']);
    Route::post('/user/edit', ['middleware' => ['user_edit_permission'], 'as' => 'user.update', 'uses' => 'User\UserController@update']);
    Route::post('/user/edit/bindings', ['middleware' => ['user_edit_permission'], 'as' => 'user.update.bindings', 'uses' => 'User\UserController@updateBindings']);
    Route::post('/user/edit/mobile', ['middleware' => ['permission:app_view_profile'], 'as' => 'user.update', 'uses' => 'User\UserController@updateMobile']);
    Route::post('/user/additional_data', ['middleware' => ['permission:edit_users'], 'as' => 'user.additional','uses' => 'User\UserController@saveAdditionalData']);
    Route::post('/users/load', ['middleware' => ['permission:edit_users'], 'as' => 'users.load','uses' => 'User\UserController@load'] );

    /**
     * Fetch entities to edit users data
     */
    Route::get('/roles/for/user', ['as' => 'roles.for.user', 'uses' => 'UserPageDataController@roles']);
    Route::get('/brands/for/user', ['as' => 'brands.for.user', 'uses' => 'UserPageDataController@brands']);
    Route::get('/country_brands/for/user', ['as' => 'country_brands.for.user', 'uses' => 'UserPageDataController@countryBrands']);
    Route::get('/channels/for/user', ['as' => 'channels.for.user', 'uses' => 'UserPageDataController@channels']);
    Route::get('/channel/items/for/user', ['as' => 'channels.items.for.user', 'uses' => 'UserPageDataController@channelItems']);

    /**
     * DELETE users endpoints
     */
    Route::delete('/user', ['as' => 'user.delete.own', 'uses' => 'User\DeleteUserController@deleteOwnAccount']);
    Route::delete('/users', ['middleware' => ['permission:edit_users'], 'as' => 'user.delete', 'uses' => 'User\DeleteUserController@delete']);
    Route::delete('/users/trash', ['middleware' => ['permission:edit_users'], 'as' => 'user.trash', 'uses' => 'User\DeleteUserController@trash']);
    Route::get('/users/restore', ['middleware' => ['permission:edit_users'], 'as' => 'user.restore', 'uses' => 'User\DeleteUserController@restore']);
    Route::delete('/users/brand', ['middleware' => ['permission:edit_users'], 'as' => 'user.delete.from.brand', 'uses' => 'User\DeleteUserController@deleteFromBrand']);
    Route::delete('/users/country_brand', ['middleware' => ['permission:edit_users'], 'as' => 'user.delete.from.country_brand', 'uses' => 'User\DeleteUserController@deleteFromCountryBrand']);
    Route::delete('/users/channel', ['middleware' => ['permission:edit_users'], 'as' => 'user.delete.from.channel', 'uses' => 'User\DeleteUserController@deleteFromChannel']);
    Route::delete('/users/channel/hq', ['middleware' => ['permission:edit_hq_users'], 'as' => 'channel.hq.delete','uses' => 'User\DeleteUserController@deleteHeadQuorterUsers'] );

    /**
     * Manage HQ users endpoints
     */
    Route::get('/users/channel/hq/get', ['middleware' => ['permission:view_hq_users'], 'as' => 'channel.hq.get','uses' => 'User\UserController@getHeadQuorterUsers'] );
    Route::get('/users/channel/hq/new', ['middleware' => ['permission:edit_hq_users'], 'as' => 'channel.hq.new.get','uses' => 'User\UserController@getNewHeadQuorterUsers'] );
    Route::post('/users/channel/hq/new', ['middleware' => ['permission:edit_hq_users'], 'as' => 'channel.hq.new.save','uses' => 'User\UserController@addHeadQuorterUsers'] );

    /**
     * Threads
     */
    Route::get('/thread/recipients', ['middleware' => ['permission:view_messages'], 'as' => 'thread.show.recipients','uses' => 'Messages\RecipientsController@index']);

    Route::get('/thread/open', ['middleware' => ['permission:view_messages'], 'as' => 'thread.open.view','uses' => 'Messages\ThreadController@openList']);
    Route::get('/thread/done', ['middleware' => ['permission:view_messages'], 'as' => 'thread.done.view','uses' => 'Messages\ThreadController@doneList']);
    Route::get('/thread/closed', ['middleware' => ['permission:view_messages'], 'as' => 'thread.closed.view','uses' => 'Messages\ThreadController@closedList']);
    Route::get('/thread/scheduled', ['middleware' => ['permission:view_messages'], 'as' => 'thread.closed.view','uses' => 'Messages\ThreadController@scheduledList']);

    Route::put('/thread/status', ['middleware' => ['permission:create_messages'], 'as' => 'thread.update.status','uses' => 'Messages\ThreadController@updateStatus']);

    Route::post('/thread/closed', ['middleware' => ['permission:edit_messages'], 'as' => 'thread.closed.create','uses' => 'Messages\ThreadController@closedCreate']);
    Route::get('/thread/user', ['middleware' => ['permission:app_view_messages_threads'], 'as' => 'thread.user.view','uses' => 'Messages\ThreadController@userList']);
    Route::post('/thread/user', ['middleware' => ['permission:create_messages'], 'as' => 'thread.user.create','uses' => 'Messages\ThreadController@userCreate']);
    Route::post('/thread/users/delete', ['middleware' => ['store_manager'], 'as' => 'thread.user.delete','uses' => 'Messages\ThreadController@userDelete']);

    Route::put('/thread/scheduled/publish', ['middleware' => ['permission:edit_messages'], 'as' => 'thread.publish','uses' => 'Messages\ThreadController@publishScheduled']);
    Route::post('/thread/scheduled', ['middleware' => ['permission:edit_messages'], 'as' => 'thread.publish','uses' => 'Messages\ThreadController@updateScheduled']);
    Route::delete('/thread/scheduled', ['middleware' => ['permission:edit_messages'], 'as' => 'thread.delete','uses' => 'Messages\ThreadController@deleteScheduled']);
    Route::delete('/thread', ['middleware' => ['role:super_admin|internal_admin|brand_admin'], 'as' => 'thread.destroy','uses' => 'Messages\ThreadController@destroy']);

    /**
     * Thread statistics
     */
    Route::get('/thread/statistics/levels', ['middleware' => ['permission:view_messages'], 'as' => 'thread.levels.view','uses' => 'Messages\ThreadSendStatisticsController@getLevelsList']);

    /**
     * Thread messages
     */
    Route::get('/message', ['middleware' => ['permission:app_view_messages_threads'], 'as' => 'message.show','uses' => 'Messages\MessageController@show']);
    Route::post('/message', ['middleware' => ['permission:create_messages'], 'as' => 'message.store','uses' => 'Messages\MessageController@store']);
    Route::delete('/message', ['middleware' => ['role:super_admin|internal_admin|brand_admin'], 'as' => 'message.destroy','uses' => 'Messages\MessageController@destroy']);

    /**
     * page elements
     */
    Route::get('/list/moderators', ['as' => 'message.moderators','uses' => 'Messages\SettingsController@showModerators']);
    Route::get('/list/dealers', ['as' => 'message.dealers','uses' => 'Messages\SettingsController@showDealers']);
    Route::get('/list/leagues', ['as' => 'message.districts','uses' => 'Messages\SettingsController@showLeagues']);
    Route::get('/list/districts', ['as' => 'message.districts','uses' => 'Messages\SettingsController@showDistricts']);
    Route::get('/list/positions', ['as' => 'message.positions','uses' => 'Messages\SettingsController@showPositions']);
    Route::get('/list/users', ['as' => 'message.users','uses' => 'Messages\SettingsController@showAppUsers']);

    /**
     * Automatic messages
     */
    Route::get('/auto/messages/types', ['middleware' => ['permission:view_messages'], 'as' => 'auto.message.types.show','uses' => 'Messages\AutomaticMessageController@getTypes']);
    Route::get('/auto/messages', ['middleware' => ['permission:view_messages'], 'as' => 'auto.messages.get.list','uses' => 'Messages\AutomaticMessageController@index']);
    Route::post('/auto/message', ['middleware' => ['permission:edit_messages'], 'as' => 'auto.message.store','uses' => 'Messages\AutomaticMessageController@store']);
    Route::put('/auto/message', ['middleware' => ['permission:edit_messages'], 'as' => 'auto.message.update','uses' => 'Messages\AutomaticMessageController@update']);
    Route::delete('/auto/messages', ['middleware' => ['permission:edit_messages'], 'as' => 'auto.message.destroy','uses' => 'Messages\AutomaticMessageController@destroy']);

    /**
     * Levels
     */
    Route::get('/levels/types', ['middleware' => ['permission:view_points'], 'as' => 'levels.types.show','uses' => 'Messages\LevelController@showTypes']);
    Route::get('/levels', ['middleware' => ['permission:view_points'], 'as' => 'levels.get.list','uses' => 'Messages\LevelController@index']);
    Route::post('/level', ['middleware' => ['permission:edit_points'], 'as' => 'level.store','uses' => 'Messages\LevelController@store']);
    Route::put('/level', ['middleware' => ['permission:edit_points'], 'as' => 'level.update','uses' => 'Messages\LevelController@update']);
    Route::put('/level/quick_edit', ['middleware' => ['permission:edit_points'], 'as' => 'levels.quick.edit','uses' => 'Messages\LevelController@quickEdit']);
    Route::delete('/levels', ['middleware' => ['permission:edit_points'], 'as' => 'levels.destroy','uses' => 'Messages\LevelController@destroy']);

    /**
     * Gift codes
     */
    Route::get('/messages/gift/codes', ['middleware' => ['permission:view_messages'], 'as' => 'gift.codes.show','uses' => 'Messages\GiftCodeController@show']);
    Route::post('/messages/gift/codes', ['middleware' => ['permission:edit_messages'], 'as' => 'gift.codes.store','uses' => 'Messages\GiftCodeController@store']);
    Route::delete('/messages/gift/codes', ['middleware' => ['permission:edit_messages'], 'as' => 'gift.codes.destroy','uses' => 'Messages\GiftCodeController@destroy']);

    /**
     * Media
     */
    Route::get('/media/tree', ['middleware' => ['permission:view_media'], 'as' => 'media.top.level','uses' => 'MediaController@topLevelList']);
    Route::get('/media/country/brands', ['middleware' => ['permission:view_media'], 'as' => 'media.brand.level','uses' => 'MediaController@countryBrandList']);
    Route::get('/media/channels', ['middleware' => ['permission:view_media'], 'as' => 'media.country.brand.level','uses' => 'MediaController@channelsList']);
    Route::get('/media/channel', ['middleware' => ['permission:view_media'], 'as' => 'media.channel.level','uses' => 'MediaController@channelsMediaList']);
    Route::get('/media/admin/items', ['middleware' => ['permission:view_media'], 'as' => 'media.admin.items','uses' => 'MediaController@superAdminItemsList']);
    Route::get('/media/channel/items', ['middleware' => ['permission:view_media'], 'as' => 'media.channels.items','uses' => 'MediaController@channelItemsList']);
    Route::delete('/media', ['middleware' => ['permission:edit_media'], 'as' => 'media.destroy','uses' => 'MediaController@destroy']);
    Route::post('/media', ['middleware' => ['permission:edit_media'], 'as' => 'media.store','uses' => 'MediaController@store']);
    Route::post('/media/edit', ['middleware' => ['permission:edit_media'], 'as' => 'media.edit','uses' => 'MediaController@update']);
    Route::post('/media/bulk', ['middleware' => ['permission:edit_media'], 'as' => 'media.edit','uses' => 'MediaController@bulkAction']);
    Route::get('/media/download', ['as' => 'media.download','uses' => 'MediaController@download']);

    /**
     * Brands
     */
    Route::get('/brands', ['middleware' => ['permission:view_brands'], 'as' => 'brands.get','uses' => 'BrandController@index']);
    Route::get('/brand', ['middleware' => ['permission:view_brands'], 'as' => 'brand.get','uses' => 'BrandController@show']);
    Route::post('/brand', ['middleware' => ['permission:edit_brands'], 'as' => 'brand.add','uses' => 'BrandController@store']);
    Route::post('/brand/edit', ['middleware' => ['permission:edit_brands'], 'as' => 'brand.update','uses' => 'BrandController@update']);
    Route::post('/brand/settings', ['middleware' => ['permission:edit_settings'], 'as' => 'brand.options.post','uses' => 'BrandController@saveSettings']);
    Route::delete('/brand', ['middleware' => ['permission:edit_brands'], 'as' => 'brand.delete','uses' => 'BrandController@destroy']);

    /**
     * CountryBrands
     */
    Route::get('/country_brands', ['middleware' => ['permission:view_country_brands'], 'as' => 'country_brands.get','uses' => 'CountryBrandController@index']);
    Route::get('/country_brand', ['middleware' => ['permission:view_country_brands'], 'as' => 'country_brand.get','uses' => 'CountryBrandController@show']);
    Route::post('/country_brand', ['middleware' => ['permission:edit_country_brands'], 'as' => 'country_brand.add','uses' => 'CountryBrandController@store']);
    Route::put('/country_brand', ['middleware' => ['permission:edit_country_brands'], 'as' => 'country_brand.update','uses' => 'CountryBrandController@update']);
    Route::delete('/country_brand', ['middleware' => ['permission:edit_country_brands'], 'as' => 'country_brand.delete','uses' => 'CountryBrandController@destroy']);

    /**
     * Channels
     */
    Route::get('/channels', ['middleware' => ['permission:view_channels'], 'as' => 'channels.get','uses' => 'ChannelController@index']);
    Route::get('/channel', ['middleware' => ['user_has_access_to_channel:id'], 'as' => 'channel.get','uses' => 'ChannelController@show']);
    Route::get('/channel/invite_code', ['middleware' => ['permission:edit_channels'], 'as' => 'channel.get.invite_code','uses' => 'ChannelController@getInviteCode']);
    Route::get('/channel/check', ['middleware' => ['permission:edit_channels'], 'as' => 'channel.invite_code.check','uses' => 'ChannelController@checkInviteCode']);
    Route::post('/channel', ['middleware' => ['permission:edit_channels'], 'as' => 'channel.add','uses' => 'ChannelController@store']);
    Route::get('/channel/copy', ['middleware' => ['permission:edit_channels'], 'as' => 'channel.copy','uses' => 'ChannelController@copy']);
    Route::put('/channel', ['middleware' => ['permission:edit_channels'], 'as' => 'channel.update','uses' => 'ChannelController@update']);
    Route::delete('/channel/trash', ['middleware' => ['permission:edit_channels'], 'as' => 'channel.trash','uses' => 'ChannelController@trash']);
    Route::get('/channel/restore', ['middleware' => ['permission:edit_channels'], 'as' => 'channel.restore','uses' => 'ChannelController@restore']);
    Route::delete('/channel', ['middleware' => ['permission:edit_channels'], 'as' => 'channel.delete','uses' => 'ChannelController@destroy']);

    Route::get('/channel/meta', ['middleware' => ['channel_meta_permission'], 'as' => 'channel.meta.get','uses' => 'ChannelController@showMeta']);
    Route::post('/channel/meta', ['middleware' => ['channel_meta_permission'], 'as' => 'channel.meta.save','uses' => 'ChannelController@saveMeta']);

    Route::get('/channel/messages/moderator', ['middleware' => ['permission:create_messages'], 'as' => 'channel.meta.get.moderator','uses' => 'ChannelController@showModerator']);

    Route::get('/channel/localization', ['as' => 'channel.localization.get','uses' => 'ChannelController@getLocalization']);
    Route::post('/channel/load/localization', ['middleware' => ['permission:edit_channels'], 'as' => 'channel.localization.load','uses' => 'ChannelController@loadLocalization']);
    Route::get('/channel/export/localization', ['middleware' => ['permission:view_channels'], 'as' => 'channel.localization.export','uses' => 'ChannelController@exportLocalization']);

    /**
     * Channel Statistics: Users
     */
    Route::get('/channel/statistics/users', ['middleware' => ['permission:view_channel_statistics'], 'as' => 'channel.statistics.users','uses' => 'ChannelStatistics\UserStatisticsController@getUsersStatistics']);
    Route::get('/channel/statistics/users/export', ['middleware' => ['permission:view_channel_statistics'], 'as' => 'channel.statistics.users.export','uses' => 'ChannelStatistics\UserStatisticsController@exportUsers']);
    Route::get('/channel/statistics/users/export/detailed', ['middleware' => ['permission:view_channel_statistics'], 'as' => 'channel.statistics.users.export.detailed','uses' => 'ChannelStatistics\UserStatisticsController@exportUsersDetailed']);

    /**
     * Channel Statistics: Users
     * for mobile app
     */
    Route::get('/channel/statistics/users/mobile', ['middleware' => ['permission:app_view_results'], 'as' => 'channel.statistics.users.mobile','uses' => 'ChannelStatistics\UserStatisticsController@getUsersMobileStatistics']);
    Route::get('/channel/statistics/user/mobile', ['middleware' => ['permission:app_view_results'], 'as' => 'channel.statistics.user.mobile','uses' => 'ChannelStatistics\UserStatisticsController@getUserMobileStatistics']);
    Route::get('/channel/statistics/user/detailed/mobile', ['middleware' => ['permission:app_view_results'], 'as' => 'channel.statistics.user.detailed.mobile','uses' => 'ChannelStatistics\UserStatisticsController@getUserDetailedMobileStatistics']);

    /**
     * Channel Statistics: Academy Items
     */
    Route::get('/channel/statistics/survey/answers/export', ['middleware' => ['permission:view_channel_statistics'], 'as' => 'survey.answers.export','uses' => 'ChannelStatistics\AcademyItemsStatisticsController@exportSurveyAnswers']);
    Route::get('/channel/statistics/academy_items/export', ['middleware' => ['permission:view_channel_statistics'], 'as' => 'channel.statistics.academy_items.export','uses' => 'ChannelStatistics\AcademyItemsStatisticsController@exportAcademyItems']);

    /**
     * Channel Statistics: Academy Items
     * for mobile app
     */
    Route::get('/channel/statistics/academy_items/counters', ['middleware' => ['permission:app_view_academy'], 'as' => 'channel.statistics.academy_items.counters','uses' => 'ChannelStatistics\AcademyItemsStatisticsController@getCounters']);

    /**
     * Channel Statistics: Dealers
     */
    Route::get('/channel/statistics/dealers', ['middleware' => ['permission:view_channel_statistics'], 'as' => 'channel.statistics.dealers','uses' => 'ChannelStatistics\DealerStatisticsController@getDealersStatistics']);
    Route::get('/channel/statistics/dealers/export', ['middleware' => ['permission:view_channel_statistics'], 'as' => 'channel.statistics.dealers.export','uses' => 'ChannelStatistics\DealerStatisticsController@exportDealersStatistics']);

    /**
     * Channel Statistics: Dealers
     * for mobile app
     */
    Route::get('/channel/statistics/dealers/mobile', ['middleware' => ['permission:app_view_results'], 'as' => 'channel.statistics.dealers.mobile','uses' => 'ChannelStatistics\DealerStatisticsController@getDealersMobileStatistics']);
    Route::get('/channel/statistics/dealer/mobile', ['middleware' => ['permission:app_view_results'], 'as' => 'channel.statistics.dealer.mobile','uses' => 'ChannelStatistics\DealerStatisticsController@getDealerMobileStatistics']);
    Route::get('/channel/statistics/dealer/detailed/mobile', ['middleware' => ['permission:app_view_results'], 'as' => 'channel.statistics.dealer.detailed.mobile','uses' => 'ChannelStatistics\DealerStatisticsController@getDealerDetailedMobileStatistics']);
    Route::get('/channel/statistics/dealer/users/mobile', ['middleware' => ['permission:app_view_results'], 'as' => 'channel.statistics.dealer.users.mobile','uses' => 'ChannelStatistics\DealerStatisticsController@getDealerUsersMobileStatistics']);

    /**
     * Channel Statistics: campaigns
     */
    Route::get('/channel/statistics/campaign/export', ['middleware' => ['permission:view_campaigns'], 'as' => 'statistics.campaigns.export','uses' => 'ChannelStatistics\CampaignStatisticsController@export']);

    /**
     * Channel dashboard
     */
    Route::get('/channel/statistics/general', ['middleware' => ['view_dashboard'], 'as' => 'statistics.general.get','uses' => 'ChannelStatistics\DashboardStatisticsController@getGeneral']);
    Route::get('/channel/statistics/chart', ['middleware' => ['view_dashboard'], 'as' => 'statistics.chart.get','uses' => 'ChannelStatistics\DashboardStatisticsController@getChart']);
    Route::get('/channel/statistics/totals', ['middleware' => ['view_dashboard'], 'as' => 'statistics.totals.get','uses' => 'ChannelStatistics\DashboardStatisticsController@getTotals']);
    Route::get('/channel/statistics/products', ['middleware' => ['view_dashboard'], 'as' => 'statistics.products.get','uses' => 'ChannelStatistics\DashboardStatisticsController@getProducts']);
    Route::get('/channel/statistics/quizzes', ['middleware' => ['view_dashboard'], 'as' => 'statistics.quizzes.get','uses' => 'ChannelStatistics\DashboardStatisticsController@getQuizzes']);
    Route::get('/channel/statistics/surveys', ['middleware' => ['view_dashboard'], 'as' => 'statistics.surveys.get','uses' => 'ChannelStatistics\DashboardStatisticsController@getSurveys']);
    Route::get('/channel/statistics/dealers/dashboard', ['middleware' => ['view_dashboard'], 'as' => 'statistics.dealers.dashboard.get','uses' => 'ChannelStatistics\DashboardStatisticsController@getDealers']);
    Route::get('/channel/statistics/users/dashboard', ['middleware' => ['view_dashboard'], 'as' => 'statistics.users.dashboard.get','uses' => 'ChannelStatistics\DashboardStatisticsController@getUsers']);

    /**
     * Campaigns
     */
    Route::get('/campaigns', ['middleware' => ['permission:view_campaigns'], 'as' => 'campaigns.get','uses' => 'CampaignController@index']);
    Route::get('/campaign', ['middleware' => ['permission:view_campaigns'], 'as' => 'campaigns.get','uses' => 'CampaignController@show']);
    Route::get('/campaign/app_layout', ['middleware' => ['permission:view_campaigns'], 'as' => 'campaigns.get','uses' => 'CampaignController@showAppLayout']);
    Route::post('/campaign', ['middleware' => ['permission:edit_campaigns'], 'as' => 'campaign.post','uses' => 'CampaignController@store']);
    Route::post('/campaign/app_layout', ['middleware' => ['permission:edit_campaigns'], 'as' => 'campaign.post','uses' => 'CampaignController@saveAppLayout']);
    Route::put('/campaign', ['middleware' => ['permission:edit_campaigns'], 'as' => 'campaign.update','uses' => 'CampaignController@update']);
    Route::delete('/campaign', ['middleware' => ['permission:edit_campaigns'], 'as' => 'campaign.delete','uses' => 'CampaignController@destroy']);
    Route::post('/campaign/draft', ['middleware' => ['permission:edit_campaigns'], 'as' => 'campaign.store.draft','uses' => 'CampaignController@storeDraft']);
    Route::put('/campaign/draft', ['middleware' => ['permission:edit_campaigns'], 'as' => 'campaign.update.draft','uses' => 'CampaignController@updateDraft']);
    Route::delete('/campaigns/trash', 'CampaignController@trash')->middleware('permission:edit_campaigns')->name('campaigns.trash');
    Route::get('/campaigns/restore', 'CampaignController@restore')->middleware('permission:edit_campaigns')->name('campaigns.restore');

    /**
     * User Profile fields
     */
    Route::get('/user_profile_fields', ['middleware' => ['permission:view_users'], 'as' => 'channel.meta.get','uses' => 'ProfileFieldController@showUserFields']);
    Route::post('/user_profile_fields', ['middleware' => ['permission:edit_users'], 'as' => 'channel.meta.post','uses' => 'ProfileFieldController@saveUserFields']);

    /**
     * Districts
     */
    Route::get('/districts', ['middleware' => ['permission:view_districts'], 'as' => 'districts.get','uses' => 'DistrictController@index']);
    Route::get('/district', ['middleware' => ['permission:view_districts'], 'as' => 'district.get','uses' => 'DistrictController@show']);
    Route::post('/district', ['middleware' => ['permission:edit_districts'], 'as' => 'district.add','uses' => 'DistrictController@store']);
    Route::post('/district/edit', ['middleware' => ['permission:edit_districts'], 'as' => 'district.update','uses' => 'DistrictController@update']);
    Route::delete('/district', ['middleware' => ['permission:edit_districts'], 'as' => 'district.delete','uses' => 'DistrictController@destroy']);
    Route::post('/district/bulk', ['middleware' => ['permission:edit_districts'], 'as' => 'district.bulk','uses' => 'DistrictController@bulkAction']);

    /**
     * Leagues
     */
    Route::get('/leagues', ['middleware' => ['permission:app_view_leagues'], 'as' => 'leagues.get','uses' => 'LeagueController@index']);
    Route::get('/league', ['middleware' => ['permission:view_leagues'], 'as' => 'league.get','uses' => 'LeagueController@show']);
    Route::post('/league', ['middleware' => ['permission:edit_leagues'], 'as' => 'league.add','uses' => 'LeagueController@store']);
    Route::post('/league/edit', ['middleware' => ['permission:edit_leagues'], 'as' => 'league.update','uses' => 'LeagueController@update']);
    Route::delete('/league', ['middleware' => ['permission:edit_leagues'], 'as' => 'league.delete','uses' => 'LeagueController@destroy']);
    Route::post('/league/bulk', ['middleware' => ['permission:edit_leagues'], 'as' => 'league.bulk','uses' => 'LeagueController@bulkAction']);

    /**
     * Dealers
     */
    /**
     * Temporary commented due to changes
     * that will be provided into the channel statistics
     * and dealers page
     * @todo uncomment this string. there is a possibility that it will be necessary to make some changes in the
     *       functionality of fetching dealers list in the channel
     */
    //Route::get('/dealers', ['middleware' => ['permission:view_dealers'], 'as' => 'dealers.get','uses' => 'DealerController@index']);
    Route::get('/dealers', ['middleware' => ['permission:view_dealers'], 'as' => 'channel.statistics.dealers','uses' => 'ChannelStatistics\DealerStatisticsController@getDealersListFroUnits']);

    Route::get('/dealers/red_list', ['middleware' => ['permission:view_channels'], 'as' => 'dealers.get.shit_list','uses' => 'DealerController@getRedList']);
    Route::get('/dealer', ['middleware' => ['permission:view_dealers'], 'as' => 'dealer.get','uses' => 'DealerController@show']);
    Route::post('/dealer', ['middleware' => ['permission:edit_dealers'], 'as' => 'dealer.add','uses' => 'DealerController@store']);
    Route::post('/dealer/load', ['middleware' => ['permission:edit_dealers'], 'as' => 'dealer.load','uses' => 'DealerController@load']);
    Route::post('/dealer/edit', ['middleware' => ['permission:edit_dealers'], 'as' => 'dealer.update','uses' => 'DealerController@update']);
    Route::post('/dealer/quick_edit', ['middleware' => ['permission:edit_dealers'], 'as' => 'dealer.quick_edit','uses' => 'DealerController@quickEdit']);
    Route::delete('/dealer', ['middleware' => ['permission:edit_dealers'], 'as' => 'dealer.delete','uses' => 'DealerController@destroy']);
    Route::post('/dealer/bulk', ['middleware' => ['permission:edit_dealers'], 'as' => 'dealer.bulk','uses' => 'DealerController@bulkAction']);
    Route::post('/dealer/logo', ['as' => 'dealer.logo.image','uses' => 'DealerController@updateImage']);

    /**
     * Positions
     */
    Route::get('/positions', ['middleware' => ['permission:view_positions'], 'as' => 'positions.get','uses' => 'PositionController@index']);
    Route::get('/position', ['middleware' => ['permission:view_positions'], 'as' => 'position.get','uses' => 'PositionController@show']);
    Route::post('/position', ['middleware' => ['permission:edit_positions'], 'as' => 'position.add','uses' => 'PositionController@store']);
    Route::put('/position', ['middleware' => ['permission:edit_positions'], 'as' => 'position.update','uses' => 'PositionController@update']);
    Route::delete('/position', ['middleware' => ['permission:edit_positions'], 'as' => 'position.delete','uses' => 'PositionController@destroy']);
    Route::post('/position/bulk', ['middleware' => ['permission:edit_positions'], 'as' => 'position.bulk','uses' => 'PositionController@bulkAction']);

    /**
     * Products
     */
    Route::get('/products', ['middleware' => ['permission:view_products'], 'as' => 'products.get','uses' => 'ProductController@index']);
    Route::get('/products/mobile', ['middleware' => ['permission:app_contest_screen'], 'as' => 'products.get.mobile','uses' => 'ProductController@showForMobile']);
    Route::put('/products/update_positions', 'ProductController@updatePositions')->middleware('permission:edit_products')->name('products.update.positions');
    Route::get('/product', ['middleware' => ['permission:view_products'], 'as' => 'product.get','uses' => 'ProductController@show']);
    Route::post('/product', ['middleware' => ['permission:edit_products'], 'as' => 'product.add','uses' => 'ProductController@store']);
    Route::post('/product/edit', ['middleware' => ['permission:edit_products'], 'as' => 'product.update','uses' => 'ProductController@update']);
    Route::delete('/product', ['middleware' => ['permission:edit_products'], 'as' => 'product.delete','uses' => 'ProductController@destroy']);
    Route::post('/product/bulk', ['middleware' => ['permission:edit_products'], 'as' => 'product.bulk','uses' => 'ProductController@bulkAction']);
    Route::post('/product/load', ['middleware' => ['permission:edit_products'], 'as' => 'product.load','uses' => 'ProductController@load']);

    /* manage points from within the web */
    Route::get('/points', ['middleware' => ['permission:view_points'], 'as' => 'points.get','uses' => 'PointController@index']);
    Route::get('/point', ['middleware' => ['permission:view_points'], 'as' => 'point.get','uses' => 'PointController@show']);
    Route::post('/point', ['middleware' => ['permission:edit_points'], 'as' => 'point.add','uses' => 'PointController@store']);
    Route::put('/point', ['middleware' => ['permission:edit_points'], 'as' => 'point.update','uses' => 'PointController@update']);
    Route::delete('/point', ['middleware' => ['permission:edit_points'], 'as' => 'point.delete','uses' => 'PointController@destroy']);
    Route::post('/point/bulk', ['middleware' => ['permission:edit_points'], 'as' => 'point.bulk','uses' => 'PointController@bulkAction']);

    Route::get('/points/manual', ['middleware' => ['permission:view_points'], 'as' => 'points.manual.get','uses' => 'ManualPointController@index']);
    Route::post('/point/manual', ['middleware' => ['permission:edit_points'], 'as' => 'point.manual.add','uses' => 'ManualPointController@store']);
    Route::delete('/point/manual', ['middleware' => ['permission:edit_points'], 'as' => 'point.manual.delete','uses' => 'ManualPointController@destroy']);
    Route::post('/point/manual/bulk', ['middleware' => ['permission:edit_points'], 'as' => 'point.manual.bulk','uses' => 'ManualPointController@bulkAction']);

    /* add points from within the app */
    Route::post('/point/product', ['middleware' => ['permission:app_contest_screen'], 'as' => 'point.addProducts','uses' => 'PointController@addProducts']);
    Route::post('/point/quiz', ['middleware' => ['permission:app_view_quizzes'], 'as' => 'point.addBrandItem','uses' => 'PointController@storeQuiz']);
    Route::post('/point/survey', ['middleware' => ['permission:app_view_surveys'], 'as' => 'point.addBrandItem','uses' => 'PointController@storeSurveyAnswers']);

    /* get quizzes from within the app */
    Route::get('/quizzes/mobile', ['middleware' => ['permission:app_view_quizzes'], 'as' => 'quizzes.user.get','uses' => 'BrandAcademyController@getUserQuizzes']);
    Route::get('/quiz/mobile', ['middleware' => ['permission:app_view_quizzes'], 'as' => 'quiz.user.get','uses' => 'BrandAcademyController@showWithUser']);

    /* manage quizzes from within the web */
    Route::get('/quizzes', ['middleware' => ['permission:view_quizzes'], 'as' => 'quizzes.get','uses' => 'BrandAcademyController@getQuizzes']);
    Route::get('/quiz', ['middleware' => ['permission:view_quizzes'], 'as' => 'quiz.get','uses' => 'BrandAcademyController@show']);
    Route::post('/quiz', ['middleware' => ['permission:edit_quizzes'], 'as' => 'quiz.add','uses' => 'BrandAcademyController@addQuiz']);
    Route::post('/quiz/edit', ['middleware' => ['permission:edit_quizzes'], 'as' => 'quiz.update','uses' => 'BrandAcademyController@update']);
    Route::post('/quiz/quick_edit', ['middleware' => ['permission:edit_quizzes'], 'as' => 'quiz.quick_edit','uses' => 'BrandAcademyController@quickEdit']);
    Route::delete('/quiz', ['middleware' => ['permission:edit_quizzes'], 'as' => 'quiz.delete','uses' => 'BrandAcademyController@destroy']);
    Route::post('/quiz/load', ['middleware' => ['permission:edit_quizzes'], 'as' => 'quiz.load','uses' => 'BrandAcademyController@loadQuizzes']);
    Route::get('/quiz/export', ['middleware' => ['permission:view_quizzes'], 'as' => 'quiz.export','uses' => 'BrandAcademyController@exportQuizzes']);
    Route::post('/quiz/bulk', ['middleware' => ['permission:edit_quizzes'], 'as' => 'quiz.bulk','uses' => 'BrandAcademyController@bulkAction']);
    Route::get('quiz/app_layout', ['middleware' => ['permission:edit_quizzes'], 'as' => 'quiz.app_layout', 'uses' => 'BrandAcademyController@getCampaignAppLayout']);

    /* get the list of surveys from within the app */
    Route::get('/surveys/mobile', ['middleware' => ['permission:app_view_surveys'], 'as' => 'surveys.user.get','uses' => 'BrandAcademyController@getUserSurveys']);
    Route::get('/survey/mobile', ['middleware' => ['permission:app_view_surveys'], 'as' => 'survey.user.get','uses' => 'BrandAcademyController@showWithUser']);

    Route::get('/surveys', ['middleware' => ['permission:view_surveys'], 'as' => 'surveys.get','uses' => 'BrandAcademyController@getSurveys']);
    Route::get('/survey', ['middleware' => ['permission:view_surveys'], 'as' => 'survey.get','uses' => 'BrandAcademyController@show']);
    Route::post('/survey', ['middleware' => ['permission:edit_surveys'], 'as' => 'survey.add','uses' => 'BrandAcademyController@addSurvey']);
    Route::post('/survey/edit', ['middleware' => ['permission:edit_surveys'], 'as' => 'survey.update','uses' => 'BrandAcademyController@update']);
    Route::post('/survey/quick_edit', ['middleware' => ['permission:edit_surveys'], 'as' => 'survey.quick_edit','uses' => 'BrandAcademyController@quickEdit']);
    Route::delete('/survey', ['middleware' => ['permission:edit_surveys'], 'as' => 'survey.delete','uses' => 'BrandAcademyController@destroy']);
    Route::post('/survey/bulk', ['middleware' => ['permission:edit_surveys'], 'as' => 'survey.bulk','uses' => 'BrandAcademyController@bulkAction']);
    Route::get('/survey/export', 'BrandAcademyController@exportSurvey')->middleware(['permission:view_surveys'])->name('survey.export');

    /* The list of endpoints that are used on brand Academy pages  */
    Route::get('/brand_academy/page/products', ['as' => 'product_assortments.get','uses' => 'ProductController@getNoPaginated']);

    Route::get('/survey/answers', ['middleware' => ['permission:view_surveys'], 'as' => 'survey.answers','uses' => 'UserAnswerController@index']);
    Route::get('/survey/answer', ['middleware' => ['permission:app_view_surveys'], 'as' => 'survey.answer','uses' => 'UserAnswerController@show']);

    /* REST API settings */
    Route::get('/settings', ['middleware' => ['permission:view_settings'], 'as' => 'api.settings','uses' => 'ApiSettingsController@show']);

    /* General report */
    Route::get('/report/info', ['middleware' => ['permission:view_channel_statistics'], 'as' => 'report.info','uses' => 'GeneralReportController@channelInfo']);
    Route::get('/report/messages', ['middleware' => ['permission:view_channel_statistics'], 'as' => 'report.messages','uses' => 'GeneralReportController@messages']);
    Route::get('/report/quizzes', ['middleware' => ['permission:view_channel_statistics'], 'as' => 'report.quizzes','uses' => 'GeneralReportController@quizzes']);
    Route::get('/report/surveys', ['middleware' => ['permission:view_channel_statistics'], 'as' => 'report.surveys','uses' => 'GeneralReportController@surveys']);

    /* To-Do items list */
    Route::get('/todo', ['middleware' => ['permission:view_settings'], 'as' => 'todo.info','uses' => 'ToDoItemController@index']);
    Route::post('/todo', ['middleware' => ['permission:edit_settings'], 'as' => 'todo.add','uses' => 'ToDoItemController@store']);
    Route::put('/todo', ['middleware' => ['permission:edit_settings'], 'as' => 'todo.update','uses' => 'ToDoItemController@update']);
    Route::delete('/todo', ['middleware' => ['permission:edit_settings'], 'as' => 'todo.delete','uses' => 'ToDoItemController@destroy']);

    Route::get('/todo/channel', ['middleware' => ['permission:view_channels'], 'as' => 'todo.channel.info','uses' => 'ToDoItemController@getInChannel']);
    Route::post('/todo/channel', ['middleware' => ['permission:view_channels'], 'as' => 'todo.channel.mark','uses' => 'ToDoItemController@markDone']);
    Route::delete('/todo/channel', ['middleware' => ['permission:view_channels'], 'as' => 'todo.channel.unmark','uses' => 'ToDoItemController@unmarkDone']);

    /* Localizations */
    Route::get('/localizations', ['middleware' => ['permission:view_settings'], 'as' => 'localizations.get','uses' => 'LocalizationController@index']);
    Route::get('/localizations/for/channel', ['as' => 'localizations.for.channel','uses' => 'LocalizationController@getForChannel']);
    Route::get('/localization', ['middleware' => ['permission:view_settings'], 'as' => 'localization.get','uses' => 'LocalizationController@show']);
    Route::post('/localization', ['middleware' => ['permission:edit_settings'], 'as' => 'localization.post','uses' => 'LocalizationController@store']);
    Route::post('/localization/edit', ['middleware' => ['permission:edit_settings'], 'as' => 'localization.update','uses' => 'LocalizationController@update']);
    Route::delete('/localization', ['middleware' => ['permission:edit_settings'], 'as' => 'localization.delete','uses' => 'LocalizationController@destroy']);
    Route::put('/localization/channel', ['middleware' => ['permission:edit_channels'], 'as' => 'localizations.channel.put','uses' => 'LocalizationController@addToChannel']);

    /* Languages */
    Route::get('/languages', ['middleware' => ['permission:view_settings'], 'as' => 'languages.get','uses' => 'LanguageController@index']);

    /* Countries */
    Route::get('/countries', ['as' => 'countries.get','uses' => 'CountryController@index']);
    Route::get('/country/localization', 'CountryController@getCountryLocalization')->name('country.localization.get');

    /* Product Assortment Categories */
    Route::get('/product_assortment/categories', ['middleware' => ['permission:view_product_assortment'], 'as' => 'product_assortment.categories.get','uses' => 'ProductAssortmentCategoryController@index']);
    Route::get('/product_assortment/page/categories', ['middleware' => ['permission:view_product_assortment'], 'as' => 'product_assortment.categories.get','uses' => 'ProductAssortmentCategoryController@getNoPaginated']);
    Route::get('/product_assortment/category', ['middleware' => ['permission:view_product_assortment'], 'as' => 'product_assortment.category.get','uses' => 'ProductAssortmentCategoryController@show']);
    Route::post('/product_assortment/category', ['middleware' => ['permission:edit_product_assortment'], 'as' => 'product_assortment.category.add','uses' => 'ProductAssortmentCategoryController@store']);
    Route::put('/product_assortment/category', ['middleware' => ['permission:edit_product_assortment'], 'as' => 'product_assortment.category.update','uses' => 'ProductAssortmentCategoryController@update']);
    Route::delete('/product_assortment/category', ['middleware' => ['permission:edit_product_assortment'], 'as' => 'product_assortment.category.delete','uses' => 'ProductAssortmentCategoryController@destroy']);
    Route::post('/product_assortment/category/bulk', ['middleware' => ['permission:edit_product_assortment'], 'as' => 'product_assortment.category.bulk','uses' => 'ProductAssortmentCategoryController@bulkAction']);
    Route::get('/product_assortment/categories/mobile', ['middleware' => ['permission:app_view_product_assortment'], 'as' => 'product_assortments.get.mobile','uses' => 'ProductAssortmentCategoryController@getForMobile']);

    /* Product Assortment */
    Route::get('/product_assortments', ['middleware' => ['permission:view_product_assortment'], 'as' => 'product_assortments.get','uses' => 'ProductAssortmentController@index']);
    Route::post('/product_assortment/load', ['middleware' => ['permission:edit_product_assortment'], 'as' => 'product_assortments.load','uses' => 'ProductAssortmentController@load']);
    Route::get('/product_assortment/export', ['middleware' => ['permission:view_product_assortment'], 'as' => 'product_assortments.export','uses' => 'ProductAssortmentController@export']);
    Route::get('/product_assortment', ['middleware' => ['permission:app_view_product_assortment'], 'as' => 'product_assortment.get','uses' => 'ProductAssortmentController@show']);
    Route::post('/product_assortment', ['middleware' => ['permission:edit_product_assortment'], 'as' => 'product_assortment.add','uses' => 'ProductAssortmentController@store']);
    Route::post('/product_assortment/edit', ['middleware' => ['permission:edit_product_assortment'], 'as' => 'product_assortment.update','uses' => 'ProductAssortmentController@update']);
    Route::delete('/product_assortment', ['middleware' => ['permission:edit_product_assortment'], 'as' => 'product_assortment.delete','uses' => 'ProductAssortmentController@destroy']);
    Route::post('/product_assortment/bulk', ['middleware' => ['permission:edit_product_assortment'], 'as' => 'product_assortment.bulk','uses' => 'ProductAssortmentController@bulkAction']);

    Route::get('/sales', ['middleware' => ['permission:app_manage_sales'], 'as' => 'sales.get','uses' => 'ChannelStatistics\SalesStatisticsController@index']);
    Route::put('/sale', ['middleware' => ['permission:app_manage_sales'], 'as' => 'sale.update','uses' => 'ChannelStatistics\SalesStatisticsController@updateProductsSale']);

    Route::get('/time', ['as' => 'sundries.server-time','uses' => 'SundriesController@getServerTime']);
    Route::get('/cache/reset', ['middleware' => ['permission:edit_settings'], 'as' => 'sundries.cache.reset','uses' => 'SundriesController@resetCache']);
});
