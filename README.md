<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

# STORE REST API

## Install and run

```
make install;
```
## Code Style

PhpStorm Settings : Open your Settings (Ctrl + Alt + S) and go to Editor > Code Style section. 
Click on the Scheme wrench on the right and select Import Scheme > Intellij IDEA code style XML.
Chose /config/codestyles/Laravel.xml

## Docker

All useful commands to work with docker listed in makefile
