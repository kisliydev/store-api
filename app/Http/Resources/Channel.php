<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Channel extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = parent::toArray($request);
        $unreadCount = $this->unreadMessages->pluck('count');
        unset($data['unread_messages']);
        $data['unread_messages_count'] = isset($unreadCount[0]) ? $unreadCount[0] : 0;
        $data['country_brand'] = [
            'id'   => $this->countryBrand->id,
            'name' => $this->countryBrand->name,
            'brand' => [
                'id'   => $this->countryBrand->brand->id,
                'name' => $this->countryBrand->brand->name,
                'logo' => $this->countryBrand->brand->logo,
            ],
        ];

        return $data;
    }
}
