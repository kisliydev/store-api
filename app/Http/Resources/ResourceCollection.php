<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection as BaseResourceCollection;
use Illuminate\Pagination\LengthAwarePaginator;

class ResourceCollection extends BaseResourceCollection
{

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($this->resource instanceof LengthAwarePaginator) {
            $items = $this->resource->getCollection()->toArray();
        } else {
            $items = parent::toArray($request);
        }

        if ($this->resource instanceof LengthAwarePaginator) {
            $response = $this->resource->toArray();
            $response['data'] = $items;
            return $response;
        } else {
            return $items;
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Http\JsonResponse $response
     */
    public function withResponse($request, $response)
    {
        $jsonResponse = json_decode($response->getContent(), true);
        unset($jsonResponse['links'],$jsonResponse['meta']);
        $response->setContent(json_encode($jsonResponse));
    }
}
