<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Recipient extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = parent::toArray($request);

        if (! empty($data['user'])) {
            $data = [
                'id' => $data['user']['id'],
                'full_name' => $data['user']['full_name'],
                'avatar' => $data['user']['avatar'] ?? null,
            ];
        } elseif (!empty($data['dealer'])) {
            $data = [
                'id'   => $data['dealer']['id'],
                'name' => $data['dealer']['name'],
                'logo' => $data['dealer']['logo'] ?? null,
            ];
        } elseif (!empty($data['district'])) {
            $data = [
                'id'   => $data['district']['id'],
                'name' => $data['district']['name'],
                'logo' => $data['district']['logo'] ?? null,
            ];
        } elseif (!empty($data['league'])) {
            $data = [
                'id'   => $data['league']['id'],
                'name' => $data['league']['name'],
                'logo' => $data['league']['logo'] ?? null,
            ];
        }

        return $data;
    }
}
