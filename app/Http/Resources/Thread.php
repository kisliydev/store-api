<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use App\Traits\ThreadResourceHelper;

class Thread extends Resource
{

    use ThreadResourceHelper;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $thread = parent::toArray($request);

        if (isset($thread['last_message'])) {
            $thread['last_message'] = $this->performLastMessage($thread['last_message']);
        }

        if (isset($thread['unread_by_admin_messages'])) {
            $thread['unread_by_admin_messages'] = empty($thread['unread_by_admin_messages']) ? 0 : count($thread['unread_by_admin_messages']);
        }

        /**
         * commented 24.01.2019
         * @deprecated due to not needed any more
         * @todo Remove after somewhile
         */
//        if (isset($thread['recipients'])) {
//            $thread['recipients'] = $this->performRecipients($thread['recipients']);
//        }

        if (isset($thread['recipients_count'])) {
            $thread['recipients_count'] = $this->performRecipientsCount($thread['recipients_count']);
        }

        return $thread;
    }
}
