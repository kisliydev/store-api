<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class ChannelMetaPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $permissions = [
            'GET' => [
                'messages_settings'   => 'view_messages_settings',
                'app_settings'        => 'view_campaigns',
                'app_localization'    => 'view_channels',
            ],
            'POST' => [
                'messages_settings'   => 'edit_messages_settings',
                'app_settings'        => 'edit_campaigns',
                'app_localization'    => 'edit_channels',
            ],
        ];
        $permission = $permissions[$request->method()][$request->key] ?? '';

        if ($permission && auth()->user()->can($permission)) {
            return $next($request);
        }
        return response()->json(['message' => 'Forbidden'], HttpResponse::HTTP_FORBIDDEN);

    }
}
