<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class SimpleAcl
{
    /**
     * Handle an incoming request.
     *
     * @param $request
     * @param \Closure $next
     * @param $permittedRoles
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function handle($request, Closure $next, $permittedRoles)
    {
        if ($this->isUserActivated() && $this->isUserEmailVerified() && $this->isPermitted($permittedRoles)) {
            return $next($request);
        }

        return response()->json(['message' => 'Unauthorized'], HttpResponse::HTTP_UNAUTHORIZED);
    }

    /**
     * @param string $permittedRoles
     * @return bool
     */
    private function isPermitted(string $permittedRoles)
    {
        $currentUserRole = auth()->user() ? auth()->user()->role : 'guest';

        foreach (explode('|', $permittedRoles) as $role) {
            if ($role === $currentUserRole) {
                return true;
            }
        }

        return false;
    }

    private function isUserActivated()
    {
        return auth()->user() && auth()->user()->status == 1;
    }

    private function isUserEmailVerified()
    {
        return auth()->user() && auth()->user()->verified == 1;
    }
}
