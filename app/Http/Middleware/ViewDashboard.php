<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class ViewDashboard
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();

        switch($this->_getFilteredObject($request->filter)) {
            case 'dealer':
                $allowed = $user->can('view_dealer_dashboard');
                break;
            case 'user':
                $allowed = $user->can('view_user_dashboard');
                break;
            default:
                $allowed = $user->can('view_channel_dashboard');
                break;
        }

        if ($allowed) {
            return $next($request);
        }

        return response()->json(['message' => 'Forbidden'], HttpResponse::HTTP_FORBIDDEN);

    }

    private function _getFilteredObject(string $filter = null)
    {
        if (empty($filter)) {
            return false;
        }

        $filters = explode(',', $filter)[0];
        preg_match("/^(dealer|user)_id:[\d]+$/", $filters, $matches);
        return $matches[1] ?? false;
    }
}
