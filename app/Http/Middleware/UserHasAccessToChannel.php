<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class UserHasAccessToChannel
{
    /**
     * @param $request
     * @param \Closure $next
     * @param string $requestParam
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function handle($request, Closure $next, string $requestParam = 'channel_id')
    {
        if (!auth()->user()->hasAccessToChannel(request()->$requestParam)) {
            return response()->json(['message' => 'Forbidden'], HttpResponse::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
