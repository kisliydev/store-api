<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class UserViewPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $data = $request->all();
        $user = auth()->user();

        if ($user->id == $data['id']) {
            $allowed = $user->can(['view_profile']);
        } else {
            $requested = User::find($data['id']);
            $allowed = $requested ? $user->can(['view_users']) : false;
        }

        if ($allowed) {
            return $next($request);
        }

        return response()->json(['message' => 'Forbidden'], HttpResponse::HTTP_FORBIDDEN);
    }
}
