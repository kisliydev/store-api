<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class StoreManager
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $channelID = abs(intval($request->channel_id));

        if (empty($channelID) || ! auth()->user()->isStoreManager($channelID)) {
            return response()->json(['message' => 'Forbidden'], HttpResponse::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
