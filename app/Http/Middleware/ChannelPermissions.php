<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class ChannelPermissions
{
    /**
     * @param $request
     * @param \Closure $next
     * @param string $permission
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function handle($request, Closure $next, string $permission)
    {
        $user = auth()->user();
        switch($permission) {
            case 'manage_dealer':
                $channelId = $request->get('channel_id');
                if (!$channelId) {
                    break;
                }

                $position = $user->position($channelId)->first();
                if($position && $position->is_manager) {
                    return $next($request);
                }
                break;
            default:
                break;

        }
        return response()->json(['message' => 'Forbidden'], HttpResponse::HTTP_FORBIDDEN);
    }
}
