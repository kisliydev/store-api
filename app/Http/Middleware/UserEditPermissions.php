<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use App\Models\User;

class UserEditPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $data = $request->all();
        $user = auth()->user();
        $allowed = false;

        if ($request->route()->getName() == 'user.update' && $user->id == $data['id']) {
            $allowed = $user->can(['edit_profile']);
        } elseif (!empty($data['id'])) {
            $managedUser = User::find($data['id']);
            $allowed = $managedUser ? $user->canManageUserByRole($managedUser->role) : false;
        } elseif (!empty($data['role'])) {
            $allowed = $user->canManageUserByRole($data['role']);
        }

        if ($allowed) {
            return $next($request);
        }

        return response()->json(['message' => 'Forbidden'], HttpResponse::HTTP_FORBIDDEN);
    }
}
