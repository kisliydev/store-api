<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class UserBindings
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();

        switch($user->role) {
            case 'global_manager':
            case 'brand_admin':
                $allowed = $user->can('view_brands');
                break;
            case 'brand_manager':
                $allowed = $user->can('view_country_brands');
                break;
            default:
                $allowed = false;
        }

        if ($allowed) {
            return $next($request);
        }

        return response()->json(['message' => 'Forbidden'], HttpResponse::HTTP_FORBIDDEN);
    }
}
