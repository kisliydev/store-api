<?php

namespace App\Http\Middleware;

use App\Models\Channel;
use App\Models\Media;
use App\Models\UserModel;
use Closure;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class MediaChannelPermissions
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        if ($user && ($user->isSystemAdmin() || $this->idsHasAccessToChannel($request) || $this->hasAccessToChannel($request))) {
            return $next($request);
        }

        return response()->json(['message' => 'Unauthorized'], HttpResponse::HTTP_UNAUTHORIZED);
    }

    /**
     * @param $request
     * @return bool
     */
    private function hasAccessToChannel($request)
    {
        if ($request->channel_id) {
            $userModel = new UserModel();
            $userModel->role = auth()->user()->role;

            return $this->isChannelAccessible($request->channel_id, $userModel);
        }

        return true;
    }

    /**
     * @param $request
     * @return bool
     */
    private function idsHasAccessToChannel($request)
    {
        $ids = json_decode($request->ids);

        if ($request->ids && is_array($ids) && array_key_exists(0, $ids)) {
            $userModel = new UserModel();
            $userModel->role = auth()->user()->role;
            $check = [];

            foreach ($ids as $id) {
                $check [] = $this->isAccessible($id, $userModel);
            }

            return ! in_array('false', $check, true);
        }

        return true;
    }

    /**
     * @param int $mediaId
     * @param \App\Models\UserModel $userModel
     * @return bool
     */
    private function isAccessible(int $mediaId, UserModel $userModel)
    {
        $media = Media::where('id', $mediaId)->first();
        if ($media) {
            return $this->isChannelAccessible($media->channel_id, $userModel);
        }

        return true;
    }

    /**
     * @param int $channelId
     * @param \App\Models\UserModel $userModel
     * @return bool
     */
    private function isChannelAccessible(int $channelId, UserModel $userModel)
    {
        $channel = Channel::where('id', $channelId)->first();
        if ($channel) {
            return $userModel->hasAccessToChannel($channel);
        }

        return true;
    }
}
