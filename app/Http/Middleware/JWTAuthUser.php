<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use \Illuminate\Support\Facades\Request;

class JWTAuthUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            if ((\App::environment() == 'testing') && array_key_exists("HTTP_AUTHORIZATION", Request::server())) {
                JWTAuth::setRequest(\Route::getCurrentRequest());
            }

            $user = JWTAuth::parseToken()->authenticate();

            if (!$user) {
                return response()->json(['message' => 'Forbidden'], HttpResponse::HTTP_FORBIDDEN);
            }

            Auth::setUser($user);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Unauthorized'], HttpResponse::HTTP_UNAUTHORIZED);
        }

        return $next($request);
    }
}
