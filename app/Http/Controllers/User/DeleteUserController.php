<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\DeleteFromBrandRequest;
use App\Http\Requests\User\DeleteFromCountryBrandRequest;
use App\Http\Requests\User\DeleteFromChannelRequest;
use App\Http\Requests\User\UsersGetRequest;
use App\Models\User;
use App\Models\Media;

class DeleteUserController extends Controller
{
    /**
     * @param \App\Http\Requests\User\UsersGetRequest $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function trash(UsersGetRequest $request, User $user)
    {
        return $this->jsonResponse($user->toTrash($request));
    }

    /**
     * @param \App\Http\Requests\User\UsersGetRequest $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function restore(UsersGetRequest $request, User $user)
    {
        return $this->jsonResponse($user->whereIn('id', $request->ids)->onlyTrashed()->restore());
    }

    /**
     * @param \App\Http\Requests\User\UsersGetRequest $request
     * @param \App\Models\User $user
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(UsersGetRequest $request, User $user, Media $media)
    {
        return $this->jsonResponse($user->remove($request, $media));
    }

    /**
     * @param \App\Models\User $user
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteOwnAccount(User $user, Media $media)
    {
        return $this->jsonResponse($user->removeOwnAccount($media));
    }

    /**
     * @param \App\Http\Requests\User\DeleteFromBrandRequest $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteFromBrand(DeleteFromBrandRequest $request, User $user)
    {
        return $this->jsonResponse($user->removeFrom($request, 'brand'));
    }

    /**
     * @param \App\Http\Requests\User\DeleteFromCountryBrandRequest $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteFromCountryBrand(DeleteFromCountryBrandRequest $request, User $user)
    {
        return $this->jsonResponse($user->removeFrom($request, 'country_brand'));
    }

    /**
     * @param \App\Http\Requests\User\DeleteFromChannelRequest $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteFromChannel(DeleteFromChannelRequest $request, User $user)
    {
        return $this->jsonResponse($user->removeFrom($request, 'channel'));
    }

    /**
     * @param \App\Http\Requests\User\UsersGetRequest $request
     * @param \App\Models\User $user
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteHeadQuorterUsers(UsersGetRequest $request, User $user, Media $media)
    {
        return $this->jsonResponse($user->deleteHeadQuorterUsers($request, $media));
    }
}
