<?php

namespace App\Http\Controllers\User;

use App\Http\Requests\User\UserBindigsGetRequest;
use App\Models\User;
use App\Models\Media;
use App\Models\ACL;
use App\Models\UserProfileField;

use App\Http\Controllers\Controller;

use App\Services\Paginator;
use App\Transformers\User\UserTransformer;
use App\Transformers\User\UsersBindingsTransformer;
use App\Transformers\User\HeadQuorterTransformer;
use App\Transformers\User\UsersListTransformer;

use App\Http\Requests\User\UserGetAllRequest;
use App\Http\Requests\User\UserGetInBrandRequest;
use App\Http\Requests\User\UserGetInCountryBrandRequest;
use App\Http\Requests\User\UserGetInChannelRequest;
use App\Http\Requests\User\UserGetInDealerRequest;
use App\Http\Requests\User\UserSaveRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Http\Requests\User\UserGetRequest;
use App\Http\Requests\User\UserLoadRequest;
use App\Http\Requests\User\UserSaveAdditionalDataRequest;
use App\Http\Requests\User\UserGetChannelsRequest;
use App\Http\Requests\User\UserGetForMobileRequest;
use App\Http\Requests\User\UserUpdateMobileRequest;
use App\Http\Requests\User\CheckEmailRequest;
use App\Http\Requests\User\UserManageBindingsRequest;
use App\Http\Requests\User\UserAddHQRequest;

class UserController extends Controller
{
    /**
     * @param \App\Transformers\User\UserTransformer $transformer
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(UserTransformer $transformer, User $user)
    {
        return $this->jsonResponse($transformer->transform($user->getAuthUserProfile())->first());
    }

    /**
     * @param \App\Http\Requests\User\UserGetForMobileRequest $request
     * @param \App\Models\User $user
     * @param \App\Transformers\User\UserTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function getForMobile(UserGetForMobileRequest $request, User $user, UserTransformer $transformer)
    {
        return $this->jsonResponse($transformer->transform($user->getAuthUserProfileForMobile($request))->first());
    }

    /**
     * @param \App\Http\Requests\User\UserGetForMobileRequest $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTotals(UserGetForMobileRequest $request, User $user)
    {
        return $this->jsonResponse($user->getTotals($request));
    }

    /**
     * @param \App\Http\Requests\User\UserBindigsGetRequest $request
     * @param \App\Transformers\User\UsersBindingsTransformer $transformer
     * @param \App\Models\User $user
     * @param \App\Services\Paginator $paginator
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBindings(UserBindigsGetRequest $request, UsersBindingsTransformer $transformer, User $user, Paginator $paginator)
    {
        $bindings = $user->getBindings($request->search);
        $bindings = $transformer->transform($bindings);
        $data = $paginator->collectionPagination($bindings, $request->per_page, $request->page);

        return $this->jsonResponse($data);
    }

    /**
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBoundChannels(User $user)
    {
        return $this->jsonResponse($user->getBoundChannels());
    }

    /**
     * @param UserGetRequest $request
     * @param User $user
     * @return mixed
     */
    public function show(UserGetRequest $request, User $user)
    {
        return $this->jsonResponse($user->getUser($request)->first());
    }

    /**
     * @param \App\Http\Requests\User\UserGetAllRequest $request
     * @param \App\Models\User $user
     * @param \App\Transformers\User\UsersListTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll(UserGetAllRequest $request, User $user, UsersListTransformer $transformer)
    {
        $data = $user->getAll($request);
        $transformer->transform($data->getCollection());
        return $this->jsonResponse($data);
    }

    /**
     * @param \App\Http\Requests\User\UserGetInBrandRequest $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInBrand(UserGetInBrandRequest $request, User $user)
    {
        return $this->jsonResponse($user->getInBrand($request));
    }

    /**
     * @param \App\Http\Requests\User\UserGetInCountryBrandRequest $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInCountryBrand(UserGetInCountryBrandRequest $request, User $user)
    {
        return $this->jsonResponse($user->getInCountryBrand($request));
    }

    /**
     * @param \App\Http\Requests\User\UserGetInChannelRequest $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInChannel(UserGetInChannelRequest $request, User $user)
    {
        return $this->jsonResponse($user->getInChannel($request));
    }

    /**
     * @param \App\Http\Requests\User\UserGetInDealerRequest $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInDealer(UserGetInDealerRequest $request, User $user)
    {
        return $this->jsonResponse($user->getInDealer($request));
    }

    /**
     * @param \App\Http\Requests\User\UserSaveRequest $request
     * @param \App\Models\User $user
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(UserSaveRequest $request, User $user, Media $media)
    {
        return $this->jsonResponse($user->add($request, $media));
    }

    /**
     * @param \App\Http\Requests\User\UserUpdateRequest $request
     * @param \App\Models\User $user
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UserUpdateRequest $request, User $user, Media $media)
    {
        return $this->jsonResponse($user->resave($request, $media));
    }

    /**
     * @param \App\Http\Requests\User\UserManageBindingsRequest $request
     * @param \App\Models\User $user
     * @param \App\Models\ACL $acl
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateBindings(UserManageBindingsRequest $request, User $user, ACL $acl)
    {
        return $this->jsonResponse($user->resaveBindings($request, $acl));
    }

    /**
     * @param $request
     * @param \App\Models\User $user
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateMobile(UserUpdateMobileRequest $request, User $user, Media $media)
    {
        return $this->jsonResponse($user->resaveMobile($request, $media));
    }

    /**
     * @param \App\Http\Requests\User\UserSaveAdditionalDataRequest $request
     * @param \App\Models\UserProfileField $fields
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveAdditionalData(UserSaveAdditionalDataRequest $request, UserProfileField $fields)
    {
        return $this->jsonResponse($fields->saveUserData($request));
    }

    /**
     * @param \App\Http\Requests\User\UserGetInChannelRequest $request
     * @param \App\Models\User $user
     * @param \App\Transformers\User\HeadQuorterTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function getHeadQuorterUsers(UserGetInChannelRequest $request, User $user, HeadQuorterTransformer $transformer)
    {
        return $this->jsonResponse($transformer->transformPaginate($user->getHeadQuorterUsers($request)));
    }

    /**
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNewHeadQuorterUsers(User $user)
    {
        return $this->jsonResponse($user->getNewHeadQuorterUsers());
    }

    /**
     * @param \App\Http\Requests\User\UserAddHQRequest $request
     * @param \App\Models\User $user
     * @param \App\Transformers\User\HeadQuorterTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function addHeadQuorterUsers(UserAddHQRequest $request, User $user, HeadQuorterTransformer $transformer)
    {
        return $this->jsonResponse($transformer->transform($user->addHeadQuorters($request)));
    }

    /**
     * @param \App\Http\Requests\User\UserLoadRequest $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function load(UserLoadRequest $request, User $user)
    {
        return $this->jsonResponse($user->loadFromExcel($request));
    }

    /**
     * @param \App\Http\Requests\User\CheckEmailRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkEmail(CheckEmailRequest $request)
    {
        return $this->jsonResponse(true);
    }
}
