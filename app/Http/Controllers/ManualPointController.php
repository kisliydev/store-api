<?php

namespace App\Http\Controllers;

use App\Http\Requests\ManualPoint\ManualPointGetListRequest;
use App\Http\Requests\ManualPoint\ManualPointGetRequest;
use App\Http\Requests\ManualPoint\ManualPointSaveRequest;
use App\Http\Requests\ManualPoint\ManualPointBulkActionRequest;
use App\Models\ManualPoint;
use App\Transformers\RecipientsListTransformer;

class ManualPointController extends Controller
{
    /**
     * @param \App\Http\Requests\ManualPoint\ManualPointGetListRequest $request
     * @param \App\Models\ManualPoint $point
     * @param \App\Transformers\RecipientsListTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ManualPointGetListRequest $request, ManualPoint $point, RecipientsListTransformer $transformer)
    {
        $data = $point->getList($request);
        $transformer->transform($data->getCollection());
        return $this->jsonResponse($data);
    }

    /**
     * @param \App\Http\Requests\ManualPoint\ManualPointSaveRequest $request
     * @param \App\Models\ManualPoint $point
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ManualPointSaveRequest $request, ManualPoint $point)
    {
        return $this->jsonResponse($point->create($request->validated()));
    }

    /**
     * @param \App\Http\Requests\ManualPoint\ManualPointGetRequest $request
     * @param \App\Models\ManualPoint $point
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(ManualPointGetRequest $request, ManualPoint $point)
    {
        return $this->jsonResponse($point->remove($request));
    }

    /**
     * @param \App\Http\Requests\ManualPoint\ManualPointBulkActionRequest $request
     * @param \App\Models\ManualPoint $point
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkAction(ManualPointBulkActionRequest $request, ManualPoint $point)
    {
        return $this->jsonResponse($point->bulkAction($request));
    }
}
