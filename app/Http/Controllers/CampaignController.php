<?php

namespace App\Http\Controllers;

use App\Http\Requests\Campaign\DraftCampaignSaveRequest;
use App\Http\Requests\Campaign\GetByChannelRequest;
use App\Http\Requests\Campaign\CampaignSaveRequest;
use App\Http\Requests\Campaign\CampaignUpdateRequest;
use App\Http\Requests\Campaign\CampaignGetRequest;
use App\Http\Requests\Campaign\CampaignSaveAppLayoutRequest;
use App\Http\Requests\Campaign\CampaignDeleteRequest;
use App\Http\Requests\Campaign\DraftCampaignUpdateRequest;
use App\Services\Base64ImageReplacer\Base64ImageReplacer;
use App\Transformers\AppLayoutTransformer;
use App\Models\Campaign;
use App\Models\Media;

class CampaignController extends Controller
{
    /**
     * @param \App\Http\Requests\Campaign\GetByChannelRequest $request
     * @param \App\Models\Campaign $campaign
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(GetByChannelRequest $request, Campaign $campaign)
    {
        return $this->jsonResponse($campaign->getAll($request));
    }

    /**
     * @param \App\Http\Requests\Campaign\CampaignGetRequest $request
     * @param \App\Models\Campaign $campaign
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(CampaignGetRequest $request, Campaign $campaign)
    {
        return $this->jsonResponse($campaign->find($request->id));
    }

    /**
     * @param \App\Http\Requests\Campaign\CampaignSaveRequest $request
     * @param \App\Models\Campaign $campaign
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CampaignSaveRequest $request, Campaign $campaign)
    {
        return $this->jsonResponse($campaign->create($request->validated()));
    }

    /**
     * @param \App\Http\Requests\Campaign\DraftCampaignSaveRequest $request
     * @param \App\Models\Campaign $campaign
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeDraft(DraftCampaignSaveRequest $request, Campaign $campaign)
    {
        return $this->jsonResponse($campaign->create($request->validated()));
    }

    /**
     * @param \App\Http\Requests\Campaign\CampaignUpdateRequest $request
     * @param \App\Models\Campaign $campaign
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CampaignUpdateRequest $request, Campaign $campaign)
    {
        return $this->jsonResponse($campaign->resave($request));
    }

    /**
     * @param \App\Http\Requests\Campaign\DraftCampaignUpdateRequest $request
     * @param \App\Models\Campaign $campaign
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateDraft(DraftCampaignUpdateRequest $request, Campaign $campaign)
    {
        return $this->jsonResponse($campaign->resave($request));
    }

    /**
     * @param \App\Http\Requests\Campaign\CampaignDeleteRequest $request
     * @param \App\Models\Campaign $campaign
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(CampaignDeleteRequest $request, Campaign $campaign)
    {
        return $this->jsonResponse($campaign->remove($request));
    }

    /**
     * @param \App\Http\Requests\Campaign\CampaignDeleteRequest $request
     * @param \App\Models\Campaign $campaign
     * @return \Illuminate\Http\JsonResponse
     */
    public function trash(CampaignDeleteRequest $request, Campaign $campaign)
    {
        return $this->jsonResponse($campaign->toTrash($request));
    }

    /**
     * @param \App\Http\Requests\Campaign\CampaignGetRequest $request
     * @param \App\Models\Campaign $campaign
     * @return \Illuminate\Http\JsonResponse
     */
    public function restore(CampaignGetRequest $request, Campaign $campaign)
    {
        return $this->jsonResponse($campaign->restoreCampaign($request));
    }

    /**
     * @param \App\Http\Requests\Campaign\CampaignGetRequest $request
     * @param \App\Models\Campaign $campaign
     * @param \App\Transformers\AppLayoutTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function showAppLayout(CampaignGetRequest $request, Campaign $campaign, AppLayoutTransformer $transformer)
    {
        $data = $campaign->where('id', $request->id)->get()->makeVisible(['app_layout']);
        return $this->jsonResponse($transformer->transform($data)->first());
    }

    /**
     * @param \App\Http\Requests\Campaign\CampaignSaveAppLayoutRequest $request
     * @param \App\Models\Campaign $campaign
     * @param \App\Models\Media $media
     * @param \App\Transformers\AppLayoutTransformer $transformer
     * @param \App\Http\Controllers\Base64ImageReplacer $imageReplacer
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveAppLayout(
        CampaignSaveAppLayoutRequest $request,
        Campaign $campaign,
        Media $media,
        AppLayoutTransformer $transformer,
        Base64ImageReplacer $imageReplacer
    )
    {
        $data = collect([$campaign->saveAppLayout($request, $media, $imageReplacer)]);
        return $this->jsonResponse($transformer->transform($data)->first());
    }
}
