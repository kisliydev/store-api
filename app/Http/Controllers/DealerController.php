<?php

namespace App\Http\Controllers;

use App\Http\Requests\Dealer\DealerGetAllRequest;
use App\Http\Requests\Dealer\DealerGetRequest;
use App\Http\Requests\Dealer\DealerSaveRequest;
use App\Http\Requests\Dealer\DealerUpdateRequest;
use App\Http\Requests\Dealer\DealerLoadRequest;
use App\Http\Requests\Dealer\DealersBulkActionRequest;
use App\Http\Requests\Dealer\DealerGetRedListRequest;
use App\Http\Requests\Dealer\DealerQuickEditRequest;
use App\Http\Requests\Dealer\DealerUpdateImageRequest;
use App\Models\Dealer;
use App\Models\Media;

/**
 * Class DealerController
 *
 * @package App\Http\Controllers
 */
class DealerController extends Controller
{
    /**
     * @param \App\Http\Requests\Dealer\DealerGetAllRequest $request
     * @param \App\Models\Dealer $dealer
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(DealerGetAllRequest $request, Dealer $dealer)
    {
        return $this->jsonResponse($dealer->getList($request));
    }

    /**
     * @param \App\Http\Requests\Dealer\DealerGetRedListRequest $request
     * @param \App\Models\Dealer $dealer
     * @return \Illuminate\Http\JsonResponse
     */
    public function getRedList(DealerGetRedListRequest $request, Dealer $dealer)
    {
        return $this->jsonResponse($dealer->getRedList($request));
    }

    /**
     * @param \App\Http\Requests\Dealer\DealerGetRequest $request
     * @param \App\Models\Dealer $dealer
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(DealerGetRequest $request, Dealer $dealer)
    {
        return $this->jsonResponse($dealer->get($request));
    }

    /**
     * @param \App\Http\Requests\Dealer\DealerSaveRequest $request
     * @param \App\Models\Dealer $dealer
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(DealerSaveRequest $request, Dealer $dealer, Media $media)
    {
        return $this->jsonResponse($dealer->add($request, $media));
    }

    /**
     * @param \App\Http\Requests\Dealer\DealerLoadRequest $request
     * @param \App\Models\Dealer $dealer
     * @return \Illuminate\Http\JsonResponse
     */
    public function load(DealerLoadRequest $request, Dealer $dealer)
    {
        return $this->jsonResponse($dealer->loadFromExcel($request));
    }

    /**
     * @param \App\Http\Requests\Dealer\DealerUpdateRequest $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(DealerUpdateRequest $request, Dealer $dealer, Media $media)
    {
        return $this->jsonResponse($dealer->resave($request, $media));
    }

    /**
     * @param \App\Http\Requests\Dealer\DealerUpdateImageRequest $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateImage(DealerUpdateImageRequest $request, Dealer $dealer, Media $media)
    {
        try {
            return $this->jsonResponse($dealer->updateImage($request, $media));
        } catch (\Exception $e) {
            return $this->jsonResponse(['message' => 'Forbidden'], 403);
        }
    }

    /**
     * @param \App\Http\Requests\Dealer\DealerQuickEditRequest $request
     * @param \App\Models\Dealer $dealer
     * @return \Illuminate\Http\JsonResponse
     */
    public function quickEdit(DealerQuickEditRequest $request, Dealer $dealer)
    {
        return $this->jsonResponse($dealer->quickEdit($request));
    }

    /**
     * @param \App\Http\Requests\Dealer\DealerGetRequest $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(DealerGetRequest $request, Dealer $dealer, Media $media)
    {
        return $this->jsonResponse($dealer->remove($request, $media));
    }

    /**
     * @param \App\Http\Requests\Dealer\DealersBulkActionRequest $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkAction(DealersBulkActionRequest $request, Dealer $dealer, Media $media)
    {
        return $this->jsonResponse($dealer->bulkAction($request, $media));
    }
}
