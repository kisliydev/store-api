<?php

namespace App\Http\Controllers;

use App\Http\Requests\BrandAcademy\BrandAcademyExportRequest;
use App\Http\Requests\BrandAcademy\BrandAcademyGetAllRequest;
use App\Http\Requests\BrandAcademy\BrandAcademyGetRequest;
use App\Http\Requests\BrandAcademy\BrandAcademyLoadRequest;
use App\Http\Requests\BrandAcademy\BrandAcademySaveRequest;
use App\Http\Requests\BrandAcademy\BrandAcademyUpdateRequest;
use App\Http\Requests\BrandAcademy\BrandAcademyBulkActionRequest;
use App\Http\Requests\BrandAcademy\BrandAcademyQuickEditRequest;
use App\Http\Requests\BrandAcademy\QuizCampaignLayoutRequest;
use App\Http\Requests\BrandAcademy\SurveyExportRequest;
use App\Models\Channel;
use App\Services\AcademyItemsExportService\AcademyItemsExporter;
use App\Services\ZipArchiveService;
use App\Transformers\AppLayoutTransformer;
use App\Transformers\BrandAcademyListTransformer;
use App\Models\AcademyItem;
use App\Models\Media;
use App\Models\Question;
use App\Models\Answer;
use App\Models\Campaign;
use App\Transformers\QuizCampaignLayoutTransformer;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

class BrandAcademyController extends Controller
{
    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademyGetAllRequest $request
     * @param \App\Models\AcademyItem $item
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserQuizzes(BrandAcademyGetAllRequest $request, AcademyItem $item)
    {
        return $this->jsonResponse($item->mobileList($request, 'quiz'));
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademyGetAllRequest $request
     * @param \App\Models\AcademyItem $item
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserSurveys(BrandAcademyGetAllRequest $request, AcademyItem $item)
    {
        return $this->jsonResponse($item->mobileList($request, 'survey'));
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademyGetAllRequest $request
     * @param \App\Models\AcademyItem $item
     * @param \App\Transformers\BrandAcademyListTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function getQuizzes(BrandAcademyGetAllRequest $request, AcademyItem $item, BrandAcademyListTransformer $transformer)
    {
        $data = $item->getList($request, 'quiz');
        $transformer->transform($data->getCollection());
        return $this->jsonResponse($data);
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademyGetAllRequest $request
     * @param \App\Models\AcademyItem $item
     * @param \App\Transformers\BrandAcademyListTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSurveys(BrandAcademyGetAllRequest $request, AcademyItem $item, BrandAcademyListTransformer $transformer)
    {
        $data = $item->getList($request, 'survey');
        $transformer->transform($data->getCollection());
        return $this->jsonResponse($data);
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademyGetRequest $request
     * @param \App\Models\AcademyItem $item
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(BrandAcademyGetRequest $request, AcademyItem $item)
    {
        return $this->jsonResponse($item->getItem($request));
    }

    public function getCampaignAppLayout(
        QuizCampaignLayoutRequest $request,
        Campaign $campaign,
        QuizCampaignLayoutTransformer $quizCampaignLayoutTransformer,
        AppLayoutTransformer $appLayoutTransformer
    )
    {
        $activeCampaign = $campaign->getCampaignLayout($request->channel_id);
        $campaign = !$activeCampaign->isEmpty() ? $activeCampaign : collect([$campaign->getCampaignLayout($request->channel_id, false)->last()]);
        if ($campaign->first()) {
            $campaign = $appLayoutTransformer->transform($campaign)->first();
            $quiz = $quizCampaignLayoutTransformer->transform(collect($campaign));

            return $this->jsonResponse($quiz);
        }

        return response()->json(['data' => null]);
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademyGetRequest $request
     * @param \App\Models\AcademyItem $item
     * @return \Illuminate\Http\JsonResponse
     */
    public function showWithUser(BrandAcademyGetRequest $request, AcademyItem $item)
    {
        return $this->jsonResponse($item->getItem($request, true));
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademySaveRequest $request
     * @param \App\Models\AcademyItem $item
     * @param \App\Models\Question $question
     * @param \App\Models\Media $media
     * @param \App\Models\Answer $answer
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function addQuiz(BrandAcademySaveRequest $request, AcademyItem $item, Question $question, Media $media, Answer $answer)
    {
        return $this->jsonResponse($item->add($request, 'quiz', $question, $media, $answer));
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademySaveRequest $request
     * @param \App\Models\AcademyItem $item
     * @param \App\Models\Question $question
     * @param \App\Models\Media $media
     * @param \App\Models\Answer $answer
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function addSurvey(BrandAcademySaveRequest $request, AcademyItem $item, Question $question, Media $media, Answer $answer)
    {
        return $this->jsonResponse($item->add($request, 'survey', $question, $media, $answer));
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademyQuickEditRequest $request
     * @param \App\Models\AcademyItem $item
     * @return \Illuminate\Http\JsonResponse
     */
    public function quickEdit(BrandAcademyQuickEditRequest $request, AcademyItem $item)
    {
        $data = $item->quickEdit($request);
        $data->questions_count = $data->questions->count();
        unset($data->questions);
        return $this->jsonResponse($data);
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademyUpdateRequest $request
     * @param \App\Models\AcademyItem $item
     * @param \App\Models\Question $question
     * @param \App\Models\Media $media
     * @param \App\Models\Answer $answer
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(BrandAcademyUpdateRequest $request, AcademyItem $item, Question $question, Media $media, Answer $answer)
    {
        return $this->jsonResponse($item->resave($request, $question, $media, $answer));
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademyGetRequest $request
     * @param \App\Models\AcademyItem $item
     * @param \App\Models\Media $media
     * @param \App\Models\Question $question
     * @param \App\Models\Answer $answer
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(BrandAcademyGetRequest $request, AcademyItem $item, Media $media, Question $question, Answer $answer)
    {
        return $this->jsonResponse($item->remove($request, $media, $question, $answer));
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademyLoadRequest $request
     * @param \App\Models\AcademyItem $item
     * @return \Illuminate\Http\JsonResponse
     */
    public function loadQuizzes(BrandAcademyLoadRequest $request, AcademyItem $item)
    {
        try {
            return $this->jsonResponse($item->loadFromExcel($request, 'quiz'));
        } catch (\Exception $e) {
            return $this->jsonResponse(['message' => $e->getMessage()], HttpResponse::HTTP_UNPROCESSABLE_ENTITY);
        }
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademyExportRequest $request
     * @param \App\Models\AcademyItem $item
     * @return \Illuminate\Http\JsonResponse
     */
    public function exportQuizzes(BrandAcademyExportRequest $request, AcademyItem $item)
    {
        return $this->exportFile(
            'channel_quizzes.xlsx',
            $item->exportToExcel($request, 'quiz')
        );
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademyBulkActionRequest $request
     * @param \App\Models\AcademyItem $item
     * @param \App\Models\Media $media
     * @param \App\Models\Question $question
     * @param \App\Models\Answer $answer
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkAction(BrandAcademyBulkActionRequest $request, AcademyItem $item, Media $media,  Question $question, Answer $answer)
    {
        return $this->jsonResponse($item->bulkAction($request, $media, $question, $answer));
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\SurveyExportRequest $request
     * @param \App\Services\AcademyItemsExportService\AcademyItemsExporter $itemsExporter
     * @param \App\Models\AcademyItem $academyItem
     * @param \App\Models\Channel $channel
     * @param \App\Services\ZipArchiveService $zipArchiveService
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws \App\Services\AcademyItemsExportService\Exceptions\IncorrectExportItemTypeException
     */
    public function exportSurvey(
        SurveyExportRequest $request,
        AcademyItemsExporter $itemsExporter,
        AcademyItem $academyItem,
        Channel $channel,
        ZipArchiveService $zipArchiveService
    ) {
        $ids = $request->ids ?? $academyItem->getItemsIdByChannel($request->channel_id, 'survey');

        $exportPath = $itemsExporter
            ->setType('survey')
            ->setParams([
                'ids' => $ids,
                'channel_name' => $channel->getChannelName($request->channel_id),
            ])->export();
        $zipPath = $zipArchiveService->make($exportPath['file_path'], $exportPath['directory_name']);

        return response()
            ->download($zipPath, basename($zipPath), ['Content-Type' => 'application/zip'])
            ->deleteFileAfterSend(true);
    }
}
