<?php

namespace App\Http\Controllers;

use App\Http\Requests\CountryBrand\CountryBrandSaveRequest;
use App\Http\Requests\CountryBrand\CountryBrandGetRequest;
use App\Http\Requests\CountryBrand\CountryBrandUpdateRequest;
use App\Http\Requests\CountryBrand\CountryBrandGetAllRequest;
use App\Models\CountryBrand;

/**
 * Class CountryBrandController
 *
 * @package App\Http\Controllers
 */
class CountryBrandController extends Controller
{
    /**
     * @param \App\Http\Requests\CountryBrand\CountryBrandGetAllRequest $request
     * @param \App\Models\CountryBrand $brand
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(CountryBrandGetAllRequest $request, CountryBrand $brand)
    {
        return $this->jsonResponse($brand->getList($request));
    }

    /**
     * @param \App\Http\Requests\CountryBrand\CountryBrandGetRequest $request
     * @param \App\Models\CountryBrand $brand
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(CountryBrandGetRequest $request, CountryBrand $brand)
    {
        return $this->jsonResponse($brand->get($request));
    }

    /**
     * @param \App\Http\Requests\CountryBrand\CountryBrandSaveRequest $request
     * @param \App\Models\CountryBrand $brand
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CountryBrandSaveRequest $request, CountryBrand $brand)
    {
        return $this->jsonResponse($brand->create($request->validated()));
    }

    /**
     * @param \App\Http\Requests\CountryBrand\CountryBrandUpdateRequest $request
     * @param \App\Models\CountryBrand $brand
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CountryBrandUpdateRequest $request, CountryBrand $brand)
    {
        return $this->jsonResponse($brand->resave($request));
    }

    /**
     * @param \App\Http\Requests\CountryBrand\CountryBrandGetRequest $request
     * @param \App\Models\CountryBrand $brand
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(CountryBrandGetRequest $request, CountryBrand $brand)
    {
        return $this->jsonResponse($brand->remove($request));
    }
}
