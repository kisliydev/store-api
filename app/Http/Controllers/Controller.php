<?php

namespace App\Http\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Collection as SCollection;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Tymon\JWTAuth\Facades\JWTAuth as JWTFacade;
use Illuminate\Http\Resources\Json\Resource;
use Illuminate\Pagination\LengthAwarePaginator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * Controller constructor.
     */
    public function __construct()
    {
        if ((app()->environment() == 'testing') && array_key_exists("HTTP_AUTHORIZATION", \Request::server())) {
            JWTFacade::setRequest(\Route::getCurrentRequest());
        }
    }

    /**
     * Prepare json response
     *
     * @param mixed $defaultResponse
     * @param int $status
     * @return \Illuminate\Http\JsonResponse
     */
    protected function jsonResponse($defaultResponse, $status = 200)
    {
        if (is_array($defaultResponse)) {
            $defaultResponse = $this->wrapArrayInData($defaultResponse);
        } elseif ($defaultResponse instanceof Model || $defaultResponse instanceof Collection || $defaultResponse instanceof SCollection) {
            $defaultResponse = $this->wrapArrayInData($defaultResponse->toArray(), true);
        } elseif (is_bool($defaultResponse) || is_int($defaultResponse)) {
            $defaultResponse = ['data' => $defaultResponse];
        } elseif ($defaultResponse instanceof Resource) {
            if ($defaultResponse->resource instanceof LengthAwarePaginator) {
                return $defaultResponse;
            }
            return ['data' => $defaultResponse];
        }

        return response()->json($defaultResponse, $status);
    }

    /**
     * @param array $array
     * @param array $forceWrap
     * @return array
     */
    protected function wrapArrayInData(array $array, bool $forceWrap = false): array
    {
        if ($forceWrap || (!array_key_exists('data', $array) && !array_key_exists('message', $array))) {
            $array = ['data' => $array];
        }

        return $array;
    }

    /**
     * @param string $filename
     * @param string $content
     * @param string|null $contentType
     * @return mixed
     */
    protected function exportFile(string $filename, string $content, string $contentType = null) {

        $contentType = $contentType ?? 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';

        return response($content)
            ->header('Content-Type', $contentType)
            ->header('Content-Disposition', "attachment; filename=\"{$filename}\"")
            ->header('Cache-Control', 'max-age=0');
    }
}
