<?php

namespace App\Http\Controllers\Messages;

use App\Http\Controllers\Controller;
use App\Models\Level;

use App\Http\Requests\Level\GetListRequest;
use App\Http\Requests\Level\CreateRequest;
use App\Http\Requests\Level\UpdateRequest;
use App\Http\Requests\Level\QuickEditRequest;
use App\Http\Requests\Level\DeleteRequest;

class LevelController extends Controller
{
    /**
     * @param Level $level
     * @return \Illuminate\Http\JsonResponse
     */
    public function showTypes(Level $level)
    {
        return $this->jsonResponse($level->getTypesSchema());
    }

    /**
     * @param GetListRequest $request
     * @param Level $level
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(GetListRequest $request, Level $level)
    {
        return $this->jsonResponse($level->getList($request));
    }

    /**
     * @param CreateRequest $request
     * @param Level $level
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateRequest $request, Level $level)
    {
        return $this->jsonResponse($level->add($request));
    }

    /**
     * @param UpdateRequest $request
     * @param Level $level
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, Level $level)
    {
        return $this->jsonResponse($level->resave($request));
    }

    /**
     * @param QuickEditRequest $request
     * @param Level $level
     * @return \Illuminate\Http\JsonResponse
     */
    public function quickEdit(QuickEditRequest $request, Level $level)
    {
        return $this->jsonResponse($level->quickEdit($request));
    }

    /**
     * @param DeleteRequest $request
     * @param Level $level
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DeleteRequest $request, Level $level)
    {
        return $this->jsonResponse($level->remove($request));
    }
}
