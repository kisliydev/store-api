<?php

namespace App\Http\Controllers\Messages;

use App\Models\Recipient;
use App\Http\Requests\Recipients\GetListRequest;
use App\Http\Controllers\Controller;
use App\Http\Resources\RecipientCollection as RecipientCollectionResource;

class RecipientsController extends Controller
{
    /**
     * @param GetListRequest $request
     * @param Recipient $recipient
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(GetListRequest $request, Recipient $recipient)
    {
        return $this->jsonResponse(new RecipientCollectionResource($recipient->getList($request)));
    }
}
