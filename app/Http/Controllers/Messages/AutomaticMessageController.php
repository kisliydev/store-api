<?php

namespace App\Http\Controllers\Messages;

use App\Http\Controllers\Controller;
use App\Models\AutomaticMessage;

use App\Http\Requests\AutomaticMessage\GetListRequest;
use App\Http\Requests\AutomaticMessage\CreateRequest;
use App\Http\Requests\AutomaticMessage\UpdateRequest;
use App\Http\Requests\AutomaticMessage\DeleteRequest;

class AutomaticMessageController extends Controller
{
    /**
     * @param AutomaticMessage $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTypes(AutomaticMessage $message)
    {
        return $this->jsonResponse($message->getTypesSchema());
    }

    /**
     * @param GetListRequest $request
     * @param AutomaticMessage $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(GetListRequest $request, AutomaticMessage $message)
    {
        return $this->jsonResponse($message->getList($request));
    }

    /**
     * @param CreateRequest $request
     * @param AutomaticMessage $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateRequest $request, AutomaticMessage $message)
    {
        return $this->jsonResponse($message->add($request));
    }

    /**
     * @param UpdateRequest $request
     * @param AutomaticMessage $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, AutomaticMessage $message)
    {
        return $this->jsonResponse($message->resave($request));
    }

    /**
     * @param DeleteRequest $request
     * @param AutomaticMessage $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DeleteRequest $request, AutomaticMessage $message)
    {
        return $this->jsonResponse($message->remove($request));
    }
}
