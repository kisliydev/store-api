<?php

namespace App\Http\Controllers\Messages;

use App\Http\Requests\Socket\BroadcastMessageRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Traits\SocketNotifications;

/**
 * Class SocketController
 *
 * @package App\Http\Controllers
 */
class SocketController extends Controller
{
    use SocketNotifications;

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function broadcast(Request $request)
    {
        $this->notifyLocaly($request->all());

        return $this->jsonResponse(true);
    }
}
