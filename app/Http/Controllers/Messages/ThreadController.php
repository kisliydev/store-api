<?php

namespace App\Http\Controllers\Messages;

use App\Http\Requests\Request;
use App\Http\Requests\Thread\ThreadDeleteRequest;
use App\Http\Requests\Thread\ThreadListRequest;
use App\Http\Requests\Thread\ThreadUpdateRequest;
use App\Http\Requests\Thread\ThreadCreateRequest;
use App\Http\Requests\Thread\ThreadUserCreateRequest;
use App\Http\Requests\Thread\DeleteUsersFromDealerRequest;
use App\Http\Requests\Thread\PublishRequest;
use App\Http\Requests\Thread\UpdateRequest;
use App\Http\Resources\ThreadCollection;
use App\Models\Thread;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use App\Transformers\Messages\ThreadsListTransformer;
use App\Transformers\Messages\UsersThreadTransformer;
use App\Traits\SocketNotifications;
use App\Http\Resources\ThreadCollection as ThreadCollectionResource;

/**
 * Class ThreadController
 *
 * @package App\Http\Controllers
 */
class ThreadController extends Controller
{
    use SocketNotifications;

    /**
     * @param ThreadCreateRequest $request
     * @param Thread $thread
     * @param ThreadsListTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function closedCreate(ThreadCreateRequest $request, Thread $thread, ThreadsListTransformer $transformer)
    {
        $request->merge([
            'type' => 'from_admin',
        ]);

        $status = empty($request->publish_at) ? 'closed' : 'scheduled';
        $thread = $transformer->transform(collect([$thread->createEntity($request, $status)]))->first();

        if ('closed' == $status) {
            $this->_notify($thread);
        }

        return $this->jsonResponse($thread);
    }

    /**
     * @param ThreadUserCreateRequest $request
     * @param Thread $thread
     * @param ThreadsListTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function userCreate(ThreadUserCreateRequest $request, Thread $thread, ThreadsListTransformer $transformer)
    {
        $userID = auth()->user()->id;
        $request->merge([
            'type' => 'from_user',
            'recipients' => "user_id:{$userID}",
        ]);

        $data = $transformer->transform(collect([$thread->userCreateEntity($request)]))->first();

        $this->_notify($data);

        return $this->jsonResponse($data);
    }

    /**
     * @param ThreadListRequest $request
     * @param Thread $thread
     * @return \Illuminate\Http\JsonResponse
     */
    public function openList(ThreadListRequest $request, Thread $thread)
    {
        return $this->_getList('open', $request, $thread);
    }

    /**
     * @param ThreadListRequest $request
     * @param Thread $thread
     * @return \Illuminate\Http\JsonResponse
     */
    public function doneList(ThreadListRequest $request, Thread $thread)
    {
        return $this->_getList('done', $request, $thread);
    }

    /**
     * @param ThreadListRequest $request
     * @param Thread $thread
     * @return \Illuminate\Http\JsonResponse
     */
    public function closedList(ThreadListRequest $request, Thread $thread)
    {
        return $this->_getList('closed', $request, $thread);
    }

    /**
     * @param ThreadListRequest $request
     * @param Thread $thread
     * @return \Illuminate\Http\JsonResponse
     */
    public function scheduledList(ThreadListRequest $request, Thread $thread)
    {
        return $this->_getList('scheduled', $request, $thread);
    }

    /**
     * @param ThreadListRequest $request
     * @param Thread $thread
     * @param ThreadsListTransformer $transformer
     * @param UsersThreadTransformer $usersThreadTransformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function userList(ThreadListRequest $request, Thread $thread, ThreadsListTransformer $transformer, UsersThreadTransformer $usersThreadTransformer)
    {
        $data = $thread->getUserList($request);

        if($request->with_pagination) {
            $transformer->chain($data->getCollection())
                ->transformer($usersThreadTransformer)
                ->endOfChain();
        } else {
            $data = $transformer->chain($data)
                ->transformer($usersThreadTransformer)
                ->endOfChain();
        }

        return $this->jsonResponse($data);
    }

    /**
     * @param \App\Http\Requests\Thread\ThreadUpdateRequest $request
     * @param \App\Models\Thread $thread
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateStatus(ThreadUpdateRequest $request, Thread $thread)
    {
        return $this->jsonResponse($thread->updateStatus($request));
    }

    /**
     * @param DeleteUsersFromDealerRequest $request
     * @param Thread $thread
     * @param ThreadsListTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function userDelete(
        DeleteUsersFromDealerRequest $request,
        Thread $thread,
        ThreadsListTransformer $transformer)
    {
        $data = $thread->addUsersDeleteThread($request);

        if ($data) {
            $this->_notify($data,$transformer);
        }

        return $this->jsonResponse($data);
    }

    /**
     * @param \App\Http\Requests\Thread\PublishRequest $request
     * @param \App\Models\Thread $thread
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function publishScheduled(PublishRequest $request, Thread $thread)
    {
        $data = $thread->publishScheduled($request);

        $this->_notify($data);

        return $this->jsonResponse($data);
    }

    /**
     * @param \App\Http\Requests\Thread\UpdateRequest $request
     * @param \App\Models\Thread $thread
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateScheduled(UpdateRequest $request, Thread $thread)
    {
        return $this->jsonResponse($thread->updateScheduled($request));
    }

    /**
     * @param \App\Http\Requests\Thread\PublishRequest $request
     * @param \App\Models\Thread $thread
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteScheduled(PublishRequest $request, Thread $thread)
    {
        return $this->jsonResponse($thread->deleteScheduled($request));
    }

    /**
     * @param \App\Http\Requests\Thread\ThreadDeleteRequest $request
     * @param \App\Models\Thread $thread
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(ThreadDeleteRequest $request, Thread $thread)
    {
        $data = $thread->deleteById($request->thread_id);
        $this->_deletionNotify($data['threads'], $data['recipients']);

        return $this->jsonResponse($data);
    }

    /**
     * @param $thread
     * @param string $notifyType
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function _notify($thread, string $notifyType = 'new_thread')
    {
        $this->notify([
            'type' => $notifyType,
            'channel_id' => $thread->channel_id,
            'item_id' => $thread->id,
            'author_id' => $thread->author_id,
        ]);
    }

    /**
     * @param \Illuminate\Support\Collection $threads
     * @param array $recipients
     */
    private function _deletionNotify(Collection $threads, array $recipients)
    {
        $this->notify([
            'type' => 'delete_thread',
            'channel_id' => $threads->first()->channel_id,
            'thread_id' => $threads->pluck('id')->all(),
            'recipients' => $recipients,
        ]);
    }

    /**
     * @param string $status
     * @param Request $request
     * @param Thread $thread
     * @return \Illuminate\Http\JsonResponse
     */
    private function _getList(string $status, Request $request, Thread $thread)
    {
        $data = $thread->getAdminList($request, $status);
        $data = new ThreadCollectionResource($data);

        return $this->jsonResponse($data);
    }
}
