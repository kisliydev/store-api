<?php

namespace App\Http\Controllers\Messages;

use App\Http\Controllers\Controller;
use App\Models\ThreadSendStatistic;
use App\Http\Requests\ThreadSendStatistics\LevelsListRequest;

class ThreadSendStatisticsController extends Controller
{
    /**
     * @param \App\Http\Requests\ThreadSendStatistics\LevelsListRequest $request
     * @param \App\Models\ThreadSendStatistic $stat
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLevelsList(LevelsListRequest $request, ThreadSendStatistic $stat)
    {
        return $this->jsonResponse($stat->getLevelsList($request));
    }
}
