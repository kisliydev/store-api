<?php

namespace App\Http\Controllers\Messages;

use App\Http\Requests\MessagesSettings\GetModeratorsRequest;
use App\Models\Dealer;
use App\Models\District;
use App\Models\League;
use App\Models\Position;
use App\Models\UserChannelStatus;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Services\MessageDataService;

class SettingsController extends Controller
{
    /**
     * @param \App\Http\Requests\MessagesSettings\GetModeratorsRequest $request
     * @param \App\Models\User $user
     * @param \App\Services\MessageDataService $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function showModerators(GetModeratorsRequest $request, User $user, MessageDataService $service)
    {
        return $this->jsonResponse($service->getMessagesModerators($request, $user));
    }

    /**
     * @param \App\Http\Requests\MessagesSettings\GetModeratorsRequest $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Services\MessageDataService $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function showDealers(GetModeratorsRequest $request, Dealer $dealer, MessageDataService $service)
    {
        return $this->jsonResponse($service->getMessagesPageItems($request, $dealer));
    }

    /**
     * @param \App\Http\Requests\MessagesSettings\GetModeratorsRequest $request
     * @param \App\Models\District $district
     * @param \App\Services\MessageDataService $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function showDistricts(GetModeratorsRequest $request, District $district, MessageDataService $service)
    {
        return $this->jsonResponse($service->getMessagesPageItems($request, $district));
    }

    /**
     * @param \App\Http\Requests\MessagesSettings\GetModeratorsRequest $request
     * @param \App\Models\League $league
     * @param \App\Services\MessageDataService $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function showLeagues(GetModeratorsRequest $request, League $league, MessageDataService $service)
    {
        return $this->jsonResponse($service->getMessagesPageItems($request, $league));
    }

    /**
     * @param \App\Http\Requests\MessagesSettings\GetModeratorsRequest $request
     * @param \App\Models\Position $position
     * @param \App\Services\MessageDataService $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function showPositions(GetModeratorsRequest $request, Position $position, MessageDataService $service)
    {
        return $this->jsonResponse($service->getMessagesPageItems($request, $position));
    }

    /**
     * @param \App\Http\Requests\MessagesSettings\GetModeratorsRequest $request
     * @param \App\Models\User $user
     * @param \App\Models\UserChannelStatus $userChannelStatus
     * @param \App\Services\MessageDataService $service
     * @return \Illuminate\Http\JsonResponse
     */
    public function showAppUsers(GetModeratorsRequest $request, User $user, UserChannelStatus $userChannelStatus, MessageDataService $service)
    {
        return $this->jsonResponse($service->getChannelAppUsers($request, $user, $userChannelStatus));
    }
}
