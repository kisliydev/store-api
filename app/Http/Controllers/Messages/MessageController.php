<?php

namespace App\Http\Controllers\Messages;

use App\Http\Requests\Message\ListRequest;
use App\Http\Requests\Message\StoreRequest;
use App\Http\Requests\Message\DeleteRequest;
use App\Models\Thread;
use App\Models\Message;
use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use App\Transformers\Messages\ThreadsListTransformer;
use App\Traits\SocketNotifications;
use Illuminate\Support\Facades\Log;

/**
 * Class MessageController
 *
 * @package App\Http\Controllers
 */
class MessageController extends Controller
{
    use SocketNotifications;

    /**
     * @param ListRequest $request
     * @param Message $message
     * @param ThreadsListTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(ListRequest $request, Message $message, ThreadsListTransformer $transformer)
    {
        $data = $message->getList($request);

        if($request->with_pagination) {
            $transformer->transform($data->getCollection());
        } else {
            $data = $transformer->transform($data);
        }

        return $this->jsonResponse($data);
    }

    /**
     * @param StoreRequest $request
     * @param Thread $thread
     * @param Message $message
     * @param ThreadsListTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function store(StoreRequest $request, Thread $thread, Message $message, ThreadsListTransformer $transformer)
    {
        /**
         * If the new message sent from within the mobile app
         * we need to check whether is a reply on circular thread (e.g. 'auto_level' or 'manual').
         * If so then we will need to create separate (individual).
         * Individually created thread is visible in the open threads list until it will be closed.
         * Individually created thread is never visible in closed threads list.
         */
        if ('user' == $request->from) {
            $request->merge(['thread_id' => $thread->manage($request)]);
        }
        $createMessage = $message->createEntity($request);
        if(!$createMessage) {
            Log::error('Failed to send message to the thread. Check your message settings.');
            return response()->json(['status' => 'error', 'message' => 'Check your message settings.'], 400);
        }

        $data = $transformer->transform($createMessage)->first();
        $this->_notify($data);

        return $this->jsonResponse($data);
    }

    public function destroy(DeleteRequest $request, Message $message)
    {
        $data = $message->deleteById($request->message_id);
        $this->_deletionNotify($data);

        return $this->jsonResponse($data);
    }

    /**
     * @param $message
     * @param string $notifyType
     */
    private function _notify($message, string $notifyType = 'new_message')
    {
        $this->notify([
            'type' => $notifyType,
            'channel_id' => $message->thread->channel_id,
            'item_id' => $message->id,
            'thread_id' => $message->thread_id,
        ]);
    }

    /**
     * @param \Illuminate\Support\Collection $messages
     */
    private function _deletionNotify(Collection $messages)
    {
        $this->notify([
            'type' => 'delete_message',
            'message_id' => $messages->pluck('id')->all(),
            'thread_id' => $messages->first()->thread_id,
            'channel_id' => $messages->first()->thread->channel_id,
        ]);
    }
}
