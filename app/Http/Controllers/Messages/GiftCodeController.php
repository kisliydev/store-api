<?php

namespace App\Http\Controllers\Messages;

use App\Http\Requests\GiftCodes\DestroyRequest;
use App\Http\Requests\GiftCodes\ShowRequest;
use App\Http\Requests\GiftCodes\StoreRequest;
use App\Models\GiftCode;
use App\Http\Controllers\Controller;

/**
 * Class MessageController
 *
 * @package App\Http\Controllers
 */
class GiftCodeController extends Controller
{
    /**
     * @param \App\Http\Requests\GiftCodes\ShowRequest $request
     * @param \App\Models\GiftCode $giftCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(ShowRequest $request, GiftCode $giftCode)
    {
        return $this->jsonResponse($giftCode->getList($request));
    }

    /**
     * @param \App\Http\Requests\GiftCodes\StoreRequest $request
     * @param \App\Models\GiftCode $giftCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request, GiftCode $giftCode)
    {
        return $this->jsonResponse($giftCode->createEntity($request));
    }

    /**
     * @param \App\Http\Requests\GiftCodes\DestroyRequest $request
     * @param \App\Models\GiftCode $giftCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DestroyRequest $request, GiftCode $giftCode)
    {
        return $this->jsonResponse($giftCode->destroyEntities($request));
    }
}
