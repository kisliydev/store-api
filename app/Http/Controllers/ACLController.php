<?php

namespace App\Http\Controllers;

use App\Models\ACL;
use App\Http\Requests\ACL\ACLUpdateRequest;
use App\Http\Requests\ACL\ACLGetRoleRequest;
use App\Http\Requests\ACL\ACLUpdateUserRequest;
use App\Http\Requests\ACL\ACLRestoreUserRequest;
use App\Http\Requests\ACL\ACLCHeckUserAccessRequest;
use App\Transformers\ACLTransformer;

class ACLController extends Controller
{
    /**
     * @param \App\Models\ACL $acl
     * @param \App\Transformers\ACLTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAll(ACL $acl, ACLTransformer $transformer)
    {
        return $this->jsonResponse($transformer->transform($acl->getAll()));
    }

    /**
     * @param \App\Http\Requests\ACL\ACLGetRoleRequest $request
     * @param \App\Models\ACL $acl
     * @param \App\Transformers\ACLTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(ACLGetRoleRequest $request, ACL $acl, ACLTransformer $transformer)
    {
        return $this->jsonResponse($transformer->transform($acl->getRole($request))->first());
    }

    /**
     * @param \App\Http\Requests\ACL\ACLUpdateRequest $request
     * @param \App\Models\ACL $acl
     * @param \App\Transformers\ACLTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ACLUpdateRequest $request, ACL $acl, ACLTransformer $transformer)
    {
        return $this->jsonResponse($transformer->transform($acl->resave($request)));
    }

    /**
     * @param \App\Models\ACL $acl
     * @param \App\Transformers\ACLTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function restore(ACL $acl, ACLTransformer $transformer)
    {
        return $this->jsonResponse($transformer->transform($acl->restore()));
    }

    /**
     * @param \App\Http\Requests\ACL\ACLUpdateUserRequest $request
     * @param \App\Models\ACL $acl
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUser(ACLUpdateUserRequest $request, ACL $acl)
    {
        return $this->jsonResponse($acl->updateUser($request));
    }

    /**
     * @param \App\Http\Requests\ACL\ACLRestoreUserRequest $request
     * @param \App\Models\ACL $acl
     * @return \Illuminate\Http\JsonResponse
     */
    public function restoreUser(ACLRestoreUserRequest $request, ACL $acl)
    {
        return $this->jsonResponse($acl->restoreUser($request));
    }

    /**
     * @param ACLCHeckUserAccessRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkAccess(ACLCHeckUserAccessRequest $request)
    {
        $callback = "hasAccessTo" . str_replace('_', '', ucwords($request->to, "_"));
        return $this->jsonResponse(auth()->user()->$callback($request->item_id));
    }
}
