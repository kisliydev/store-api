<?php

namespace App\Http\Controllers;

use App\Http\Requests\Media\ChannelItemsRequest;
use App\Http\Requests\Media\ChannelsListRequest;
use App\Http\Requests\Media\CountryBrandListRequest;
use App\Http\Requests\Media\DestroyRequest;
use App\Http\Requests\Media\MediaListRequest;
use App\Http\Requests\Media\StoreRequest;
use App\Http\Requests\Media\SuperAdminItemsRequest;
use App\Http\Requests\Media\MediaUpdateRequest;
use App\Http\Requests\Media\BulkActionRequest;
use App\Http\Requests\Media\ChannelItemsListRequest;
use App\Http\Requests\Media\GetRequest;
use App\Models\BrandUser;
use App\Models\Channel;
use App\Models\CountryBrand;
use App\Models\Media;

class MediaController extends Controller
{
    /**
     * @param \App\Http\Requests\Media\MediaListRequest $request
     * @param \App\Models\BrandUser $brandUser
     * @return \Illuminate\Http\JsonResponse
     */
    public function topLevelList(MediaListRequest $request, BrandUser $brandUser)
    {
        return $this->jsonResponse($brandUser->getListForMedia($request));
    }

    /**
     * @param \App\Http\Requests\Media\CountryBrandListRequest $request
     * @param \App\Models\CountryBrand $countryBrand
     * @return \Illuminate\Http\JsonResponse
     */
    public function countryBrandList(CountryBrandListRequest $request, CountryBrand $countryBrand)
    {
        return $this->jsonResponse($countryBrand->getListForMedia($request));
    }

    /**
     * @param \App\Http\Requests\Media\ChannelsListRequest $request
     * @param \App\Models\Channel $channel
     * @return \Illuminate\Http\JsonResponse
     */
    public function channelsList(ChannelsListRequest $request, Channel $channel)
    {
        return $this->jsonResponse($channel->getListForMedia($request));
    }

    /**
     * @param \App\Http\Requests\Media\ChannelItemsListRequest $request
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function channelsMediaList(ChannelItemsListRequest $request, Media $media)
    {
        return $this->jsonResponse($media->getChannelMediaFolders($request));
    }

    /**
     * @param \App\Http\Requests\Media\SuperAdminItemsRequest $request
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function superAdminItemsList(SuperAdminItemsRequest $request, Media $media)
    {
        return $this->jsonResponse($media->getList($request, false));
    }

    /**
     * @param \App\Http\Requests\Media\ChannelItemsRequest $request
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function channelItemsList(ChannelItemsRequest $request, Media $media)
    {
        return $this->jsonResponse($media->getList($request));
    }

    /**
     * @param \App\Http\Requests\Media\DestroyRequest $request
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DestroyRequest $request, Media $media)
    {
        return $this->jsonResponse($media->removeMedia($request->id));
    }

    /**
     * @param \App\Http\Requests\Media\StoreRequest $request
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request, Media $media)
    {
        return $this->jsonResponse($media->storeUploads($request));
    }

    /**
     * @param \App\Http\Requests\Media\MediaUpdateRequest $request
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(MediaUpdateRequest $request, Media $media)
    {
        return $this->jsonResponse($media->resave($request));
    }

    /**
     * @param \App\Http\Requests\Media\BulkActionRequest $request
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkAction(BulkActionRequest $request, Media $media)
    {
        return $this->jsonResponse($media->bulkAction($request));
    }

    /**
     * @param \App\Http\Requests\Media\GetRequest $request
     * @param \App\Models\Media $media
     * @return bool|mixed
     */
    public function download(GetRequest $request, Media $media)
    {
        $file     = $media->find($request->id);
        $content  = $media->getContent($file->path);
        $fileName = basename($file->path);

        if (! $content) {
            return $this->jsonResponse([
                'message' => "Can't get the file",
                'errors' => [
                    'id' => "Can't get the file by the given ID"
                ]
            ], 422);
        }

        return $this->exportFile(
            $fileName,
            file_get_contents($content['file']->path()),
            $content['file']->getClientMimeType());

    }
}
