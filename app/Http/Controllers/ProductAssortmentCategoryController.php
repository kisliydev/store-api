<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductAssortmentCategory\GetAllRequest;
use App\Http\Requests\ProductAssortmentCategory\GetForMobileRequest;
use App\Http\Requests\ProductAssortmentCategory\GetRequest;
use App\Http\Requests\ProductAssortmentCategory\SaveRequest;
use App\Http\Requests\ProductAssortmentCategory\UpdateRequest;
use App\Http\Requests\ProductAssortmentCategory\BulkActionRequest;
use App\Models\ProductAssortmentCategory;

class ProductAssortmentCategoryController extends Controller
{
    /**
     * @param \App\Http\Requests\ProductAssortmentCategory\GetAllRequest $request
     * @param \App\Models\ProductAssortmentCategory $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(GetAllRequest $request, ProductAssortmentCategory $category)
    {
        return $this->jsonResponse($category->getList($request));
    }

    /**
     * @param \App\Http\Requests\ProductAssortmentCategory\GetForMobileRequest $request
     * @param \App\Models\ProductAssortmentCategory $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNoPaginated(GetForMobileRequest $request, ProductAssortmentCategory $category)
    {
        return $this->jsonResponse($category->where('channel_id', $request->channel_id)->get(['id', 'name']));
    }

    /**
     * @param \App\Http\Requests\ProductAssortmentCategory\GetForMobileRequest $request
     * @param \App\Models\ProductAssortmentCategory $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function getForMobile(GetForMobileRequest $request, ProductAssortmentCategory $category)
    {
        return $this->jsonResponse($category->getForMobile($request));
    }

    /**
     * @param \App\Http\Requests\ProductAssortmentCategory\GetRequest $request
     * @param \App\Models\ProductAssortmentCategory $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(GetRequest $request, ProductAssortmentCategory $category)
    {
        return $this->jsonResponse($category->where('id', $request->id)->first());
    }

    /**
     * @param \App\Http\Requests\ProductAssortmentCategory\SaveRequest $request
     * @param \App\Models\ProductAssortmentCategory $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SaveRequest $request, ProductAssortmentCategory $category)
    {
        return $this->jsonResponse($category->create($request->validated()));
    }

    /**
     * @param \App\Http\Requests\ProductAssortmentCategory\UpdateRequest $request
     * @param \App\Models\ProductAssortmentCategory $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, ProductAssortmentCategory $category)
    {
        return $this->jsonResponse($category->resave($request));
    }

    /**
     * @param \App\Http\Requests\ProductAssortmentCategory\GetRequest $request
     * @param \App\Models\ProductAssortmentCategory $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(GetRequest $request, ProductAssortmentCategory $category)
    {
        return $this->jsonResponse($category->remove($request));
    }

    /**
     * @param \App\Http\Requests\ProductAssortmentCategory\BulkActionRequest $request
     * @param \App\Models\ProductAssortmentCategory $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkAction(BulkActionRequest $request, ProductAssortmentCategory $category)
    {
        return $this->jsonResponse($category->bulkAction($request));
    }
}
