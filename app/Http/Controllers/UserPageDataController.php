<?php

namespace App\Http\Controllers;

use App\Transformers\RolesListTransformer;

use App\Http\Requests\User\EditPage\GetCountryBrandsRequest;
use App\Http\Requests\User\EditPage\GetChannelsRequest;
use App\Http\Requests\User\EditPage\GetChannelItemsRequest;

use Spatie\Permission\Models\Role;
use App\Models\Brand;
use App\Models\CountryBrand;
use App\Models\Channel;

class UserPageDataController extends Controller
{
    /**
     * @param \Spatie\Permission\Models\Role $role
     * @param \App\Transformers\RolesListTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function roles(Role $role, RolesListTransformer $transformer)
    {
        return $this->jsonResponse($transformer->transform($this->_getNameAndId($role->where('name', '<>','hq'))));
    }

    /**
     * @param \App\Models\Brand $brand
     * @return \Illuminate\Http\JsonResponse
     */
    public function brands(Brand $brand)
    {
        return $this->jsonResponse($this->_getNameAndId($brand->query()));
    }

    /**
     * @param \App\Http\Requests\User\EditPage\GetCountryBrandsRequest $request
     * @param \App\Models\CountryBrand $brand
     * @return \Illuminate\Http\JsonResponse
     */
    public function countryBrands(GetCountryBrandsRequest $request, CountryBrand $brand)
    {
        return $this->jsonResponse($this->_getNameAndId($brand->where('brand_id', $request->brand_id)));
    }

    /**
     * @param \App\Http\Requests\User\EditPage\GetChannelsRequest $request
     * @param \App\Models\Channel $channel
     * @return \Illuminate\Http\JsonResponse
     */
    public function channels(GetChannelsRequest $request, Channel $channel)
    {
        return $this->jsonResponse($this->_getNameAndId($channel->where('country_brand_id', $request->country_brand_id)));
    }

    /**
     * @param \App\Http\Requests\User\EditPage\GetChannelItemsRequest $request
     * @param \App\Models\Channel $channel
     * @return \Illuminate\Http\JsonResponse
     */
    public function channelItems(GetChannelItemsRequest $request, Channel $channel)
    {
        $channel = $channel->where('id', $request->channel_id)->with([
            'dealers' => function ($query) {
                $this->_getNameAndId($query, true);
            },
            'positions' => function ($query) {
                $this->_getNameAndId($query, true);
            },
        ])->first();

        return $this->jsonResponse([
            'dealers' => $channel->dealers,
            'positions' => $channel->positions,
        ]);
    }

    /**
     * @param $query
     * @param bool $withChannelId
     * @return mixed
     */
    private function _getNameAndId($query, bool $withChannelId = false)
    {
        return $withChannelId ? $query->select('id', 'name', 'channel_id')->get() : $query->select('id', 'name')->get();
    }
}
