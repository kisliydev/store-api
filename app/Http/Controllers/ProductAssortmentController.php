<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductAssortment\GetAllRequest;
use App\Http\Requests\ProductAssortment\GetRequest;
use App\Http\Requests\ProductAssortment\LoadRequest;
use App\Http\Requests\ProductAssortment\ExportRequest;
use App\Http\Requests\ProductAssortment\SaveRequest;
use App\Http\Requests\ProductAssortment\UpdateRequest;
use App\Http\Requests\ProductAssortment\BulkActionRequest;
use App\Models\ProductAssortment;
use App\Models\Media;

class ProductAssortmentController extends Controller
{
    /**
     * @param \App\Http\Requests\ProductAssortment\GetAllRequest $request
     * @param \App\Models\ProductAssortment $assortment
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(GetAllRequest $request, ProductAssortment $assortment)
    {
        return $this->jsonResponse($assortment->getList($request));
    }

    /**
     * @param \App\Http\Requests\ProductAssortment\LoadRequest $request
     * @param \App\Models\ProductAssortment $assortment
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function load(LoadRequest $request, ProductAssortment $assortment, Media $media)
    {
        return $this->jsonResponse($assortment->loadFromExcel($request, $media));
    }

    /**
     * @param \App\Http\Requests\ProductAssortment\ExportRequest $request
     * @param \App\Models\ProductAssortment $assortment
     * @return mixed
     */
    public function export(ExportRequest $request, ProductAssortment $assortment)
    {
        return $this->exportFile(
            'product_assortment.xlsx',
            $assortment->export($request)
        );
    }

    /**
     * @param \App\Http\Requests\ProductAssortment\GetRequest $request
     * @param \App\Models\ProductAssortment $assortment
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(GetRequest $request, ProductAssortment $assortment)
    {
        return $this->jsonResponse($assortment->getItem($request));
    }

    /**
     * @param \App\Http\Requests\ProductAssortment\SaveRequest $request
     * @param \App\Models\ProductAssortment $assortment
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SaveRequest $request, ProductAssortment $assortment, Media $media)
    {
        return $this->jsonResponse($assortment->add($request, $media));
    }

    /**
     * @param \App\Http\Requests\ProductAssortment\UpdateRequest $request
     * @param \App\Models\ProductAssortment $assortment
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, ProductAssortment $assortment, Media $media)
    {
        return $this->jsonResponse($assortment->resave($request, $media));
    }

    /**
     * @param \App\Http\Requests\ProductAssortment\GetRequest $request
     * @param \App\Models\ProductAssortment $assortment
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(GetRequest $request, ProductAssortment $assortment, Media $media)
    {
        return $this->jsonResponse($assortment->remove($request, $media));
    }

    /**
     * @param \App\Http\Requests\ProductAssortment\BulkActionRequest $request
     * @param \App\Models\ProductAssortment $assortment
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkAction(BulkActionRequest $request, ProductAssortment $assortment, Media $media)
    {
        return $this->jsonResponse($assortment->bulkAction($request, $media));
    }
}
