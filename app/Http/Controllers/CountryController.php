<?php

namespace App\Http\Controllers;

use App\Http\Requests\Country\CountryLocalizationRequest;
use App\Models\Country;
use App\Models\Localization;

class CountryController extends Controller
{
    /**
     * @param \App\Models\Country $country
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Country $country)
    {
        return $this->jsonResponse($country->all());
    }

    /**
     * @param \App\Http\Requests\Country\CountryLocalizationRequest $request
     * @param \App\Models\Country $country
     * @param \App\Models\Localization $localization
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCountryLocalization(
        CountryLocalizationRequest $request,
        Country $country,
        Localization $localization
    ) {
        $countryLanguage = $country->getCountryLanguage($request->country_name);
        $localizationData = $localization->getLocalizationByLanguage($countryLanguage->name);
        if ($localizationData->isEmpty()) {
            $localizationData = $localization->getLocalizationByLanguage($localization::DEFAULT_LOCALIZATION_NAME);
        }

        return $this->jsonResponse($localizationData);
    }
}
