<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Artisan;

class SundriesController extends Controller
{
    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getServerTime()
    {
        return $this->jsonResponse([date('Y-m-d H:i:s')]);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function resetCache()
    {
        Artisan::call('cache:clear');

        return $this->jsonResponse(true);
    }
}
