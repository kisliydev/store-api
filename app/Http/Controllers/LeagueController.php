<?php

namespace App\Http\Controllers;

use App\Http\Requests\League\LeagueGetAllRequest;
use App\Http\Requests\League\LeagueGetRequest;
use App\Http\Requests\League\LeagueSaveRequest;
use App\Http\Requests\League\LeagueUpdateRequest;
use App\Http\Requests\League\LeagueBulkActionRequest;
use App\Models\League;
use App\Models\Media;

class LeagueController extends Controller
{
    /**
     * @param \App\Http\Requests\League\LeagueGetAllRequest $request
     * @param \App\Models\League $district
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(LeagueGetAllRequest $request, League $district)
    {
        return $this->jsonResponse($district->getList($request));
    }

    /**
     * @param \App\Http\Requests\League\LeagueGetRequest $request
     * @param \App\Models\League $district
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(LeagueGetRequest $request, League $district)
    {
        return $this->jsonResponse($district->get($request));
    }

    /**
     * @param LeagueSaveRequest $request
     * @param League $district
     * @param Media $media
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(LeagueSaveRequest $request, League $district, Media $media)
    {
        return $this->jsonResponse($district->add($request, $media));
    }

    /**
     * @param \App\Http\Requests\League\LeagueUpdateRequest $request
     * @param \App\Models\League $district
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(LeagueUpdateRequest $request, League $district, Media $media)
    {
        return $this->jsonResponse($district->resave($request, $media));
    }

    /**
     * @param \App\Http\Requests\League\LeagueGetRequest $request
     * @param \App\Models\League $district
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(LeagueGetRequest $request, League $district, Media $media)
    {
        return $this->jsonResponse($district->remove($request, $media));
    }

    /**
     * @param \App\Http\Requests\League\LeagueBulkActionRequest $request
     * @param \App\Models\League $district
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkAction(LeagueBulkActionRequest $request, League $district, Media $media)
    {
        return $this->jsonResponse($district->bulkAction($request, $media));
    }
}
