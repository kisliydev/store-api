<?php

namespace App\Http\Controllers;

use App\Models\Position;
use App\Http\Requests\Position\PositionGetAllRequest;
use App\Http\Requests\Position\PositionGetRequest;
use App\Http\Requests\Position\PositionSaveRequest;
use App\Http\Requests\Position\PositionUpdateRequest;
use App\Http\Requests\Position\PositionBulkActionRequest;

/**
 * Class PositionController
 *
 * @package App\Http\Controllers
 */
class PositionController extends Controller
{
    /**
     * @param \App\Http\Requests\Position\PositionGetAllRequest $request
     * @param \App\Models\Position $position
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(PositionGetAllRequest $request, Position $position)
    {
        return $this->jsonResponse($position->getList($request));
    }

    /**
     * @param \App\Http\Requests\Position\PositionGetRequest $request
     * @param \App\Models\Position $position
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(PositionGetRequest $request, Position $position)
    {
        return $this->jsonResponse($position->get($request));
    }

    /**
     * @param \App\Http\Requests\Position\PositionSaveRequest $request
     * @param \App\Models\Position $position
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PositionSaveRequest $request, Position $position)
    {
        return $this->jsonResponse($position->add($request));
    }

    /**
     * @param \App\Http\Requests\Position\PositionUpdateRequest $request
     * @param \App\Models\Position $position
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PositionUpdateRequest $request, Position $position)
    {
        return $this->jsonResponse($position->resave($request));
    }

    /**
     * @param \App\Http\Requests\Position\PositionGetRequest $request
     * @param \App\Models\Position $position
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(PositionGetRequest $request, Position $position)
    {
        return $this->jsonResponse($position->remove($request));
    }

    /**
     * @param \App\Http\Requests\Position\PositionBulkActionRequest $request
     * @param \App\Models\Position $position
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkAction(PositionBulkActionRequest $request, Position $position)
    {
        return $this->jsonResponse($position->bulkAction($request));
    }
}
