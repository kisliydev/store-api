<?php

namespace App\Http\Controllers;

use App\Http\Requests\Channel\ChannelGetByCountryBrandRequest;
use App\Http\Requests\Channel\ChannelGetRequest;
use App\Http\Requests\Channel\ChannelSaveRequest;
use App\Http\Requests\Channel\ChannelUpdateRequest;
use App\Http\Requests\Channel\ChannelGetMetaRequest;
use App\Http\Requests\Channel\ChannelSaveMetaRequest;
use App\Http\Requests\Channel\ChannelLoadLocalizationRequest;
use App\Http\Requests\Channel\CheckInviteCodeRequest;
use App\Models\Campaign;
use App\Transformers\ChannelListTransformer;
use App\Transformers\AppLocalizationTransformer;
use App\Transformers\AppLocalizationListTransformer;
use App\Transformers\AppLayoutTransformer;
use App\Models\Channel;
use App\Models\User;
use App\Http\Resources\Channel as ChannelResource;
use Illuminate\Support\Facades\DB;

/**
 * Class ChannelController
 *
 * @package App\Http\Controllers
 */
class ChannelController extends Controller
{
    /**
     * @param \App\Http\Requests\Channel\ChannelGetByCountryBrandRequest $request
     * @param \App\Transformers\ChannelListTransformer $transformer
     * @param \App\Models\Channel $channel
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ChannelGetByCountryBrandRequest $request, ChannelListTransformer $transformer, Channel $channel)
    {
        $data = $channel->getList($request);
        $transformer->transform($data->getCollection());
        return $this->jsonResponse($data);
    }

    /**
     * @param ChannelGetRequest $request
     * @param Channel $channel
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(ChannelGetRequest $request, Channel $channel)
    {
        return $this->jsonResponse(new ChannelResource($channel->getItem($request)));
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelGetMetaRequest $request
     * @param \App\Models\Channel $channel
     * @return \Illuminate\Http\JsonResponse
     */
    public function showMeta(ChannelGetMetaRequest $request, Channel $channel)
    {
        $data = $channel->where('id', $request->id)->get()->makeVisible([$request->key]);
        $key = $request->key;
        return $this->jsonResponse([$key => $data->first()->$key]);
    }

    /**
     * @param ChannelGetRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function showModerator(ChannelGetRequest $request, Channel $channel)
    {
        $userID = $channel->select(
                DB::raw('json_extract(messages_settings, "$.reply_from") as reply_from_id')
            )->where('id', $request->id)
            ->first()->reply_from_id ?? null;

        $result = $userID ? $channel->removeAppends(User::select('id', 'first_name', 'last_name')
            ->where('id', $userID)->get(), ['full_name'])->first() : null;

        return $this->jsonResponse($result);
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelGetRequest $request
     * @param \App\Models\Channel $channel
     * @param \App\Transformers\AppLocalizationTransformer $appLocalizationTransformer
     * @param \App\Transformers\AppLayoutTransformer $appLayoutTransformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function showMetaMobile(
        ChannelGetRequest $request,
        Channel $channel,
        AppLocalizationTransformer $appLocalizationTransformer,
        AppLayoutTransformer $appLayoutTransformer
    ) {
        $channelCollection  = $channel->where('id', $request->id)->get()->makeVisible(['app_localization', 'app_settings']);
        $currentChannel     = $channelCollection->first();
        $campaignCollection = $currentChannel->currentCampaign()->get()->makeVisible(['app_layout']);

        return $this->jsonResponse(array_merge(
            ['app_settings' => $currentChannel->app_settings],
            $appLocalizationTransformer->transform($channelCollection)->first(),
            $appLayoutTransformer->transform($campaignCollection)->first()
        ));
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelSaveRequest $request
     * @param \App\Models\Channel $channel
     * @param \App\Models\User $user
     * @param \App\Models\Campaign $campaign
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(ChannelSaveRequest $request, Channel $channel, User $user, Campaign $campaign)
    {
        return $this->jsonResponse($channel->add($request, $user, $campaign));
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelSaveMetaRequest $request
     * @param \App\Models\Channel $channel
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveMeta(ChannelSaveMetaRequest $request, Channel $channel)
    {
        return $this->jsonResponse($channel->saveMeta($request));
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelGetRequest $request
     * @param \App\Models\Channel $channel
     * @param \App\Transformers\AppLocalizationListTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function getLocalization(ChannelGetRequest $request, Channel $channel, AppLocalizationListTransformer $transformer)
    {
        $channelData   = $channel->where('id', $request->id)->first()->makeVisible(['app_localization']);
        $locallization = $transformer->transform(collect($channelData->app_localization));

        return $this->jsonResponse(['app_localization' => $locallization]);
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelLoadLocalizationRequest $request
     * @param \App\Models\Channel $channel
     * @return \Illuminate\Http\JsonResponse
     */
    public function loadLocalization(ChannelLoadLocalizationRequest $request, Channel $channel)
    {
        return $this->jsonResponse($channel->loadLocalization($request));
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelGetRequest $request
     * @param \App\Models\Channel $channel
     * @return string
     */
    public function exportLocalization(ChannelGetRequest $request, Channel $channel)
    {
        return $this->exportFile(
            'channel_localization.xlsx',
            $channel->exportLocalization($request)
        );
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelGetRequest $request
     * @param \App\Models\Channel $channel
     * @return \Illuminate\Http\JsonResponse
     */
    public function copy(ChannelGetRequest $request, Channel $channel)
    {
        return $this->jsonResponse($channel->duplicate($request));
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelUpdateRequest $request
     * @param \App\Models\Channel $channel
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ChannelUpdateRequest $request, Channel $channel)
    {
        return $this->jsonResponse($channel->resave($request));
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelGetRequest $request
     * @param \App\Models\Channel $channel
     * @return \Illuminate\Http\JsonResponse
     */
    public function trash(ChannelGetRequest $request, Channel $channel)
    {
        return $this->jsonResponse($channel->trash($request));
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelGetRequest $request
     * @param \App\Models\Channel $channel
     * @return \Illuminate\Http\JsonResponse
     */
    public function restore(ChannelGetRequest $request, Channel $channel)
    {
        return $this->jsonResponse($channel->restoreFromTrash($request));
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelGetRequest $request
     * @param \App\Models\Channel $channel
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(ChannelGetRequest $request, Channel $channel)
    {
        return $this->jsonResponse($channel->remove($request));
    }

    /**
     * @param \App\Models\Channel $channel
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInviteCode(Channel $channel)
    {
        return $this->jsonResponse($channel->getInviteCode());
    }

    /**
     * @param \App\Http\Requests\Channel\CheckInviteCodeRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkInviteCode(CheckInviteCodeRequest $request)
    {
        return $this->jsonResponse(true);
    }
}
