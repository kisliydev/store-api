<?php

namespace App\Http\Controllers;

use App\Http\Requests\Product\ProductGetAllRequest;
use App\Http\Requests\Product\ProductGetRequest;
use App\Http\Requests\Product\ProductLoadRequest;
use App\Http\Requests\Product\ProductSaveRequest;
use App\Http\Requests\Product\ProductUpdatePositionRequest;
use App\Http\Requests\Product\ProductUpdateRequest;
use App\Http\Requests\Product\ProductBulkActionRequest;
use App\Models\Product;

class ProductController extends Controller
{
    /**
     * @param \App\Http\Requests\Product\ProductGetAllRequest $request
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ProductGetAllRequest $request, Product $product)
    {
        return $this->jsonResponse($product->getList($request));
    }

    /**
     * @param \App\Http\Requests\Product\ProductGetAllRequest $request
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function getNoPaginated(ProductGetAllRequest $request, Product $product)
    {
        return $this->jsonResponse($product->where('channel_id', $request->channel_id)->get(['id', 'name']));
    }

    /**
     * @param \App\Http\Requests\Product\ProductGetAllRequest $request
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function showForMobile(ProductGetAllRequest $request, Product $product)
    {
        return $this->jsonResponse(
            $product->where('channel_id', $request->channel_id)
            ->orderBy('position')
            ->with('thumbnail')
            ->get()
        );
    }

    /**
     * @param \App\Http\Requests\Product\ProductGetRequest $request
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(ProductGetRequest $request, Product $product)
    {
        return $this->jsonResponse($product->get($request));
    }

    /**
     * @param \App\Http\Requests\Product\ProductSaveRequest $request
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ProductSaveRequest $request, Product $product)
    {
        return $this->jsonResponse($product->add($request));
    }

    /**
     * @param \App\Http\Requests\Product\ProductUpdateRequest $request
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ProductUpdateRequest $request, Product $product)
    {
        return $this->jsonResponse($product->resave($request));
    }

    /**
     * @param \App\Http\Requests\Product\ProductGetRequest $request
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(ProductGetRequest $request, Product $product)
    {
        return $this->jsonResponse($product->remove($request));
    }

    /**
     * @param \App\Http\Requests\Product\ProductBulkActionRequest $request
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkAction(ProductBulkActionRequest $request, Product $product)
    {
        return $this->jsonResponse($product->bulkAction($request));
    }

    /**
     * @param \App\Http\Requests\Product\ProductUpdatePositionRequest $request
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePositions(ProductUpdatePositionRequest $request, Product $product)
    {
        $product->updateProductPosition($request);

        return $this->jsonResponse(['message' => 'OK']);
    }

    /**
     * @param \App\Http\Requests\Product\ProductLoadRequest $request
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function load(ProductLoadRequest $request, Product $product)
    {
        return $this->jsonResponse($product->excelUpload($request));
    }
}
