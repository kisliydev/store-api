<?php
namespace App\Http\Controllers;

use App\Http\Requests\Localization\LocalizationGetRequest;
use App\Http\Requests\Localization\LocalizationSaveRequest;
use App\Http\Requests\Localization\LocalizationUpdateRequest;
use App\Http\Requests\Localization\LocalizationAddToChannelRequest;
use App\Http\Requests\Localization\LocalizationGetAllRequest;

use App\Models\Channel;
use App\Models\Localization;
use App\Transformers\AppLocalizationListTransformer;

class LocalizationController extends Controller
{
    /**
     * @param \App\Http\Requests\Localization\LocalizationGetAllRequest $request
     * @param \App\Models\Localization $localization
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(LocalizationGetAllRequest $request, Localization $localization)
    {
        return $this->jsonResponse($localization->getList($request));
    }

    /**
     * @param \App\Models\Localization $localization
     * @return \Illuminate\Http\JsonResponse
     */
    public function getForChannel(Localization $localization)
    {
        return $this->jsonResponse($localization->get(['id', 'name']));
    }

    /**
     * @param \App\Http\Requests\Localization\LocalizationGetRequest $request
     * @param \App\Models\Localization $localization
     * @param \App\Transformers\AppLocalizationListTransformer $localizationListTransformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(
        LocalizationGetRequest $request,
        Localization $localization,
        AppLocalizationListTransformer $localizationListTransformer
    ) {
        $translation = $localization->find($request->id);
        $translation->translations = $localizationListTransformer->transform(collect($translation->translations));
        return $this->jsonResponse($translation);
    }

    /**
     * @param \App\Http\Requests\Localization\LocalizationSaveRequest $request
     * @param \App\Models\Localization $localization
     * @param \App\Models\Channel $channel
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(LocalizationSaveRequest $request, Localization $localization, Channel $channel)
    {
        return $this->jsonResponse($localization->add($request, $channel));
    }

    /**
     * @param \App\Http\Requests\Localization\LocalizationUpdateRequest $request
     * @param \App\Models\Localization $localization
     * @param \App\Models\Channel $channel
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(LocalizationUpdateRequest $request, Localization $localization, Channel $channel)
    {
        return $this->jsonResponse($localization->resave($request, $channel));
    }


    /**
     * @param \App\Http\Requests\Localization\LocalizationGetRequest $request
     * @param \App\Models\Localization $localization
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(LocalizationGetRequest $request, Localization $localization)
    {
        return $this->jsonResponse($localization->remove($request));
    }

    /**
     * @param \App\Http\Requests\Localization\LocalizationAddToChannelRequest $request
     * @param \App\Models\Localization $localization
     * @param \App\Models\Channel $channel
     * @return \Illuminate\Http\JsonResponse
     */
    public function addToChannel(LocalizationAddToChannelRequest $request, Localization $localization, Channel $channel)
    {
        return $this->jsonResponse($localization->addToChannel($request, $channel));
    }
}
