<?php

namespace App\Http\Controllers\ChannelStatistics;

use App\Http\Controllers\Controller;
use App\Models\AppStatistic;
use App\Models\PointsStatistic;
use App\Http\Requests\Sales\GetUserSalesRequest;
use App\Http\Requests\Sales\UpdateSaleRequest;
use App\Transformers\SalesListTransformer;

class SalesStatisticsController extends Controller
{
    /**
     * @param \App\Http\Requests\Sales\GetUserSalesRequest $request
     * @param \App\Models\AppStatistic $statistic
     * @param \App\Transformers\SalesListTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(GetUserSalesRequest $request, AppStatistic $statistic, SalesListTransformer $transformer)
    {
        return $this->jsonResponse($transformer->transform($statistic->getByUserChannel($request)));
    }

    /**
     * @param \App\Http\Requests\Sales\UpdateSaleRequest $request
     * @param \App\Models\PointsStatistic $statistic
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProductsSale(UpdateSaleRequest $request, PointsStatistic $statistic)
    {
        return $this->jsonResponse($statistic->updateProductsSale($request));
    }
}
