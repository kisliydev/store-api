<?php

namespace App\Http\Controllers\ChannelStatistics;

use App\Http\Controllers\Controller;
use App\Http\Requests\Campaign\CampaignExportRequest;
use App\Services\Statistics\CampaignStatisticsService;
use App\Models\Campaign;

class CampaignStatisticsController extends Controller
{
    /**
     * @param \App\Http\Requests\Campaign\CampaignExportRequest $request
     * @param \App\Services\Statistics\CampaignStatisticsService $service
     * @param \App\Models\Campaign $campaign
     * @return mixed
     */
    public function export(CampaignExportRequest $request, CampaignStatisticsService $service, Campaign $campaign)
    {
        return $this->exportFile(
        'campaign_statistics.xlsx',
            $service->exportStatistics($request, $campaign)
        );
    }
}
