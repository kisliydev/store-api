<?php

namespace App\Http\Controllers\ChannelStatistics;

use App\Http\Controllers\Controller;

use App\Http\Requests\Request;
use App\Http\Requests\ChannelStatistics\UsersStatisticsGetRequest;
use App\Http\Requests\ChannelStatistics\UsersExportStatisticsRequest;
use App\Http\Requests\ChannelStatistics\UserStatisticsGetMobileRequest;

use App\Transformers\UserStatisticsTransformer;
use App\Transformers\ChannelStatisticsTypeTransformer;

use App\Models\User;
use App\Models\PointsStatistic;
use App\Models\Product;

use App\Services\Statistics\UserStatisticsService;

class UserStatisticsController extends Controller
{
    /**
     * @param \App\Services\Statistics\UserStatisticsService $service
     * @param \App\Http\Requests\ChannelStatistics\UsersStatisticsGetRequest $request
     * @param \App\Transformers\UserStatisticsTransformer $transformer
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsersStatistics(
        UserStatisticsService $service,
        UsersStatisticsGetRequest $request,
        UserStatisticsTransformer $transformer,
        User $user
    ) {
        if (! $request->sort) {
            $request->merge(['sort' => '-rank']);
        }

        $data = $service->getUsersStatistics($request, $user);

        $transformer->transform($data->getCollection());

        return $this->jsonResponse($data);
    }

    /**
     * @deprecated
     * @aee todo in routes/api.php for the endpoint GET '/users/channel'
     *
     * @param \App\Services\Statistics\UserStatisticsService $service
     * @param \App\Http\Requests\ChannelStatistics\UsersStatisticsGetRequest $request
     * @param \App\Transformers\UserStatisticsTransformer $transformer
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsersListFroUnits(
        UserStatisticsService $service,
        UsersStatisticsGetRequest $request,
        UserStatisticsTransformer $transformer,
        User $user
    ) {
        $data = $service->getUsersStatistics($request, $user);

        $transformer->transform($data->getCollection());

        return $this->jsonResponse($data);
    }

    /**
     * @param \App\Services\Statistics\UserStatisticsService $service
     * @param \App\Http\Requests\ChannelStatistics\UsersExportStatisticsRequest $request
     * @param \App\Models\User $user
     * @return mixed
     */
    public function exportUsers(UserStatisticsService $service, UsersExportStatisticsRequest $request, User $user)
    {
        return $this->exportFile(
            'user_statistics.xlsx',
            $service->exportUsers($request, $user)
        );
    }

    /**
     * @param \App\Services\Statistics\UserStatisticsService $service
     * @param \App\Http\Requests\ChannelStatistics\UsersExportStatisticsRequest $request
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @param \App\Models\User $user
     * @param \App\Models\Product $product
     * @param \App\Transformers\ChannelStatisticsTypeTransformer $transformer
     * @return mixed
     */
    public function exportUsersDetailed(
        UserStatisticsService $service,
        UsersExportStatisticsRequest $request,
        PointsStatistic $pointsStatistic,
        User $user,
        Product $product,
        ChannelStatisticsTypeTransformer $transformer
    ) {
        return $this->exportFile(
            'user_detailed_statistics.xlsx',
            $service->exportUsersDetailed($request, $user, $product, $pointsStatistic, $transformer)
        );
    }

    /**
     * @param \App\Services\Statistics\UserStatisticsService $service
     * @param \App\Http\Requests\ChannelStatistics\UsersStatisticsGetRequest $request
     * @param \App\Transformers\UserStatisticsTransformer $transformer
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsersMobileStatistics(
        UserStatisticsService $service,
        UsersStatisticsGetRequest $request,
        UserStatisticsTransformer $transformer,
        User $user
    ) {
        return $this->jsonResponse($transformer->transform($service->getUsersMobileStatistics($request, $user)));
    }

    /**
     * @param \App\Services\Statistics\UserStatisticsService $service
     * @param \App\Http\Requests\ChannelStatistics\UserStatisticsGetMobileRequest $request
     * @param \App\Models\User $user
     * @param \App\Transformers\UserStatisticsTransformer $userStatisticsTransformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserMobileStatistics(
        UserStatisticsService $service,
        UserStatisticsGetMobileRequest $request,
        User $user,
        UserStatisticsTransformer $userStatisticsTransformer
    ) {
        $this->_addFilterByUser($request);

        $currentlyViewedUser = $request->user_id == auth()->user()->id ? auth()->user() : $user->find($request->user_id);

        if ('app_user' == $currentlyViewedUser->role) {

            $data = $userStatisticsTransformer->transform($service->getUserMobileStatistics($request, $currentlyViewedUser));

            return $this->jsonResponse(collect($data->first()));
        }

        return $this->jsonResponse($service->getAdminStatistics($request, $user)->first());
    }

    /**
     * @param \App\Services\Statistics\UserStatisticsService $service
     * @param \App\Http\Requests\ChannelStatistics\UserStatisticsGetMobileRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserDetailedMobileStatistics(
        UserStatisticsService $service,
        UserStatisticsGetMobileRequest $request
    ) {
        return $this->jsonResponse($service->getUserDetailedMobileStatistics($request));
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @return void
     */
    private function _addFilterByUser(Request &$request)
    {
        /* Manually add filter by user */
        $this->_maybeAddUser($request);
        $request->merge(['filter' => 'id:' . $request->user_id]);
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @return void
     */
    private function _maybeAddUser(Request &$request)
    {
        if (empty($request->user_id)) {
            $request->merge(['user_id' => auth()->user()->id]);
        }
    }
}
