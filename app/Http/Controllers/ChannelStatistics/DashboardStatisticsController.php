<?php

namespace App\Http\Controllers\ChannelStatistics;

use App\Models\User;
use App\Services\Statistics\DashboardStatisticsService;

use App\Http\Controllers\Controller;
use App\Http\Requests\Dashboard\DataGetRequest;
use App\Http\Requests\Dashboard\ChannelDataGetRequest;

use App\Models\PointsStatistic;
use App\Models\Dealer;
use App\Models\UserChannelStatus;

use App\Transformers\DashboardChartTransformer;
use App\Transformers\DashboardItemsStatisticsTransformer;
use App\Transformers\UserStatisticsTransformer;
use App\Transformers\DealerStatisticsTransformer;

class DashboardStatisticsController extends Controller
{
    /**
     * @param \App\Services\Statistics\DashboardStatisticsService $service
     * @param \App\Http\Requests\Dashboard\DataGetRequest $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\UserChannelStatus $userChannelStatus
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function getGeneral(
        DashboardStatisticsService $service,
        DataGetRequest $request,
        Dealer $dealer,
        UserChannelStatus $userChannelStatus,
        User $user
    ) {
        return $this->jsonResponse($service->getGeneral($request, $dealer, $userChannelStatus, $user));
    }

    /**
     * @param \App\Services\Statistics\DashboardStatisticsService $service
     * @param \App\Http\Requests\Dashboard\DataGetRequest $request
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @param \App\Transformers\DashboardChartTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function getChart(
        DashboardStatisticsService $service,
        DataGetRequest $request,
        PointsStatistic $pointsStatistic,
        DashboardChartTransformer $transformer
    ) {
        return $this->jsonResponse($service->getChart($request, $pointsStatistic, $transformer));
    }

    /**
     * @param \App\Services\Statistics\DashboardStatisticsService $service
     * @param \App\Http\Requests\Dashboard\DataGetRequest $request
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTotals(
        DashboardStatisticsService $service,
        DataGetRequest $request,
        PointsStatistic $pointsStatistic
    ) {
        return $this->jsonResponse($service->getTotals($request, $pointsStatistic));
    }

    /**
     * @param \App\Services\Statistics\DashboardStatisticsService $service
     * @param \App\Http\Requests\Dashboard\DataGetRequest $request
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProducts(
        DashboardStatisticsService $service,
        DataGetRequest $request,
        PointsStatistic $pointsStatistic
    ) {
        return $this->jsonResponse($service->getProducts($request, $pointsStatistic));
    }

    /**
     * @param \App\Services\Statistics\DashboardStatisticsService $service
     * @param \App\Http\Requests\Dashboard\DataGetRequest $request
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @param \App\Transformers\DashboardItemsStatisticsTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function getQuizzes(DashboardStatisticsService $service, DataGetRequest $request, PointsStatistic $pointsStatistic, DashboardItemsStatisticsTransformer $transformer)
    {
        return $this->jsonResponse($transformer->transform(
            $service->getAcademyItems($request, $pointsStatistic, 'quiz')
        ));
    }

    /**
     * @param \App\Services\Statistics\DashboardStatisticsService $service
     * @param \App\Http\Requests\Dashboard\DataGetRequest $request
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @param \App\Transformers\DashboardItemsStatisticsTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSurveys(DashboardStatisticsService $service, DataGetRequest $request, PointsStatistic $pointsStatistic, DashboardItemsStatisticsTransformer $transformer)
    {
        return $this->jsonResponse($transformer->transform(
            $service->getAcademyItems($request, $pointsStatistic, 'survey')
        ));
    }

    /**
     * @param \App\Services\Statistics\DashboardStatisticsService $service
     * @param \App\Http\Requests\Dashboard\ChannelDataGetRequest $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @param \App\Transformers\DealerStatisticsTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDealers(
        DashboardStatisticsService $service,
        ChannelDataGetRequest $request,
        Dealer $dealer,
        PointsStatistic $pointsStatistic,
        DealerStatisticsTransformer $transformer
    ) {
        return $this->jsonResponse($transformer->transform(
            $service->getDealers($request, $dealer, $pointsStatistic)
        ));
    }

    /**
     * @param \App\Services\Statistics\DashboardStatisticsService $service
     * @param \App\Http\Requests\Dashboard\ChannelDataGetRequest $request
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @param \App\Transformers\UserStatisticsTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUsers(DashboardStatisticsService $service, ChannelDataGetRequest $request, PointsStatistic $pointsStatistic, UserStatisticsTransformer $transformer)
    {
        return $this->jsonResponse($transformer->transform(
            $service->getUsers($request, $pointsStatistic)
        ));
    }
}
