<?php

namespace App\Http\Controllers\ChannelStatistics;

use App\Http\Controllers\Controller;

use App\Http\Requests\Request;
use App\Http\Requests\ChannelStatistics\DealerStatisticsGetRequest;
use App\Http\Requests\ChannelStatistics\DealerStatisticsGetMobileRequest;
use App\Http\Requests\ChannelStatistics\DealersExportStatisticsRequest;
use App\Http\Requests\ChannelStatistics\DealerStatisticsGetListMobileRequest;

use App\Services\Statistics\DealerStatisticsService;

use App\Transformers\ChannelStatisticsTypeTransformer;
use App\Transformers\DealerStatisticsTransformer;
use App\Transformers\UserStatisticsTransformer;

use App\Models\PointsStatistic;
use App\Models\Dealer;
use App\Models\Product;

class DealerStatisticsController extends Controller
{
    /**
     * @param \App\Services\Statistics\DealerStatisticsService $service
     * @param \App\Http\Requests\ChannelStatistics\DealerStatisticsGetRequest $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @param \App\Transformers\DealerStatisticsTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDealersStatistics(DealerStatisticsService $service, DealerStatisticsGetRequest $request, Dealer $dealer, PointsStatistic $pointsStatistic, DealerStatisticsTransformer $transformer)
    {
        if (! $request->sort) {
            $request->merge(['sort' => '-rank']);
        }

        $data = $service->getDealersStatistics($request, $dealer, $pointsStatistic);

        $transformer->transform($data->getCollection());

        return $this->jsonResponse($data);

    }

    public function getDealersListFroUnits(DealerStatisticsService $service, DealerStatisticsGetRequest $request, Dealer $dealer, PointsStatistic $pointsStatistic, DealerStatisticsTransformer $transformer)
    {
        $data = $service->getDealersStatistics($request, $dealer, $pointsStatistic);

        $transformer->transform($data->getCollection());

        return $this->jsonResponse($data);

    }

    /**
     * @param \App\Services\Statistics\DealerStatisticsService $service
     * @param \App\Http\Requests\ChannelStatistics\DealerStatisticsGetListMobileRequest $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @param \App\Transformers\DealerStatisticsTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDealersMobileStatistics(
        DealerStatisticsService $service,
        DealerStatisticsGetListMobileRequest $request,
        Dealer $dealer,
        PointsStatistic $pointsStatistic,
        DealerStatisticsTransformer $transformer
    ) {
        return $this->jsonResponse($transformer->transform($service->getDealersMobileStatistics($request, $dealer, $pointsStatistic)));
    }

    /**
     * @param \App\Services\Statistics\DealerStatisticsService $service
     * @param \App\Http\Requests\ChannelStatistics\DealersExportStatisticsRequest $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @param \App\Models\Product $product
     * @param \App\Transformers\ChannelStatisticsTypeTransformer $transformer
     * @return mixed
     */
    public function exportDealersStatistics(DealerStatisticsService $service, DealersExportStatisticsRequest $request, Dealer $dealer, PointsStatistic $pointsStatistic, Product $product, ChannelStatisticsTypeTransformer $transformer)
    {
        return $this->exportFile(
            'dealer_statistics.xlsx',
            $service->exportDealersStatistics($request, $dealer, $pointsStatistic, $product, $transformer)
        );
    }

    /**
     * @param \App\Services\Statistics\DealerStatisticsService $service
     * @param \App\Http\Requests\ChannelStatistics\DealerStatisticsGetMobileRequest $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @param \App\Transformers\DealerStatisticsTransformer $dealerStatisticsTransformer
     * @param \App\Transformers\UserStatisticsTransformer $userStatisticsTransformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDealerMobileStatistics(
        DealerStatisticsService $service,
        DealerStatisticsGetMobileRequest $request,
        Dealer $dealer,
        PointsStatistic $pointsStatistic,
        DealerStatisticsTransformer $dealerStatisticsTransformer,
        UserStatisticsTransformer $userStatisticsTransformer
    ) {
        if (! $this->_addFilterByDealer($request)) {
            return $this->jsonResponse(false);
        }

        $data = $dealerStatisticsTransformer->transform($service->getDealerMobileStatistics($request, $dealer, $pointsStatistic, $userStatisticsTransformer));

        return $this->jsonResponse(collect($data->first()));
    }

    /**
     * @param \App\Services\Statistics\DealerStatisticsService $service
     * @param \App\Http\Requests\ChannelStatistics\DealerStatisticsGetMobileRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDealerDetailedMobileStatistics(
        DealerStatisticsService $service,
        DealerStatisticsGetMobileRequest $request
    ) {
        if (! $this->_addDealer($request)) {
            return $this->jsonResponse(false);
        }

        return $this->jsonResponse($service->getDealerDetailedMobileStatistics($request));
    }

    /**
     * @param \App\Services\Statistics\DealerStatisticsService $service
     * @param \App\Http\Requests\ChannelStatistics\DealerStatisticsGetMobileRequest $request
     * @param \App\Transformers\UserStatisticsTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDealerUsersMobileStatistics(
        DealerStatisticsService $service,
        DealerStatisticsGetMobileRequest $request,
        UserStatisticsTransformer $transformer
    ) {
        if (! $this->_addDealer($request)) {
            return $this->jsonResponse(false);
        }

        return $this->jsonResponse($transformer->transform($service->getDealerUsersMobileStatistics($request)));
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @return bool
     */
    private function _addFilterByDealer(Request &$request)
    {
        /* Manually add filter by dealer */
        if (! $this->_addDealer($request)) {
           return false;
        }

        $request->merge(['filter' => 'id:' . $request->dealer_id]);

        return true;
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @return bool
     */
    private function _addDealer(Request &$request)
    {
        if (empty($request->dealer_id)) {
            $relation = auth()->user()->dealer($request->channel_id)->first();

            if (! $relation) {
                return false;
            }

            $request->merge(['dealer_id' => $relation->dealer_id]);
        }

        return true;
    }

}
