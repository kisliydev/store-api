<?php

namespace App\Http\Controllers\ChannelStatistics;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChannelStatistics\ExportSurveyAnswersRequest;
use App\Http\Requests\ChannelStatistics\AcademyItemsExportStatisticsRequest;

use App\Services\Statistics\AcademyItemsStatisticsService;

use App\Models\User;
use App\Models\UsersDealers;
use App\Models\Dealer;
use App\Models\AcademyItem;
use App\Models\District;

class AcademyItemsStatisticsController extends Controller
{
    /**
     * @param \App\Services\Statistics\AcademyItemsStatisticsService $service
     * @param \App\Http\Requests\ChannelStatistics\ExportSurveyAnswersRequest $request
     * @param \App\Models\User $user
     * @param \App\Models\AcademyItem $academyItem
     * @return mixed
     */
    public function exportSurveyAnswers(AcademyItemsStatisticsService $service, ExportSurveyAnswersRequest $request, User $user, AcademyItem $academyItem)
    {
        return $this->exportFile('survey_answers.xlsx', $service->exportSurveyAnswers($request, $user, $academyItem));
    }

    /**
     * @param AcademyItemsStatisticsService $service
     * @param ExportSurveyAnswersRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCounters(AcademyItemsStatisticsService $service, ExportSurveyAnswersRequest $request)
    {
        return $this->jsonResponse($service->getCounters($request));
    }

    /**
     * @param \App\Services\Statistics\AcademyItemsStatisticsService $service
     * @param \App\Http\Requests\ChannelStatistics\AcademyItemsExportStatisticsRequest $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\District $district
     * @param \App\Models\AcademyItem $academyItem
     * @param \App\Models\UsersDealers $usersDealers
     * @return mixed
     */
    public function exportAcademyItems(AcademyItemsStatisticsService $service, AcademyItemsExportStatisticsRequest $request, Dealer $dealer, District $district, AcademyItem $academyItem, UsersDealers $usersDealers)
    {
        return $this->exportFile('academy_items_statistics.xlsx', $service->exportAcademyItems($request, $dealer, $district, $academyItem, $usersDealers));
    }
}
