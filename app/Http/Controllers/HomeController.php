<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\App;

class HomeController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        if(in_array(App::environment(), ['preprod', 'production'])) {
            return view('home');
        }

        return redirect('/api/documentation');
    }
}
