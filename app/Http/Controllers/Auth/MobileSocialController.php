<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\MobileSocialLoginRequest;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Tymon\JWTAuth\JWTAuth;

class MobileSocialController extends Controller
{
    /**
     * @param \App\Http\Requests\Auth\MobileSocialLoginRequest $request
     * @param \Tymon\JWTAuth\JWTAuth $JWTAuth
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(MobileSocialLoginRequest $request, JWTAuth $JWTAuth, User $user)
    {
        $result = $user->mobileLogin($request, $JWTAuth);

        if (isset($result['message'])) {
            $status = $result['code'] ?? HttpResponse::HTTP_FORBIDDEN;
            unset($result['code']);
            return $this->jsonResponse($result, $status);
        } elseif (isset($result['need'])) {
            $status = $result['code'];
            unset($result['code']);
            return $this->jsonResponse($result, $status);
        }

        return $this->jsonResponse($result);
    }
}
