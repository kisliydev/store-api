<?php

namespace App\Http\Controllers\Auth;
use App\Http\Requests\Auth\VerifyResendRequest;
use Symfony\Component\HttpFoundation\Response as HttpResponse;

use App\Http\Controllers\Controller;
use App\Http\Requests\Request;
use App\Http\Requests\Auth\GetChannelIDRequest;
use App\Http\Requests\Auth\GetListForSignUpFormRequest;
use App\Http\Requests\Auth\RegisterRequest;
use App\Models\Channel;
use App\Models\Media;
use App\Models\Position;
use App\Models\Dealer;
use App\Models\Model;
use App\Models\User;

class RegisterController extends Controller
{
    /**
     * @param \App\Http\Requests\Auth\GetChannelIDRequest $request
     * @param \App\Models\Channel $channelModel
     * @return \Illuminate\Http\JsonResponse
     */
    public function getChannelId(GetChannelIDRequest $request, Channel $channelModel)
    {
        $channel = $channelModel->where('invite_code', $request->invite_code)
            ->with('currentCampaign')->first();

        if (! $channel) {
            return $this->jsonResponse(['message' => 'Can\'t find the channel.' ], HttpResponse::HTTP_UNPROCESSABLE_ENTITY);
        }

        if ($channel->currentCampaign && $channel->currentCampaign->isAllowedStatus()) {
            $response = [
                'channel_id' => $channel->id,
            ];

            if ($request->skip_brand_id) {
                $response['brand_id'] = $channel->countryBrand->brand_id;
                $brandSettings = $channel->countryBrand->brand->settings;
                $response['onesignal_app_id'] = $brandSettings['onesignal_app_id'] ?? '';
                $response['onesignal_rest_api_key'] = $brandSettings['onesignal_rest_api_key'] ?? '';
            }

            return $this->jsonResponse($response);

        }

        return $this->jsonResponse(['message' => 'You can\'t join to the channel with non-active campaign.' ], HttpResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @param \App\Http\Requests\Auth\GetListForSignUpFormRequest $request
     * @param \App\Models\Dealer $dealer
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDealersList(GetListForSignUpFormRequest $request, Dealer $dealer)
    {
        return $this->jsonResponse($this->_getList($request, $dealer));
    }

    /**
     * @param \App\Http\Requests\Auth\GetListForSignUpFormRequest $request
     * @param \App\Models\Position $position
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPositionsList(GetListForSignUpFormRequest $request, Position $position)
    {
        return $this->jsonResponse($this->_getList($request, $position));
    }

    /**
     * @param \App\Http\Requests\Auth\RegisterRequest $request
     * @param \App\Models\User $user
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function register(RegisterRequest $request, User $user, Media $media)
    {
        return $this->jsonResponse($user->register($request, $media));
    }

    /**
     * Get the user who has the same token and change status to verified
     *
     * @param $token
     * @param \App\Models\User $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function verify($token, User $user)
    {
        $user = $user->where('email_token', $token)->first();

        if ($user) {
            $user->userVerified();
            return $this->jsonResponse(true);
        } else {
            return $this->jsonResponse(false);
        }
    }

    public function verifyResend(VerifyResendRequest $request, User $user)
    {
        $user = $user->where('email', $request->email)->first();
        if ($user->verified == false) {
            $user->sendAuthEmail($user, 'register');

            return response()->json(['status' => 'success']);
        }

        return response()->json([
            'status' => 'error',
            'message' => 'user is already verified'
        ], 400);
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param \App\Models\Model $model
     * @return mixed
     */
    private function _getList(Request $request, Model $model)
    {
        return $model->where('channel_id', $request->channel_id)->get(['id', 'name']);
    }
}
