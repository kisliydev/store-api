<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Models\PlayerId;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\SwitchChannelRequest;
use App\Http\Requests\Auth\JoinChannelRequest;
use App\Http\Requests\Auth\MobileLogoutRequest;
use Tymon\JWTAuth\JWTAuth;

class LogoutController extends Controller
{
    /**
     * @param \App\Models\User $user
     * @param \Tymon\JWTAuth\JWTAuth $auth
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(User $user, JWTAuth $auth)
    {
        $auth->invalidate($auth->getToken());
        return $this->jsonResponse($user->setOnlineStatus(0));
    }

    /**
     * @param \App\Http\Requests\Auth\MobileLogoutRequest $request
     * @param \App\Models\PlayerId $playerId
     * @param \Tymon\JWTAuth\JWTAuth $auth
     * @return \Illuminate\Http\JsonResponse
     */
    public function mobileLogout(MobileLogoutRequest $request, PlayerId $playerId, JWTAuth $auth)
    {
        $auth->invalidate($auth->getToken());
        return $this->jsonResponse($playerId->where('player_id', $request->player_id)->delete());
    }

    /**
     * @param \App\Http\Requests\Auth\SwitchChannelRequest $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function switchChannel(SwitchChannelRequest $request, User $user)
    {
        return $this->jsonResponse($user->switchChannel($request));
    }

    /**
     * @param \App\Http\Requests\Auth\JoinChannelRequest $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function joinChannel(JoinChannelRequest $request, User $user)
    {
        return $this->jsonResponse($user->joinChannel($request));
    }
}
