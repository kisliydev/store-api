<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\ForgotPasswordResetRequest;
use App\Http\Requests\Auth\ForgotPasswordTokenRequest;
use App\Http\Requests\Auth\PreliminaryForgotTokenCheck;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use App\Models\User;

class ForgotPasswordController extends Controller
{
    /**
     * Send a reset link to the given user.
     *
     * @param \App\Http\Requests\Auth\ForgotPasswordTokenRequest $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function setResetToken(ForgotPasswordTokenRequest $request, User $user)
    {
        $user = $user->where('email', $request->email)->first();
        $user->forgotPassword();

        return $this->jsonResponse(['message' => 'Token sent']);
    }

    /**
     * Send a reset link to the given user.
     *
     * @param \App\Http\Requests\Auth\ForgotPasswordTokenRequest $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function setResetTokenMobile(ForgotPasswordTokenRequest $request, User $user)
    {
        return $this->jsonResponse($user->where('email', $request->email)->first()->forgotPassword(true));
    }

    /**
     * @param \App\Http\Requests\Auth\PreliminaryForgotTokenCheck $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkForgotTokens(PreliminaryForgotTokenCheck $request, User $user)
    {
        if (!! $user->getLostPasswordUser($request)) {
            return $this->jsonResponse(['message' => 'OK'], HttpResponse::HTTP_OK);
        }

        return $this->jsonResponse([
            'message' => 'User not found',
            'errors' => ['forgot_mail_token' => 'The given token is invalid']
        ], HttpResponse::HTTP_UNPROCESSABLE_ENTITY);
    }

    /**
     * @param \App\Http\Requests\Auth\ForgotPasswordResetRequest $request
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function reset(ForgotPasswordResetRequest $request, User $user)
    {
        if ($user->resetForgotPassword($request)) {

            return $this->jsonResponse(['message' => 'Password updated'], HttpResponse::HTTP_OK);
        }

        return $this->jsonResponse([
            'message' => 'User not found',
            'errors' => ['forgot_mail_token' => 'The given token is invalid']
        ], HttpResponse::HTTP_UNPROCESSABLE_ENTITY);
    }
}
