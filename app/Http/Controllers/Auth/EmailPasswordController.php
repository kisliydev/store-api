<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Requests\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\MobileLoginRequest;
use App\Http\Requests\Auth\ChannelJoinAndLoginRequest;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Tymon\JWTAuth\JWTAuth;

class EmailPasswordController extends Controller
{

    /**
     * @param \App\Http\Requests\Auth\LoginRequest $request
     * @param \Tymon\JWTAuth\JWTAuth $JWTAuth
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request, JWTAuth $JWTAuth, User $user)
    {
        return $this->_handle($request, $JWTAuth, $user, 'login');
    }

    /**
     * @param \App\Http\Requests\Auth\MobileLoginRequest $request
     * @param \Tymon\JWTAuth\JWTAuth $JWTAuth
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function mobileLogin(MobileLoginRequest $request, JWTAuth $JWTAuth, User $user)
    {
        return $this->_handle($request, $JWTAuth, $user, 'mobileLogin');
    }

    /**
     * @param ChannelJoinAndLoginRequest $request
     * @param JWTAuth $JWTAuth
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function mobileJoinChannelAndLogin(ChannelJoinAndLoginRequest $request, JWTAuth $JWTAuth, User $user)
    {
        return $this->_handle($request, $JWTAuth, $user, 'mobileJoinChannelAndLogin');
    }

    /**
     * @param \Tymon\JWTAuth\JWTAuth $auth
     * @return \Illuminate\Http\JsonResponse
     */
    public function refreshToken(JWTAuth $auth)
    {
        $token = $auth->getToken();

        if (! $token) {
            return response()->json(['message' => 'Unauthorized'], HttpResponse::HTTP_UNAUTHORIZED);
        }

        return $this->jsonResponse(['token' => $auth->refresh($token)]);
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param \Tymon\JWTAuth\JWTAuth $JWTAuth
     * @param \App\Models\User $user
     * @param string $callback
     * @return \Illuminate\Http\JsonResponse
     */
    private function _handle(Request $request, JWTAuth $JWTAuth, User $user, string $callback)
    {
        $result = $user->$callback($request, $JWTAuth);

        if (isset($result['message'])) {
            $status = $result['code'] ?? HttpResponse::HTTP_FORBIDDEN;
            unset($result['code']);
            return $this->jsonResponse($result, $status);
        } elseif (isset($result['need'])) {
            $status = $result['code'];
            unset($result['code']);
            return $this->jsonResponse($result, $status);
        }

        return $this->jsonResponse($result);
    }
}
