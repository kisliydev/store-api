<?php

namespace App\Http\Controllers;

use App\Models\Brand;
use App\Transformers\PushSettingsTransformer;

class ApiSettingsController extends Controller
{
    /**
     * @param \App\Models\Brand $brand
     * @param \App\Transformers\PushSettingsTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Brand $brand, PushSettingsTransformer $transformer)
    {
        return $this->jsonResponse([
            'mail' => config('mail'),
            'database' => config('database.connections.mysql'),
            'brands' => $transformer->transform($brand->getSettings()),
        ]);
    }
}
