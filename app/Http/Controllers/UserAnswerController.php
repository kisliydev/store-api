<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserAnswer\UserAnswerGetListRequest;
use App\Http\Requests\UserAnswer\UserAnswerGetRequest;
use App\Models\DoneAcademyItem;
use App\Models\UserAnswer;
use App\Models\User;
use App\Models\AcademyItem;
use App\Models\Question;

class UserAnswerController extends Controller
{
    /**
     * @param \App\Http\Requests\UserAnswer\UserAnswerGetListRequest $request
     * @param \App\Models\DoneAcademyItem $doneAcademyItem
     * @param \App\Models\AcademyItem $academyItem
     * @param \App\Models\User $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(UserAnswerGetListRequest $request, DoneAcademyItem $doneAcademyItem, AcademyItem $academyItem, User $user)
    {
        return $this->jsonResponse($doneAcademyItem->getList($request, $academyItem, $user));
    }

    /**
     * @param \App\Http\Requests\UserAnswer\UserAnswerGetRequest $request
     * @param \App\Models\UserAnswer $userAnswer
     * @param \App\Models\Question $question
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(UserAnswerGetRequest $request, UserAnswer $userAnswer, Question $question)
    {
        return $this->jsonResponse($userAnswer->getSurveyAnswers($request, $question));
    }

}
