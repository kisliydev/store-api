<?php

namespace App\Http\Controllers;

use App\Http\Requests\Brand\BrandGetAllRequest;
use App\Http\Requests\Brand\BrandSaveRequest;
use App\Http\Requests\Brand\BrandGetRequest;
use App\Http\Requests\Brand\BrandUpdateRequest;
use App\Http\Requests\Brand\BrandSaveSettingsRequest;
use App\Models\Brand;
use App\Models\Media;

/**
 * Class BrandController
 *
 * @package App\Http\Controllers
 */
class BrandController extends Controller
{
    /**
     * @param \App\Http\Requests\Brand\BrandGetAllRequest $request
     * @param \App\Models\Brand $brand
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(BrandGetAllRequest $request, Brand $brand)
    {
        return $this->jsonResponse($brand->getList($request));
    }

    /**
     * @param \App\Http\Requests\Brand\BrandGetRequest $request
     * @param \App\Models\Brand $brand
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(BrandGetRequest $request, Brand $brand)
    {
        return $this->jsonResponse($brand->get($request));
    }

    /**
     * @param BrandSaveRequest $request
     * @param Brand $brand
     * @param Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(BrandSaveRequest $request, Brand $brand, Media $media)
    {
        return $this->jsonResponse($brand->add($request, $media));
    }

    /**
     * @param BrandUpdateRequest $request
     * @param Brand $brand
     * @param Media $media
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(BrandUpdateRequest $request, Brand $brand, Media $media)
    {
        return $this->jsonResponse($brand->resave($request, $media));
    }

    /**
     * @param \App\Http\Requests\Brand\BrandSaveSettingsRequest $request
     * @param \App\Models\Brand $brand
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveSettings(BrandSaveSettingsRequest $request, Brand $brand) {
        return $this->jsonResponse($brand->saveSettings($request));

    }

    /**
     * @param \App\Http\Requests\Brand\BrandGetRequest $request
     * @param \App\Models\Brand $brand
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(BrandGetRequest $request, Brand $brand)
    {
        return $this->jsonResponse($brand->remove($request));
    }
}
