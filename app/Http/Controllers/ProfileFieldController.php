<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserProfileField\FieldsGetRequest;
use App\Http\Requests\UserProfileField\FieldsSaveRequest;
use App\Models\ProfileField;

class ProfileFieldController extends Controller
{
    /**
     * @param \App\Http\Requests\UserProfileField\FieldsGetRequest $request
     * @param \App\Models\ProfileField $field
     * @return \Illuminate\Http\JsonResponse
     */
    public function showUserFields(FieldsGetRequest $request, ProfileField $field)
    {
        return $this->jsonResponse($field->where('channel_id', $request->channel_id)->get());
    }

    /**
     * @param \App\Http\Requests\UserProfileField\FieldsSaveRequest $request
     * @param \App\Models\ProfileField $field
     * @return \Illuminate\Http\JsonResponse
     */
    public function saveUserFields(FieldsSaveRequest $request, ProfileField $field)
    {
        return $this->jsonResponse($field->resave($request));
    }

}
