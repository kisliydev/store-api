<?php

namespace App\Http\Controllers;

use App\Http\Requests\GeneralReport\AcademyListRequest;
use App\Http\Requests\GeneralReport\InfoRequest;
use App\Http\Requests\GeneralReport\MessagesRequest;
use App\Models\AcademyItem;
use App\Models\Channel;
use App\Models\Thread;
use App\Transformers\User\ChannelUsersTransformer;

class GeneralReportController extends Controller
{
    /**
     * @param \App\Http\Requests\GeneralReport\InfoRequest $request
     * @param \App\Models\Channel $channel
     * @param \App\Transformers\User\ChannelUsersTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function channelInfo(InfoRequest $request, Channel $channel, ChannelUsersTransformer $transformer)
    {
        return $this->jsonResponse($channel->getChannelInfo($request, $transformer));
    }

    /**
     * @param \App\Http\Requests\GeneralReport\MessagesRequest $request
     * @param \App\Models\Thread $thread
     * @return \Illuminate\Http\JsonResponse
     */
    public function messages(MessagesRequest $request, Thread $thread)
    {
        return $this->jsonResponse($thread->getChannelAutoThreads($request));
    }

    /**
     * @param AcademyListRequest $request
     * @param AcademyItem $item
     * @return \Illuminate\Http\JsonResponse
     */
    public function quizzes(AcademyListRequest $request, AcademyItem $item)
    {
        return $this->jsonResponse($item->getReportList($request, 'quiz'));
    }

    /**
     * @param AcademyListRequest $request
     * @param AcademyItem $item
     * @return \Illuminate\Http\JsonResponse
     */
    public function surveys(AcademyListRequest $request, AcademyItem $item)
    {
        return $this->jsonResponse($item->getReportList($request, 'survey'));
    }
}
