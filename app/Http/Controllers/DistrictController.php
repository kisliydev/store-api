<?php

namespace App\Http\Controllers;

use App\Http\Requests\District\DistrictGetAllRequest;
use App\Http\Requests\District\DistrictGetRequest;
use App\Http\Requests\District\DistrictSaveRequest;
use App\Http\Requests\District\DistrictUpdateRequest;
use App\Http\Requests\District\DistrictBulkActionRequest;
use App\Models\District;
use App\Models\Media;

/**
 * Class DistrictController
 *
 * @package App\Http\Controllers
 */
class DistrictController extends Controller
{
    /**
     * @param \App\Http\Requests\District\DistrictGetAllRequest $request
     * @param \App\Models\District $district
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(DistrictGetAllRequest $request, District $district)
    {
        return $this->jsonResponse($district->getList($request));
    }

    /**
     * @param \App\Http\Requests\District\DistrictGetRequest $request
     * @param \App\Models\District $district
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(DistrictGetRequest $request, District $district)
    {
        return $this->jsonResponse($district->get($request));
    }

    /**
     * @param DistrictSaveRequest $request
     * @param District $district
     * @param Media $media
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function store(DistrictSaveRequest $request, District $district, Media $media)
    {
        return $this->jsonResponse($district->add($request, $media));
    }

    /**
     * @param \App\Http\Requests\District\DistrictUpdateRequest $request
     * @param \App\Models\District $district
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(DistrictUpdateRequest $request, District $district, Media $media)
    {
        return $this->jsonResponse($district->resave($request, $media));
    }

    /**
     * @param \App\Http\Requests\District\DistrictGetRequest $request
     * @param \App\Models\District $district
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(DistrictGetRequest $request, District $district, Media $media)
    {
        return $this->jsonResponse($district->remove($request, $media));
    }

    /**
     * @param \App\Http\Requests\District\DistrictBulkActionRequest $request
     * @param \App\Models\District $district
     * @param \App\Models\Media $media
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkAction(DistrictBulkActionRequest $request, District $district, Media $media)
    {
        return $this->jsonResponse($district->bulkAction($request, $media));
    }
}
