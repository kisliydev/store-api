<?php

namespace App\Http\Controllers;
use App\Models\Language;
use App\Models\Localization;

class LanguageController extends Controller
{
    public function index(Language $language, Localization $localization)
    {
        return $this->jsonResponse($language->getList($localization));
    }
}
