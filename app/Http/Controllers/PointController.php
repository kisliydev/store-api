<?php

namespace App\Http\Controllers;

use App\Http\Requests\Point\PointGetAllRequest;
use App\Http\Requests\Point\PointGetRequest;
use App\Http\Requests\Point\PointSaveRequest;
use App\Http\Requests\Point\PointSaveFromWebRequest;
use App\Http\Requests\Point\PointSaveProductsRequest;
use App\Http\Requests\Point\PointUpdateRequest;
use App\Http\Requests\Point\PointBulkActionRequest;
use App\Http\Requests\Point\PointSaveUserAnswersRequest;
use App\Transformers\PointsListTransformer;
use App\Models\PointsStatistic;
use App\Models\AppStatistic;
use App\Models\Product;

class PointController extends Controller
{
    /**
     * @param \App\Http\Requests\Point\PointGetAllRequest $request
     * @param \App\Transformers\PointsListTransformer $transformer
     * @param \App\Models\PointsStatistic $point
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(PointGetAllRequest $request, PointsListTransformer $transformer, PointsStatistic $point)
    {
        $data = $point->getList($request);
        $transformer->transform($data->getCollection());
        return $this->jsonResponse($data);
    }

    /**
     * @param \App\Http\Requests\Point\PointGetRequest $request
     * @param \App\Models\PointsStatistic $point
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(PointGetRequest $request, PointsStatistic $point)
    {
        return $this->jsonResponse($point->getItem($request));
    }

    /**
     * @param PointSaveFromWebRequest $request
     * @param PointsStatistic $point
     * @param PointsListTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PointSaveFromWebRequest $request, PointsStatistic $point, PointsListTransformer $transformer)
    {
        $data = $point->add($request);
        return $this->jsonResponse($transformer->transform($data)->first());
    }

    /**
     * @param \App\Http\Requests\Point\PointSaveProductsRequest $request
     * @param \App\Models\PointsStatistic $point
     * @param \App\Models\AppStatistic $appStatistic
     * @param \App\Models\Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function addProducts(PointSaveProductsRequest $request, PointsStatistic $point, AppStatistic $appStatistic, Product $product)
    {
        return $this->jsonResponse($point->addProducts($request, $appStatistic, $product));
    }

    /**
     * @param \App\Http\Requests\Point\PointSaveUserAnswersRequest $request
     * @param \App\Models\PointsStatistic $point
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeSurveyAnswers(PointSaveUserAnswersRequest $request, PointsStatistic $point)
    {
        return $this->jsonResponse($point->addAcademyItem($request, 'survey'));
    }

    /**
     * @param \App\Http\Requests\Point\PointSaveRequest $request
     * @param \App\Models\PointsStatistic $point
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeQuiz(PointSaveRequest $request, PointsStatistic $point)
    {
        return $this->jsonResponse($point->addAcademyItem($request, 'quiz'));
    }

    /**
     * @param PointUpdateRequest $request
     * @param PointsStatistic $point
     * @param PointsListTransformer $transformer
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PointUpdateRequest $request, PointsStatistic $point, PointsListTransformer $transformer)
    {
        $data = $point->resave($request);
        return $this->jsonResponse($transformer->transform($data)->first());
    }

    /**
     * @param \App\Http\Requests\Point\PointGetRequest $request
     * @param \App\Models\PointsStatistic $point
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(PointGetRequest $request, PointsStatistic $point)
    {
        return $this->jsonResponse($point->remove($request));
    }

    /**
     * @param \App\Http\Requests\Point\PointBulkActionRequest $request
     * @param \App\Models\PointsStatistic $point
     * @return \Illuminate\Http\JsonResponse
     */
    public function bulkAction(PointBulkActionRequest $request, PointsStatistic $point)
    {
        return $this->jsonResponse($point->bulkAction($request));
    }
}
