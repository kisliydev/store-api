<?php

namespace App\Http\Controllers;

use App\Models\ToDoItem;
use App\Models\ToDoItemChannel;
use App\Http\Requests\ToDo\DeleteRequest;
use App\Http\Requests\ToDo\GetInChannelRequest;
use App\Http\Requests\ToDo\MarkDoneRequest;
use App\Http\Requests\ToDo\SaveRequest;
use App\Http\Requests\ToDo\UnmarkDoneRequest;
use App\Http\Requests\ToDo\UpdateRequest;


class ToDoItemController extends Controller
{
    /**
     * @param \App\Models\ToDoItem $item
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(ToDoItem $item)
    {
        return $this->jsonResponse($item->orderBy('order')->get(['id', 'text', 'order']));
    }

    /**
     * @param \App\Http\Requests\ToDo\SaveRequest $request
     * @param \App\Models\ToDoItem $item
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SaveRequest $request, ToDoItem $item)
    {
        return $this->jsonResponse($item->create($request->validated()));
    }

    /**
     * @param \App\Http\Requests\ToDo\UpdateRequest $request
     * @param \App\Models\ToDoItem $item
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, ToDoItem $item)
    {
        return $this->jsonResponse($item->where('id', $request->id)->update($request->validated()));
    }

    /**
     * @param \App\Http\Requests\ToDo\DeleteRequest $request
     * @param \App\Models\ToDoItem $item
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DeleteRequest $request, ToDoItem $item)
    {
        return $this->jsonResponse($item->where('id', $request->id)->delete());
    }

    /**
     * @param \App\Http\Requests\ToDo\GetInChannelRequest $request
     * @param \App\Models\ToDoItem $item
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInChannel(GetInChannelRequest $request, ToDoItem $item)
    {
        return $this->jsonResponse($item->getInChannel($request));
    }

    /**
     * @param \App\Http\Requests\ToDo\MarkDoneRequest $request
     * @param \App\Models\ToDoItemChannel $item
     * @return \Illuminate\Http\JsonResponse
     */
    public function markDone(MarkDoneRequest $request, ToDoItemChannel $item)
    {
        return $this->jsonResponse($item->updateOrCreate(
            ['channel_id' => $request->channel_id, 'item_id' => $request->item_id],
            ['done_by' => auth()->user()->id]
        ));
    }

    /**
     * @param \App\Http\Requests\ToDo\UnmarkDoneRequest $request
     * @param \App\Models\ToDoItemChannel $item
     * @return \Illuminate\Http\JsonResponse
     */
    public function unmarkDone(UnmarkDoneRequest $request, ToDoItemChannel $item)
    {
        return $this->jsonResponse($item->where('channel_id', $request->channel_id)
            ->where('item_id', $request->item_id)->delete());
    }
}
