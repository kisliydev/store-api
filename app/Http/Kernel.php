<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * These middleware are run during every request to your application.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Illuminate\Foundation\Http\Middleware\ValidatePostSize::class,
        \App\Http\Middleware\TrimStrings::class,
        \Illuminate\Foundation\Http\Middleware\ConvertEmptyStringsToNull::class,
        \App\Http\Middleware\TrustProxies::class,
        \App\Http\Middleware\CORS::class,
        \App\Http\Middleware\ConvertStringBooleans::class,
    ];

    /**
     * The application's route middleware groups.
     *
     * @var array
     */
    protected $middlewareGroups = [
        'web' => [
            \App\Http\Middleware\EncryptCookies::class,
            \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
            \Illuminate\Session\Middleware\StartSession::class,
            // \Illuminate\Session\Middleware\AuthenticateSession::class,
            \Illuminate\View\Middleware\ShareErrorsFromSession::class,
            //\App\Http\Middleware\VerifyCsrfToken::class,
            \Illuminate\Routing\Middleware\SubstituteBindings::class,
        ],

        'api' => [
            'throttle:180,1',
            'bindings',
        ],
    ];

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Illuminate\Auth\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'bindings' => \Illuminate\Routing\Middleware\SubstituteBindings::class,
        'can' => \Illuminate\Auth\Middleware\Authorize::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'throttle' => \Illuminate\Routing\Middleware\ThrottleRequests::class,
        'cors' => \App\Http\Middleware\CORS::class,
        'jwt' => \App\Http\Middleware\JWTAuthUser::class,
        'acl' => \App\Http\Middleware\SimpleAcl::class,
        'role' => \Spatie\Permission\Middlewares\RoleMiddleware::class,
        'permission' => \Spatie\Permission\Middlewares\PermissionMiddleware::class,
        'channel_meta_permission' => \App\Http\Middleware\ChannelMetaPermissions::class,
        'user_edit_permission' => \App\Http\Middleware\UserEditPermissions::class,
        'user_view_permission' => \App\Http\Middleware\UserViewPermissions::class,
        /**
         * @todo remove in case if it won't be necessary
         */
        //'media_channel' => \App\Http\Middleware\MediaChannelPermissions::class,
        'channel_permissions' => \App\Http\Middleware\ChannelPermissions::class,
        'store_manager' => \App\Http\Middleware\StoreManager::class,
        'user_has_access_to_channel' => \App\Http\Middleware\UserHasAccessToChannel::class,
        'user_bindings' => \App\Http\Middleware\UserBindings::class,
        'view_dashboard' => \App\Http\Middleware\ViewDashboard::class,

    ];
}
