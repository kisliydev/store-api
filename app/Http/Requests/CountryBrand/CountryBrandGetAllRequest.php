<?php

namespace App\Http\Requests\CountryBrand;

use App\Http\Requests\Request;
use App\Rules\SortRule;
use App\Rules\FilterRule;
use App\Models\CountryBrand;

class CountryBrandGetAllRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $fields = (new CountryBrand)->getFillable();
        return [
            'brand_id' => ['required', 'numeric'],
            'page' => ['nullable', 'numeric'],
            'per_page' => ['numeric', 'nullable'],
            'sort' => ['nullable', 'string', new SortRule($fields)],
            'filter' => ['nullable', 'string', new FilterRule($fields)],
            'search' => ['nullable', 'string'],
        ];
    }
}