<?php

namespace App\Http\Requests\CountryBrand;

use App\Http\Requests\Request;

class CountryBrandUpdateRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric|exists:country_brands,id',
        'name' => 'required|string|max:100',
    ];
}