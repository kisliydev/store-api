<?php

namespace App\Http\Requests\CountryBrand;

use App\Http\Requests\Request;

class CountryBrandSaveRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'brand_id' => 'required|numeric|exists:brands,id',
        'name' => 'required|string|max:100',
    ];
}