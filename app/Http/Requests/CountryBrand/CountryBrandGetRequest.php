<?php

namespace App\Http\Requests\CountryBrand;

use App\Http\Requests\Request;

class CountryBrandGetRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric',
    ];
}