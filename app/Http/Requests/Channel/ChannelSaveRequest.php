<?php
namespace App\Http\Requests\Channel;

use App\Http\Requests\Request;

class ChannelSaveRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            /* channel data */
            'country_brand_id' => ['required', 'numeric', 'exists:country_brands,id'],
            'name'             => ['required', 'string', 'max:100'],
            'store_name'       => ['nullable', 'string', 'max:100'],
            'store_legal_name' => ['nullable', 'string', 'max:100'],
            'store_owner'      => ['nullable', 'string', 'max:100'],
            'invite_code'      => ['required', 'regex:/^[a-zA-Z0-9\-\_]+$/', 'min:6', 'max:6', 'unique:channels,invite_code'],
            'localization_id'  => ['nullable', 'numeric', 'exists:localizations,id'],

            /* campaign data */
            'campaign_name'    => ['required', 'string', 'max:100'],
            'started_at'       => ['required', 'date_format:Y-m-d H:i:s', 'after_or_equal:now', 'before:finished_at'],
            'sale_started_at'  => ['required', 'date_format:Y-m-d H:i:s', 'after_or_equal:started_at', 'before:sale_finished_at'],
            'sale_finished_at' => ['required', 'date_format:Y-m-d H:i:s', 'after:sale_started_at', 'before_or_equal:finished_at'],
            'finished_at'      => ['required', 'date_format:Y-m-d H:i:s', 'after:started_at'],
        ];
    }
}
