<?php
namespace App\Http\Requests\Channel;

use App\Http\Requests\Request;

class ChannelGetRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric|exists:channels,id',
    ];
}