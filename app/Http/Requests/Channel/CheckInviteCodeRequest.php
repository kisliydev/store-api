<?php

namespace App\Http\Requests\Channel;

use App\Http\Requests\Request;

class CheckInviteCodeRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = ['invite_code' => 'required|regex:/^[a-zA-Z0-9\-\_]+$/|min:6|max:6|unique:channels,invite_code'];
}
