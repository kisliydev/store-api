<?php

namespace App\Http\Requests\Channel;

use App\Http\Requests\Request;
use App\Rules\ExcelFile;

class ChannelLoadLocalizationRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required', 'numeric'],
            'localization' => ['required', new ExcelFile],
        ];
    }
}
