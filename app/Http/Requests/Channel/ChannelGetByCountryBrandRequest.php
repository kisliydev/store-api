<?php
namespace App\Http\Requests\Channel;

use App\Http\Requests\Request;
use App\Rules\SortRule;
use App\Rules\FilterRule;
use App\Models\Channel;

class ChannelGetByCountryBrandRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $fields = (new Channel)->getFillable();
        return [
            'country_brand_id' => ['required', 'numeric', 'exists:country_brands,id'],
            'trashed' => ['nullable', 'boolean'],
            'page' => ['nullable', 'numeric'],
            'per_page' => ['numeric', 'nullable'],
            'sort' => ['nullable', 'string', new SortRule($fields)],
            'filter' => ['nullable', 'string', new FilterRule($fields)],
            'search' => ['nullable', 'string'],
        ];
    }
}
