<?php

namespace App\Http\Requests\Channel;

use App\Http\Requests\Request;

class ChannelSaveMetaRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric|exists:channels,id',
        'key' => 'required|string|in:messages_settings,app_settings,app_layout,app_localization',
        'data' => 'required|array',
    ];
}
