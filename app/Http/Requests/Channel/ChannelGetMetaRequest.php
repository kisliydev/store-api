<?php

namespace App\Http\Requests\Channel;

use App\Http\Requests\Request;

class ChannelGetMetaRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric',
        'key' => 'required|string|in:messages_settings,app_settings,app_localization',
    ];
}
