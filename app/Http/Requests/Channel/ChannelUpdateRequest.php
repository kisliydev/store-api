<?php
namespace App\Http\Requests\Channel;

use App\Http\Requests\Request;

class ChannelUpdateRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id'               => 'required|numeric',
        'name'             => 'required|string|max:100',
        'store_name'       => 'nullable|string|max:100',
        'store_legal_name' => 'nullable|string|max:100',
        'store_owner'      => 'nullable|string|max:100',
        'invite_code'      => 'required|regex:/^[a-zA-Z0-9\-\_]+$/|min:6|max:6',
    ];
}
