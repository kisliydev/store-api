<?php

namespace App\Http\Requests\AutomaticMessage;

use App\Http\Requests\Request;
use App\Models\AutomaticMessage;

class CreateRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $types = implode(',', app()->make(AutomaticMessage::class)->getTypesList());

        return [
            'channel_id'  => ['required', 'numeric', 'exists:channels,id'],
            'title'       => ['required', 'string', 'max:100'],
            'description' => ['required', 'string', 'max:500'],
            'type'        => ['required', 'string', 'max:25', "in:{$types}"],
        ];
    }
}
