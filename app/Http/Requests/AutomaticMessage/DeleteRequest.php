<?php

namespace App\Http\Requests\AutomaticMessage;

use App\Http\Requests\Request;

class DeleteRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'   => ['required', 'array'],
            'id.*' => ['required', 'numeric', 'exists:automatic_messages,id'],
        ];
    }
}
