<?php

namespace App\Http\Requests\Localization;

use App\Http\Requests\Request;

class LocalizationGetRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric|exists:localizations,id',
    ];
}
