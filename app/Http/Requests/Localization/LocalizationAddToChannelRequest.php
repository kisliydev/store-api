<?php

namespace App\Http\Requests\Localization;

use App\Http\Requests\Request;

class LocalizationAddToChannelRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric|exists:localizations,id',
        'channel_id' => 'required|numeric|exists:channels,id',
    ];
}
