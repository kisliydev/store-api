<?php

namespace App\Http\Requests\Localization;

use App\Http\Requests\Request;
use App\Rules\SortRule;
use App\Rules\FilterRule;
use App\Models\Language;

class LocalizationGetAllRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $fields = (new Language)->getFillable();
        return [
            'page' => ['nullable', 'numeric'],
            'per_page' => ['numeric', 'nullable'],
            'sort' => ['nullable', 'string', new SortRule(array_merge($fields, ['created_at']))],
            'filter' => ['nullable', 'string', new FilterRule($fields)],
            'search' => ['nullable', 'string'],
        ];
    }
}
