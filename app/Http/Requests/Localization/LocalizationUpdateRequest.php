<?php

namespace App\Http\Requests\Localization;

use App\Http\Requests\Request;

class LocalizationUpdateRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric|exists:localizations,id',
        'translations' => 'required|array',
        'translations.*' => 'nullable|string',
    ];
}
