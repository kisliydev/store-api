<?php

namespace App\Http\Requests\Localization;

use App\Http\Requests\Request;
use App\Rules\ExcelFile;

class LocalizationSaveRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:100'],
            'translations' => ['required', new ExcelFile],
        ];
    }
}
