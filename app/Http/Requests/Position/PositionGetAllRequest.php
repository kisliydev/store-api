<?php

namespace App\Http\Requests\Position;

use App\Http\Requests\Request;
use App\Rules\SortRule;
use App\Rules\FilterRule;
use App\Models\Position;

class PositionGetAllRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $fields = (new Position)->getFillable();
        return [
            'channel_id' => ['required', 'numeric'],
            'page' => ['nullable', 'numeric'],
            'per_page' => ['numeric', 'nullable'],
            'sort' => ['nullable', 'string', new SortRule($fields)],
            'filter' => ['nullable', 'string', new FilterRule($fields)],
            'search' => ['nullable', 'string'],
        ];
    }
}