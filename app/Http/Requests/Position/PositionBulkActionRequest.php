<?php

namespace App\Http\Requests\Position;

use App\Http\Requests\Request;

class PositionBulkActionRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|array',
        'id.*' => 'required|numeric',
        'action' => 'required|string|in:delete',
    ];
}
