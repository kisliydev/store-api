<?php

namespace App\Http\Requests\Position;

use App\Http\Requests\Request;

class PositionGetRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric',
    ];
}
