<?php

namespace App\Http\Requests\Position;

use App\Http\Requests\Request;

class PositionSaveRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'required|numeric',
        'name' => 'required|string|max:100',
        'weight_points' => 'required|numeric|min:0.01|max:99.9',
        'is_manager' => 'nullable|boolean',
    ];
}
