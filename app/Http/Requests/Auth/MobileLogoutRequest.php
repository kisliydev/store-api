<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class MobileLogoutRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'player_id' => ['required', 'string', 'regex:/^[a-zA-Z0-9]{8}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{12}$/i'],
    ];
}
