<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class PreliminaryForgotTokenCheck extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'forgot_password_token' => 'required|string|max:70|exists:users,forgot_password_token',
        'forgot_mail_token' => 'required|string|max:255',
    ];
}
