<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;
use App\Rules\ChannelBelongsToBrandRule;

class SwitchChannelRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric'],
            'player_id' => ['required', 'string', 'regex:/^[a-zA-Z0-9]{8}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{12}$/i'],
            'app_name' => ['nullable', 'string', 'in:' . join(',', config('onesignal.universal_apps'))],
        ];
    }
}
