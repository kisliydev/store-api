<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class LoginRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'email' => 'required|string|email|max:100',
        'password' => 'required|string|min:6',
    ];
}
