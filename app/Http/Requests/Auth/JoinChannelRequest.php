<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;
use App\Rules\DealerBelongsToChannelRule;
use App\Rules\PositionBelongsToChannelRule;

class JoinChannelRequest extends Request
{
    public function rules()
    {
        return [
            'channel_id'  => ['required', 'numeric', 'exists:channels,id'],
            'dealer_id'   => ['required', 'numeric', new DealerBelongsToChannelRule($this)],
            'position_id' => ['required', 'numeric', new PositionBelongsToChannelRule($this)],
            'player_id'   => ['required', 'string', 'regex:/^[a-zA-Z0-9]{8}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{12}$/i'],
            'app_name'    => ['nullable', 'string', 'in:' . join(',', config('onesignal.universal_apps'))],
        ];
    }
}
