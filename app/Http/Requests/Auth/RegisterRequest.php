<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;
use App\Models\Channel;
use App\Models\Dealer;
use App\Models\Position;
use App\Rules\UserRegistrationRule;
use App\Rules\UserSocialRegisterRule;
use App\Rules\UniqueEmail;

class RegisterRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric', new UserRegistrationRule($this, new Channel(), new Dealer(), new Position())],
            'dealer_id' => ['required', 'numeric'],
            'position_id' => ['required', 'numeric'],

            'first_name' => ['required', 'string', 'max:100'],
            'last_name' => ['required', 'string', 'max:100'],

            'email' => ['required_without:social_id', 'string', 'max:100', 'email', new UniqueEmail($this)],
            'phone' => ['nullable', 'string', 'regex:/^\+[\d]{5,15}$/', 'unique:users'],

            'social_vendor' => ['nullable', 'string', 'in:facebook,google'],
            'social_id' => ['required_with:social_vendor', 'string', 'max:100', new UserSocialRegisterRule($this)],

            'avatar_url' => ['nullable', 'string'],
            'avatar' => ['nullable', 'mimes:jpeg,jpg,png,gif', 'max:10000'],

            'password' => ['required_without:social_vendor', 'string', 'min:6', 'max:32', 'confirmed'],

            'is_agree' => ['required', 'accepted'],

            'player_id' => ['required', 'string', 'regex:/^[a-zA-Z0-9]{8}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{12}$/i'],
            'app_name' => ['nullable', 'string', 'in:' . join(',', config('onesignal.universal_apps'))],
        ];

    }
}
