<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;
use App\Rules\ChannelBelongsToBrandRule;

class GetListForSignUpFormRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'brand_id' => ['required', 'numeric'],
            'channel_id' => ['required', 'numeric', new ChannelBelongsToBrandRule($this)],
        ];
    }
}
