<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;
use App\Rules\ChannelBelongsToBrandRule;

class MobileLoginRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'email' => ['required', 'string', 'email', 'max:100'],
            'password' => ['required', 'string', 'min:6'],
            'brand_id' => ['required', 'numeric'],
            'channel_id' => ['required', 'numeric', new ChannelBelongsToBrandRule($this)],
            'player_id' => ['required', 'string', 'regex:/^[a-zA-Z0-9]{8}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{12}$/i'],
            'app_name' => ['nullable', 'string', 'in:' . join(',', config('onesignal.universal_apps'))],
        ];
    }
}
