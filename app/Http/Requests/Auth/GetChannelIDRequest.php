<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;
use App\Rules\ChannelBelongsToBrandRule;

class GetChannelIDRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'brand_id'      => ['required_unless:skip_brand_id,true', 'numeric'],
            'skip_brand_id' => ['nullable', 'boolean'],
            'invite_code'   => ['required', 'regex:/^[a-zA-Z0-9\-\_]+$/', 'min:6', 'max:6', new ChannelBelongsToBrandRule($this)],
        ];
    }
}
