<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;

class ForgotPasswordTokenRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'email' => 'required|string|email|max:100|exists:users,email',
    ];
}
