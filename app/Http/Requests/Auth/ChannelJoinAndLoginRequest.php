<?php

namespace App\Http\Requests\Auth;

use App\Http\Requests\Request;
use App\Rules\DealerBelongsToChannelRule;
use App\Rules\PositionBelongsToChannelRule;
use App\Rules\ChannelBelongsToBrandRule;

class ChannelJoinAndLoginRequest extends Request
{
    public function rules()
    {
        return [
            'social_vendor' => ['required_without:email', 'in:facebook,google'],
            'social_id'     => ['required_with:social_vendor', 'string', 'max:100'],

            'email'       => ['required_without:social_vendor', 'string', 'email', 'max:100'],
            'password'    => ['required_with:email', 'string', 'min:6'],

            'dealer_id'   => ['required', 'numeric', new DealerBelongsToChannelRule($this)],
            'position_id' => ['required', 'numeric', new PositionBelongsToChannelRule($this)],

            'brand_id'   => ['required', 'numeric'],
            'channel_id' => ['required', 'numeric', new ChannelBelongsToBrandRule($this)],
            'player_id'  => ['required', 'string', 'regex:/^[a-zA-Z0-9]{8}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{12}$/i'],
            'app_name'   => ['nullable', 'string', 'in:' . join(',', config('onesignal.universal_apps'))],
        ];
    }
}
