<?php

namespace App\Http\Requests\Country;

use App\Http\Requests\Request;

class CountryLocalizationRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_name' => 'required|string|exists:countries,name',
        ];
    }
}
