<?php

namespace App\Http\Requests\ToDo;

use App\Http\Requests\Request;

class GetInChannelRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' =>  'required|numeric|exists:channels,id',
    ];
}
