<?php

namespace App\Http\Requests\ToDo;

use App\Http\Requests\Request;

class UpdateRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' =>  'required|numeric|exists:to_do_items,id',
        'text' => 'required|string',
        'order' => 'nullable|numeric',
    ];
}

