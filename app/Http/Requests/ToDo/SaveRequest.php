<?php

namespace App\Http\Requests\ToDo;

use App\Http\Requests\Request;

class SaveRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'text' => 'required|string',
        'order' => 'nullable|numeric',
    ];
}
