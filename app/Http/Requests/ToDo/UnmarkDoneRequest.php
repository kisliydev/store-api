<?php

namespace App\Http\Requests\ToDo;

use App\Http\Requests\Request;

class UnmarkDoneRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'item_id' => 'required|numeric|exists:to_do_items,id',
        'channel_id' =>  'required|numeric|exists:channels,id',
    ];
}
