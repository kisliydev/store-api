<?php

namespace App\Http\Requests\Sales;

use App\Http\Requests\Request;

class GetUserSalesRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'required|numeric',
    ];
}
