<?php

namespace App\Http\Requests\Sales;

use App\Http\Requests\Request;

class UpdateSaleRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'event_id' => 'required|numeric|exists:points_statistics,event_id',
        'products' => 'required|array',
        'products.*.object_id' => 'required|numeric',
        'products.*.units_number' => 'required|numeric|min:0',
    ];
}
