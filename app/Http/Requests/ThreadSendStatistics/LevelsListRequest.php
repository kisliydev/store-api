<?php

namespace App\Http\Requests\ThreadSendStatistics;

use App\Http\Requests\Request;

class LevelsListRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric', 'exists:channels,id'],
            'per_page' => ['nullable', 'numeric'],
        ];
    }
}
