<?php

namespace App\Http\Requests\Recipients;

use App\Http\Requests\Request;

class GetListRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'thread_id' => ['required', 'numeric', 'exists:threads,id'],
            'page'      => ['nullable', 'numeric'],
            'type'      => ['required', 'string', 'in:users,dealers,districts,leagues'],
            'per_page'  => ['nullable', 'numeric'],
        ];
    }
}
