<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\Request;

class ProductSaveRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'required|numeric',
        'name' => 'required|string|max:100',
        'text_before_amount' => 'string|nullable|max:100',
        'text_after_amount' => 'string|nullable|max:100',
        'default_amount' => 'numeric|nullable',
        'points' => 'required|numeric',
        'use_weight_points' => 'boolean|nullable',
        'order' => 'numeric|nullable',
    ];
}
