<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\Request;

class ProductLoadRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => 'required|integer|exists:channels,id',
            'products' => 'required|mimes:xls,xlsx,zip', // Excel file is the base zip archive. For this version the excel file is defined mimes as zip
        ];
    }

    public function messages()
    {
        return [
            'products.mimes' => 'The products must be a file of type: xls, xlsx.'
        ];
    }
}
