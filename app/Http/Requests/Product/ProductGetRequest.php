<?php

namespace App\Http\Requests\Product;

use App\Http\Requests\Request;

class ProductGetRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric',
    ];
}
