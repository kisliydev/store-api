<?php

namespace App\Http\Requests\BrandAcademy;

use App\Http\Requests\Request;
use App\Rules\HEXColoredString;

class BrandAcademySaveRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric'],
            'title' => ['required', 'string', 'max:100'],
            'article_title' => ['nullable', 'string', 'max:100'],
            'article_link' => ['nullable', 'string'],
            'description' => ['string', 'nullable', 'max:500'],
            'short_description' => ['string', 'nullable', 'max:500'],
            'complete_message' => ['string', 'nullable'],
            'fail_message' => ['string', 'nullable'],
            'start_button_text' => ['required', 'string', 'max:100'],
            'video_url' => ['string', 'nullable'],
            'thumbnail' => ['mimes:jpeg,jpg,png,gif', 'max:10000', 'nullable'],
            'media_thumbnail_id' => ['nullable', 'numeric', 'exists:media,id'],
            'background' => ['mimes:jpeg,jpg,png,gif', 'max:10000', 'nullable'],
            'media_background_id' => ['nullable', 'numeric', 'exists:media,id'],
            'order' => ['numeric', 'nullable'],

            'app_layout' => ['nullable', 'array'],
            'app_layout.*' => ['required', 'string', app()->make(HEXColoredString::class, ['request' => $this])],

            'questions' => ['nullable', 'array'],
            'questions.*.type' => ['nullable', 'string', 'in:text_input,multi_choice,single_choice'],
            'questions.*.text' => ['required', 'string', 'max:255'],
            'questions.*.image' => ['nullable', 'mimes:jpeg,jpg,png,gif', 'max:10000'],
            'questions.*.media_image_id' => ['nullable', 'numeric', 'exists:media,id'],
            'questions.*.required' => ['nullable', 'boolean'],
            'questions.*.order' => ['nullable', 'numeric'],
            'questions.*.show_alternative' => ['nullable', 'boolean'],
            'questions.*.alternative_label' => ['nullable', 'string', 'max:100'],

            'questions.*.new_answers' => ['nullable', 'array'],
            'questions.*.new_answers.*.text' => ['nullable', 'string', 'max:255'],
            'questions.*.new_answers.*.image' => ['nullable', 'mimes:jpeg,jpg,png,gif', 'max:10000'],
            'questions.*.new_answers.*.media_image_id' => ['nullable', 'numeric', 'exists:media,id'],
            'questions.*.new_answers.*.correct' => ['nullable', 'boolean'],
            'questions.*.new_answers.*.order' => ['nullable', 'numeric'],
        ];
    }
}
