<?php

namespace App\Http\Requests\BrandAcademy;

use App\Http\Requests\Request;

class BrandAcademyExportRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'required|numeric',
    ];
}
