<?php

namespace App\Http\Requests\BrandAcademy;

use App\Http\Requests\Request;
use App\Rules\BrandAcademyStartedAtRule;
use App\Rules\BrandAcademyFinishedAtRule;

class BrandAcademyQuickEditRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'id'                => ['required', 'numeric', 'exists:academy_items,id'],
            'points'            => ['required', 'numeric'],
            'use_weight_points' => ['nullable', 'boolean'],
            'started_at'        => ['nullable', new BrandAcademyStartedAtRule($this)],
            'finished_at'       => ['nullable', new BrandAcademyFinishedAtRule($this)],
            'status'            => ['required', 'string', 'in:draft,scheduled,published'],
        ];
    }
}
