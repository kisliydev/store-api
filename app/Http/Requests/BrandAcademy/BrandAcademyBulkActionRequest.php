<?php

namespace App\Http\Requests\BrandAcademy;

use App\Http\Requests\Request;

class BrandAcademyBulkActionRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|array',
        'id.*' => 'required|numeric',
        'action' => 'required|string|in:delete',
    ];
}
