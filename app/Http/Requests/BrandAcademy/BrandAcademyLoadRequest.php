<?php

namespace App\Http\Requests\BrandAcademy;

use App\Http\Requests\Request;
use App\Rules\ExcelFile;

class BrandAcademyLoadRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric', 'exists:channels,id'],
            'items' => ['required', new ExcelFile],
        ];
    }
}
