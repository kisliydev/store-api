<?php

namespace App\Http\Requests\BrandAcademy;

use App\Http\Requests\Request;

class SurveyExportRequest extends Request
{
   /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => 'required|integer|exists:channels,id',
            'ids' => 'array',
            'ids.*' => 'required_with:ids|integer|exists:academy_items,id',
        ];
    }
}
