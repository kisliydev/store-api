<?php

namespace App\Http\Requests\BrandAcademy;

use App\Http\Requests\Request;
use App\Rules\HEXColoredString;

class BrandAcademyUpdateRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            /* AcademItem data */
            'id' => ['required', 'numeric'],
            'title' => ['required', 'string', 'max:100'],
            'article_title' => ['nullable', 'string', 'max:100'],
            'article_link' => ['nullable', 'string'],
            'description' => ['string', 'nullable'],
            'short_description' => ['string', 'nullable'],
            'complete_message' => ['string', 'nullable'],
            'fail_message' => ['string', 'nullable'],
            'start_button_text' => ['required', 'string', 'max:100'],
            'video_url' => ['string', 'nullable'],
            'thumbnail' => ['mimes:jpeg,jpg,png,gif', 'max:10000', 'nullable'],
            'media_thumbnail_id' => ['nullable', 'numeric', 'exists:media,id'],
            'remove_thumbnail' => ['boolean', 'nullable'],
            'background' => ['mimes:jpeg,jpg,png,gif', 'max:10000', 'nullable'],
            'remove_background' => ['boolean', 'nullable'],
            'media_background_id' => ['nullable', 'numeric', 'exists:media,id'],
            'order' => ['numeric', 'nullable'],

            'app_layout' => ['nullable', 'array'],
            'app_layout.*' => ['required', 'string', app()->make(HEXColoredString::class, ['request' => $this])],

            /* Existed questions data */
            'questions' => ['nullable', 'array'],
            'questions.*.id' => ['required', 'numeric'],
            'questions.*.remove_question' => ['boolean', 'nullable'],
            'questions.*.type' => ['nullable', 'string', 'in:text_input,multi_choice,single_choice'],
            'questions.*.text' => ['required_unless:questions.*.remove_question,true', 'string', 'max:255'],
            'questions.*.image' => ['nullable', 'mimes:jpeg,jpg,png,gif', 'max:10000'],
            'questions.*.media_image_id' => ['nullable', 'numeric', 'exists:media,id'],
            'questions.*.remove_image' => ['boolean', 'nullable'],
            'questions.*.required' => ['nullable', 'boolean'],
            'questions.*.order' => ['nullable', 'numeric'],
            'questions.*.show_alternative' => ['nullable', 'boolean'],
            'questions.*.alternative_label' => ['nullable', 'string', 'max:100'],

            /* Existed answers data */
            'questions.*.answers' => ['nullable', 'array'],
            'questions.*.answers.*.id' => ['required', 'numeric'],
            'questions.*.answers.*.remove_answer' => ['boolean', 'nullable'],
            'questions.*.answers.*.text' => ['required_unless:questions.*.answers.*.remove_answer,true', 'string', 'max:255'],
            'questions.*.answers.*.image' => ['nullable', 'mimes:jpeg,jpg,png,gif', 'max:10000'],
            'questions.*.answers.*.media_image_id' => ['nullable', 'numeric', 'exists:media,id'],
            'questions.*.answers.*.remove_image' => ['boolean', 'nullable'],
            'questions.*.answers.*.correct' => ['nullable', 'boolean'],
            'questions.*.answers.*.order' => ['nullable', 'numeric'],

            /* New answers data */
            'questions.*.new_answers' => ['nullable', 'array'],
            'questions.*.new_answers.*.text' => ['required', 'string', 'max:255'],
            'questions.*.new_answers.*.image' => ['nullable', 'mimes:jpeg,jpg,png,gif', 'max:10000'],
            'questions.*.new_answers.*.media_image_id' => ['nullable', 'numeric', 'exists:media,id'],
            'questions.*.new_answers.*.correct' => ['nullable', 'boolean'],
            'questions.*.new_answers.*.order' => ['nullable', 'numeric'],

            /* New questions data */
            'new_questions' => ['nullable', 'array'],
            'new_questions.*.type' => ['nullable', 'string', 'in:text_input,multi_choice,single_choice'],
            'new_questions.*.text' => ['required', 'string', 'max:255'],
            'new_questions.*.image' => ['nullable', 'mimes:jpeg,jpg,png,gif', 'max:10000'],
            'new_questions.*.media_image_id' => ['nullable', 'numeric', 'exists:media,id'],
            'new_questions.*.required' => ['nullable', 'boolean'],
            'new_questions.*.order' => ['nullable', 'numeric'],
            'new_questions.*.show_alternative' => ['nullable', 'boolean'],
            'new_questions.*.alternative_label' => ['nullable', 'string', 'max:255'],
            'new_questions.*.new_answers.*.text' => ['required', 'string', 'max:255'],
            'new_questions.*.new_answers.*.image' => ['nullable', 'mimes:jpeg,jpg,png,gif', 'max:10000'],
            'new_questions.*.new_answers.*.media_image_id' => ['nullable', 'numeric', 'exists:media,id'],
            'new_questions.*.new_answers.*.correct' => ['nullable', 'boolean'],
            'new_questions.*.new_answers.*.order' => ['nullable', 'numeric'],
        ];
    }
}
