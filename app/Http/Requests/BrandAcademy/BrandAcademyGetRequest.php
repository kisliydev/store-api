<?php

namespace App\Http\Requests\BrandAcademy;

use App\Http\Requests\Request;

class BrandAcademyGetRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric',
    ];
}
