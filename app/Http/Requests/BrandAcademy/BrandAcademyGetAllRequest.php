<?php

namespace App\Http\Requests\BrandAcademy;

use App\Http\Requests\Request;
use App\Rules\SortRule;
use App\Rules\FilterRule;
use App\Models\AcademyItem;

class BrandAcademyGetAllRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $fields = (new AcademyItem)->getFillable();
        return [
            'channel_id' => ['required', 'numeric'],
            'page' => ['nullable', 'numeric'],
            'per_page' => ['numeric', 'nullable'],
            'sort' => ['nullable', 'string', new SortRule($fields)],
            'filter' => ['nullable', 'string', new FilterRule($fields)],
            'search' => ['nullable', 'string'],
        ];
    }
}
