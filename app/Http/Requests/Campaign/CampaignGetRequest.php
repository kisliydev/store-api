<?php

namespace App\Http\Requests\Campaign;

use App\Http\Requests\Request;

class CampaignGetRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric|exists:campaigns,id',
    ];
}
