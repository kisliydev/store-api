<?php

namespace App\Http\Requests\Campaign;

use App\Traits\CampaignValidator;
use App\Http\Requests\Request;

class DraftCampaignUpdateRequest extends Request
{
    use CampaignValidator;

    /**
     * @return bool
     */
    public function authorize()
    {
        return $this->campaignIs(['scheduled', 'draft']);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'  => ['required', 'numeric', 'exists:campaigns,id'],
            'name'  => ['required', 'string', 'max:100'],
            'started_at'  => ['sometimes', 'date_format:Y-m-d H:i:s'],
            'sale_started_at'  => ['sometimes', 'date_format:Y-m-d H:i:s'],
            'sale_finished_at' => ['sometimes', 'date_format:Y-m-d H:i:s'],
            'finished_at' => ['sometimes', 'date_format:Y-m-d H:i:s'],
            'status' => ['required', 'in:draft'],
        ];
    }
}
