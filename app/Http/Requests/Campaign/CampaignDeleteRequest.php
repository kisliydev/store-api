<?php

namespace App\Http\Requests\Campaign;

use App\Http\Requests\Request;
use App\Traits\CampaignValidator;

class CampaignDeleteRequest extends Request
{
    use CampaignValidator;

    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric|exists:campaigns,id',
    ];

    /**
     * @return bool
     */
    public function authorize()
    {
        return $this->campaignIs([
            'archived',
            'scheduled',
            'draft',
            'finished',
        ]);
    }
}
