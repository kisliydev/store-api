<?php

namespace App\Http\Requests\Campaign;

use App\Http\Requests\Request;
use App\Rules\CampaignEditRule;
use App\Traits\CampaignValidator;

class CampaignUpdateRequest extends Request
{
    use CampaignValidator;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'id'  => [ 'required', 'numeric', 'exists:campaigns,id'],
            'name'  => [ 'required', 'string', 'max:100'],
            'started_at'  => [ 'required', 'date_format:Y-m-d H:i:s', 'before:finished_at', new CampaignEditRule($this)],
            'sale_started_at'  => [ 'required', 'date_format:Y-m-d H:i:s', 'after_or_equal:started_at', 'before:sale_finished_at'],
            'sale_finished_at' => [ 'required', 'date_format:Y-m-d H:i:s', 'after:sale_started_at', 'before_or_equal:finished_at'],
            'finished_at' => [ 'required', 'date_format:Y-m-d H:i:s', 'after:started_at'],
            'status' => ['sometimes', 'string'],
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return $this->campaignIs(['pre_active', 'active', 'post_active', 'finished', 'scheduled', 'draft']);
    }
}
