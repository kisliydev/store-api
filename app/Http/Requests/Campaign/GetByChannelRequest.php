<?php

namespace App\Http\Requests\Campaign;

use App\Http\Requests\Request;
use App\Rules\SortRule;
use App\Rules\FilterRule;


class GetByChannelRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $sorters = [
            'name',
            'started_at',
            'finished_at',
            'sale_started_at',
            'sale_finished_at'
        ];
        $filters = ['status'];

        return [
            'channel_id' => ['required', 'numeric', 'exists:channels,id'],
            'page' => ['nullable', 'numeric'],
            'per_page' => ['numeric', 'nullable'],
            'sort' => ['nullable', 'string', new SortRule($sorters)],
            'filter' => ['nullable', 'string', new FilterRule($filters)],
            'search' => ['nullable', 'string'],
            'trashed' => 'nullable|boolean',
        ];
    }
}
