<?php

namespace App\Http\Requests\Campaign;

use App\Http\Requests\Request;
use App\Traits\CampaignValidator;
use App\Rules\AppLayoutFieldsFormat;

class CampaignSaveAppLayoutRequest extends Request
{
    use CampaignValidator;

    /**
     * Get the validation rules that apply to the request.
     * @todo After the finalization of the lists of app_layout fields
     *       add custom rule in order to check the fields' received content
     *       (uploaded file, hex-color string, etc.)
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required', 'numeric', 'exists:campaigns,id'],
            'data' => ['required', 'array', app()->make(AppLayoutFieldsFormat::class, ['request' => $this])],
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
       return $this->campaignIs(['pre_active', 'active', 'post_active', 'finished', 'scheduled', 'draft']);
    }
}
