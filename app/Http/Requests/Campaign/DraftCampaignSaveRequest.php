<?php

namespace App\Http\Requests\Campaign;

use App\Http\Requests\Request;

class DraftCampaignSaveRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id'  => ['required', 'numeric', 'exists:channels,id'],
            'name'        => ['required', 'string', 'max:100'],
            'started_at'  => ['sometimes', 'date_format:Y-m-d H:i:s'],
            'sale_started_at' => ['sometimes', 'date_format:Y-m-d H:i:s'],
            'sale_finished_at' => ['sometimes', 'date_format:Y-m-d H:i:s'],
            'finished_at' => ['sometimes', 'date_format:Y-m-d H:i:s'],
            'status' => ['required', 'in:draft'],
        ];
    }
}
