<?php

namespace App\Http\Requests\Campaign;

use App\Http\Requests\Request;
use App\Rules\CampaignSaveRule;

class CampaignSaveRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id'  => ['required', 'numeric', 'exists:channels,id'],
            'name'        => ['required', 'string', 'max:100'],
            'started_at'  => ['required', 'date_format:Y-m-d H:i:s', 'after_or_equal:now', 'before:finished_at', new CampaignSaveRule($this)],
            'sale_started_at' => ['required', 'date_format:Y-m-d H:i:s', 'after_or_equal:started_at', 'before:sale_finished_at'],
            'sale_finished_at' => ['required', 'date_format:Y-m-d H:i:s', 'after:sale_started_at', 'before_or_equal:finished_at'],
            'finished_at' => ['required', 'date_format:Y-m-d H:i:s', 'after:started_at'],
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return[
            'started_at.after_or_equal' => 'The campaign start cannot be in the past.'
        ];
    }
}
