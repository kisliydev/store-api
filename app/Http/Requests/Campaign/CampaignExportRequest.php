<?php

namespace App\Http\Requests\Campaign;

use App\Http\Requests\Request;
use App\Traits\CampaignValidator;

class CampaignExportRequest extends Request
{
    use CampaignValidator;

    /**
     * @var array
     */
    protected $customValidationRules = [
        'campaign_id' => 'required|numeric|exists:campaigns,id',
    ];

    /**
     * @return bool
     */
    public function authorize()
    {
        return $this->campaignIs(['archived'], $this->campaign_id);
    }
}
