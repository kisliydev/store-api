<?php

namespace App\Http\Requests\Point;

use App\Http\Requests\Request;

class PointSaveQuizRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'required|numeric',
        'object_id' => 'required|numeric',
    ];
}
