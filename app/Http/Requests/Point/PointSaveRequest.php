<?php

namespace App\Http\Requests\Point;

use App\Http\Requests\Request;
use App\Traits\PointsStatisticsValidator;

class PointSaveRequest extends Request
{
    use PointsStatisticsValidator;
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'required|numeric',
        'object_id' => 'required|numeric',
    ];

    /**
     * @return bool
     */
    public function authorize()
    {
        return $this->channelCampaignIs(['pre_active', 'active', 'post_active'])
            && $this->academyItemPublished()
            && ! $this->academyItemIsPassed();
    }
}
