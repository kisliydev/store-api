<?php

namespace App\Http\Requests\Point;

use App\Http\Requests\Request;

class PointGetRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric',
    ];
}
