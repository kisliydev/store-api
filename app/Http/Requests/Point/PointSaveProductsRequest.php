<?php

namespace App\Http\Requests\Point;

use App\Http\Requests\Request;
use App\Traits\PointsStatisticsValidator;

class PointSaveProductsRequest extends Request
{
    use PointsStatisticsValidator;
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'required|numeric',
        'products' => 'required|array',
        'products.*.object_id' => 'required|numeric',
        'products.*.units_number' => 'required|numeric|min:1',
    ];

    /**
     * @return bool
     */
    public function authorize()
    {
        return $this->channelCampaignIs(['active']);
    }
}
