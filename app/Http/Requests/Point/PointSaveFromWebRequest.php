<?php

namespace App\Http\Requests\Point;

use App\Http\Requests\Request;
use App\Models\PointsStatistic;

class PointSaveFromWebRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $types = implode(',', app()->make(PointsStatistic::class)->getObjectTypesList());

        return [
            'channel_id' => ['required', 'numeric', 'exists:channels,id'],
            'user_id' => ['required', 'numeric', 'exists:users,id'],
            'object_type' => ['required', 'string', "in:{$types}"],
            'object_id' => ['required','numeric'],
            'earned_points' => ['required', 'numeric'],
            'units_number' => ['required', 'numeric', 'min:1'],
        ];
    }
}
