<?php

namespace App\Http\Requests\Point;

use App\Http\Requests\Request;
use App\Rules\SortRule;
use App\Rules\FilterRule;
use App\Models\PointsStatistic;

class PointGetAllRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $filterable = (new PointsStatistic)->getFillable();
        $sortable = array_merge($filterable, ['id', 'created_at']);
        return [
            'channel_id' => ['required', 'numeric'],
            'page' => ['nullable', 'numeric'],
            'per_page' => ['numeric', 'nullable'],
            'sort' => ['nullable', 'string', new SortRule($sortable)],
            'filter' => ['nullable', 'string', new FilterRule($filterable)],
            'search' => ['nullable', 'string'],
        ];
    }
}
