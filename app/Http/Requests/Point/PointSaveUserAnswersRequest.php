<?php

namespace App\Http\Requests\Point;

use App\Http\Requests\Request;
use App\Traits\PointsStatisticsValidator;

class PointSaveUserAnswersRequest extends Request
{
    use PointsStatisticsValidator;
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'required|numeric',
        'object_id' => 'required|numeric',
        'answers' => 'required|array',
        'answers.*.question_id' => 'required|numeric',
        'answers.*.text' => 'required|string',
    ];

    /**
     * @return bool
     */
    public function authorize()
    {
        return $this->channelCampaignIs(['pre_active', 'active', 'post_active', 'finished'])
            && $this->academyItemPublished()
            && ! $this->academyItemIsPassed();
    }
}
