<?php

namespace App\Http\Requests\Point;

use App\Http\Requests\Request;
use App\Models\PointsStatistic;

class PointUpdateRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $types = implode(',', app()->make(PointsStatistic::class)->getObjectTypesList());

        return [
            'id' => ['required', 'numeric'],
            'user_id' => ['required', 'numeric'],
            'object_type' => ['required', 'string', "in:{$types}"],
            'object_id' => ['required', 'numeric'],
            'earned_points' => ['required', 'numeric'],
            'units_number' => ['required', 'numeric', 'min:1'],
        ];
    }
}
