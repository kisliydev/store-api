<?php

namespace App\Http\Requests\Point;

use App\Http\Requests\Request;

class PointBulkActionRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|array',
        'id.*' => 'required|numeric',
        'action' => 'required|string|in:delete',
    ];
}
