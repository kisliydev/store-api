<?php

namespace App\Http\Requests\League;

use App\Http\Requests\Request;

class LeagueUpdateRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric',
        'name' => 'required|string|max:100',
        'logo' => 'mimes:jpeg,jpg,png,gif|max:10000|nullable',
        'media_id' => 'nullable|numeric|exists:media,id',
        'remove_logo' => 'boolean|nullable',
    ];
}
