<?php

namespace App\Http\Requests\League;

use App\Http\Requests\Request;

class LeagueBulkActionRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|array',
        'id.*' => 'required|numeric',
        'action' => 'required|string|in:delete',
    ];
}
