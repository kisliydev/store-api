<?php

namespace App\Http\Requests\League;

use App\Http\Requests\Request;

class LeagueGetRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric',
    ];
}
