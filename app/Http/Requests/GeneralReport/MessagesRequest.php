<?php

namespace App\Http\Requests\GeneralReport;

use App\Http\Requests\Request;

class MessagesRequest extends Request
{
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric'],
            'per_page' => ['nullable', 'numeric'],
            'page' => ['nullable', 'numeric'],
        ];
    }
}
