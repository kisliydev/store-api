<?php

namespace App\Http\Requests\GeneralReport;

use App\Http\Requests\Request;

class InfoRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric'],
        ];
    }
}
