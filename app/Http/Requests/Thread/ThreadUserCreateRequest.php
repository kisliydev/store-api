<?php

namespace App\Http\Requests\Thread;

use App\Http\Requests\Request;
use App\Traits\AttachmentValidator;

class ThreadUserCreateRequest extends Request
{
    use AttachmentValidator;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge([
            'channel_id' => ['required', 'numeric', 'exists:channels,id'],
            'subject' => ['required', 'string'],
            'data' => ['nullable', 'json'],
            'message_content' => ['required', 'string'],
        ], $this->getAttachmentRules());
    }
}
