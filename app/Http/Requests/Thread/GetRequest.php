<?php

namespace App\Http\Requests\Thread;

use App\Http\Requests\Request;

class GetRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'thread_id' => ['required', 'numeric', 'exists:threads,id'],
        ];
    }
}
