<?php

namespace App\Http\Requests\Thread;

use App\Http\Requests\Request;

class DeleteUsersFromDealerRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric', 'exists:channels,id'],
            'users' => ['required', 'array'],
            'users.*' => ['required', 'numeric', 'exists:users,id'],
            'reason' => ['required','string'],
        ];
    }
}
