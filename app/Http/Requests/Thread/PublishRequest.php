<?php

namespace App\Http\Requests\Thread;

use App\Http\Requests\Request;
use App\Rules\ScheduledThreadRule;

class PublishRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'thread_id' => ['required', 'integer', new ScheduledThreadRule()]
        ];
    }
}
