<?php

namespace App\Http\Requests\Thread;

use App\Http\Requests\Request;

class ThreadUpdateRequest extends Request
{
    /**
     * @var array
     * @deprecated status 'close'
     */
    protected $customValidationRules = [
        'id' => 'required|numeric|exists:threads,id',
        'status' => 'required|string|in:open,closed,done,read_by_user,read_by_admin',
    ];
}
