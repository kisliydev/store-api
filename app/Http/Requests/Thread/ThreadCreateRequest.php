<?php

namespace App\Http\Requests\Thread;

use App\Http\Requests\Request;
use App\Rules\RecipientRule;
use App\Models\Recipient;
use App\Traits\AttachmentValidator;

class ThreadCreateRequest extends Request
{
    use AttachmentValidator;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge([
            'channel_id'      => ['required', 'numeric', 'exists:channels,id'],
            'recipients'      => ['required', 'string', new RecipientRule((new Recipient())->recipientTypes)],
            'subject'         => ['required', 'string'],
            'data'            => ['nullable', 'json'],
            'message_content' => ['required', 'string'],
            'publish_at'      => ['nullable', 'date_format:Y-m-d H:i:s', 'after_or_equal:now'],
        ], $this->getAttachmentRules());
    }
}
