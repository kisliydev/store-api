<?php

namespace App\Http\Requests\Thread;

use App\Http\Requests\Request;
use App\Rules\RecipientRule;
use App\Models\Recipient;
use App\Traits\AttachmentValidator;
use App\Rules\ScheduledThreadRule;

class UpdateRequest extends Request
{
    use AttachmentValidator;

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return array_merge([
            'thread_id'       => ['required', 'numeric', new ScheduledThreadRule()],
            'recipients'      => ['required', 'string', new RecipientRule((new Recipient())->recipientTypes)],
            'subject'         => ['required', 'string'],
            'message_content' => ['required', 'string'],
            'publish_at'      => ['required', 'date_format:Y-m-d H:i:s', 'after_or_equal:now'],
        ], $this->getUpdateAttachmentsRules());
    }
}
