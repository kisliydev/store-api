<?php

namespace App\Http\Requests\Thread;

use App\Http\Requests\Request;
use App\Rules\SortRule;
use App\Rules\FilterRule;
use App\Models\Thread;

class ThreadListRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric'],
            'per_page' => ['nullable', 'numeric'],
            'simple_paginate' => ['nullable', 'boolean'],
            'page' => ['nullable', 'numeric'],
            'sort' => ['nullable', 'string', new SortRule((new Thread)->getFillable())],
            'filter' => ['nullable', 'string', new FilterRule(['keyword', 'district_id', 'dealer_id', 'user_id', 'league_id'])],
            'search' => ['nullable', 'string'],
            'with_pagination' => ['nullable', 'boolean'],
        ];
    }
}
