<?php

namespace App\Http\Requests\ProductAssortmentCategory;

use App\Http\Requests\Request;
use App\Rules\SortRule;
use App\Rules\FilterRule;

class GetAllRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $sortable = [
            'id',
            'created_at',
            'name',
            'order',
        ];

        return [
            'channel_id' => ['required', 'numeric'],
            'page' => ['nullable', 'numeric'],
            'per_page' => ['numeric', 'nullable'],
            'sort' => ['nullable', 'string', new SortRule($sortable)],
            'search' => ['nullable', 'string'],
        ];
    }
}
