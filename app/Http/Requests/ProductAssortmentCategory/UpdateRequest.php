<?php

namespace App\Http\Requests\ProductAssortmentCategory;

use App\Http\Requests\Request;

class UpdateRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric|exists:product_assortment_categories,id',
        'name' => 'required|string|max:100',
        'order' => 'nullable|numeric',
    ];
}
