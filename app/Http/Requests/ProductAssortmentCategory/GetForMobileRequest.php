<?php

namespace App\Http\Requests\ProductAssortmentCategory;

use App\Http\Requests\Request;

class GetForMobileRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'required|numeric',
    ];
}
