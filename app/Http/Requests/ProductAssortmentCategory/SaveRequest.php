<?php

namespace App\Http\Requests\ProductAssortmentCategory;

use App\Http\Requests\Request;

class SaveRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'required|numeric|exists:channels,id',
        'name' => 'required|string|max:100',
        'order' => 'nullable|numeric',
    ];
}
