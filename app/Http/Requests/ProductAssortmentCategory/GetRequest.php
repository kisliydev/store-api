<?php

namespace App\Http\Requests\ProductAssortmentCategory;

use App\Http\Requests\Request;

class GetRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric',
    ];
}
