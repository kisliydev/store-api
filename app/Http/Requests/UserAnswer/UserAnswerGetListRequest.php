<?php

namespace App\Http\Requests\UserAnswer;

use App\Http\Requests\Request;
use App\Rules\SortRule;
use App\Rules\FilterRule;
use App\Models\DoneAcademyItem;

class UserAnswerGetListRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $fields = (new DoneAcademyItem)->getFillable();
        return [
            'channel_id' => ['required', 'numeric', 'exists:channels,id'],
            'page' => ['nullable', 'numeric'],
            'per_page' => ['numeric', 'nullable'],
            'sort' => ['nullable', 'string', new SortRule($fields)],
            'filter' => ['nullable', 'string', new FilterRule($fields)],
        ];
    }
}
