<?php

namespace App\Http\Requests\UserAnswer;

use App\Http\Requests\Request;

class UserAnswerGetRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'survey_id' => 'required|numeric|exists:academy_items,id',
        'user_id' => 'required|numeric|exists:users,id',
    ];
}
