<?php

namespace App\Http\Requests\Media;

use App\Http\Requests\Request;

class ChannelsListRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'country_brand_id' => ['required', 'numeric'],
            'per_page' => ['nullable', 'numeric'],
            'simple_paginate' => ['nullable', 'boolean'],
            'page' => ['nullable', 'numeric']
        ];
    }
}
