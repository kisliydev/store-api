<?php

namespace App\Http\Requests\Media;

use App\Http\Requests\Request;
use App\Rules\JsonIntegersArrayRule;

class DestroyRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required', 'array'],
            'id.*' => ['required', 'numeric'],
        ];
    }
}
