<?php

namespace App\Http\Requests\Media;

use App\Http\Requests\Request;
use App\Rules\ArrayRule;

class StoreRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric'],
            'images' => ['required', new ArrayRule()],
            'images.*' => ['required', 'mimes:jpeg,jpg,png,gif', 'max:10000']
        ];
    }
}
