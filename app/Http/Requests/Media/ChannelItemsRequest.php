<?php

namespace App\Http\Requests\Media;

use App\Http\Requests\Request;

class ChannelItemsRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric'],
            'type' => ['required', 'in:attachments,app_layout,brand_items,logos,uploads,products'],
            'per_page' => ['nullable', 'numeric'],
            'simple_paginate' => ['nullable', 'boolean'],
            'page' => ['nullable', 'numeric']
        ];
    }
}
