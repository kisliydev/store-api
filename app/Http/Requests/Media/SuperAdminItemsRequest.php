<?php

namespace App\Http\Requests\Media;

class SuperAdminItemsRequest extends ChannelItemsRequest
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'type' => ['required', 'in:avatars,brand_logos'],
            'per_page' => ['nullable', 'numeric'],
            'simple_paginate' => ['nullable', 'boolean'],
            'page' => ['nullable', 'numeric']
        ];
    }
}
