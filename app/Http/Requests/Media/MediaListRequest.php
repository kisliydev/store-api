<?php

namespace App\Http\Requests\Media;

use App\Http\Requests\Request;

class MediaListRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'per_page' => ['nullable', 'numeric'],
            'simple_paginate' => ['nullable', 'boolean'],
            'page' => ['nullable', 'numeric'],
        ];
    }
}
