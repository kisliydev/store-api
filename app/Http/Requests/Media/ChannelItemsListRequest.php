<?php

namespace App\Http\Requests\Media;

use App\Http\Requests\Request;

class ChannelItemsListRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric'],
        ];
    }
}
