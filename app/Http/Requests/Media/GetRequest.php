<?php

namespace App\Http\Requests\Media;

use App\Http\Requests\Request;

class GetRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric|exists:media,id',
    ];
}
