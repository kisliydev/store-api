<?php

namespace App\Http\Requests\Dashboard;

use App\Http\Requests\Request;

class ChannelDataGetRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric'],
            'date_from' => ['required_with:date_to', 'date_format:Y-m-d H:i:s', 'before:date_to'],
            'date_to' => ['required_with:date_from', 'date_format:Y-m-d H:i:s', 'after:date_from'],
            'league_id' => 'nullable|integer|exists:leagues,id',
        ];
    }
}
