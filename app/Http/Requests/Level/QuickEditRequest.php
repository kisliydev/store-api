<?php

namespace App\Http\Requests\Level;

use App\Http\Requests\Request;

class QuickEditRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id'                => ['required', 'numeric', 'exists:levels,id'],
            'points'            => ['required', 'numeric', 'min:0'],
            'use_weight_points' => ['required', 'boolean'],
        ];
    }
}
