<?php

namespace App\Http\Requests\Level;

use App\Http\Requests\Request;
use App\Rules\SortRule;

class GetListRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id'      => ['required', 'numeric', 'exists:channels,id'],
            'per_page'        => ['nullable', 'numeric'],
            'simple_paginate' => ['nullable', 'boolean'],
            'page'            => ['nullable', 'numeric'],
            'sort'            => ['nullable', 'string', new SortRule(['id', 'title', 'created_at', 'points'])],
            'search'          => ['nullable', 'string'],
        ];
    }
}
