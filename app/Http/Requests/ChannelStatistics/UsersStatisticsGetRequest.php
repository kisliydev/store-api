<?php

namespace App\Http\Requests\ChannelStatistics;

use App\Http\Requests\Request;
use App\Rules\SortRule;
use App\Rules\FilterRule;
use App\Services\Statistics\UserStatisticsService;

class UsersStatisticsGetRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $stat = new UserStatisticsService();

        return [
            'channel_id' => ['required', 'numeric', 'exists:channels,id'],
            'page' => ['nullable', 'numeric'],
            'per_page' => ['numeric', 'nullable'],
            'sort' => ['nullable', 'string', new SortRule($stat->getSortable())],
            'filter' => ['nullable', 'string', new FilterRule($stat->getFilterable())],
            'search' => ['nullable', 'string'],
            'date_from' => ['required_with:date_to', 'date_format:Y-m-d H:i:s', 'before:date_to'],
            'date_to' => ['required_with:date_from', 'date_format:Y-m-d H:i:s', 'after:date_from'],
            'league_id' => 'nullable|integer|exists:leagues,id',
        ];
    }
}
