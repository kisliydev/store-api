<?php

namespace App\Http\Requests\ChannelStatistics;

use App\Http\Requests\Request;

class UserStatisticsGetMobileRequest extends Request
{
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric', 'exists:channels,id'],
            'user_id' => ['nullable', 'numeric', 'exists:users,id'],
        ];
    }
}
