<?php

namespace App\Http\Requests\ChannelStatistics;

use App\Http\Requests\Request;
use App\Rules\DealerBelongsToChannelRule;

class DealerStatisticsGetMobileRequest extends Request
{
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric', 'exists:channels,id'],
            'dealer_id' => ['nullable', 'numeric', new DealerBelongsToChannelRule($this)],
            'league_id' => 'integer|exists:leagues,id',
        ];
    }
}
