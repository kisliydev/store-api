<?php

namespace App\Http\Requests\ChannelStatistics;

use App\Http\Requests\Request;

class ExportSurveyAnswersRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'required|numeric|exists:channels,id',
    ];
}
