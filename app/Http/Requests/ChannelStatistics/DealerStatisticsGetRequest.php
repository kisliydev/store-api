<?php

namespace App\Http\Requests\ChannelStatistics;

use App\Http\Requests\Request;
use App\Rules\SortRule;
use App\Rules\FilterRule;
use App\Services\Statistics\DealerStatisticsService;

class DealerStatisticsGetRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $stat = new DealerStatisticsService();

        return [
            'channel_id' => ['required', 'numeric', 'exists:channels,id'],
            'page' => ['nullable', 'numeric'],
            'per_page' => ['numeric', 'nullable'],
            'sort' => ['nullable', 'string', new SortRule($stat->getSortable())],
            'filter' => ['nullable', 'string', new FilterRule($stat->getFilterable())],
            'search' => ['nullable', 'string'],
            'date_from' => ['required_with:date_to', 'date_format:Y-m-d H:i:s', 'before:date_to'],
            'date_to' => ['required_with:date_from', 'date_format:Y-m-d H:i:s', 'after:date_from'],
        ];
    }
}
