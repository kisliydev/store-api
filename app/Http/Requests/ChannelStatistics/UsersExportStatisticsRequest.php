<?php

namespace App\Http\Requests\ChannelStatistics;

use App\Http\Requests\Request;
use App\Rules\FilterRule;
use App\Services\Statistics\UserStatisticsService;

class UsersExportStatisticsRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $stat = new UserStatisticsService();

        return [
            'channel_id' => ['required', 'numeric', 'exists:channels,id'],
            'filter' => ['nullable', 'string', new FilterRule($stat->getFilterable())],
            'date_from' => ['required_with:date_to', 'date_format:Y-m-d H:i:s', 'before:date_to'],
            'date_to' => ['required_with:date_from', 'date_format:Y-m-d H:i:s', 'after:date_from'],
        ];
    }
}
