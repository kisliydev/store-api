<?php

namespace App\Http\Requests\ChannelStatistics;

use App\Http\Requests\Request;

class DealerStatisticsGetListMobileRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric', 'exists:channels,id'],
            'get_average' => ['nullable', 'boolean'],
            'league_id' => 'nullable|integer|exists:leagues,id',
        ];
    }
}
