<?php

namespace App\Http\Requests\District;

use App\Http\Requests\Request;
use App\Rules\SortRule;
use App\Rules\FilterRule;
use App\Models\District;

class DistrictGetAllRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $filters = (new District)->getFillable();
        $sorters = array_merge($filters, ['dealers_count', 'users_count']);
        return [
            'channel_id' => ['required', 'numeric'],
            'page' => ['nullable', 'numeric'],
            'per_page' => ['numeric', 'nullable'],
            'sort' => ['nullable', 'string', new SortRule($sorters)],
            'filter' => ['nullable', 'string', new FilterRule($filters)],
            'search' => ['nullable', 'string'],
        ];
    }
}
