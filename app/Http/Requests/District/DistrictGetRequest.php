<?php

namespace App\Http\Requests\District;

use App\Http\Requests\Request;

class DistrictGetRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric',
    ];

}
