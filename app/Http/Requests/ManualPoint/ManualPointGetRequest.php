<?php

namespace App\Http\Requests\ManualPoint;

use App\Http\Requests\Request;

class ManualPointGetRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric|exists:manual_points,id'
    ];
}
