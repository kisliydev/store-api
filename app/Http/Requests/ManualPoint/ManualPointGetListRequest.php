<?php

namespace App\Http\Requests\ManualPoint;

use App\Http\Requests\Request;
use App\Rules\SortRule;
use App\Models\ManualPoint;

class ManualPointGetListRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $fields = (new ManualPoint)->getFillable();
        return [
            'channel_id' => ['required', 'numeric', 'exists:channels,id'],
            'page' => ['nullable', 'numeric'],
            'per_page' => ['numeric', 'nullable'],
            'sort' => ['nullable', 'string', new SortRule($fields)],
            'search' => ['nullable', 'string'],
        ];
    }
}
