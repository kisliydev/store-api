<?php

namespace App\Http\Requests\ManualPoint;

use App\Http\Requests\Request;
use App\Rules\RecipientRule;
use App\Models\Recipient;

class ManualPointSaveRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric', 'exists:channels,id'],
            'recipients' => ['required', 'string', new RecipientRule((new Recipient())->recipientTypes)],
            'title' => ['required','string'],
            'description' => ['required','string'],
            'points' => ['required','numeric'],
        ];
    }
}
