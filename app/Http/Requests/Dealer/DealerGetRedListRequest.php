<?php

namespace App\Http\Requests\Dealer;

use App\Http\Requests\Request;

class DealerGetRedListRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'required|numeric|exists:channels,id',
        'league_id' => 'nullable|integer|exists:leagues,id',
    ];
}
