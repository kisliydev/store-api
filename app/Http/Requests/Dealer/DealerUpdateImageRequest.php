<?php

namespace App\Http\Requests\Dealer;

use App\Http\Requests\Request;

class DealerUpdateImageRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id'   => 'required|numeric|exists:dealers,id',
        'logo' => 'mimes:jpeg,jpg,png,gif|max:10000|nullable',
    ];
}
