<?php

namespace App\Http\Requests\Dealer;

use App\Http\Requests\Request;
use App\Rules\SortRule;
use App\Rules\FilterRule;
use App\Models\Dealer;

class DealerGetAllRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $sortable   = ['name', 'created_at', 'id', 'location'];
        $filterable = ['district_id', 'league_id'];
        return [
            'channel_id' => ['required', 'numeric'],
            'page' => ['nullable', 'numeric'],
            'per_page' => ['numeric', 'nullable'],
            'sort' => ['nullable', 'string', new SortRule($sortable)],
            'filter' => ['nullable', 'string', new FilterRule($filterable)],
            'search' => ['nullable', 'string'],
        ];
    }
}
