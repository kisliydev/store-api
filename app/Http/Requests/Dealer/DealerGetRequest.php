<?php

namespace App\Http\Requests\Dealer;

use App\Http\Requests\Request;

class DealerGetRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric',
    ];
}
