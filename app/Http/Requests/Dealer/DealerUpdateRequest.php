<?php

namespace App\Http\Requests\Dealer;

use App\Http\Requests\Request;

class DealerUpdateRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric|exists:dealers,id',
        'district_id' => 'numeric|nullable|exists:districts,id',
        'league_id' => 'numeric|nullable|exists:leagues,id',
        'name' => 'required|string|max:100',
        'location' => 'nullable|string|max:100',
        'logo' => 'mimes:jpeg,jpg,png,gif|max:10000|nullable',
        'media_id' => 'nullable|numeric|exists:media,id',
        'remove_logo' => 'boolean|nullable',
    ];
}
