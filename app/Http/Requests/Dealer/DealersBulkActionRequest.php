<?php

namespace App\Http\Requests\Dealer;

use App\Http\Requests\Request;

class DealersBulkActionRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|array',
        'id.*' => 'required|numeric',
        'action' => 'required|string|in:delete',
    ];
}
