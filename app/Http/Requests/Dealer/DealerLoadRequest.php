<?php

namespace App\Http\Requests\Dealer;

use App\Http\Requests\Request;
use App\Rules\ExcelFile;

class DealerLoadRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric', 'exists:channels,id'],
            'dealers' => ['required', new ExcelFile],
        ];
    }
}