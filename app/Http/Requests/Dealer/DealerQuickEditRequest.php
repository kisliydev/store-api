<?php

namespace App\Http\Requests\Dealer;

use App\Http\Requests\Request;

class DealerQuickEditRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric|exists:dealers,id',
        'weight_points' => 'required|numeric|min:0.01|max:99.9',
    ];
}
