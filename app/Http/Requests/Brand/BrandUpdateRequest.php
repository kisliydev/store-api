<?php

namespace App\Http\Requests\Brand;

use App\Http\Requests\Request;

class BrandUpdateRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id'          => 'required|numeric',
        'name'        => 'required|string|max:100',
        'legal_name'  => 'nullable|string|max:100',
        'owner'       => 'nullable|string|max:100',
        'description' => 'nullable|string|max:500',
        'logo'        => 'mimes:jpeg,jpg,png,gif|max:10000|nullable',
        'media_id'    => 'nullable|numeric|exists:media,id',
        'remove_logo' => 'boolean|nullable',
    ];
}
