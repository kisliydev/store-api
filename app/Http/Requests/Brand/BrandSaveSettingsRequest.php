<?php

namespace App\Http\Requests\Brand;

use App\Http\Requests\Request;
use App\Rules\BrandUniqueSlugRule;

class BrandSaveSettingsRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'id'                     => ['required', 'numeric'],
            'onesignal_app_id'       => ['required', 'string', 'max:100'],
            'onesignal_rest_api_key' => ['required', 'string', 'max:100'],
            'slug'                   => ['required', 'string', 'max:65', 'regex:/^[a-z0-9\-]+$/', new BrandUniqueSlugRule()],
            'app_name'               => ['nullable', 'string', 'max:100'],
            'app_url_apple'          => ['nullable', 'string', 'max:2048'],
            'app_url_google'         => ['nullable', 'string', 'max:2048'],
        ];
    }
}
