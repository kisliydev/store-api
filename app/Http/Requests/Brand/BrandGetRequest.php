<?php

namespace App\Http\Requests\Brand;

use App\Http\Requests\Request;

class BrandGetRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric',
    ];
}