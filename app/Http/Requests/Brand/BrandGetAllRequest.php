<?php

namespace App\Http\Requests\Brand;

use App\Http\Requests\Request;
use App\Rules\SortRule;
use App\Rules\FilterRule;
use App\Models\Brand;

class BrandGetAllRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $fields = (new Brand)->getFillable();
        return [
            'page' => ['nullable', 'numeric'],
            'per_page' => ['numeric', 'nullable'],
            'sort' => ['nullable', 'string', new SortRule($fields)],
            'filter' => ['nullable', 'string', new FilterRule($fields)],
            'search' => ['nullable', 'string'],
        ];
    }
}
