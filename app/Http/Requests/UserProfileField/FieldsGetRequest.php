<?php

namespace App\Http\Requests\UserProfileField;

use App\Http\Requests\Request;

class FieldsGetRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'required|numeric|exists:channels,id',
    ];
}
