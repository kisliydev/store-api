<?php

namespace App\Http\Requests\UserProfileField;

use App\Http\Requests\Request;

class FieldsSaveRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'required|numeric|exists:channels,id',
        'fields' => 'required|array',
        'fields.*.id' => 'nullable|numeric',
        'fields.*.remove' => 'nullable|boolean',
        'fields.*.type' => 'required|string|in:text,checkboxes,radiobuttons,dropdown',
        'fields.*.label' => 'nullable|string|max:100',
        'fields.*.options' => 'required_unless:fields.*.type,text|array',
        'fields.*.options.*' => 'required|string|max:100',
    ];
}
