<?php

namespace App\Http\Requests\Message;

use App\Http\Requests\Request;
use App\Traits\AttachmentValidator;
use App\Rules\OpenThreadRule;

class StoreRequest extends Request
{
    use AttachmentValidator;

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge([
            'thread_id' => ['required', 'numeric', new OpenThreadRule($this)],
            'content' => ['required', 'string'],
            'from' => ['required', 'string', 'in:admin,user'],
        ], $this->getAttachmentRules());
    }
}
