<?php

namespace App\Http\Requests\Message;

use App\Http\Requests\Request;
use App\Rules\SortRule;
use App\Rules\FilterRule;
use App\Models\Message;

class ListRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $sortable = array_merge(['created_at'], (new Message)->getFillable());
        return [
            'thread_id' => ['required', 'numeric', 'exists:threads,id'],
            'per_page' => ['nullable', 'numeric'],
            'page' => ['nullable', 'numeric'],
            'sort' => ['nullable', 'string', new SortRule($sortable)],
            'filter' => ['nullable', 'string', new FilterRule((new Message)->getFillable())],
            'with_pagination' => ['nullable', 'boolean'],
        ];
    }
}
