<?php

namespace App\Http\Requests\Socket;

use App\Http\Requests\Request;

class BroadcastRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'token' => 'required|string',
        'message' => 'required|json',
        'receivers' => 'required|json',
        'channel_id' => 'required|numeric|exists:channels,id',
    ];
}
