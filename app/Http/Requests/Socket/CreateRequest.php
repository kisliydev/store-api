<?php

namespace App\Http\Requests\Socket;

use App\Http\Requests\Request;

class CreateRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'url' => 'required|string',
        'token' => 'required|string',
    ];
}
