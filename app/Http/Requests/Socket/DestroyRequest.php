<?php

namespace App\Http\Requests\Socket;

use App\Http\Requests\Request;

class DestroyRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'url' => 'required|string',
    ];
}
