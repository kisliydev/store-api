<?php

namespace App\Http\Requests\Socket;

use App\Http\Requests\Request;

class BroadcastMessageRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'type' => 'required|string|in:new_thread,new_message,new_point',
        'channel_id' => 'required|numeric|exists:channels,id',
        'item_id' => 'required|numeric',
    ];
}
