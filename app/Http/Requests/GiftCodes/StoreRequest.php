<?php

namespace App\Http\Requests\GiftCodes;

use App\Http\Requests\Request;

class StoreRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric'],
            'title' => ['required', 'string', 'max:100'],
            'description' => ['required', 'string', 'max:500'],
        ];
    }
}
