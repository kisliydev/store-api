<?php

namespace App\Http\Requests\GiftCodes;

use App\Http\Requests\Request;
use App\Rules\JsonIntegersArrayRule;

class DestroyRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return ['codes' => ['required', 'json', new JsonIntegersArrayRule()]];
    }
}
