<?php

namespace App\Http\Requests\GiftCodes;

use App\Http\Requests\Request;

class ShowRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric'],
            'per_page' => ['nullable', 'numeric'],
            'simple_paginate' => ['nullable', 'boolean'],
            'page' => ['nullable', 'numeric'],
        ];
    }
}
