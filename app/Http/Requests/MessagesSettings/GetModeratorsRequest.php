<?php

namespace App\Http\Requests\MessagesSettings;

use App\Http\Requests\Request;

class GetModeratorsRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'required|numeric|exists:channels,id',
    ];
}
