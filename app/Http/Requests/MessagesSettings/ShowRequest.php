<?php

namespace App\Http\Requests\MessagesSettings;

use App\Http\Requests\Request;

class ShowRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'required|numeric',
    ];
}
