<?php

namespace App\Http\Requests\ACL;

use App\Http\Requests\Request;

class ACLRestoreUserRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'user_id' => 'required|numeric|exists:users,id',
        'user_role' => 'required|string|in:internal_admin,brand_admin,global_manager,app_user,hq',
    ];
}
