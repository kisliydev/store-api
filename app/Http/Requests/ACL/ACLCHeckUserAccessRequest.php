<?php

namespace App\Http\Requests\ACL;

use App\Http\Requests\Request;

class ACLCHeckUserAccessRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'to' => 'required|string|in:brand,country_brand,channel',
        'item_id' => 'required|numeric',
    ];
}
