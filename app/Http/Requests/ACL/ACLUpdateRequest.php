<?php

namespace App\Http\Requests\ACL;

use App\Http\Requests\Request;

class ACLUpdateRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'permissions' => 'required|array',
        'permissions.internal_admin' => 'required|array',
        'permissions.internal_admin.*' => 'required|string|regex:/^[a-zA-Z0-9\-\_]+$/',
        'permissions.brand_admin' => 'required|array',
        'permissions.brand_admin.*' => 'required|string|regex:/^[a-zA-Z0-9\-\_]+$/',
        'permissions.global_manager' => 'required|array',
        'permissions.global_manager.*' => 'required|string|regex:/^[a-zA-Z0-9\-\_]+$/',
        'permissions.brand_manager' => 'required|array',
        'permissions.brand_manager.*' => 'required|string|regex:/^[a-zA-Z0-9\-\_]+$/',
        'permissions.app_user' => 'required|array',
        'permissions.app_user.*' => 'required|string|regex:/^[a-zA-Z0-9\-\_]+$/',
        'permissions.hq' => 'required|array',
        'permissions.hq.*' => 'required|string|regex:/^[a-zA-Z0-9\-\_]+$/',
    ];
}
