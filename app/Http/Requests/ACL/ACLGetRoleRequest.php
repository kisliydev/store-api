<?php

namespace App\Http\Requests\ACL;

use App\Http\Requests\Request;

class ACLGetRoleRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'role_name' => 'required|string|in:internal_admin,brand_admin,global_manager,app_user,hq,super_admin',
    ];
}
