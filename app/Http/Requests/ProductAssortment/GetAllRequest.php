<?php

namespace App\Http\Requests\ProductAssortment;

use App\Http\Requests\Request;
use App\Rules\SortRule;
use App\Rules\FilterRule;

class GetAllRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $sortable = [
            'id',
            'created_at',
            'name',
            'description',
            'sku',
            'link',
            'order',
        ];

        $filterable = [
            'category_id',
        ];

        return [
            'channel_id' => ['required', 'numeric'],
            'page' => ['nullable', 'numeric'],
            'per_page' => ['numeric', 'nullable'],
            'sort' => ['nullable', 'string', new SortRule($sortable)],
            'filter' => ['nullable', 'string', new FilterRule($filterable)],
            'search' => ['nullable', 'string'],
        ];
    }
}
