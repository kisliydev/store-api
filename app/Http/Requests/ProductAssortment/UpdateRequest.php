<?php

namespace App\Http\Requests\ProductAssortment;

use App\Http\Requests\Request;

class UpdateRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric|exists:product_assortments,id',
        'category_id' => 'required|numeric|exists:product_assortment_categories,id',
        'name' => 'required|string|max:100',
        'description' => 'nullable|string|max:500',
        'link' => 'nullable|string',
        'sku' => 'nullable|string',
        'thumbnail' => 'mimes:jpeg,jpg,png,gif|max:10000|nullable',
        'remove_thumbnail' => 'boolean|nullable',
        'media_id' => 'nullable|numeric|exists:media,id',
        'order' => 'nullable|numeric',
    ];
}
