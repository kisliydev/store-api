<?php

namespace App\Http\Requests\ProductAssortment;

use App\Http\Requests\Request;

class GetRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'id' => 'required|numeric',
    ];
}
