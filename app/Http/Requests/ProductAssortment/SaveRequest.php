<?php

namespace App\Http\Requests\ProductAssortment;

use App\Http\Requests\Request;

class SaveRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'required|numeric|exists:channels,id',
        'category_id' => 'required|numeric|exists:product_assortment_categories,id',
        'name' => 'required|string|max:100',
        'description' => 'nullable|string|max:500',
        'link' => 'nullable|string',
        'sku' => 'nullable|string',
        'thumbnail' => 'mimes:jpeg,jpg,png,gif|max:10000|nullable',
        'media_id' => 'nullable|numeric|exists:media,id',
        'order' => 'nullable|numeric',
    ];
}
