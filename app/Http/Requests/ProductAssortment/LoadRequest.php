<?php

namespace App\Http\Requests\ProductAssortment;

use App\Http\Requests\Request;
use App\Rules\ExcelFile;

class LoadRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric', 'exists:channels,id'],
            'items' => ['required', new ExcelFile],
        ];
    }
}
