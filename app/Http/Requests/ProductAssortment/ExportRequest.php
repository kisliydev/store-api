<?php

namespace App\Http\Requests\ProductAssortment;

use App\Http\Requests\Request;

class ExportRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'required|numeric',
    ];
}
