<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

class UsersGetRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'ids' => 'required|array',
        'ids.*' => 'required|numeric|exists:users,id',
    ];
}
