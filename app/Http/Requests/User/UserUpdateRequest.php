<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;
use App\Rules\UserUniqueData;

class UserUpdateRequest extends Request
{
   
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required', 'numeric', 'exists:users,id'],
            'password' => ['nullable', 'string', 'min:6', 'confirmed'],
            'avatar' => ['mimes:jpeg,jpg,png,gif', 'max:10000', 'nullable'],
            'remove_avatar' => ['boolean', 'nullable'],
            'media_id' => ['nullable', 'numeric', 'exists:media,id'],
            'first_name' => ['required', 'string', 'max:100'],
            'last_name' => ['required', 'string', 'max:100'],
            'email' => ['required', 'string', 'email', 'max:100', new UserUniqueData($this)],
            'phone' => ['nullable', 'string', 'regex:/^\+[\d]{5,15}$/'],
            'country_id' => ['nullable', 'numeric', 'exists:countries,id'],
            'skype' => ['nullable', 'string', 'max:100'],
        ];
    }
}
