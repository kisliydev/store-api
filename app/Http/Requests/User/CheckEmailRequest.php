<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

class CheckEmailRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = ['email' => 'required|string|email|max:100|unique:users'];
}
