<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;
use App\Rules\UserUniqueData;
use App\Models\Channel;
use App\Models\Dealer;
use App\Models\Position;
use App\Rules\UserRegistrationRule;

class UserUpdateMobileRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric', new UserRegistrationRule($this, new Channel(), new Dealer(), new Position())],
            'dealer_id' => ['required', 'numeric'],
            'position_id' => ['required', 'numeric'],

            'id' => ['required', 'numeric', 'exists:users,id'],
            'password' => ['nullable', 'string', 'min:6', 'confirmed'],
            'avatar' => ['mimes:jpeg,jpg,png,gif', 'max:10000', 'nullable'],
            'remove_avatar' => ['string', 'nullable'],
            'first_name' => ['required', 'string', 'max:100'],
            'last_name' => ['required', 'string', 'max:100'],
            'email' => ['nullable', 'string', 'email', 'max:100', new UserUniqueData($this)],
            'country_id' => ['nullable', 'numeric', 'exists:countries,id'],
            'phone' => ['nullable', 'string', 'regex:/^\+[\d]{5,15}$/', new UserUniqueData($this)],
        ];
    }

    /**
     * @return bool
     */
    public function authorize()
    {
        return $this->id == auth()->user()->id;
    }
}
