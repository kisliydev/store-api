<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;
use App\Rules\UserInChannel;

class UserSaveAdditionalDataRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric', new UserInChannel($this)],
            'id' => ['required', 'numeric', 'exists:users,id'],
            'additional_data' => ['required', 'array'],
            'additional_data.*.field_id' => ['required', 'numeric', 'exists:profile_fields,id'],
            'additional_data.*.value' => ['required', 'string'],
        ];
        
    }
}
