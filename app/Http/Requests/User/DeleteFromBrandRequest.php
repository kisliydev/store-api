<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

class DeleteFromBrandRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'brand_id' => 'required|numeric|exists:brands,id',
        'ids' => 'required|array',
        'ids.*' => 'required|numeric|exists:users,id',
    ];
}
