<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

class DeleteFromCountryBrandRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'country_brand_id' => 'required|numeric|exists:channels,id',
        'ids' => 'required|array',
        'ids.*' => 'required|numeric|exists:users,id',
    ];
}
