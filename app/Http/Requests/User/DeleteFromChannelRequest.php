<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

class DeleteFromChannelRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'required|numeric|exists:channels,id',
        'ids' => 'required|array',
        'ids.*' => 'required|numeric|exists:users,id',
    ];
}
