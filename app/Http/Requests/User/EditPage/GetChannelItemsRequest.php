<?php

namespace App\Http\Requests\User\EditPage;

use App\Http\Requests\Request;

class GetChannelItemsRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'required|numeric|exists:channels,id',
    ];
}
