<?php

namespace App\Http\Requests\User\EditPage;

use App\Http\Requests\Request;

class GetCountryBrandsRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'brand_id' => 'required|numeric|exists:brands,id',
    ];
}
