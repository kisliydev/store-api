<?php

namespace App\Http\Requests\User\EditPage;

use App\Http\Requests\Request;

class GetChannelsRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'country_brand_id' => 'required|numeric|exists:country_brands,id',
    ];
}
