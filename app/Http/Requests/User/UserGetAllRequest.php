<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;
use App\Rules\SortRule;
use App\Rules\FilterRule;
use App\Models\User;

class UserGetAllRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $fields =  (new User)->getFillable();
        $sorters = array_merge($fields, ['created_at']);
        $filters = array_merge($fields, ['role']);
        return [
            'page' => ['nullable', 'numeric'],
            'trashed' => ['nullable', 'boolean'],
            'per_page' => ['numeric', 'nullable'],
            'sort' => ['nullable', 'string', new SortRule($sorters)],
            'filter' => ['nullable', 'string', new FilterRule($filters)],
            'search' => ['nullable', 'string'],
        ];
    }
}
