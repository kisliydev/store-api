<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

class UserGetChannelsRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'brand_id' => 'required|numeric|exists:brands,id',
    ];
}
