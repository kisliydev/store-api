<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

class UserSaveRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'password' => 'required|string|min:6|confirmed',
        'avatar' => 'mimes:jpeg,jpg,png,gif|max:10000|nullable',
        'media_id' => 'nullable|numeric|exists:media,id',
        'first_name' => 'required|string|max:100',
        'last_name' => 'required|string|max:100',
        'email' => 'required|string|email|max:100|unique:users',
        'country_id' => 'nullable|numeric|exists:countries,id',
        'phone' => 'nullable|string|regex:/^\+[\d]{5,15}$/',
        'skype' => 'nullable|string|max:100',
        'skip_confirmation' => 'nullable|boolean',
        'role' => 'required|string|in:super_admin,internal_admin,brand_admin,global_manager,brand_manager,app_user',

        'brands' => 'required_if:role,brand_admin,global_manager|array',
        'brands.*.brand_id' => 'required|numeric|exists:brands,id',

        'country_brands' => 'required_if:role,brand_manager|array',
        'country_brands.*.country_brand_id' => 'required|numeric|exists:country_brands,id',

        'channels' => 'required_if:role,app_user|array',
        'channels.*.channel_id' => 'required|numeric|exists:channels,id',
        'channels.*.position_id' => 'nullable|numeric|exists:positions,id',
        'channels.*.dealer_id' => 'nullable|numeric|exists:dealers,id',
    ];
}
