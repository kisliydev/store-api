<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;
use App\Rules\ExcelFile;

class UserLoadRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'channel_id' => ['required', 'numeric', 'exists:channels,id'],
            'users' => ['required', new ExcelFile],
        ];
    }
}
