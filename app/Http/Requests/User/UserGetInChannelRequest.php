<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;
use App\Rules\SortRule;
use App\Rules\FilterRule;
use App\Models\User;

class UserGetInChannelRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $fields = (new User)->getFillable();
        $sorters = array_merge($fields, ['created_at', 'full_name']);

        return [
            'channel_id' => ['required', 'numeric', 'exists:channels,id'],
            'page' => ['nullable', 'numeric'],
            'per_page' => ['numeric', 'nullable'],
            'sort' => ['nullable', 'string', new SortRule($sorters)],
            'filter' => ['nullable', 'string', new FilterRule($fields)],
            'search' => ['nullable', 'string'],
        ];
    }
}
