<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

class UserAddHQRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'required|numeric|exists:channels,id',
        'users' => 'required|array',
        'users.*.password' => 'required|string|min:6',
        'users.*.first_name' => 'required|string|max:100',
        'users.*.last_name' => 'required|string|max:100',
        'users.*.email' => 'required|string|email|max:100|unique:users',
    ];
}
