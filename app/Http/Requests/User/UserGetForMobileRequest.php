<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

class UserGetForMobileRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'required|numeric|exists:channels,id',
    ];
}
