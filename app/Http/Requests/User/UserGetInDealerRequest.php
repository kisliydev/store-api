<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;
use App\Rules\SortRule;
use App\Rules\FilterRule;
use App\Models\User;

class UserGetInDealerRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        $fields = (new User)->getFillable();
        $sorters = array_merge($fields, ['created_at']);
        return [
            'dealer_id' => ['required', 'numeric'],
            'page' => ['nullable', 'numeric'],
            'per_page' => ['numeric', 'nullable'],
            'sort' => ['nullable', 'string', new SortRule($sorters)],
            'filter' => ['nullable', 'string', new FilterRule($fields)],
            'search' => ['nullable', 'string'],
        ];
    }
}
