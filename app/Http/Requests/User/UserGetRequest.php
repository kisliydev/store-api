<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

class UserGetRequest extends Request
{
    /**
     * @var array
     */
    protected $customValidationRules = [
        'channel_id' => 'nullable|numeric|exists:channels,id',
        'id' => 'required|numeric|exists:users,id',
    ];
}
