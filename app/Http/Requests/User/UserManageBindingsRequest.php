<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

class UserManageBindingsRequest extends Request
{
    /**
     * @return array
     */
    public function rules()
    {
        return [
            'id' => ['required', 'numeric', 'exists:users,id'],
            'role' => ['required', 'string', 'in:super_admin,internal_admin,brand_admin,global_manager,brand_manager,app_user'],

            'brands' => ['required_if:role,brand_admin,global_manager', 'array'],
            'brands.*.brand_id' => ['required', 'numeric', 'exists:brands,id'],

            'country_brands' => ['required_if:role,brand_manager', 'array'],
            'country_brands.*.country_brand_id' => ['required', 'numeric', 'exists:country_brands,id'],

            'channels' => ['required_if:role,app_user', 'array'],
            'channels.*.channel_id' => ['required', 'numeric', 'exists:channels,id'],
            'channels.*.remove_from_channel' => ['boolean', 'nullable'],
            'channels.*.position_id' => ['nullable', 'numeric', 'exists:positions,id'],
            'channels.*.dealer_id' => ['nullable', 'numeric', 'exists:dealers,id'],
        ];
    }
}
