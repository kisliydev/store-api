<?php

namespace App\Traits;

use App\Models\Channel;

trait MessageShortcodes
{
    use Shortcodes;

    /**
     * Callback for %%app_url_android%% shortcode
     * @param null $args
     * @param array $additionalData
     * @return string
     */
    public function getAppUrlAndroidShortcodeContent($args = null, array $additionalData)
    {
        $url = $this->getBrandSettingsData($additionalData['thread_id'],'app_url_google');

        return empty($url) ? '' : "<a href=\"{$url}\" target=\"_blank\">Android</a>";
    }

    /**
     * Callback for %%app_url_android%% shortcode
     * @param null $args
     * @param array $additionalData
     * @return string
     */
    public function getAppUrlIosShortcodeContent($args = null, array $additionalData)
    {
        $url = $this->getBrandSettingsData($additionalData['thread_id'],'app_url_apple');

        return empty($url) ? '' : "<a href=\"{$url}\" target=\"_blank\">iPhone</a>";
    }

    /**
     * Callback for %%app_name%% shortcode
     * @param null $args
     * @param array $additionalData
     * @return null|string
     */
    public function getAppNameShortcodeContent($args = null, array $additionalData)
    {
        return $this->getBrandSettingsData($additionalData['thread_id'],'app_name');
    }

    /**
     * @param int $threadID
     * @param string $key
     * @return null|string
     */
    protected function getBrandSettingsData(int $threadID, string $key)
    {
        $channel = Channel::join('threads', function($join) use ($threadID) {
            $join->on("threads.channel_id", '=', "channels.id")
                ->where("threads.id", '=', $threadID);
        })->with('countryBrand.brand')->first();

        if (empty($channel)) {
            return null;
        }

        return $channel->countryBrand->brand->settings[$key] ?? '';
    }
}
