<?php
namespace App\Traits;

trait ChannelMetaShortcodes
{
    use Shortcodes;

    /**
     * Callback for %%brand_name%% shortcode
     * @return mixed
     */
    public function getBrandNameShortcodeContent()
    {
        return $this->_getBrandData('name');
    }

    /**
     * Callback for %%brand_legal_name%% shortcode
     * @return mixed
     */
    public function getBrandLegalNameShortcodeContent()
    {
        return $this->_getBrandData('legal_name');
    }

    /**
     * Callback for %%brand_owner%% shortcode
     * @return mixed
     */
    public function getBrandOwnerShortcodeContent()
    {
        return $this->_getBrandData('owner');
    }

    /**
     * Callback for %%store_name%% shortcode
     * @return mixed
     */
    public function getStoreNameShortcodeContent()
    {
        return $this->_getChannelData('store_name');
    }

    /**
     * Callback for %%store_legal_name%% shortcode
     * @return mixed
     */
    public function getStoreLegalNameShortcodeContent()
    {
        return $this->_getChannelData('store_legal_name');
    }

    /**
     * Callback for %%store_owner%% shortcode
     * @return mixed
     */
    public function getStoreOwnerShortcodeContent()
    {
        return $this->_getChannelData('store_owner');
    }

    /**
     * @param string $key
     * @return mixed
     */
    private function _getBrandData(string $key)
    {
        return $this->_channel->countryBrand->brand->$key;
    }

    /**
     * @param string $key
     * @return mixed
     */
    private function _getChannelData(string $key)
    {
        return $this->_channel->$key;
    }
}
