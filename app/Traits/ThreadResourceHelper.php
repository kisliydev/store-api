<?php

namespace App\Traits;


trait ThreadResourceHelper
{
    /**
     * @param array $recipients
     * @return array
     */
    protected function performRecipients(array $recipients)
    {
        $data = [
            'all'       => false,
            'users'     => [],
            'dealers'   => [],
            'districts' => [],
            'leagues'   => [],
        ];

        foreach($recipients as $recipient) {
            if ($recipient['all']) {
                $data['all'] = true;
            } elseif (!empty($recipient['user'])) {
                $data['users'][] = [
                    'id'        => $recipient['user']['id'],
                    'full_name' => $recipient['user']['full_name'],
                    'avatar'    => $recipient['user']['avatar'] ?? null,
                ];
            } elseif (!empty($recipient['dealer'])) {
                $data['dealers'][] = [
                    'id'   => $recipient['dealer']['id'],
                    'name' => $recipient['dealer']['name'],
                    'logo' => $recipient['dealer']['logo'] ?? null,
                ];
            } elseif (!empty($recipient['district'])) {
                $data['districts'][] = [
                    'id'   => $recipient['district']['id'],
                    'name' => $recipient['district']['name'],
                    'logo' => $recipient['district']['logo'] ?? null,
                ];
            } elseif (!empty($recipient['league'])) {
                $data['leagues'][] = [
                    'id'   => $recipient['league']['id'],
                    'name' => $recipient['league']['name'],
                    'logo' => $recipient['league']['logo'] ?? null,
                ];
            }
        }

        return $data;
    }

    /**
     * @param array $counters
     * @return array
     */
    protected function performRecipientsCount(array $counters)
    {
        $data = [
            'all'       => 0,
            'users'     => 0,
            'dealers'   => 0,
            'districts' => 0,
            'leagues'   => 0,
        ];

        foreach($counters as $item) {

            $key = 'all' == $item['object_type'] ? 'all' : str_replace('_id', 's', $item['object_type']);

            if (!$key) {
                continue;
            }

            $data[$key] = $item['count'];
        }

        return $data;
    }


    /**
     * @param array $lastMessage
     * @return array
     */
    protected function performLastMessage(array $lastMessage)
    {
        unset($lastMessage['t_id']);
        unset($lastMessage['m_id']);

        $lastMessage['author'] = [
            'id'        => $lastMessage['author']['id'],
            'full_name' => $lastMessage['author']['full_name'],
            'avatar'    => $lastMessage['author']['avatar'] ?? null,
            'dealer'    => $lastMessage['author']['dealer'] ?? null,
            'position'  => $lastMessage['author']['position'] ?? null,
        ];

        return $lastMessage;
    }
}