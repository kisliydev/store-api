<?php

namespace App\Traits;

use App\Http\Requests\Request;
use App\Models\Campaign;
use Illuminate\Auth\Access\AuthorizationException;

trait CampaignValidator
{
    private $_message;

    /**
     * @param array $statuses
     * @param int|null $campaignID
     * @return bool
     */
    public function campaignIs(array $statuses, int $campaignID = null)
    {
        $campaignID = $campaignID ?? $this->id;

        /**
         * let the validator use 'exists' rule to generate 422 error
         */
        if (empty($campaignID)) {
            return true;
        }

        $campaign = Campaign::find($campaignID);

        /**
         * let the validator use 'exists' rule to generate 422 error
         */
        if (empty($campaign)) {
            return true;
        }

        $this->_message = "Can't manage campaigns with the status {$campaign->status}";
        return in_array($campaign->status, $statuses);
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @return \Closure
     */
    public function getOverlappedCampaignQueryClosure(Request $request)
    {
        return function($query) use ($request) {
            $query->where(function($subWhere) use ($request) {
                $subWhere->whereBetween('started_at', [$request->started_at, $request->finished_at])
                    ->orWhereBetween('finished_at', [$request->started_at, $request->finished_at]);
            })->orwhere(function($subWhere) use ($request) {
                $subWhere->where('started_at', '>=', $request->started_at)
                    ->where('finished_at', '<=', $request->finished_at);
            })->orwhere(function($subWhere) use ($request) {
                $subWhere->where('started_at', '<=', $request->started_at)
                    ->where('finished_at', '>=', $request->finished_at);
            });
        };
    }

    /**
     * Handle a failed authorization attempt.
     *
     * @return void
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    protected function failedAuthorization()
    {
        throw new AuthorizationException($this->_message);
    }
}
