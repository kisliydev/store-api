<?php

namespace App\Traits;


trait FullTextSearch
{

    /**
     * @param $query
     * @param string $keyword
     */
    public function addFullTextSearch($query, string $keyword)
    {
        $search = $this->_getFullTextSearchKeyword($keyword);
        if ($search) {
            $query->orWhereRaw("MATCH(first_name, last_name) AGAINST('{$search}' IN BOOLEAN MODE) > 0");
        }
    }

    /**
     * @param string $keyword
     * @return string
     */
    private function _getFullTextSearchKeyword(string $keyword)
    {
        $arg = array_filter(explode(' ', $keyword));

        if (! $arg) {
            return '';
        }

        $arg =array_map(function($word) {
            return "\"+{$word}\"";
        }, $arg);

        return implode(' ', $arg);
    }
}