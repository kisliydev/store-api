<?php

namespace App\Traits;

use Spatie\Permission\Traits\HasRoles as SpatieHasRoles;
use Illuminate\Support\Collection;
use Spatie\Permission\Contracts\Permission;
use Spatie\Permission\Exceptions\PermissionDoesNotExist;

trait HasRoles
{
    use SpatieHasRoles;

    /**
     * @param \Spatie\Permission\Contracts\Permission $permission
     * @return bool
     */
    protected function permissionDenied(Permission $permission)
    {
        return !! $this->denied_permissions()
            ->where('permission', $permission->name)
            ->first();
    }

    /**
     * @return mixed
     */
    public function denied_permissions()
    {
        return $this->hasMany('App\Models\DeniedPermission');
    }

    /**
     * Return all permissions the directory coupled to the model.
     */
    public function getDirectPermissions(): Collection
    {
        return $this->permissions()->whereNotIn('name', $this->denied_permissions()->pluck('permission')->toArray());
    }

    /**
     * Return all the permissions the model has via roles.
     */
    public function getPermissionsViaRoles(): Collection
    {
        return $this->load('roles', 'roles.permissions')
            ->roles->flatMap(function ($role) {
                return $role->permissions()
                    ->whereNotIn('name', $this->denied_permissions()->pluck('permission')->toArray());
            })->sort()->values();
    }

    /**
     * @param $permission
     * @param null $guardName
     * @return bool
     */
    public function hasPermissionTo($permission, $guardName = null): bool
    {
        return !! array_intersect((array)$permission, $this->acl);
    }

    /**
     * @param $permission
     * @param array $arguments
     * @return bool
     */
    public function can($permission, $arguments = [])
    {
        return $this->hasPermissionTo($permission);
    }

    /**
     * @param string $role
     * @return bool
     */
    public function canManageUserByRole(string $role)
    {
        switch($role) {
            case 'super_admin':
                return 'super_admin' == $this->role;
            case 'internal_admin':
            case 'brand_admin':
            case 'global_manager':
            case 'brand_manager':
                return in_array($this->role, ['super_admin', 'internal_admin']);
            case 'app_user':
            case 'hq':
                return true;
            default:
                return false;
        }
    }

}
