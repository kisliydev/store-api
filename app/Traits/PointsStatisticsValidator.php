<?php

namespace App\Traits;

use App\Models\Campaign;
use App\Models\AcademyItem;
use App\Models\DoneAcademyItem;
use Illuminate\Auth\Access\AuthorizationException;

trait PointsStatisticsValidator
{
    /**
     * @var string
     */
    private $_message;

    /**
     * @param array $statuses
     * @param int|null $channelID
     * @return bool
     */
    public function channelCampaignIs(array $statuses, int $channelID = null)
    {
        $channelID = $channelID ?? $this->channel_id;

        /**
         * let the validator use 'exists' rule to generate 422 error
         */
        if (empty($channelID)) {
            return true;
        }

        $this->_message = "This action is authorized only for campaigns with following statuses: " . implode(', ', $statuses);

        return Campaign::current()
            ->where('channel_id', $channelID)
            ->whereIn('campaigns.status', $statuses)
            ->first();
    }

    /**
     * @return mixed
     */
    public function academyItemPublished() {
        $this->_message = "You can't pass unpublished item";

        return AcademyItem::where('id', $this->object_id)
            ->where('status', 'published')
            ->first();
    }

    /**
     * @return mixed
     */
    public function academyItemIsPassed() {

        $this->_message = "An item has already been passed";

        return DoneAcademyItem::where('item_id', $this->object_id)
            ->where('user_id', auth()->user()->id)
            ->first();
    }

    /**
     * Handle a failed authorization attempt.
     *
     * @return void
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    protected function failedAuthorization()
    {
        throw new AuthorizationException($this->_message);
    }
}
