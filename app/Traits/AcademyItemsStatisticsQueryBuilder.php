<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

trait AcademyItemsStatisticsQueryBuilder
{
    public function getAcademyItemsTotalCountQuery(string $type, int $channelID)
    {
        return DB::table($this->ai)
            ->select(DB::raw("COUNT({$this->ai}.id) as count"))
            ->where("{$this->ai}.channel_id", $channelID)
            ->where("{$this->ai}.type", $type)
            ->where('status', 'published')
            ->whereNotNull('started_at')
            ->whereDate('started_at', '<=', Carbon::today()->toDateString());
    }

    public function getAcademyItemsPassedCountQuery(string $type, int $channelID)
    {
        return $this->getAcademyItemsTotalCountQuery($type, $channelID)
            ->join($this->dai, function($join) {
                $join->on("{$this->dai}.item_id", "=", "{$this->ai}.id")
                    ->where("{$this->dai}.user_id", auth()->user()->id);
            });
    }

}
