<?php
namespace App\Traits;

use PHPExcel_Style_Color;
use PHPExcel_Cell;

trait ExcelStyling
{
    /**
     * @var string
     */
    protected $percentageFormat = "0.0%";

    /**
     * @param $row
     */
    public function addHeaderStyles($row)
    {
        $row->setBackground('#cccccc');
        $row->setFontWeight('bold');
        $row->setAlignment('center');
    }

    /**
     * @param $row
     */
    public function addFooterStyle($row)
    {
        $row->setBackground('#eeeeee');
    }

    /**
     * @param $sheet
     * @param string|array $columns
     * @param int $index
     */
    public function setPercentFormat($sheet, int $index, $columns)
    {
        foreach((array)$columns as $column ) {
            $sheet->getStyle("{$column}{$index}")
                ->getNumberFormat()
                ->setFormatCode($this->percentageFormat);
        }
    }

    /**
     * @param $sheet
     * @param int $index
     * @param array $columns
     */
    public function setColors($sheet, int $index, array $columns)
    {
        $alert = new PHPExcel_Style_Color();
        $alert->setRGB('ff0000');

        $ok = new PHPExcel_Style_Color();
        $ok->setRGB('296ab0');

        foreach((array)$columns as $column => $color) {
            $sheet->getStyle("{$column}{$index}")->getFont()->setColor($$color);
        }
    }

    /**
     * @param $sheet
     * @param int $index
     * @param string $label
     * @param bool $highlight
     * @return int
     */
    public function addTableSignature($sheet, int $index, string $label, bool $highlight = true)
    {
        $sheet->row($index, [$label]);
        $sheet->row($index, function ($row) {
            $row->setFontWeight('bold');
            $row->setAlignment('center');
        });
        if ($highlight) {
            $this->setColors($sheet, $index, ['A' => 'ok']);
        }
        return $index;
    }

    /**
     * @param $sheet
     * @param int $index
     * @param array $headers
     * @return int
     */
    public function addTableHeaders($sheet, int $index, array $headers)
    {
        $colNumber = -1;
        foreach($headers as $header => $offset) {
            $colNumber++;
            $columnStartIndex = PHPExcel_Cell::stringFromColumnIndex($colNumber);
            $columnFinishIndex = PHPExcel_Cell::stringFromColumnIndex($colNumber + $offset);
            $sheet->cell("{$columnStartIndex}{$index}", function($cell) use ($header) {
                $cell->setValue($header);
            });
            if ($offset) {
                $range = "{$columnStartIndex}{$index}:{$columnFinishIndex}{$index}";
                $sheet->mergeCells($range);
            }

            $colNumber += $offset;
        }
        $sheet->row($index, function ($row) {
            $this->addHeaderStyles($row);
        });
        return $index;
    }

    /**
     * @param $sheet
     * @param int $lastColumn
     * @param int $index
     */
    public function setTextWrapper($sheet, int $lastColumn, int $index)
    {
        $column = PHPExcel_Cell::stringFromColumnIndex($lastColumn);
        $sheet->getStyle("A1:{$column}{$index}")->getAlignment()->setWrapText(true);
    }
}