<?php

namespace App\Traits;

use App\Rules\ArrayRule;
use App\Rules\AttachmentsMimeTypeRule;

trait AttachmentValidator
{
    /**
     * @return array
     */
    public function getAttachmentRules()
    {
        return [
            'attachments'         => ['nullable', new ArrayRule()],
            'attachments.*.media' => ['nullable', 'max:50000', new AttachmentsMimeTypeRule()],
            'attachments.*.link'  => ['nullable', 'string', 'max:2048'],
        ];
    }

    /**
     * @return array
     */
    public function getUpdateAttachmentsRules()
    {
        return array_merge([
            'attachments.*.id'     => ['nullable', 'integer', 'exists:attachments,id'],
            'attachments.*.remove' => ['nullable', 'boolean'],
        ], $this->getAttachmentRules());
    }
}
