<?php

namespace App\Traits;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;
use Tymon\JWTAuth\JWTAuth;
use App\Jobs\SocketNotification;

trait SocketNotifications
{
    /**
     * @param array $payload
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function notify(array $payload)
    {
        /**
         * in order to implement an ability to setup
         * separate app instance that won't be observed by AWS Load Balancer
         * and push all socket notifications via this instance.
         */
        if ('local' == config('socket.socket_notification')) {
            $this->notifyLocaly($payload);
        } else {
            $this->notifyRemotely($payload);
        }
    }

    /**
     * @param array $payload
     */
    public function notifyLocaly(array $payload)
    {
        if (! empty($payload['type']) && 'new_point' == $payload['type']) {
            $this->_pushMessage(array_merge($payload,['recipients_type' => 'channel_dashboard']));
            return;
        }

        foreach(['admin', 'user'] as $recipient) {
            $this->_pushMessage(array_merge($payload,['recipients_type' => $recipient]));
        }
    }

    /**
     * @param array $payload
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function notifyRemotely(array $payload)
    {
        try {
            $client = new Client();

            $res = $client->request('POST', config('app.socket_url') . 'api/socket/broadcast', [
                //'debug' => true,
                'headers' => [
                    'Authorization' => 'Bearer '. app()->make(JWTAuth::class)->getToken(),
                    'accept'        => 'application/json',
                    'content-type'  => 'application/x-www-form-urlencoded',
                    'cache-control' => 'no-cache',
                ],
                'form_params' => $payload,
            ]);

            if ($res->getStatusCode() === 200) {
                return true;
            } else {
                Log::info("Can't broadcast message: {$res->getBody()}");
                return false;
            }
        } catch (\Exception $e) {
            Log::info("Can't broadcast message: {$e->getMessage()}");
            return false;
        }
    }

    /**
     * @param array $payload
     */
    private function _pushMessage(array $payload)
    {
        /*
         * direct notification
         */
        $instance = new SocketNotification($payload);
        app()->call([$instance, 'handle']);

        /*
         * notification via queues
         */
        //dispatch(new SocketNotification($payload))->onQueue('high');
    }
}
