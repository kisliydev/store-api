<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 4/26/18
 * Time: 10:43 AM
 */

namespace App\Traits;

trait Tick
{
    /**
     * @var int
     */
    private $_stepLength = 3600; // tick will be changed every hour

    /**
     * @return int
     */
    public function getCurrentTick()
    {
        return intval(strtotime('now')/$this->_stepLength);
    }

    /**
     * @return int
     */
    public function getPreviousTick()
    {
        return intval((strtotime('now') - $this->_stepLength)/$this->_stepLength);
    }
}
