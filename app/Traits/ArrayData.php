<?php

namespace App\Traits;

trait ArrayData
{
    /**
     * @param array $arr
     * @return bool
     */
    public function isNumericKeysArray($arr)
    {
        if (! is_array($arr) || is_array(reset($arr))) {
            return false;
        }
        $count = count($arr);
        return $count && array_keys($arr) == range(0, $count - 1);
    }

    /**
     * @param array $targetArr
     * @param array $needleArr
     * @return array
     */
    public function replaceRecursive(array $targetArr, array $needleArr)
    {
        foreach ($targetArr as $key => $value) {
            if(array_key_exists($key, $needleArr)) {
                if ($this->isNumericKeysArray($needleArr[$key])) {
                    $targetArr[$key] = array_unique($needleArr[$key]);
                } elseif (is_array($targetArr[$key])) {
                    $targetArr[$key] = $this->replaceRecursive($targetArr[$key], $needleArr[$key]);
                } else {
                    $targetArr[$key] = $needleArr[$key];
                }
            }
        }

        return $targetArr;
    }

    /**
     * @param array $targetArr
     * @param array $needleArr
     * @return array
     */
    public function fillRecursive(array $targetArr, array $needleArr)
    {
        foreach($needleArr as $key => $value) {
            if (is_array($value)) {
                if (!isset($targetArr[$key])) {
                    $targetArr[$key] = $value;
                } else {
                    $targetArr[$key] = $this->fillRecursive($targetArr[$key], $value);
                }
            } elseif (!isset($targetArr[$key])) {
                $targetArr[$key] = $value;
            }
        }
        return $targetArr;
    }
}
