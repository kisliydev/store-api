<?php

namespace App\Traits;

use App\Models\Brand;
use App\Models\Channel;
use App\Models\User;

use App\Mail\ForgotPassword;
use App\Mail\EmailVerification;
use App\Mail\EmailVerified;

use App\Transformers\Messages\MessagesSettingsDecodeTransformer;
use App\Transformers\Messages\MessagesSettingsPopulateEmailsTransformer;
use Illuminate\Support\Facades\DB;
use Mail;

trait Notifications
{
    /**
     * @param User $user
     * @param string|null $case
     */
    public function sendAuthEmail(User $user, string $case = null)
    {
        if (! starts_with($user->email, 'missing_')) {

            $brand     = $this->_getBrand();
            $brandName = empty($brand) ? '' : $brand->name;

            switch ($case) {
                case 'forgotPassword':
                    $email = new ForgotPassword(
                        new User([
                            'forgot_password_token' => $user->forgot_password_token,
                            'first_name' => $user->first_name,
                        ]),
                        ['forgot_mail_token' => $this->base64url_encode($user->email)],
                        false,
                        $brandName
                    );
                    break;
                case 'forgotPasswordMobile':
                    $email = new ForgotPassword(
                        new User([
                            'forgot_password_token' => $user->forgot_password_token,
                            'first_name' => $user->first_name,
                        ]),
                        [],
                        true,
                        $brandName
                    );
                    break;
                case 'register':
                    if ($user->verified) {
                        return;
                    }

                    $email = new EmailVerification(
                        new User([
                           'email_token' => $user->email_token,
                            'first_name' => $user->first_name,
                        ]),
                        $brandName,
                        $this->_getBrandUrl($brand)
                    );
                    break;
                default:
                    $email = new EmailVerified(
                        new User(['first_name' => $user->first_name]),
                        $brandName
                    );
                    break;
            }

            Mail::to($user->email)->queue(($email)->onQueue(config('queue.tube.high')));
        }
    }

    /**
     * @param string $class
     * @param int $channelID
     * @param null $data
     */
    public function maybeNotifyModerators(string $class, int $channelID, $data = null)
    {
        $settings = $this->_getChannelSettings($channelID);

        if (! $settings || ! $settings->send_email_notification_to_moderators || ! $settings->send_to || ! $settings->reply_from) {
            return;
        }

        $this->_send($class, $settings->send_to, $settings, $data);
    }

    /**
     * @param $users
     * @param string $class
     * @param int $channelID
     * @param null $data
     */
    public function maybeNotifyUsers($users, string $class, int $channelID, $data = null)
    {
        $settings = $this->_getChannelSettings($channelID);

        if (! $settings || ! $settings->send_email_notification_to_users || ! $settings->reply_from) {
            return;
        }

        $this->_send($class, $users, $settings, $data);
    }

    /**
     * @param $class
     * @param $receivers
     * @param $settings
     * @param null $data
     */
    private function _send($class, $receivers, $settings, $data = null)
    {
        $from = $settings->reply_from->first() ?? null;

        foreach ($receivers as $receiver) {

            if (empty($receiver->email)) {
                continue;
            }
            Mail::to($receiver->email)
                ->queue((new $class(new User($receiver->toArray()), $from, $data, $this->_getBrandName()))->onQueue(config('queue.tube.high')));
        }
    }

    /**
     * @param int $channelID
     * @return mixed
     */
    private function _getChannelSettings(int $channelID)
    {
        return app()->make(MessagesSettingsDecodeTransformer::class)
            ->chain(Channel::select('messages_settings')->where('id', $channelID)->get())
            ->transformer(app()->make(MessagesSettingsPopulateEmailsTransformer::class))
            ->endOfchain()->first();
    }

    /**
     * @param int $channelID
     * @return mixed
     */
    private function _setMessageAuthorID(int $channelID)
    {
        $isFromChannelSettings = Channel::select(
                DB::raw("json_extract(messages_settings, '$.reply_from') as reply_from")
            )->where('id', $channelID)->first()->reply_from ?? null;

        if ($isFromChannelSettings) {
            return trim($isFromChannelSettings, '"');
        }


        $currentlyLoggedIn = auth()->user();

        return $currentlyLoggedIn ? $currentlyLoggedIn->id : null;
    }

    /**
     * @return null
     */
    private function _getBrand()
    {
        $request = request()->all();
        $brand   = null;

        if (!empty($request['brand_id'])) {
            $brand = Brand::find($request['brand_id']);
        } elseif (!empty($request['country_brand_id'])) {
            $cb = 'country_brands';
            $b  = 'brands';
            $brand = Brand::join($cb, function($join) use ($request, $b, $cb) {
                $join->on("{$cb}.brand_id", '=', "{$b}.id")
                    ->where("{$cb}.id", '=', $request['country_brand_id']);
            })->first();
        } elseif (!empty($request['channel_id'])) {
            $channel = Channel::with('countryBrand.brand')->find($request['channel_id']);
            $brand   = empty($channel) ? null : $channel->countryBrand->brand;
        }

        return $brand;
    }

    /**
     * @return null
     */
    private function _getBrandName()
    {
        $brand = $this->_getBrand();
        return empty($brand) ? '' : $brand->name;
    }

    /**
     * @param $brand
     * @return \Illuminate\Config\Repository|mixed|null|string|string[]
     */
    private function _getBrandUrl($brand)
    {
        $url = config('app.spa_url');

        if (empty($brand) || empty($brand->settings) || empty($brand->settings['slug'])) {
            return $url;
        }

        return preg_replace(
            "/(http(s)?):\/\/(admin\.)?/",
            "$1://{$brand->settings['slug']}.",
            $url
        );
    }
}
