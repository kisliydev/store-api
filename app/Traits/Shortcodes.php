<?php

namespace App\Traits;

/**
 * Trait Shortcodes
 * @uses to replace shortodes like %%shortcode_name parameters%% with the apropriate data
 * @example
 * 1. add trait to necessary class definition
 * 2. register method
 *      public function getTestShortcodeContent() { return "Test"; }
 * 3. use %%test_shortcode%% wherever you need
 * 4. to replace shortcodes
 *    a. use parseShortcodes method for strings
 *         $content = $this->parseShortcodes($rawContent);
 *    b. use parseShortcodesRecursive methods for arrays
 *         $arrayContent = $this->parseShortcodesRecursive($rawArrayContent);
 * 5. Using paramenters:
 *    a.  %%test_shortcode 1 2 3 4%% ------>
 *      public function getTestShortcodeContent($args)
 *      {
 *           var_dump($args); // [1,2,3,4]
 *      }
 *    b. %%test_shortcode param1=1 param1=2 param1=3%% ------>
 *      public function getTestShortcodeContent($args)
 *      {
 *           var_dump($args); // ['param1' => 1, 'param2' => 2, 'param3' => 3]
 *      }
 * 6. In order to use additional data inside the shortcode handler you can pass them in the second parameter of
 *    parseShortcodes or parseShortcodesRecursive methods.
 *
 *    $shortcodeData = 'somedata';
 *    $data = $this->parseShortcodes('string with %%some_data%%', $shortcodeData);
 *
 *    public function getSomeDataShortcodeContent($args = null, $shortcodeData)
 *    {
 *          return $shortcodeData;
 *    }
 */

trait Shortcodes
{
    /**
     * @param array $arg
     * @param null $additionalData
     * @return array
     */
    public function parseShortcodesRecursive(array $arg, $additionalData = null)
    {
        foreach($arg as $key => $data) {
            $callback = is_array($data) ? 'parseShortcodesRecursive' : 'parseShortcodes';
            $arg[$key] = $this->$callback($data, $additionalData);
        }
        return $arg;
    }

    /**
     * @param $content
     * @param null $additionalData
     * @return null|string|string[]
     */
    public function parseShortcodes($content, $additionalData = null)
    {
        return is_string($content) ? $this->_replaceShortcodes($content, $additionalData) : $content;
    }

    /**
     * @param string $content
     * @param null $additionalData
     * @return null|string|string[]
     */
    private function _replaceShortcodes(string $content, $additionalData = null)
    {
        return preg_replace_callback(
            "/\%\%(?P<shortcode>[a-zA-Z][a-zA-Z-_]{1,})[\s]*(?P<params>(.*?))?\%\%/",
            function($matches) use ($additionalData) {
                $callback = $this->_getCallbackName($matches['shortcode']);

                /* return content without any changes */
                if (!method_exists($this, $callback)) {
                    return $matches[0];
                }

                return empty($matches['params']) ? $this->$callback(null, $additionalData) : $this->$callback($this->_getParams($matches['params']), $additionalData);
            },
            $content
        );
    }

    /**
     * @param string $shortcode
     * @return mixed
     */
    private function _getCallbackName(string $shortcode)
    {
        $shortcode = preg_replace("/[-_]+/", " ", strtolower($shortcode));
        $shortcode = str_replace(' ', '', ucwords($shortcode));
        return "get{$shortcode}ShortcodeContent";
    }

    /**
     * @param string $rawParams
     * @return array
     */
    private function _getParams(string $rawParams)
    {
        $params = [];
        foreach(explode(' ', $rawParams) as $raw ) {
            $data = explode('=', $raw);
            if (count($data) == 2) {
                $params[$data[0]] = $data[1];
            } else {
                $params[] = $data;
            }
        }
        return $params;
    }
}
