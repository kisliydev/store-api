<?php

namespace App\Traits;
use App\Models\c;
use App\Models\Message;
use Illuminate\Support\Facades\DB;

trait ThreadListQueryBuilder
{

    /**
     * @param $request
     * @return mixed
     */
    public function getUserThreadQuery($query, $request)
    {
        $userDealer = $this->getUsersChannelDealer($request->channel_id);
        $t  = 'threads';

        $childThreads = $this->getRawUserThreadsQuery($query->where('status', '<>' , 'scheduled'), $request->channel_id, $userDealer)
            ->select("{$t}.parent_id")
            ->whereNotNull("{$t}.parent_id")->get()->pluck('parent_id')->toArray();

        return $this->getRawUserThreadsQuery($this->where('status', '<>' , 'scheduled'), $request->channel_id, $userDealer)
            ->select("{$t}.*")
            ->whereNotIn("{$t}.id", $childThreads)
            ->orderBy('updated_at', 'desc');
    }

    /**
     * @param $channelID
     * @return mixed
     */
    public function getUsersChannelDealer($channelID)
    {
        return auth()->user()->dealer($channelID)->first();
    }

    /**
     * @param $query
     * @param $channelID
     * @param $dealer
     * @return mixed
     */
    public function getRawUserThreadsQuery($query, $channelID, $dealer = null) {
        $r = 'recipients';
        $t = 'threads';

        return $query->join($r, function($join) use ($r, $t, $dealer){
                $join->on("{$r}.thread_id", '=', "{$t}.id")
                    ->where(function($clause) use ($r, $dealer) {

                        $clause->where(function($subClause) use ($r) {
                                $subClause->where("{$r}.object_type", 'user_id')
                                    ->where("{$r}.object_id", auth()->user()->id);
                        })->orWhere("{$r}.object_type", 'all');
                        if (!empty($dealer->dealer_id)) {
                            $clause->orWhere(function($subClause) use ($r, $dealer) {
                                $subClause->where("{$r}.object_type", 'dealer_id')
                                    ->where("{$r}.object_id", $dealer->dealer_id);
                            });
                        }

                        if (!empty($dealer->district_id)) {
                            $clause->orWhere(function($subClause) use ($r, $dealer) {
                                $subClause->where("{$r}.object_type", 'district_id')
                                    ->where("{$r}.object_id", $dealer->district_id);
                            });
                        }

                        if (!empty($dealer->league_id)) {
                            $clause->orWhere(function($subClause) use ($r, $dealer) {
                                $subClause->where("{$r}.object_type", 'league_id')
                                    ->where("{$r}.object_id", $dealer->league_id);
                            });
                        }
                    });
            })
            ->where("{$t}.channel_id", $channelID);
    }

    /**
     * @return \Closure
     */
    public function getNotReadByAdminClosure()
    {
        return function($query) {
            $query->select('*')
                ->from('message_statuses')
                ->whereRaw("message_id=messages.id")
                ->where('read_by', 'admin');
        };
    }

    /**
     * @param int $userID
     * @return \Closure
     */
    public function getNotReadByUserClosure(int $userID = null)
    {
        $userID = $userID ?? auth()->user()->id;

        return function($query) use ($userID) {
            $query->select('*')
                ->from('message_statuses')
                ->whereRaw("message_id=messages.id")
                ->where('user_id', $userID)
                ->where('read_by', 'user');
        };
    }

    /**
     * @param int|null $channelID
     * @return \Closure
     */
    public function lastThreadMessageClosure(int $channelID = null)
    {
        return function($query) use ($channelID) {
            /*
             * Fetch the list of last messages for each
             * thread in channel
             */
            $subQuery = DB::table('messages')->select(
                    'thread_id as t_id',
                    DB::raw("MAX(messages.id) as m_id")
                )
                ->join('threads', function($join) use ($channelID)  {
                    $join->on("threads.id", "=", "messages.thread_id")
                        ->where("threads.channel_id", $channelID);
                })
                ->groupBy('t_id');

            $query->join(DB::raw("({$subQuery->toSql()}) as max"), "max.m_id", '=', 'messages.id')
                ->mergeBindings($subQuery)
                ->with(['author' => $this->getAuthorClosure($channelID), 'attachments.media']);
        };
    }

    /**
     * @param int|null $channelID
     * @return \Closure
     */
    public function getAuthorClosure(int $channelID = null)
    {
        $relations = ['avatar'];
        if (!empty($channelID)) {
            $relations['dealer'] = function($query) use ($channelID) {
                $query->where('channel_id', '=', $channelID);
            };
            $relations['position'] = function($query) use ($channelID) {
                $query->where('channel_id', '=', $channelID);
            };
        }

        return function($query) use ($relations) {
            $query->select(
                'id',
                'avatar_id',
                'first_name',
                'last_name'
            )->with($relations);
        };
    }

}
