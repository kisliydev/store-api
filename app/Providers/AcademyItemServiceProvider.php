<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\AcademyItem;

class AcademyItemServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        AcademyItem::deleted(function($item) {
            app()['cache']->forget("{$item->type}_{$item->id}");
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
