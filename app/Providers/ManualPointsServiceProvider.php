<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\ManualPoint;
use App\Jobs\ManualPointsUpdateStatistics;
use App\Traits\Notifications;

class ManualPointsServiceProvider extends ServiceProvider
{
    use Notifications;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        ManualPoint::created(function($points) {
            dispatch((new ManualPointsUpdateStatistics($points, get_class($this), $this->_setMessageAuthorID($points->channel_id)))->onQueue(config('queue.tube.default')));
        });

        ManualPoint::deleted(function($item) {
            app()['cache']->forget("manual_{$item->id}");
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
