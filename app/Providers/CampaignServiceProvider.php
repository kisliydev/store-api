<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Campaign;

class CampaignServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     * @return void
     */
    public function boot()
    {
        Campaign::created(function($campaign) {
            if (empty($campaign->app_layout)) {
                $campaign->app_layout = $campaign->getDefaultAppLayout();
                $campaign->save();
            }
        });
    }

    /**
     * Register the application services.
     * @return void
     */
    public function register()
    {
        //
    }
}
