<?php

namespace App\Providers;

use App\Models\User;
use App\Traits\SocketNotifications;
use Illuminate\Support\ServiceProvider;
use App\Models\PointsStatistic;
use App\Models\AppStatistic;
use App\Jobs\LevelsNotifications;

class PointsStatisticsProvider extends ServiceProvider
{
    use SocketNotifications;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        PointsStatistic::deleted(function($item) {
            if (! $item->event_id) {
                return;
            }

            /**
             * An event should be deleted
             * in case if there are not any statistics records
             * relative to the currently checked event.
             */
            AppStatistic::where('id', $item->event_id)
                ->whereNotIn('id', function($query) use ($item) {
                    $query->select('event_id')
                        ->from($item->getTable())
                        ->where("event_id", '=', "{$item->event_id}");
                })->delete();
        });

        PointsStatistic::created(function($item) {

            /**
             * Can't use auth()->user() facade
             * due to the callback can be triggered in the queue worker
             */
            $user = User::find($item->user_id);

            if (!$user) {
                return;
            }

            if($user->hasRole('app_user')) {
                $this->notify([
                    'type'       => 'new_point',
                    'channel_id' => $item->channel_id,
                    'item_id'    => $item->id,
                ]);
            }

            dispatch((new LevelsNotifications($item->toArray(), get_class($this)))->onQueue(config('queue.tube.default')));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
