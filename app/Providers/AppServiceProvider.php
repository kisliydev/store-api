<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Queue;
use App\Models\JobsLog;
use Illuminate\Queue\Events\JobProcessed;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

use Berkayk\OneSignal\OneSignalClient;
use NotificationChannels\OneSignal\OneSignalChannel;
use App\Classes\OnesignalClientDecorator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Queue::after(function (JobProcessed $event) {
            JobsLog::create(['data' => json_encode($event->job->payload())]);
        });

        $this->app->when(OneSignalChannel::class)
            ->needs(OneSignalClient::class)
            ->give(function () {

                $oneSignalConfig = config('services.onesignal');

                return new OnesignalClientDecorator(
                    $oneSignalConfig['app_id'],
                    $oneSignalConfig['rest_api_key'],
                    ''
                );
            });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        HasMany::macro('toHasOne', function(string $foreignKey, string $localKey){
            return new HasOne(
                $this->getQuery(),
                $this->getParent(),
                $foreignKey,
                $localKey
            );
        });
    }
}
