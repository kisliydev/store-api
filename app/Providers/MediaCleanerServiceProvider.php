<?php

namespace App\Providers;

use App\Models\Attachment;
use App\Models\Thread;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use App\Models\Brand;
use App\Models\CountryBrand;
use App\Models\Channel;
use App\Models\Campaign;
use App\Models\Media;


class MediaCleanerServiceProvider extends ServiceProvider
{
    /**
     * @var object App\Models\Media
     */
    private $_media;


    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Brand::deleted(function ($brand) {
            if ($brand->logo_id) {
                $this->_media->removeMedia([$brand->logo_id]);
            }
            $this->_media->removeChannelMedia($brand->channels->pluck('id')->toArray());
        });

        CountryBrand::deleted(function ($brand) {
            $this->_media->removeChannelMedia($brand->channels->pluck('id')->toArray());
        });

        Channel::deleted(function ($channel) {
            $this->_media->removeChannelMedia([$channel->id]);
        });

        Campaign::deleted(function ($campaign) {
            $this->_media->removeMedia($campaign->getAppLayoutImagesIDs());
        });

        Thread::deleting(function($thread) {
            $media = [];

            Attachment::whereIn('message_id', function($clause) use ($thread) {
                    $clause->select('id')
                        ->from('messages')
                        ->where('thread_id', $thread->id);
                })
                ->get()
                ->each(function($item) use (&$media) {

                    if($item->media_id) {
                        $media[] = $item->media_id;
                    }
                });

            if ($media) {
                $this->_media->removeMedia($media);
            }
        });

        Attachment::deleted(function ($attachment) {
            if ($attachment->media_id) {
                $this->_media->removeMedia([$attachment->media_id]);
            }
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->_media = new Media();
    }


}
