<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ChannelStatisticsServiceProvider extends ServiceProvider
{
    /**
     * @var bool
     */
    protected $defer = true;

    /**
     * @return array
     */
    public function provides()
    {
        return [
            'App\Services\ChannelGarbageCollectorService',
            'App\Services\Statistics\CampaignStatisticsService',
            'App\Services\Statistics\ChannelStatisticsService',
            'App\Services\Statistics\UserStatisticsService',
            'App\Services\Statistics\DealerStatisticsService',
            'App\Services\Statistics\DashboardStatisticsService',
            'App\Services\Statistics\AcademyItemsStatisticsService',
            'App\Services\Statistics\CampaignArchiverService',
        ];
    }

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $role = app()->make('Spatie\Permission\Models\Role');
        foreach($this->provides() as $class) {
            $this->app->resolving($class, function ($instance) use ($role) {
                $instance->setData($role);
            });
        }
    }
}
