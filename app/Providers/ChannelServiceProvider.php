<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Channel;

class ChannelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Channel::created(function($channel) {
            if (empty($channel->messages_settings)) {
                $channel->messages_settings = $channel->getDefaultMessagesSettings();
            }

            if (empty($channel->messages_settings)) {
                $channel->app_settings = $channel->getDefaultAppSettings();
            }

            $channel->save();
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
