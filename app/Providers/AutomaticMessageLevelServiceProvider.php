<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Level;

class AutomaticMessageLevelServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Level::deleted(function($item) {
            app()['cache']->forget("level_{$item->id}");
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
