<?php
namespace App\Classes;

use Berkayk\OneSignal\OneSignalClient;
use Illuminate\Support\Facades\Log;

class OnesignalClientDecorator extends OneSignalClient {
    public function sendNotificationCustom($parameters = []) {

        $parameters['app_id']  = config('services.onesignal.app_id');
        $parameters['api_key'] = config('services.onesignal.rest_api_key');

        $response = parent::sendNotificationCustom($parameters);

        /*
         * I've decided to leave these rows
         * for debugging purposes
         * until the push messages functionality will be tested properly.
         */
//        Log::info($parameters);
//        Log::info($this->headers);
//        Log::info(config('services.onesignal'));
//        Log::info($response->getBody());

        return $response;
    }

}