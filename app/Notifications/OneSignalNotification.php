<?php

namespace App\Notifications;

use App\Models\Thread;
use NotificationChannels\OneSignal\OneSignalChannel;
use NotificationChannels\OneSignal\OneSignalMessage;
use Illuminate\Notifications\Notification;

class OneSignalNotification extends Notification
{
    const NOTIFICATION_TYPE_CHAT_MESSAGE = 'message';
    const NOTIFICATION_TYPE_AUTOREPLY_MESSAGE = 'autoreply';
    const NOTIFICATION_TYPE_AUTOMATIC_MESSAGE = 'automatic';
    const NOTIFICATION_TYPE_SCHEDULED_MESSAGE = 'scheduled';
    const NOTIFICATION_TYPE_LEVEL_MESSAGE = 'level';
    const NOTIFICATION_TYPE_MANUAL_POINT = 'manual_point';

    /**
     * @var array
     */
    private $userIDs;

    /**
     * @var array
     */
    private $data;

    /**
     * OneSignalNotification constructor.
     *
     * @param array $userIDs
     * @param array $data
     * @param string|null $appName
     */
    public function __construct(array $userIDs, array $data)
    {
        $this->data    = $data;
        $this->userIDs = $userIDs;
    }

    public static function getPushData(string $type, Thread $thread)
    {
        return [
            'thread_id' => $thread->id,
            'channel_id' => $thread->channel_id,
            'channel_name' => $thread->channel->name,
            'type' => $type
        ];
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        if ($this->isDataValid()) {
            return [OneSignalChannel::class];
        }
    }

    /**
     * @param $notifiable
     * @return OneSignalMessage
     */
    public function toOneSignal($notifiable)
    {
        if ($this->isDataValid()) {
            $message = OneSignalMessage::create()->subject($this->data['subject'])->body($this->data['body']);
            return $this->pushData($message);
        }
    }

    /**
     * @param OneSignalMessage $message
     * @return OneSignalMessage
     */
    public function pushData(OneSignalMessage $message)
    {
        foreach ($this->data['data'] as $key => $value) {
            $message->setData($key, $value);
        }

        return $message;
    }

    /**
     * @return bool
     */
    private function isDataValid()
    {
        return array_key_exists('subject', $this->data) && array_key_exists('body', $this->data) && array_key_exists('data', $this->data);
    }
}
