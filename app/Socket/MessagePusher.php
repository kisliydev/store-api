<?php

namespace App\Socket;

use ZMQContext;

class MessagePusher extends MessageBasePusher
{
    use Logger;

    /**
     * @param array $data
     */
    public function sendToServer(array $data)
    {
        try {
            $context = new ZMQContext();
            $socket = $context->getSocket(\ZMQ::SOCKET_PUSH, 'my pusher');
            $socket->connect("tcp://127.0.0.1:5555");
            foreach($data as $message) {
                $socket->send(json_encode($message));
            }
        } catch (\Exception $e) {
            $this->info("Error in sending messages: {$e->getMessage()}");
        }
    }

    /**
     * @param string $data
     */
    public function broadcast(string $data)
    {
        $data  = json_decode($data, true);
        $alias = $data['alias'] ?? null;

        if (! $alias) {
            return;
        }

        unset($data['alias']);

        $topic = $this->getTopics()[$alias] ?? null;
        if ($topic) {
            $topic->broadcast($data);
            $this->info("[{$alias}] Message Sent: " . json_encode($data, 128));
        }
    }
}
