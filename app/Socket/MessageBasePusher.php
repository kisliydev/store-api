<?php
namespace App\Socket;

use Ratchet\ConnectionInterface;
use Ratchet\Wamp\WampServerInterface;
use Tymon\JWTAuth\Facades\JWTAuth;

class MessageBasePusher implements WampServerInterface
{
    use Logger;

    /**
     * @var array
     */
    protected $topics = [];

    /**
     * @return array
     */
    public function getTopics()
    {
        return $this->topics;
    }

    /**
     * @param string $alias
     * @param $topic
     */
    public function addTopic(string $alias, $topic)
    {
        $this->topics[$alias] = $topic;
    }

    /**
     * @param \Ratchet\ConnectionInterface $conn
     * @param \Ratchet\Wamp\Topic|string $topic
     */
    public function onSubscribe(ConnectionInterface $conn, $topic)
    {
        try {
            $data = json_decode($topic->getId(), true);

            $this->_validate($data);
            $this->_checkUser($data);

            $alias = "Channel:{$data['channel_id']}";
            switch ($data['connection']) {
                case 'user':
                    $alias .= ",User:{$data['user_id']}";
                    break;
                case 'channel_dashboard':
                    $alias .= ",Dashboard";
                    break;
                default:
                    break;
            }

            $this->addTopic($alias, $topic);

            $conn->send(json_encode([
                'type' => 'approve',
                'data' => ['connected_channel' => $data['channel_id']],
            ]));
            $this->info("Subscribed! {$data['user_id']} to {$data['channel_id']}");

        } catch(\Exception $e) {
            $this->info("Connection closed due to error! ({$conn->resourceId}): {$e->getMessage()}");
            $conn->send(json_encode([
                'type' => 'error',
                'data' => ['message' => $e->getMessage()]
            ]));
            $conn->close();
        }
    }

    /**
     * @param \Ratchet\ConnectionInterface $conn
     * @param \Ratchet\Wamp\Topic|string $topic
     */
    public function onUnSubscribe(ConnectionInterface $conn, $topic)
    {

    }

    /**
     * @param \Ratchet\ConnectionInterface $conn
     * @param string $id
     * @param \Ratchet\Wamp\Topic|string $topic
     * @param array $params
     */
    public function onCall(ConnectionInterface $conn, $id, $topic, array $params)
    {
        $conn->callError($id, $topic, "You are not allowe to make calls")->close();
        $this->info("Connection closed due to non-authorized attempt! ({$conn->resourceId})");
    }

    /**
     * @param \Ratchet\ConnectionInterface $conn
     */
    public function onClose(ConnectionInterface $conn)
    {
        $this->info("Connection closed! ({$conn->resourceId})");
    }

    /**
     * @param \Ratchet\ConnectionInterface $conn
     * @param \Exception $e
     */
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $this->info("Connection closed due to error! ({$conn->resourceId}): {$e->getMessage()}");
        $conn->close();
    }

    /**
     * @param \Ratchet\ConnectionInterface $conn
     */
    public function onOpen(ConnectionInterface $conn)
    {
        $this->info("New connection! ({$conn->resourceId})");
    }

    /**
     * @param \Ratchet\ConnectionInterface $conn
     * @param \Ratchet\Wamp\Topic|string $topic
     * @param string $event
     * @param array $exclude
     * @param array $eligible
     */
    public function onPublish(ConnectionInterface $conn, $topic, $event, array $exclude, array $eligible)
    {
        $conn->close();
    }

    /**
     * @param array $topics
     */
    public function setTopics(array $topics)
    {
        $this->topics = $topics;
    }

    /**
     * @param array $data
     * @throws \Exception
     */
    private function _validate(array $data)
    {
        $required = [
            'connection',
            'channel_id',
            'user_id',
            'token',
        ];

        if (4 > count(array_intersect_key($required, array_keys(array_filter($data))))) {
            throw new \Exception('Not enough data');
        }

        foreach (['channel_id', 'user_id'] as $key) {
            if (! is_numeric($data[$key])) {
                throw new \Exception("Wrong {$key} format");
            }
        }

        /**
         * Connection description:
         * - admin - when admin goes to the web app
         * - user  - when user opened the mobile app
         * - channel_dashboard - when admin visited channel's dashboard page
         */
        if (! in_array($data['connection'], ['admin', 'user', 'channel_dashboard'])) {
            throw new \Exception("Wrong {$key} format");
        }
    }

    /**
     * @param array $data
     * @throws \Exception
     */
    private function _checkUser(array $data)
    {
        $user = JWTAuth::setToken($data['token'])->toUser();

        if (! $user->hasAccessToChannel($data['channel_id'])) {
            throw new \Exception("Access denied");
        }
    }
}


