<?php

namespace App\Socket;

trait Logger
{
    /**
     * Print message to stdout
     *
     * @param string $message
     */
    public function info(string $message = "")
    {
        $ramPeak = (memory_get_peak_usage(true) / 1024 / 1024);
        $ramUsage = round(memory_get_usage() / 1024 / 1024, 3);

        echo date("Y-m-d H:i:s")." (RAM: $ramPeak/$ramUsage) - ".$message."\n";
    }
}