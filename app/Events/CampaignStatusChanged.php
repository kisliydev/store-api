<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class CampaignStatusChanged
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var array
     */
    public $campaign;

    /**
     * CampaignStatusChanged constructor.
     *
     * @param array $campaign
     */
    public function __construct(array $campaign)
    {
        $this->campaign = $campaign;
    }
}
