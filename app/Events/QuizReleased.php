<?php

namespace App\Events;

use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class QuizReleased
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $quizData;

    /**
     * QuizReleased constructor.
     *
     * @param array $quizData
     */
    public function __construct(array $quizData)
    {
        $this->quizData = $quizData;
    }
}
