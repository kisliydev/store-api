<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Models\AcademyItem;
use Illuminate\Support\Facades\Log;

class PublishAcademyItems extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'academy_items:publish_scheduled';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Set the publish status for items according to their options';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $items = AcademyItem::where('status', 'scheduled')->where('started_at', '<=', Carbon::now())->get();

        if ($items->count()) {

            $items->each(function($item) {
                $item->publish();
            });

            Log::useDailyFiles(storage_path().'/logs/daily/cron.log');
            Log::info('Published Academy Items:');
            Log::info($items->toArray());
        }
    }
}
