<?php

namespace App\Console\Commands\OPCache;

use App\Cache\Opcache;
use Illuminate\Console\Command;

class OpcacheClear extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'opcache:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clear OPCache cache';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param \App\Cache\Opcache $cache
     * @return mixed
     */
    public function handle(Opcache $cache)
    {
        $cache->clear();
    }
}
