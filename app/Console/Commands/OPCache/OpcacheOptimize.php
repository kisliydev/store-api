<?php

namespace App\Console\Commands\OPCache;

use App\Cache\Opcache;
use Illuminate\Console\Command;

class OpcacheOptimize extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'opcache:optimize';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run OPCache optimization';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param \App\Cache\Opcache $cache
     * @return mixed
     */
    public function handle(Opcache $cache)
    {
        $cache->optimize();
    }
}
