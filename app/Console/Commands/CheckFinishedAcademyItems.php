<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\AcademyItem;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

class CheckFinishedAcademyItems extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'academy_items:check_finished';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks whether there are Quizzes and Surveys that are finished in order to change their status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $items = AcademyItem::whereNotNull('finished_at')
            ->where('finished_at', '<=', Carbon::now())
            ->where('status', '<>', 'finished')
            ->get();

        if ($items->count()) {

            AcademyItem::whereIn('id', $items->pluck('id')->toArray())
                ->update(['status' => 'finished']);

            Log::useDailyFiles(storage_path().'/logs/daily/cron.log');
            Log::info('Finished Academy Items:');
            Log::info($items->toArray());
        }
    }
}
