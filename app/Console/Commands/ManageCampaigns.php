<?php

namespace App\Console\Commands;

use App\Models\Media;
use Illuminate\Console\Command;
use App\Models\Campaign;
use App\Services\Statistics\CampaignArchiverService;
use App\Services\ChannelGarbageCollectorService;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use App\Events\CampaignStatusChanged;

class ManageCampaigns extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'campaigns:manage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Search campaigns which status needs to be changed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param \App\Models\Campaign $campaign
     * @param \App\Services\Statistics\CampaignArchiverService $acrhiver
     * @param \App\Services\ChannelGarbageCollectorService $collector
     */
    public function handle(Campaign $campaign, CampaignArchiverService $acrhiver, ChannelGarbageCollectorService $collector)
    {
        $campaigns = $campaign->reachedDatePoints(Carbon::now()->toDateTimeString())->get();

        if (!$campaigns->count()) {
            return;
        }

        Log::useDailyFiles(storage_path().'/logs/daily/cron.log');

        $campaigns->each(function($campaign) use ($acrhiver, $collector) {
            $newStatus = $campaign->getNextStatus();
            switch ($newStatus) {
                case 'pre_active':
                case 'active':
                case 'post_active':
                case 'finished':
                default:
                    if ($campaign->status == 'scheduled') {
                        $this->_archiveRunningCampaign($campaign, $acrhiver, $collector);
                    }
                    break;
                case 'archived':
                    /*
                     * Due to the fact, that even after campaign finished,
                     * users still should be able to pass surveys,
                     * campaign should be given the status 'archived' ONLY in case if new campaign starts.
                     * So that we can link earned points with the recently finished campaign.
                     */
                    $newStatus = 'finished';
                    break;
            }

            $campaign->status = $newStatus;
            $campaign->save();
            $this->_logStatus($campaign);
            event(new CampaignStatusChanged($campaign->toArray()));
        });
    }

    private function _archiveRunningCampaign(Campaign $campaign, CampaignArchiverService $acrhiver, ChannelGarbageCollectorService $collector)
    {
        $runningCampaign = Campaign::previous($campaign->channel_id, $campaign->id)->first();
        if ($runningCampaign) {
            $acrhiver->makeArchive($runningCampaign);
            $collector->cleanChannel($runningCampaign->channel_id, app()->make(Media::class));
            $runningCampaign->status = 'archived';
            $runningCampaign->save();
            $this->_logStatus($campaign);
        }
    }

    /**
     * @param \App\Models\Campaign $campaign
     */
    private function _logStatus(Campaign $campaign)
    {
        Log::info('Campaign\'s status changed:');
        Log::info($campaign->toArray());
    }
}
