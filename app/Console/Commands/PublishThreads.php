<?php

namespace App\Console\Commands;

use App\Models\Thread;
use App\Notifications\OneSignalNotification;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class PublishThreads extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'threads:publish';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check whether ther eare scheduled threads that need to be published';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Log::useDailyFiles(storage_path().'/logs/daily/cron.log');

        Thread::where('status', 'scheduled')
            ->where('publish_at', '<=', Carbon::now())
            ->get()
            ->each(function($thread) {
                $thread->publish(OneSignalNotification::NOTIFICATION_TYPE_SCHEDULED_MESSAGE);
                Log::info('Published Thread:');
                Log::info($thread->toArray());
            });
    }
}
