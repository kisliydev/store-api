<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class LoadPoints extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'points:load {level} {load_images}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fills point statistics table with test data.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        Artisan::call('migrate:refresh');
        Artisan::call('db:seed');
        app()->make(\PointsStatisticsSeeder::class)->run(
            $this->argument('level'),
            !!$this->argument('load_images')
        );
    }
}
