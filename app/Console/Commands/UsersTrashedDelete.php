<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;

use App\Models\User;
use App\Models\Media;
use Illuminate\Support\Facades\Log;

class UsersTrashedDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:delete_trashed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Permanently delete all users that were moved to the trash.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::whereDate('deleted_at', '<', Carbon::now()->subDays(30) )->onlyTrashed()->get();

        if ($users->count()) {
            $media = new Media();
            foreach($users as $user) {
                $user->deletePermanently($media);
            }

            Log::useDailyFiles(storage_path().'/logs/daily/cron.log');
            Log::info('Deleted users:');
            Log::info($users->toArray());
        }
    }
}
