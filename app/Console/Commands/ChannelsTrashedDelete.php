<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Models\Channel;

class ChannelsTrashedDelete extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'channels:delete_trashed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Permanently delete all channels that were moved to the trash.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $channels = Channel::whereDate('deleted_at', '<', Carbon::now()->subDays(30) )->onlyTrashed()->get();

        if ($channels->count()) {

            $channels->each(function($channel) {
                $channel->forceDelete();
            });

            Log::useDailyFiles(storage_path().'/logs/daily/cron.log');
            Log::info('Deleted channels:');
            Log::info($channels->toArray());
        }
    }
}
