<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use Ratchet\Wamp\WampServer;

use React\EventLoop\Factory as ReactLoop;
use React\ZMQ\Context as ReactContext;
use React\Socket\Server as ReactServer;

use App\Socket\MessagePusher;

class SocketMessagesPusherServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'socket_message_pusher:serve';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @throws \ZMQSocketException
     */
    public function handle()
    {
        try{
            $loop    = ReactLoop::create();
            $pusher  = new MessagePusher();
            $context = new ReactContext($loop);

            $pull = $context->getSocket(\ZMQ::SOCKET_PULL);
            $pull->bind("tcp://127.0.0.1:5555");
            $pull->on('message', [$pusher, 'broadcast']);

            $socket = new ReactServer($loop);
            $socket->listen(9090, '0.0.0.0');
            $server = new IoServer(
                new HttpServer(
                    new WsServer(
                        new WampServer($pusher)
                    )
                ),
                $socket
            );

            $this->info('MessagePusher run');
            $loop->run();
        } catch(\Exception $e) {
            $this->info("MessagePusher can't run: {$e->getMessage()}");
        }

    }
}
