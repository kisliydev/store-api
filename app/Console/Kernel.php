<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\SocketMessagesPusherServer::Class,
        \App\Console\Commands\UsersTrashedDelete::class,
        \App\Console\Commands\ChannelsTrashedDelete::class,
        \App\Console\Commands\PublishAcademyItems::class,
        \App\Console\Commands\ManageCampaigns::class,
        \App\Console\Commands\CheckFinishedAcademyItems::class,
        \App\Console\Commands\PublishThreads::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        if (! env('USE_CRON', true)) {
            return;
        }

        $schedule->command('users:delete_trashed')->daily();
        $schedule->command('channels:delete_trashed')->daily();
        $schedule->command('academy_items:publish_scheduled')->everyMinute();
        $schedule->command('academy_items:check_finished')->everyMinute();
        $schedule->command('campaigns:manage')->everyMinute();
        $schedule->command('threads:publish')->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
