<?php

namespace App\Listeners;

use App\Events\QuizReleased;
use App\Models\AutomaticMessage;
use App\Helpers\NotificationsHelper;
use Illuminate\Support\Facades\Log;

class SendQuizReleasedNotification
{
    /**
     * @var \App\Models\AutomaticMessage
     */
    private $messages;

    /**
     * SendQuizReleasedNotification constructor.
     *
     * @param \App\Models\AutomaticMessage $messages
     */
    public function __construct(AutomaticMessage $messages) {
        $this->messages = $messages;
    }

    /**
     * @param \App\Events\QuizReleased $event
     * @return mixed
     */
    public function handle(QuizReleased $event)
    {
        if (! $this->isEventDataValid($event)) {
            return Log::error('SendQuizReleasedNotification - not valid event data.');
        }

        $autoMessages = $this->messages
            ->getAcademyRelativeMessages($event->quizData['channel_id'], $event->quizData['type']);

        if (!$autoMessages->count()) {
            return;
        }

        foreach ($autoMessages as $message) {
            app()->call(NotificationsHelper::class . '@notifyAll', [
                'message' => $message,
                'data'  => ["{$event->quizData['type']}_id" => $event->quizData['id']]
            ]);
        }
    }

    /**
     * @param \App\Events\QuizReleased $event
     * @return bool
     */
    private function isEventDataValid(QuizReleased $event)
    {
        return isset($event->quizData)
            && array_key_exists('channel_id', $event->quizData)
            && array_key_exists('type', $event->quizData);
    }
}
