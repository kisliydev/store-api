<?php

namespace App\Listeners;

use App\Events\CampaignStatusChanged;
use Illuminate\Support\Facades\Log;
use App\Helpers\NotificationsHelper;
use App\Models\AutomaticMessage;

class SendCampaignStatusChangedNotification
{
    private $_messages;

    /**
     * SendCampaignStatusChangedNotification constructor.
     * @param AutomaticMessage $messages
     */
    public function __construct(AutomaticMessage $messages) {
        $this->_messages = $messages;
    }

    /**
     * Handle the event.
     *
     * @param  CampaignStatusChanged  $event
     */
    public function handle(CampaignStatusChanged $event)
    {
        if (! $this->_isEventDataValid($event)) {
            Log::error(self::class . " - not valid event data.");
            Log::error($event->campaign);
        }

        foreach($this->_messages->getCampaignRelativeMessages($event->campaign['status'], $event->campaign['channel_id']) as $message) {
            app()->call(NotificationsHelper::class . '@notifyAll', ['message' => $message]);
        }
    }

    /**
     * @param \App\Events\CampaignStatusChanged $event
     * @return bool
     */
    private function _isEventDataValid(CampaignStatusChanged $event)
    {
        return isset($event->campaign)
            && array_key_exists('channel_id', $event->campaign)
            && array_key_exists('status', $event->campaign)
            && in_array($event->campaign['status'], ['pre_active', 'active', 'post_active', 'finished']);
    }
}
