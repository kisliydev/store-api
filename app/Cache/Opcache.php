<?php

namespace App\Cache;

use Adoy\FastCGI\ForbiddenException;
use CacheTool\CacheTool;
use CacheTool\Proxy\OpcacheProxy;
use Illuminate\Support\Facades\File;
use CacheTool\Adapter\FastCGI;

/**
 * Class OpcacheClass.
 */
class Opcache
{
    public $cache;

    /**
     * Opcache constructor.
     */
    public function __construct()
    {
        $this->init();
    }

    /**
     * Initialize FastCGI connection
     */
    private function init()
    {
        $adapter = new FastCGI('127.0.0.1:9000');
        $cache = new CacheTool('/tmp');
        $cache->setAdapter($adapter);
        $cache->addProxy(new OpcacheProxy());

        $this->cache = $cache;
    }

    /**
     * @param string $file
     * @return mixed
     * @throws \Exception
     */
    private function invalidateFile(string $file)
    {
        try {
            return $this->cache->opcache_invalidate($file);
        } catch (\RuntimeException $e) {
            // FastCGI might throw ForbiddenException
            // due to re-spawning php-fpm pool worker
            // In this case we should re-create FastCGI connection
            if ($e->getPrevious() instanceof ForbiddenException) {
                $this->init();
                return $this->invalidateFile($file);
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * @param string $file
     * @return mixed
     * @throws \Exception
     */
    private function optimizeFile(string $file)
    {
        try {
            return $this->cache->opcache_compile_file($file);
        } catch (\RuntimeException $e) {
            // FastCGI might throw ForbiddenException
            // due to re-spawning php-fpm pool worker
            // In this case we should re-create FastCGI connection
            if ($e->getPrevious() instanceof ForbiddenException) {
                $this->init();
                return $this->optimizeFile($file);
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Clear the cache.
     *
     * @return bool
     */
    public function clear()
    {
        return $this->cache->opcache_reset();
    }

    /**
     * Get configuration values.
     *
     * @return mixed
     */
    public function getConfig()
    {
        return $this->cache->opcache_get_configuration();
    }

    /**
     * Get status info.
     *
     * @return mixed
     */
    public function getStatus()
    {
        return $this->cache->opcache_get_status(false);
    }

    /**
     * @return static
     */
    public function getFilesList()
    {
        $files = File::allFiles(config('opcache.directories'));

        $files = collect($files);

        // filter on php extension
        return $files->filter(function (\SplFileInfo $value) {
            if (str_contains(strtolower($value->getPathname()), ['test', 'tests', 'examples', 'demos'])) {
                return false;
            }
            return File::extension($value) == 'php';
        });
    }

    /**
     * Precompile app.
     *
     * @return bool | array
     */
    public function optimize()
    {
        // Get files in these paths
        $files = $this->getFilesList();
        // optimized files
        $optimized = 0;

        $files->each(function ($file) use (&$optimized) {
            $this->optimizeFile((string) $file);

            $optimized++;
        });

        return [
            'total_files_count' => $files->count(),
            'compiled_count'    => $optimized,
        ];
    }

    /**
     * @return array|bool
     */
    public function invalidate()
    {
        // Get files in these paths
        $files = $this->getFilesList();

        // optimized files
        $invalidated = 0;

        $files->each(function ($file) use (&$invalidated) {
            if ($this->invalidateFile((string) $file)) {
                $invalidated++;
            }
        });

        return [
            'total_files_count' => $files->count(),
            'invalidated_count'    => $invalidated,
        ];
    }
}
