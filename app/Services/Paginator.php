<?php

namespace App\Services;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class Paginator
{
    /**
     * @param Collection $items
     * @param int $perPage
     * @param int $page
     * @param array $options
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function collectionPagination(Collection $items, int $perPage = 10, int $page = 1, array $options = []): LengthAwarePaginator
    {
        $data = $items->forPage($page, $perPage)->values();

        return new LengthAwarePaginator($data, $items->count(), $perPage, $page, $options);
    }
}
