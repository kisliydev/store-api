<?php
namespace App\Services;

use App\Models\Media;
use Illuminate\Support\Facades\DB;

class ChannelGarbageCollectorService extends Service
{
    private $channelID;

    /**
     * @param int $channelID
     * @param \App\Models\Media $media
     */
    public function cleanChannel(int $channelID, Media $media)
    {
        $this->channelID = $channelID;
        $this->removeChannelPoints();
        $this->removeChannelAttachments($media);
        $this->removeChannelMessages();
        $this->removeSalesStatistics();
        $this->removeAchievedLevels();
    }

    /**
     * @return void
     */
    protected function removeChannelPoints() {
        DB::table($this->ps)->where('channel_id', $this->channelID)->delete();
    }

    /**
     * @return void
     */
    protected function removeChannelMessages()
    {
        DB::table($this->t)->where('channel_id', $this->channelID)->delete();
    }

    /**
     * @param \App\Models\Media $media
     */
    protected function removeChannelAttachments(Media $media)
    {
        $ids = DB::table($this->a)
            ->select("{$this->a}.media_id as id")
            ->join($this->mess, "{$this->mess}.id", '=', "{$this->a}.message_id")
            ->join($this->t, function($join) {
                $join->on("{$this->t}.id", '=', "{$this->mess}.thread_id")
                    ->where("{$this->t}.channel_id", '=', $this->channelID);
            })->get()->pluck('id')->toArray();

        if ($ids) {
            $media->removeMedia($ids);
        }
    }

    /**
     * @return void
     */
    protected function removeSalesStatistics()
    {
        DB::table($this->as)
            ->where('channel_id', $this->channelID)
            ->where('event', 'sale')
            ->delete();
    }

    /**
     * @return void
     */
    protected function removeAchievedLevels()
    {
        $levels = DB::table($this->aml)
            ->where('channel_id', $this->channelID)
            ->get();

        if (! $levels->all()) {
            return;
        }

        DB::table($this->al)
            ->whereIn('level_id', $levels->pluck('id')->toArray())
            ->delete();
    }
}
