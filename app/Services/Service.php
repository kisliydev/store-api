<?php
namespace App\Services;

use Spatie\Permission\Models\Role;

class Service
{
    /**
     * @var object Collection
     */
    protected $appRoleIDs;

    /**
     * @param \Spatie\Permission\Models\Role $role
     */
    public function setData(Role $role) {

        /* set table names' aliases */
        $this->pra  = 'product_assortments';
        $this->u    = 'users';
        $this->ucs  = 'user_channel_statuses';
        $this->ud   = 'users_dealers';
        $this->d    = 'dealers';
        $this->dis  = 'districts';
        $this->p    = 'positions';
        $this->up   = 'user_positions';
        $this->ps   = 'points_statistics';
        $this->ps   = 'points_statistics';
        $this->as   = 'app_statistics';
        $this->al   = 'achieved_levels';
        $this->r    = 'roles';
        $this->m    = 'media';
        $this->pr   = 'products';
        $this->ua   = 'user_answers';
        $this->ai   = 'academy_items';
        $this->q    = 'questions';
        $this->dai  = 'done_academy_items';
        $this->mp   = 'manual_points';
        $this->aml  = 'levels';
        $this->c    = 'campaigns';
        $this->ch   = 'channel';
        $this->ca   = 'campaign_archives';
        $this->t    = 'threads';
        $this->mess = 'messages';
        $this->a    = 'attachments';
        $this->l    = 'leagues';
        $this->mhr  = config('permission.table_names.model_has_roles');

        /* config('database.default') is used in order to prevent use different caches during tests */
        $this->appRoleIDs = app()['cache']->remember('roles', config('cache.lifetime'), function () use ($role) {
            return $role->where('name', 'app_user')->pluck('id')->toArray();
        });
    }
}
