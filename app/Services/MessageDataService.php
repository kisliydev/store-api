<?php

namespace App\Services;

use App\Models\Model;
use App\Models\User;
use App\Models\Channel;
use App\Models\Brand;
use App\Models\CountryBrand;
use App\Models\BrandUser;
use App\Models\UserChannelStatus;

use App\Http\Requests\MessagesSettings\GetModeratorsRequest;

class MessageDataService
{
    const TOP_ADMIN_ROLES = [
        'super_admin',
        'internal_admin',
    ];
    /**
     * @param \App\Http\Requests\MessagesSettings\GetModeratorsRequest $request
     * @param \App\Models\User $user
     * @return mixed
     */
    public function getMessagesModerators(GetModeratorsRequest $request, User $user)
    {
        $channel = Channel::with('countryBrand.brand')->find($request->channel_id);
        $topAdmins = collect([]);
        if (in_array(auth()->user()->role, self::TOP_ADMIN_ROLES)) {
            $topAdmins = $user->select('users.id', 'users.email', 'users.first_name', 'users.last_name')
                ->leftJoin('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                ->leftJoin('roles', 'model_has_roles.role_id', '=', 'roles.id')
                ->whereIn('roles.name', self::TOP_ADMIN_ROLES)
                ->where('users.verified', true)->get();
        }
        $brandAdmins = $user->select('users.id', 'users.email', 'users.first_name', 'users.last_name')
            ->join('brand_users', 'users.id', '=', 'brand_users.user_id')
            ->where(function($query) use ($channel) {
                $query->where('brand_users.brand_type', CountryBrand::class)
                    ->where('brand_users.brand_id', $channel->countryBrand->id);
            })->orWhere(function($query) use ($channel) {
                $query->where('brand_users.brand_type', Brand::class)
                    ->where('brand_users.brand_id', $channel->countryBrand->brand->id);
            })->where('users.verified', true)->get();

        return $user->removeAppends($topAdmins->merge($brandAdmins), ['full_name']);
    }

    /**
     * @param \App\Http\Requests\MessagesSettings\GetModeratorsRequest $request
     * @param \App\Models\Model $model
     * @return mixed
     */
    public function getMessagesPageItems(GetModeratorsRequest $request, Model $model)
    {
        return $model->where('channel_id', $request->channel_id)->select('id', 'name')->get();
    }

    /**
     * @param \App\Http\Requests\MessagesSettings\GetModeratorsRequest $request
     * @param \App\Models\User $user
     * @param \App\Models\UserChannelStatus $userChannelStatus
     * @return \Illuminate\Support\Collection
     */
    public function getChannelAppUsers(GetModeratorsRequest $request, User $user, UserChannelStatus $userChannelStatus)
    {
        $u = $user->getTable();
        $ucs = $userChannelStatus->getTable();
        return $user->removeAppends($user->select("{$u}.id", "{$u}.first_name", "{$u}.last_name")->role(["app_user", "hq"])
            ->join($ucs, function($join) use ($ucs, $u, $request) {
                $join->on("{$ucs}.user_id", "{$u}.id")
                    ->where("{$ucs}.status", 'active')
                    ->where("{$ucs}.channel_id", $request->channel_id);
            })->orderBy("{$u}.first_name")->get(), ['full_name']);

    }

    /**
     * @param int $channelID
     * @return bool
     */
    public function getChannelModeratorsIDs(int $channelID) {
        $channel = Channel::with('countryBrand')->find($channelID);

        if (! $channel) {
            return false;
        }

        $brandUsers = BrandUser::where(function($clause) use ($channel) {
                $clause->where('brand_type', '=', CountryBrand::class)
                    ->where('brand_id', '=', $channel->country_brand_id);
            })->orWhere(function($clause) use ($channel) {
                $clause->where('brand_type', '=', Brand::class)
                    ->where('brand_id', '=', $channel->countryBrand->brand_id);
            })
            ->select('user_id');

        return User::role(['super_admin', 'internal_admin'])->select('id as user_id')
            ->union($brandUsers)->get()->pluck(['user_id'])->toArray();
    }

    /**
     * @param int $channelID
     * @return bool
     */
    public function getChannelAppUsersIDs(int $channelID)
    {
        $ids = UserChannelStatus::where('channel_id', $channelID)
            ->where('status', 'active')
            ->select('user_id')
            ->get();

        return $ids->all() ? $ids->pluck(['user_id'])->toArray() : false;
    }
}
