<?php

namespace App\Services\AcademyItemsExportService\Contracts;

interface AcademyItemsExportInterface
{
    public function getSurveysData(array $params);
}
