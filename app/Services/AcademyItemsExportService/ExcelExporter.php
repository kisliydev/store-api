<?php

namespace App\Services\AcademyItemsExportService;

use App\Models\AcademyItem;
use App\Models\DoneAcademyItem;
use App\Services\AcademyItemsExportService\Contracts\AcademyItemsExportInterface;
use App\Transformers\SurveyExportTransformer;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class ExcelExporter implements AcademyItemsExportInterface
{
    /**
     * @var \App\Models\AcademyItem
     */
    private $academyItem;

    /**
     * @var \App\Models\DoneAcademyItem
     */
    private $doneAcademyItem;

    /**
     * @var \App\Transformers\SurveyExportTransformer
     */
    private $transformer;

    /**
     * ExcelExporter constructor.
     *
     * @param \App\Models\AcademyItem $academyItem
     * @param \App\Models\DoneAcademyItem $doneAcademyItem
     * @param \App\Transformers\SurveyExportTransformer $transformer
     */
    public function __construct(AcademyItem $academyItem, DoneAcademyItem $doneAcademyItem, SurveyExportTransformer $transformer)
    {
        $this->academyItem = $academyItem;
        $this->doneAcademyItem = $doneAcademyItem;
        $this->transformer = $transformer;
    }

    public function getSurveysData(array $params)
    {
        $data = [
            'channel_name' => $params['channel_name']
        ];
        foreach ($params['ids'] as $id) {
            $data['questions_count'] = $this->academyItem->getQuestionsCount($id);
            $data['required_questions'] = $this->academyItem->getQuestionsCount($id, true);
            $data['respondents_count'] = $this->doneAcademyItem->getRespondents($id, true);
            $itemQuestions = $this->academyItem->getQuestionsList($id);
            $data['items_data'] = $this->transformer->transform(collect([
                'users' => $this->doneAcademyItem->getRespondents($id),
                'survey_results' => $itemQuestions,
            ]))->toArray();
            $data['date'] = $itemQuestions->first()->status == 'draft'
                ? ''
                : Carbon::parse($itemQuestions->first()->started_at)->format('d.m.Y');
            $data['current_date'] = Carbon::now()->format('d.m.Y');
            $directoryName = sprintf('%s_%s', $data['channel_name'], Carbon::now()->format('d.m.Y'));
            $filePath = storage_path("exports/$directoryName");
            $this->export($data, $filePath);
        }

        return ['file_path' => $filePath, 'directory_name' => $directoryName];
    }

    private function export(array $data, string $filePath)
    {
        Excel::create(
            sprintf('%s_%s_%s', $data['channel_name'], $data['items_data']['title'], $data['date']),
            function ($excel) use ($data) {
                $excel->sheet('Summary', function ($sheet) use ($data) {
                    $sheet->setStyle([
                        'font' => [
                            'size' => 12
                        ],
                    ]);
                    $sheet->rows([
                        ['Report created on:', $data['current_date']],
                        ['Total number of questions:', $data['questions_count']],
                        ['Total number of respondents:', $data['respondents_count']],
                        ['Obligatory questions:', $data['required_questions']],
                        [''],
                    ]);
                    foreach ($data['items_data']['summary'] as $item) {
                        $questionRow = isset($item['answers'])
                            ? array_merge($item['question'], $item['answers'])
                            : array_merge($item['question'], ['Result']);
                        $sheet->appendRow($questionRow);
                        if (isset($item['responses'])) {
                            $sheet->appendRow(array_merge(['Responses'], $item['responses']));
                        }
                        $sheet->appendRow(array_merge(['Total'], $item['total']));
                        $sheet->appendRow(['']);
                    }
                });
                $excel->sheet('Personal answers', function ($sheet) use ($data) {
                    $sheet->setStyle([
                        'font' => [
                            'size' => 12
                        ],
                    ]);
                    $sheet->rows([
                        ['Report created on:', $data['current_date']],
                        ['Total number of questions:', $data['questions_count']],
                        ['Total number of respondents:', $data['respondents_count']],
                        ['Obligatory questions:', $data['required_questions']],
                        [],
                        array_merge(['Question'], $data['items_data']['questions'])
                    ]);
                    foreach ($data['items_data']['answers'] as $answer) {
                        $sheet->appendRow($answer);
                    }
                });
            }
        )->store('xlsx', $filePath);
    }
}
