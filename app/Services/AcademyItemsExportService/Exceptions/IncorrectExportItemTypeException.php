<?php

namespace App\Services\AcademyItemsExportService\Exceptions;

class IncorrectExportItemTypeException extends \Exception
{
}