<?php

namespace App\Services\AcademyItemsExportService;

use App\Services\AcademyItemsExportService\Contracts\AcademyItemsExportInterface;
use App\Services\AcademyItemsExportService\Exceptions\IncorrectExportItemTypeException;

class AcademyItemsExporter
{
    private $params;
    private $type;
    private $exporter;

    /**
     * AcademyItemsExport constructor.
     *
     * @param \App\Services\AcademyItemsExportService\ExcelExporter $excelExporter
     */
    public function __construct(ExcelExporter $excelExporter)
    {
        $this->exporter = $excelExporter;
    }

    /**
     * @param array $params
     * @return AcademyItemsExporter
     */
    public function setParams(array $params): AcademyItemsExporter
    {
        $this->params = $params;

        return $this;
    }

    /**
     * @param mixed $type
     * @return AcademyItemsExporter
     */
    public function setType($type): AcademyItemsExporter
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @param Contracts\AcademyItemsExportInterface $exporter
     * @return AcademyItemsExporter
     */
    public function setExporter(AcademyItemsExportInterface $exporter): AcademyItemsExporter
    {
        $this->exporter = $exporter;

        return $this;
    }

    /**
     * @return array
     * @throws \App\Services\AcademyItemsExportService\Exceptions\IncorrectExportItemTypeException
     */
    public function export()
    {
        switch ($this->type) {
            case 'survey':
                return $this->exporter->getSurveysData($this->params);
            default:
                throw new IncorrectExportItemTypeException;
        }
    }
}
