<?php

namespace App\Services\Base64ImageReplacer;

use Illuminate\Support\Facades\Storage;
use Symfony\Component\DomCrawler\Crawler;

class Base64ImageReplacer
{
    private $content;
    private $crawler;

    /**
     * ImageReplacer constructor.
     *
     * @param \Symfony\Component\DomCrawler\Crawler $crawler
     */
    public function __construct(Crawler $crawler)
    {
        $this->crawler = $crawler;
    }

    /**
     * @param string $content
     * @return \App\Services\Base64ImageReplacer\Base64ImageReplacer
     */
    public function setContent(string $content): Base64ImageReplacer
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Replace base64 images with urls
     *
     * @return string
     * @throws \Throwable
     */
    public function replaceImages(): string
    {
        throw_if(!isset($this->content), new UndefinedContentException('You must set content first'));
        $base64Images = $this->getBase64Images();
        if (!empty($base64Images)) {
            foreach ($base64Images as $base64Image) {
                $imageUrl = $this->uploadImages($base64Image);
                $this->content = str_replace($base64Image, $imageUrl, $this->content);
            }
        }

        return $this->content;
    }

    /**
     * Get base64 images from img tags
     *
     * @return array
     */
    private function getBase64Images(): array
    {
        $this->crawler->addHtmlContent($this->content);
        $images = $this->crawler->filter('img')->extract(['src']);
        $base64Images = preg_grep(
            sprintf(
                '/^data:(%s);base64,/',
                addcslashes(implode('|', config('media.app_layout.available_mime_types')), '/')
            ),
            $images
        );

        return $base64Images;
    }

    /**
     * Decode and upload base64 images
     *
     * @param string $base64Image
     * @return string
     * @throws \Exception
     */
    private function uploadImages(string $base64Image): string
    {
        $pattern = sprintf(
            '/^data:(%s);base64,/',
            addcslashes(implode('|', config('media.app_layout.available_mime_types')), '/')
        );
        $image = base64_decode(preg_replace($pattern, '', $base64Image));
        preg_match($pattern, $base64Image, $mimeType);
        $mimeType = explode('/', $mimeType[1]);
        $extension = end($mimeType);
        $fileName = sprintf('%s.%s', bin2hex(random_bytes(10)), $extension);
        $filePath = sprintf('%s/%s', config('media.app_layout.folder'), $fileName);
        Storage::put($filePath, $image, 'public');

        return Storage::url($filePath);
    }
}
