<?php

namespace App\Services;

use Chumper\Zipper\Zipper;
use Illuminate\Support\Facades\File;

class ZipArchiveService
{
    private $zipper;

    public function __construct(Zipper $zipper)
    {
        $this->zipper = $zipper;
    }

    public function make(string $path, string $name, bool $deleteSourcePath = true): string
    {
        $zipPath = storage_path("exports/$name.zip");
        $this->zipper->make($zipPath)->add($path)->close();
        if ($deleteSourcePath) {
            File::deleteDirectory($path);
        }

        return $zipPath;
    }
}
