<?php

namespace App\Services\Statistics;

use App\Http\Requests\Dashboard\DataGetRequest;
use App\Http\Requests\Dashboard\ChannelDataGetRequest;
use App\Http\Requests\Request;

use App\Models\Channel;
use App\Models\User;
use App\Transformers\DashboardChartTransformer;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

use App\Models\Dealer;
use App\Models\UserChannelStatus;
use App\Models\PointsStatistic;

class DashboardStatisticsService extends ChannelStatisticsService
{
    /**
     * @var array
     */
    protected $filterable = [
        'user_id',
        'dealer_id',
    ];

    /**
     * @param \App\Http\Requests\Dashboard\DataGetRequest $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\UserChannelStatus $userChannelStatus
     * @param \App\Models\User $user
     * @return mixed
     */
    public function getGeneral(DataGetRequest $request, Dealer $dealer, UserChannelStatus $userChannelStatus, User $user)
    {
        $dealers = $dealer
            ->select(DB::raw("COUNT({$this->ud}.id) as users_count"))
            ->leftJoin($this->ud, "{$this->ud}.dealer_id", '=', "{$this->d}.id")
            ->where("{$this->d}.channel_id", $request->channel_id)
            ->when($request->league_id, function ($q) use ($request) {
                return $q->where("$this->d.league_id", $request->league_id);
            })
            ->groupBy("{$this->d}.id");

        if (! empty($request->filter)) {
            $filters = $this->getQueriedFilters($request);
            $filter = key($filters);
            $searchedId = abs(intval(reset($filters)));

            if ($filter == 'dealer_id' && $searchedId) {
                return $dealers->where("{$this->d}.id", $searchedId)->first();
            }
        }

        $dealers->addSelect("{$this->d}.id");

        $users = $userChannelStatus
            ->select(DB::raw("COUNT({$this->ucs}.id) as users_count"))
            ->where("{$this->ucs}.channel_id", '=', $request->channel_id);

        $this->onlyAppUsers($users, $this->ucs);
        
        if ($request->league_id) {
            $dealerUsers = $user->whereHas('dealers', function ($q) use ($request) {
                $q->where('league_id', $request->league_id);
            });
            $users->whereIn('user_id', $dealerUsers->get()->pluck('id')->all());
        }

        $query = DB::table(DB::raw("({$dealers->toSql()}) as d, ({$users->toSql()}) as u"))
            ->select(
                DB::raw('u.users_count as users_count'),
                DB::raw('COUNT(d.id) as total_dealers'),
                DB::raw('COUNT(CASE d.users_count WHEN 0 THEN 1 ELSE NULL END) as zero_dealers')
            )
            ->addBinding($dealers->getQuery()->getBindings())
            ->addBinding($users->getQuery()->getBindings())
            ->groupBy('u.users_count');

        return ['data' => $query->first()];
    }

    /**
     * @param \App\Http\Requests\Dashboard\DataGetRequest $request
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @param \App\Transformers\DashboardChartTransformer $transformer
     * @return mixed
     */
    public function getChart(
        DataGetRequest $request,
        PointsStatistic $pointsStatistic,
        DashboardChartTransformer $transformer
    ) {
        if (empty($request->date_from)) {
            $now = Carbon::now();
            $channel = Channel::with('currentCampaign')->find($request->channel_id);

            $dateFrom = empty($channel) || empty($channel->currentCampaign)
                ? $now->subDays(7)->toDateTimeString()
                : $channel->currentCampaign->started_at;
            $dateTo = $now->toDateTimeString();
        } else {
            $dateFrom = $request->date_from;
            $dateTo = $request->date_to;
        }
        $subQuery = $pointsStatistic->select(
                DB::raw("DATE_FORMAT({$this->ps}.created_at, '%Y-%m-%d') as date"),
                DB::raw("COUNT({$this->ps}.earned_points) as total_count"),
                DB::raw("SUM(CASE WHEN {$this->ps}.object_type='product' THEN {$this->ps}.units_number ELSE NULL END) as sold_products"),
                DB::raw("COUNT(CASE WHEN {$this->ps}.object_type IN ('quiz','survey') THEN 1 ELSE NULL END) as passed_items"),
                DB::raw("COUNT(CASE WHEN {$this->ps}.object_type IN ('manual','level') THEN 1 ELSE NULL END) as others")
            )
            ->where("{$this->ps}.channel_id", $request->channel_id)
            ->whereBetween("{$this->ps}.created_at", [$dateFrom, $dateTo])
            ->getItemsByUserLeague($request)
            ->groupBy(
                DB::raw("DAY({$this->ps}.created_at)"),
                DB::raw('date')
            )
            ->orderBy('date', 'asc');

        $this->addFilters($subQuery, $request);

        $query = DB::table(DB::raw(
                "({$subQuery->toSql()}) as ps, 
                (SELECT
                   @cum_total_count := 0,
                   @cum_sold_products := 0,
                   @cum_passed_items := 0,
                   @cum_others := 0
                ) as vars"
            ))
            ->addBinding($subQuery->getQuery()->getBindings())
            ->select(
                DB::raw("ps.date"),
                DB::raw("(COALESCE(ps.sold_products, 0) + COALESCE(ps.passed_items, 0) + COALESCE(ps.others, 0)) as total_count"),
                DB::raw("ps.sold_products"),
                DB::raw("ps.passed_items"),
                DB::raw("ps.others"),
                DB::raw("@cum_total_count := @cum_total_count + ps.total_count as cum_total_count"),
                DB::raw("@cum_sold_products := @cum_sold_products + ps.sold_products as cum_sold_products"),
                DB::raw("@cum_passed_items := @cum_passed_items + ps.passed_items as cum_sold_products"),
                DB::raw("@cum_others := @cum_others + ps.others as cum_others")
            );

        return $transformer->setDates($dateFrom, $dateTo)->transform($query->get());
    }

    /**
     * @param \App\Http\Requests\Dashboard\DataGetRequest $request
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @return mixed
     */
    public function getTotals(DataGetRequest $request, PointsStatistic $pointsStatistic)
    {
        $query = $pointsStatistic
            ->select(
                DB::raw("SUM({$this->ps}.earned_points) as total_points"),
                DB::raw("SUM(CASE WHEN {$this->ps}.object_type='product' THEN {$this->ps}.earned_points ELSE NULL END) as product_points"),
                DB::raw("SUM(CASE WHEN {$this->ps}.object_type='quiz' THEN {$this->ps}.earned_points ELSE NULL END) as quizzes_points"),
                DB::raw("SUM(CASE WHEN {$this->ps}.object_type='survey' THEN {$this->ps}.earned_points ELSE NULL END) as surveys_points"),
                DB::raw("SUM(CASE WHEN {$this->ps}.object_type='manual' THEN {$this->ps}.earned_points ELSE NULL END) as manual_points"),
                DB::raw("SUM(CASE WHEN {$this->ps}.object_type='level' THEN {$this->ps}.earned_points ELSE NULL END) as level_points"),
                DB::raw("SUM(CASE WHEN {$this->ps}.object_type='quiz' OR {$this->ps}.object_type='survey' THEN {$this->ps}.earned_points ELSE NULL END) as items_points"),
                DB::raw("SUM(CASE WHEN {$this->ps}.object_type='manual' OR {$this->ps}.object_type='level' THEN {$this->ps}.earned_points ELSE NULL END) as other_points"),
                DB::raw("SUM(CASE WHEN {$this->ps}.object_type='product' THEN {$this->ps}.units_number ELSE NULL END) as sold_products"),
                DB::raw("COUNT(CASE WHEN {$this->ps}.object_type='quiz' THEN 1 ELSE NULL END) as passed_quizzes"),
                DB::raw("COUNT(CASE WHEN {$this->ps}.object_type='survey' THEN 1 ELSE NULL END) as passed_surveys"),
                DB::raw("COUNT(CASE WHEN {$this->ps}.object_type='manual' THEN 1 ELSE NULL END) as added_manually"),
                DB::raw("COUNT(CASE WHEN {$this->ps}.object_type='level' THEN 1 ELSE NULL END) as achieved_levels"),
                DB::raw("COUNT(CASE WHEN {$this->ps}.object_type='quiz' OR {$this->ps}.object_type='survey' THEN 1 ELSE NULL END) as passed_items"),
                DB::raw("COUNT(CASE WHEN {$this->ps}.object_type='manual' OR {$this->ps}.object_type='level' THEN 1 ELSE NULL END) as others")
            )
            ->getItemsByUserLeague($request);

        $this->addFilters($query, $request);

        return ['data' => $query->first()];
    }

    /**
     * @param \App\Http\Requests\Dashboard\DataGetRequest $request
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @return mixed
     */
    public function getProducts(DataGetRequest $request, PointsStatistic $pointsStatistic)
    {
        $subQuery = $pointsStatistic->select(
                "{$this->ps}.object_id",
                DB::raw("SUM({$this->ps}.units_number) as sold_products"),
                DB::raw("SUM({$this->ps}.earned_points) as product_points")
            )
            ->where("{$this->ps}.object_type", 'product')
            ->getItemsByUserLeague($request)
            ->groupBy("{$this->ps}.object_id");

        $this->addFilters($subQuery, $request);

        $query = DB::table($this->pr)
            ->select(
                "{$this->pr}.id",
                "{$this->pr}.name",
                "{$this->pr}.points",
                DB::raw("IFNULL(sold.sold_products, 0) as sold_products"),
                DB::raw("IFNULL(sold.product_points, 0) as product_points")
            )
            ->leftJoin(
                DB::raw("({$subQuery->toSql()}) as sold"),
                'sold.object_id', '=', "{$this->pr}.id"
            )
            ->mergeBindings($subQuery->getQuery())
            ->where("{$this->pr}.channel_id", '=', $request->channel_id)
            ->orderBy("sold_products", 'desc');

        return $query->get();
    }

    /**
     * @param \App\Http\Requests\Dashboard\ChannelDataGetRequest $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @return mixed
     */
    public function getDealers(
        ChannelDataGetRequest $request,
        Dealer $dealer,
        PointsStatistic $pointsStatistic
    ) {
        $usersInDealersQuery = $dealer->select(
                "{$this->d}.id",
                DB::raw("COUNT({$this->ud}.user_id) as users_count")
            )
            ->leftJoin($this->ud, "{$this->ud}.dealer_id", '=', "{$this->d}.id")
            ->where("{$this->d}.channel_id", $request->channel_id)
            ->when($request->league_id, function ($q) use ($request) {
                return $q->where("{$this->d}.league_id", $request->league_id);
            })
            ->groupBy("{$this->d}.id");

        $statQuery = $pointsStatistic->select(
                "{$this->ps}.user_id",
                "{$this->ud}.dealer_id",
                DB::raw("SUM(CASE WHEN {$this->ps}.object_type='product' THEN {$this->ps}.earned_points ELSE NULL END) as product_points"),
                DB::raw("SUM(CASE WHEN {$this->ps}.object_type='survey' OR {$this->ps}.object_type='quiz' THEN {$this->ps}.earned_points ELSE NULL END) as items_points"),
                DB::raw("SUM(CASE WHEN {$this->ps}.object_type='auto' OR {$this->ps}.object_type='manual' OR {$this->ps}.object_type='level' THEN {$this->ps}.earned_points ELSE NULL END) as extra_points"),
                DB::raw("SUM({$this->ps}.earned_points) as total_points"),
                DB::raw("COUNT({$this->ps}.id) as total_points_count")
            )
            ->leftJoin($this->ud, "{$this->ud}.user_id", '=', "{$this->ps}.user_id")
            ->where("{$this->ps}.channel_id", $request->channel_id)
            ->getItemsByUserLeague($request)
            ->groupBy(
                "{$this->ps}.user_id",
                "{$this->ud}.dealer_id"
            );
        $this->addDateRange($statQuery, $request);

        $query = DB::table($this->d)
            ->select(
                "{$this->d}.id",
                "{$this->d}.name",
                "{$this->m}.path as logo_path",
                "{$this->d}.logo_id",
                "{$this->m}.thumbnails as thumbnails",
                "uid.users_count",
                DB::raw("SUM(stat.total_points) as total_points"),
                DB::raw("SUM(stat.total_points_count) as total_points_count"),
                DB::raw("SUM(stat.product_points) as product_points"),
                DB::raw("SUM(stat.items_points) as items_points"),
                DB::raw("SUM(stat.extra_points) as extra_points")
            )
            ->leftJoin($this->m, "{$this->m}.id", '=', "{$this->d}.logo_id")
            ->leftJoin(
                DB::raw("({$usersInDealersQuery->toSql()}) as uid"),
                "uid.id", "=", "{$this->d}.id"
            )
            ->leftJoin(
                DB::raw("({$statQuery->toSql()}) as stat"),
                "stat.dealer_id", "=", "{$this->d}.id"
            )
            ->mergeBindings($usersInDealersQuery->getQuery())
            ->mergeBindings($statQuery->getQuery())
            ->where("{$this->d}.channel_id", $request->channel_id)
            ->when($request->league_id, function ($q) use ($request) {
                return $q->where("{$this->d}.league_id", $request->league_id);
            })
            ->groupBy("{$this->d}.id")
            ->orderBy('total_points', 'desc')
            ->limit(5);

        return $query->get();
    }

    /**
     * @param \App\Http\Requests\Dashboard\ChannelDataGetRequest $request
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @return mixed
     */
    public function getUsers(ChannelDataGetRequest $request, PointsStatistic $pointsStatistic)
    {
        $subQuery = $pointsStatistic->select(
                "{$this->ps}.user_id",
                DB::raw("SUM({$this->ps}.earned_points) as total_points"),
                DB::raw("SUM(CASE WHEN {$this->ps}.object_type='product' THEN {$this->ps}.earned_points ELSE NULL END) as product_points"),
                DB::raw("SUM(CASE WHEN {$this->ps}.object_type='quiz' OR {$this->ps}.object_type='survey' THEN {$this->ps}.earned_points ELSE NULL END) as items_points"),
                DB::raw("SUM(CASE WHEN {$this->ps}.object_type='manual' OR {$this->ps}.object_type='level' THEN {$this->ps}.earned_points ELSE NULL END) as other_points")
            )
            ->where("{$this->ps}.channel_id", '=', $request->channel_id)
            ->groupBy("{$this->ps}.user_id");

        $this->addDateRange($subQuery, $request);

        $query = DB::table($this->d)
            ->select(
                "{$this->u}.id",
                "{$this->d}.id as dealer_id",
                "{$this->d}.name as dealer_name",
                "{$this->m}.path as avatar_path",
                "{$this->m}.id as avatar_id",
                "{$this->m}.thumbnails as thumbnails",
                DB::raw("TRIM(CONCAT({$this->u}.first_name, ' ', {$this->u}.last_name)) as full_name"),
                DB::raw("IFNULL(total.total_points, 0) as total_points"),
                DB::raw("IFNULL(total.product_points, 0) as product_points"),
                DB::raw("IFNULL(total.items_points, 0) as items_points"),
                DB::raw("IFNULL(total.other_points, 0) as other_points")
            )
            ->leftJoin($this->ud, "{$this->d}.id", '=', "{$this->ud}.dealer_id")
            ->leftJoin($this->u, "{$this->ud}.user_id", '=', "{$this->u}.id")
            ->leftJoin($this->ps, "{$this->u}.id", '=', "{$this->ps}.user_id")
            ->leftJoin($this->m, "{$this->m}.id", '=', "{$this->u}.avatar_id")
            ->leftJoin(
                DB::raw("({$subQuery->toSql()}) as total"),
                "total.user_id", "=", "{$this->u}.id"
            )
            ->where("{$this->d}.channel_id", $request->channel_id);

        if ($request->league_id) {
            $query->where("{$this->d}.league_id", $request->league_id);
        }

        $query->addBinding($subQuery->getQuery()->getBindings(), 'select')
            ->groupBy(
                "{$this->u}.id",
                "{$this->d}.id",
                "{$this->m}.id"
            )
            ->whereNull("{$this->u}.deleted_at")
            ->orderBy('total_points', 'desc')
            ->limit(5);

        $this->onlyChannelUsers($query, $request, $this->u, $this->ucs, 'id');

        return $query->get();
    }

    /**
     * @param \App\Http\Requests\Dashboard\DataGetRequest $request
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @param string $type
     * @return mixed
     */
    public function getAcademyItems(DataGetRequest $request, PointsStatistic $pointsStatistic, string $type)
    {
        $subQuery = $pointsStatistic->select(
                "{$this->ps}.object_id",
                DB::raw("COUNT({$this->ps}.object_id) as passed_items"),
                DB::raw("SUM({$this->ps}.earned_points) as items_points")
            )
            ->join($this->ai, "{$this->ai}.id", '=', "{$this->ps}.object_id")
            ->where("{$this->ps}.object_type", $type)
            ->where("{$this->ps}.channel_id", $request->channel_id)
            ->getItemsByUserLeague($request)
            ->groupBy("{$this->ps}.object_id");

        $this->addFilters($subQuery, $request);

        $query = DB::table($this->ai)
            ->select(
                "{$this->ai}.id",
                "{$this->ai}.title",
                "{$this->ai}.started_at",
                "{$this->ai}.points",
                "{$this->m}.id as thumbnail_id",
                "{$this->m}.path as thumbnail_path",
                "{$this->m}.thumbnails as thumbnails",
                DB::raw("IFNULL(passed.passed_items, 0) as passed_items"),
                DB::raw("IFNULL(passed.items_points, 0) as items_points")
            )
            ->leftJoin($this->m, "{$this->m}.id", '=', "{$this->ai}.thumbnail_id")
            ->leftJoin(
                DB::raw("({$subQuery->toSql()}) as passed"),
                'passed.object_id', '=', "{$this->ai}.id"
            )
            ->mergeBindings($subQuery->getQuery())
            ->where("{$this->ai}.channel_id", $request->channel_id)
            ->where("{$this->ai}.status", 'published')
            ->where("{$this->ai}.type", $type)
            ->orderBy("passed_items", 'desc');

        return $query->get();
    }

    /**
     * @param $query
     * @param \App\Http\Requests\Request $request
     */
    protected function addFilters($query,  Request $request)
    {

        if (empty($request->filter)) {
           $this->onlyChannelUsers($query, $request);
        } else {
            $filters = $this->getQueriedFilters($request);
            $filter = key($filters);
            $searchedId = abs(intval(reset($filters)));
            if ($searchedId) {
                switch($filter) {
                    case 'dealer_id':
                        $this->onlyDealerUsers($query, $searchedId);
                        break;
                    case 'user_id':
                        $this->onlyUser($query, $searchedId);
                        break;
                    default:
                        break;
                }
            }
        }
        $this->addDateRange($query, $request);

    }

    /**
     * @param $query
     * @param int $dealer_id
     * @param string|null $table
     */
    protected function onlyDealerUsers($query,  int $dealer_id, string $table = null)
    {
        $table = $table ? $table : $this->ps;
        $query->join($this->ud, "{$this->ud}.user_id", '=', "{$table}.user_id")
            ->join($this->d, function($join) use ($dealer_id) {
                $join->on( "{$this->d}.id", '=', "{$this->ud}.dealer_id")
                    ->where("{$this->d}.id", $dealer_id);
            });
    }

    /**
     * @param $query
     * @param int $user_id
     * @param string|null $table
     */
    protected function onlyUser($query, int $user_id, string $table = null)
    {
        $table = $table ? $table : $this->ps;
        $query->where("{$table}.user_id", $user_id);
    }
}
