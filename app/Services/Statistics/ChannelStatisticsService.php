<?php

namespace App\Services\Statistics;

use App\Http\Requests\Request;

use App\Models\User;
use App\Services\Service;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

use App\Traits\ExcelStyling;
use App\Transformers\CollectionTransformer;

use App\Models\PointsStatistic;
use App\Models\UserChannelStatus;
use App\Models\Product;
use App\Models\AcademyItem;

class ChannelStatisticsService extends Service
{
    use ExcelStyling;

    /**
     * @var array
     */
    protected $searchable = [];

    /**
     * @var array
     */
    protected $filterable = [];

    /**
     * @param int|null $total
     * @param int|null $part
     * @return float|int
     */
    protected function getPercentage(int $total = null, int $part = null)
    {
        return $total ? $part / $total : 0;
    }

    /**
     * @return array
     */
    public function getFilterable()
    {
        return $this->filterable;
    }

    /**
     * @return array
     */
    public function getSortable()
    {
        return $this->sortable;
    }

    /**
     * @param $query
     * @param $request
     * @param string|null $table
     * @param null $whereTable
     * @param string|null $column
     */
    protected function onlyChannelUsers($query, $request, string $table = null, $whereTable = null, string $column = null)
    {
        $table = $table ? $table : $this->ps;
        $whereTable = $whereTable ? $whereTable : $this->ps;
        $channelID = is_numeric($request) ? $request : $request->channel_id;
        $column = $column ? $column : 'user_id';

        $query->join($this->ucs, function($clause) use ($table, $column, $channelID) {
            $clause->on("{$this->ucs}.user_id", '=', "{$table}.{$column}")
                ->where("{$this->ucs}.channel_id", '=', $channelID);
        })
            ->where("{$whereTable}.channel_id", '=', $channelID);

        $this->onlyAppUsers($query, $table, $column);
    }

    /**
     * @param $query
     * @param string|null $table
     * @param string|null $column
     */
    protected function onlyAppUsers($query, string $table = null, string $column = null)
    {
        $table = $table ? $table : $this->ps;
        $column = $column ? $column : 'user_id';

        $query->join($this->mhr, function($clause) use ($table, $column) {
            $clause->on("{$this->mhr}.model_id", '=', "{$table}.{$column}")
                ->whereIn("{$this->mhr}.role_id", $this->appRoleIDs);
        });
    }

    /**
     * @param $query
     * @param \App\Http\Requests\Request $request
     * @param string|null $table
     */
    protected function addDateRange($query, Request $request, string $table = null)
    {
        $table = $table ? $table : $this->ps;
        if (! empty($request->date_from)) {
            $query->whereBetween("{$table}.created_at", [new Carbon($request->date_from), new Carbon($request->date_to)]);
        }
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @return array
     */
    protected function getQueriedFilters(Request $request)
    {
        $filters = [];
        $raw = explode(',', $request->filter);
        foreach ($raw as $filter) {
            list($criteria, $value) = explode(':', $filter);
            $filters[$criteria] = $value;
        }
        return $filters;
    }

    /**
     * @param $query
     * @param \App\Http\Requests\Request $request
     */
    protected function addPointsStatisticsFilters($query, Request $request)
    {
        $this->addDateRange($query, $request);

        if (! empty($request->filter)) {
            $filters = $this->getQueriedFilters($request);
            $filter = key($filters);
            $searchedId = abs(intval(reset($filters)));
            if ($searchedId) {
                switch($filter) {
                    case "dealer_id":
                        $query->whereIn("{$this->ps}.user_id", function($subQuery) use ($searchedId) {
                            $subQuery->select("user_id")
                                ->from($this->ud)
                                ->where('dealer_id', $searchedId);
                        });
                        break;
                    case "district_id":
                        $query->whereIn("{$this->ps}.user_id", function($subQuery) use ($searchedId) {
                            $subQuery->select("{$this->ud}.user_id")
                                ->from($this->ud)
                                ->join($this->d, function($join) use ($searchedId) {
                                    $join->on("{$this->d}.id", '=', "{$this->ud}.dealer_id")
                                        ->where("{$this->d}.district_id", $searchedId);
                                });
                        });
                        break;
                    case "position_id":
                        $query->whereIn("{$this->ps}.user_id", function($subQuery) use ($searchedId) {
                            $subQuery->select("user_id")
                                ->from($this->up)
                                ->where('position_id', $searchedId);
                        });
                        break;
                }
            }
        }
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param \App\Models\Product $product
     * @return mixed
     */
    protected function getChannelProducts(Request $request, Product $product)
    {
        return $product->where('channel_id', $request->channel_id)
            ->orderBy('id', 'asc')
            ->get(['id', 'name']);
    }

    /**
     * @param $query
     * @param \App\Http\Requests\Request $request
     * @param bool $withPagination
     * @return mixed
     */
    protected function getList($query, Request $request, bool $withPagination = true)
    {
        /**
         * filters
         */
        if (! empty($request->filter)) {
            $filters = $this->getQueriedFilters($request);
            $filter = key($filters);
            $searchedId = abs(intval(reset($filters)));
            if ($searchedId) {
                $query->where($filter, $searchedId);
            }
        }

        /**
         * search
         */
        if (! empty($request->search)) {
            $i = 0;
            foreach ($this->searchable as $column) {
                $where = $i ? 'orWhere' : 'where';
                $query = $query->$where($column, 'like', "%{$request->search}%");
                $i++;
            }
        }

        /**
         * sorting
         */
        if (! empty($request->sort)) {
            $sorts = explode(',', $request->sort);
            foreach ($sorts as $sortCol) {
                $sortDir = starts_with($sortCol, '-') ? 'desc' : 'asc';
                $sortCol = ltrim($sortCol, '-');

                $query->orderBy($sortCol, $sortDir);
            }
        }

        /**
         * pagination
         */
        return $withPagination ? $query->paginate($request->per_page ?? 20) : $query->get();
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    protected function getByPointType(Request $request, PointsStatistic $pointsStatistic)
    {
        $types = $pointsStatistic->select(
            "{$this->ps}.object_type",
            DB::raw("SUM({$this->ps}.units_number) as amount"),
            DB::raw("SUM({$this->ps}.earned_points) as points")
        );
        $this->addPointsStatisticsFilters($types, $request);
        $this->onlyChannelUsers($types, $request);
        $types->groupBy("{$this->ps}.object_type");

        $total = $pointsStatistic->select(DB::raw("SUM({$this->ps}.earned_points) as points"));
        $this->addPointsStatisticsFilters($total, $request);
        $this->onlyChannelUsers($total, $request);

        $list = DB::table(DB::raw("({$types->toSql()}) as base, ({$total->toSql()}) as total"))
            ->addBinding($types->getQuery()->getBindings())
            ->addBinding($total->getQuery()->getBindings())
            ->select(
                DB::raw("base.*"),
                DB::raw("IFNULL(base.points/total.points, 0) as percentage")
            );

        $query = DB::table(DB::raw( "({$list->toSql()}) as r"))
            ->orderBy('percentage', 'DESC')
            ->addBinding($types->getQuery()->getBindings())
            ->addBinding($total->getQuery()->getBindings());

        return $query->get();
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    protected function getByProducts(Request $request, PointsStatistic $pointsStatistic)
    {

        $products = $pointsStatistic->select(
                "{$this->pr}.name as name",
                DB::raw("SUM({$this->ps}.units_number) as amount"),
                DB::raw("SUM({$this->ps}.earned_points) as points")
            )
            ->leftJoin($this->pr, "{$this->pr}.id", '=', "{$this->ps}.object_id")
            ->where("{$this->ps}.object_type", 'product')
            ->groupBy("{$this->pr}.name");
        $this->addPointsStatisticsFilters($products, $request);
        $this->onlyChannelUsers($products, $request);

        $total = $pointsStatistic->select(DB::raw("SUM({$this->ps}.earned_points) as points"))
            ->where("{$this->ps}.object_type", 'product');
        $this->addPointsStatisticsFilters($total, $request);
        $this->onlyChannelUsers($total, $request);

        $list = DB::table(DB::raw("({$products->toSql()}) as base, ({$total->toSql()}) as total"))
            ->addBinding($products->getQuery()->getBindings())
            ->addBinding($total->getQuery()->getBindings())
            ->select(
                DB::raw("base.*"),
                DB::raw("IFNULL(base.points/total.points, 0) as percentage")
            );

        $query = DB::table(DB::raw( "({$list->toSql()}) as r"))
            ->orderBy('percentage', 'DESC')
            ->addBinding($products->getQuery()->getBindings())
            ->addBinding($total->getQuery()->getBindings());

        return $query->get();
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param $sheet
     * @param int $index
     * @param string $label
     * @return int
     */
    public function addSheetSignatures(Request $request, $sheet, int $index, string $label)
    {
        $from = empty($request->date_from) ? Carbon::today() : $request->date_from;
        $to = empty($request->date_to) ? Carbon::today() : $request->date_to;
        $headers = [
            'STORE Report' => "STORE {$label} Statistics",
            'Date range' => Carbon::parse($from)->format('Y-m-d').' - '.Carbon::parse($to)->format('Y-m-d'),
            'Generated' => Carbon::today()->format('Y-m-d'),
            'By' => auth()->user()->full_name,
        ];

        foreach($headers as $label => $value) {
            $sheet->row($index, [$label, $value]);
            $sheet->row($index, function ($row) {
                $row->setFontWeight('bold');
            });
            $index++;
        }
        return $index;
    }

    /**
     * @param $sheet
     * @param int $index
     * @param \App\Http\Requests\Request $request
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @param \App\Transformers\CollectionTransformer $transformer
     * @return int
     */
    protected function addPointsStatistics($sheet, int $index, Request $request, PointsStatistic $pointsStatistic, CollectionTransformer $transformer)
    {
        $sheet->row($index, [
            'Category',
            'Reg',
            'Points',
            'Percentage'
        ]);
        $sheet->row($index, function ($row) {
            $this->addHeaderStyles($row);
        });
        $index++;

        $pointsTypeStatistics = $transformer->transform(collect(
            $this->getByPointType($request, $pointsStatistic)
        ));

        $firstInRange = $index;
        $pointsTypeStatistics->each(function($item) use ($sheet, &$index) {
            $sheet->row($index, [
                $item->object_type,
                $item->amount,
                $item->points,
                $item->percentage,
            ]);
            $sheet->row($index, function($row) {
                $row->setAlignment('right');
            });
            $this->setPercentFormat($sheet, $index, 'D');
            $index++;
        });
        if ($firstInRange !== $index) {
            $lastInRange = $index - 1;
            $sheet->row($index, [
                '',
                "=SUM(B{$firstInRange}:B{$lastInRange})",
                "=SUM(C{$firstInRange}:C{$lastInRange})",
                "=SUM(D{$firstInRange}:D{$lastInRange})",
            ]);
            $sheet->row($index, function ($row) {
                $this->addFooterStyle($row);
            });
        }
        $this->setPercentFormat($sheet, $index, 'D');
        return $index;
    }

    /**
     * @param $sheet
     * @param int $index
     * @param \App\Http\Requests\Request $request
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @return int
     */
    protected function addProductsStatistics($sheet, int $index, Request $request, PointsStatistic $pointsStatistic)
    {
        $sheet->row($index, [
            'Product',
            'Reg',
            'Points',
            'Percentage'
        ]);
        $sheet->row($index, function ($row) {
            $this->addHeaderStyles($row);
        });
        $index++;

        $products = $this->getByProducts($request, $pointsStatistic);
        $firstInRange = $index;
        $products->each(function($item) use ($sheet, &$index) {
            $sheet->row($index, [
                $item->name,
                $item->amount,
                $item->points,
                $item->percentage,
            ]);
            $sheet->row($index, function($row) {
                $row->setAlignment('right');
            });
            $this->setPercentFormat($sheet, $index, 'D');
            $index++;
        });
        if ($firstInRange !== $index) {
            $lastInRange = $index - 1;
            $sheet->row($index, [
                '',
                "=SUM(B{$firstInRange}:B{$lastInRange})",
                "=SUM(C{$firstInRange}:C{$lastInRange})",
                "=SUM(D{$firstInRange}:D{$lastInRange})",
            ]);
            $sheet->row($index, function ($row) {
                $this->addFooterStyle($row);
            });
        }
        $this->setPercentFormat($sheet, $index, 'D');
        return $index;
    }

    /**
     * @param int $channelID
     * @param int|null $leagueId
     * @return array
     */
    protected function getChannelItemsCount(int $channelID, ?int $leagueId = null)
    {
        $counts = [];
        /* Academy Items */
        foreach (['survey', 'quiz'] as $type) {
            $counts[$type] = AcademyItem::select(DB::raw('COUNT(*) as count'))
                ->where('type', $type)
                ->where('status', 'published')
                ->where('channel_id', $channelID)
                ->first()->count;
        }

        /* Users */
        $query = User::whereHas('channelStatus', function ($q) use ($channelID) {
            $q->where('channel_id', $channelID);
        })->whereHas('roles', function ($q) {
            $q->whereIn('roles.id', $this->appRoleIDs);
        })->when($leagueId, function ($query) use ($channelID, $leagueId) {
            $query->whereHas('dealers', function ($q) use ($channelID, $leagueId) {
                $q->where(['channel_id' => $channelID, 'league_id' => $leagueId]);
            });
        });
        $counts['users'] = $query->count();

        return $counts;
    }
}
