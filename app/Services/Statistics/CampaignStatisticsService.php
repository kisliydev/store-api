<?php

namespace App\Services\Statistics;

use App\Http\Requests\Campaign\CampaignExportRequest;
use App\Models\Campaign;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

use Excel;
use PHPExcel_Cell;

class CampaignStatisticsService extends ChannelStatisticsService
{
    /**
     * @var array
     */
    protected $searchable = [];

    /**
     * @var array
     */
    protected $filterable = [];

    /**
     * @param \App\Http\Requests\Campaign\CampaignExportRequest $request
     * @param \App\Models\Campaign $campaign
     * @return mixed
     */
    public function exportStatistics(CampaignExportRequest $request, Campaign $campaign)
    {
        $campaign = $campaign->with('channel.countryBrand.brand')->find($request->campaign_id);

        return Excel::create('statistics', function($excel) use ($request, $campaign) {
            $excel->sheet('Statistics', function($sheet) use ($request, $campaign) {

                $lastColumnIndex = 16;

                $i = $this->addSignatures($campaign, $sheet, 2, 'Campaign' );
                $i += 2;

                /**
                 * Add table Header
                 */
                $i = $this->addTableHeaders($sheet, $i, [
                    'Personal Details' => 5,
                    'Total' => 0,
                    'Quizzes' => 1,
                    'Surveys' => 1,
                    'Products' => 1,
                    'Manual' => 1,
                    'Level' => 1,
                ]);
                $i++;

                $subHeaders = [
                    'Rank',
                    'Name',
                    'Email',
                    'Phone',
                    'Position',
                    'Dealer',
                    '',
                ];
                $temp = [
                    'Reg',
                    'Points',
                ];
                for ($j = 0; $j < 5; $j++ ) {
                    $subHeaders = array_merge($subHeaders, $temp);
                }
                $sheet->row($i, $subHeaders);
                $sheet->row($i, function ($row) {
                    $this->addHeaderStyles($row);
                });
                $i++;

                /**
                 * Fill main table with data
                 */
                $firstInRange = $i;
                $this->getUsersStatistics($campaign->id)->each(function($user) use ($sheet, &$i) {
                    $sheet->row($i, [
                        $user->rank,
                        $user->full_name,
                        $user->email,
                        $user->phone,
                        $user->position_name,
                        $user->dealer_name,

                        intval($user->total_points),

                        intval($user->passed_quizzes),
                        intval($user->quizzes_points),

                        intval($user->passed_surveys),
                        intval($user->surveys_points),

                        intval($user->sold_products),
                        intval($user->product_points),

                        intval($user->added_manually),
                        intval($user->manual_points),

                        intval($user->achieved_levels),
                        intval($user->level_points),
                    ]);
                    $i++;
                });

                /**
                 * Add table footer
                 */
                if ($firstInRange !== $i) {
                    $lastInRange = $i - 1;
                    $data = [];
                    foreach (range(0, 5) as $index) {
                        $data[] = "";
                    }
                    foreach (range(6, 16) as $index) {
                        $column = PHPExcel_Cell::stringFromColumnIndex($index);
                        $data[] = "=SUM({$column}{$firstInRange}:{$column}{$lastInRange})";
                    }

                    $sheet->row($i, $data);
                    $sheet->row($i, function ($row) {
                        $this->addFooterStyle($row);
                    });
                }
                $i += 3;

                /**
                 * Points type statistics
                 */
                $i = $this->addTableSignature($sheet, $i, 'Statistics By Points Categories') + 2;
                $sheet->row($i, [
                    'Category',
                    'Reg',
                    'Points',
                    'Percentage'
                ]);
                $sheet->row($i, function ($row) {
                    $this->addHeaderStyles($row);
                });
                $i++;

                $firstInRange = $i;
                $this->getPointsStatistics($campaign->id)->each(function($item) use ($sheet, &$i) {
                    $sheet->row($i, [
                        $item->object_type,
                        $item->amount,
                        $item->points,
                        $item->percentage,
                    ]);
                    $sheet->row($i, function($row) {
                        $row->setAlignment('right');
                    });
                    $this->setPercentFormat($sheet, $i, 'D');
                    $i++;
                });
                if ($firstInRange !== $i) {
                    $lastInRange = $i - 1;
                    $sheet->row($i, [
                        '',
                        "=SUM(B{$firstInRange}:B{$lastInRange})",
                        "=SUM(C{$firstInRange}:C{$lastInRange})",
                        "=SUM(D{$firstInRange}:D{$lastInRange})",
                    ]);
                    $sheet->row($i, function ($row) {
                        $this->addFooterStyle($row);
                    });
                }
                $this->setPercentFormat($sheet, $i, 'D');
                $i += 2;

                /**
                 * Products statistics
                 */
                $i = $this->addTableSignature($sheet, $i, 'Statistics By Products') + 2;
                $sheet->row($i, [
                    'Product',
                    'Reg',
                    'Points',
                    'Percentage'
                ]);
                $sheet->row($i, function ($row) {
                    $this->addHeaderStyles($row);
                });
                $i++;

                $products = $this->getProductsStatistics($campaign->id);
                $firstInRange = $i;
                $products->each(function($item) use ($sheet, &$i) {
                    $sheet->row($i, [
                        $item->name,
                        $item->amount,
                        $item->points,
                        $item->percentage,
                    ]);
                    $sheet->row($i, function($row) {
                        $row->setAlignment('right');
                    });
                    $this->setPercentFormat($sheet, $i, 'D');
                    $i++;
                });
                if ($firstInRange !== $i) {
                    $lastInRange = $i - 1;
                    $sheet->row($i, [
                        '',
                        "=SUM(B{$firstInRange}:B{$lastInRange})",
                        "=SUM(C{$firstInRange}:C{$lastInRange})",
                        "=SUM(D{$firstInRange}:D{$lastInRange})",
                    ]);
                    $sheet->row($i, function ($row) {
                        $this->addFooterStyle($row);
                    });
                }
                $this->setPercentFormat($sheet, $i, 'D');

                $this->setTextWrapper($sheet, $lastColumnIndex, $i);
            });
        })->string('xlsx');
    }

    /**
     * @param \App\Models\Campaign $campaign
     * @param $sheet
     * @param int $index
     * @param string $label
     * @return int
     */
    public function addSignatures(Campaign $campaign, $sheet, int $index, string $label)
    {
        $headers = [
            'Store Report' => "STORE {$label} Statistics",
            'Campaign' => $campaign->name,
            'Channel' => $campaign->channel->name,
            'Country' => $campaign->channel->countryBrand->name,
            'Brand' => $campaign->channel->countryBrand->brand->name,
            'Date Range' => Carbon::parse($campaign->started_at)->format('Y-m-d').' - '.Carbon::parse($campaign->finished_at)->format('Y-m-d'),
            'Generated' => Carbon::today()->format('Y-m-d'),
            'By' => auth()->user()->full_name,
        ];

        foreach($headers as $label => $value) {
            $sheet->row($index, [$label, $value]);
            $sheet->row($index, function ($row) {
                $row->setFontWeight('bold');
            });
            $index++;
        }
        return $index;
    }

    /**
     * @param int $campaignID
     * @return mixed
     */
    public function getUsersStatistics(int $campaignID)
    {
        $users = DB::table($this->ca)->select(
            "{$this->ca}.user_id",
            "{$this->u}.email",
            "{$this->u}.phone",
            "{$this->d}.id as dealer_id",
            "{$this->d}.district_id as district_id",
            "{$this->dis}.name as district_name",
            "{$this->d}.name as dealer_name",
            "{$this->p}.id as position_id",
            "{$this->p}.name as position_name",

            DB::raw("TRIM(CONCAT({$this->u}.first_name, ' ', {$this->u}.last_name)) as full_name"),
            DB::raw("SUM({$this->ca}.earned_points) as total_points"),
            DB::raw("SUM(CASE WHEN {$this->ca}.object_type='product' THEN {$this->ca}.earned_points ELSE NULL END) as product_points"),
            DB::raw("SUM(CASE WHEN {$this->ca}.object_type='quiz' THEN {$this->ca}.earned_points ELSE NULL END) as quizzes_points"),
            DB::raw("SUM(CASE WHEN {$this->ca}.object_type='survey' THEN {$this->ca}.earned_points ELSE NULL END) as surveys_points"),
            DB::raw("SUM(CASE WHEN {$this->ca}.object_type='manual' THEN {$this->ca}.earned_points ELSE NULL END) as manual_points"),
            DB::raw("SUM(CASE WHEN {$this->ca}.object_type='level' THEN {$this->ca}.earned_points ELSE NULL END) as level_points"),

            DB::raw("SUM(CASE WHEN {$this->ca}.object_type='product' THEN {$this->ca}.units_number ELSE NULL END) as sold_products"),
            DB::raw("COUNT(CASE WHEN {$this->ca}.object_type='quiz' THEN 1 ELSE NULL END) as passed_quizzes"),
            DB::raw("COUNT(CASE WHEN {$this->ca}.object_type='survey' THEN 1 ELSE NULL END) as passed_surveys"),
            DB::raw("COUNT(CASE WHEN {$this->ca}.object_type='manual' THEN 1 ELSE NULL END) as added_manually"),
            DB::raw("COUNT(CASE WHEN {$this->ca}.object_type='level' THEN 1 ELSE NULL END) as achieved_levels")
        )
            ->leftJoin($this->c, "{$this->c}.id", '=',"{$this->ca}.campaign_id")
            ->leftJoin($this->u, "{$this->u}.id", '=', "{$this->ca}.user_id")
            ->leftJoin($this->ud, "{$this->ud}.user_id", '=', "{$this->u}.id")
            ->leftJoin($this->d, function($join) {
                $join->on("{$this->d}.id", '=', "{$this->ud}.dealer_id")
                    ->on("{$this->d}.channel_id", '=', "{$this->c}.channel_id");
            })
            ->leftJoin($this->dis, "{$this->dis}.id", '=', "{$this->d}.district_id")
            ->leftJoin($this->up, "{$this->up}.user_id", '=', "{$this->u}.id")
            ->leftJoin($this->p, function($join) {
                $join->on("{$this->p}.id", '=', "{$this->up}.position_id")
                    ->on("{$this->p}.channel_id", '=', "{$this->c}.channel_id");
            })
            ->where("{$this->ca}.campaign_id", $campaignID)
            ->groupBy(
                "{$this->ca}.user_id",
                "{$this->d}.id",
                "{$this->d}.district_id",
                "{$this->p}.id"
            )
            ->orderBy('total_points', 'desc');

        $rank = DB::table(DB::raw("({$users->toSql()}) as users, (SELECT @rank := 0) as init"))
            ->mergeBindings($users)
            ->select(
                DB::raw('users.*'),
                DB::raw('@rank := @rank + 1 as rank')
            );

        return DB::table(DB::raw( "({$rank->toSql()}) as r"))
            ->mergeBindings($users)->get();
    }

    /**
     * @param int $campaignID
     * @return mixed
     */
    public function getPointsStatistics(int $campaignID)
    {
        $types = DB::table($this->ca)
            ->select(
                "{$this->ca}.object_type",
                DB::raw("SUM({$this->ca}.units_number) as amount"),
                DB::raw("SUM({$this->ca}.earned_points) as points")
            )
            ->groupBy("{$this->ca}.object_type")
            ->where("{$this->ca}.campaign_id", $campaignID);

        $total = DB::table($this->ca)
            ->select(DB::raw("SUM({$this->ca}.earned_points) as points"))
            ->where("{$this->ca}.campaign_id", $campaignID);

        $list = DB::table(DB::raw("({$types->toSql()}) as base, ({$total->toSql()}) as total"))
            ->mergeBindings($types)
            ->mergeBindings($total)
            ->select(
                DB::raw("base.*"),
                DB::raw("IFNULL(base.points/total.points, 0) as percentage")
            );

        return DB::table(DB::raw( "({$list->toSql()}) as r"))
            ->orderBy('percentage', 'DESC')
            ->mergeBindings($types)
            ->mergeBindings($total)
            ->get();
    }

    /**
     * @param int $campaignID
     * @return mixed
     */
    public function getProductsStatistics(int $campaignID)
    {
        $products = DB::table($this->ca)
            ->select(
                "{$this->pr}.name as name",
                DB::raw("SUM({$this->ca}.units_number) as amount"),
                DB::raw("SUM({$this->ca}.earned_points) as points")
            )
            ->leftJoin($this->pr, "{$this->pr}.id", '=', "{$this->ca}.object_id")
            ->groupBy("{$this->pr}.name")
            ->where("{$this->ca}.campaign_id", $campaignID)
            ->where("{$this->ca}.object_type", 'product');

        $total = DB::table($this->ca)
            ->select(DB::raw("SUM({$this->ca}.earned_points) as points"))
            ->where("{$this->ca}.campaign_id", $campaignID)
            ->where("{$this->ca}.object_type", 'product');

        $list = DB::table(DB::raw("({$products->toSql()}) as base, ({$total->toSql()}) as total"))
            ->mergeBindings($products)
            ->mergeBindings($total)
            ->select(
                DB::raw("base.*"),
                DB::raw("IFNULL(base.points/total.points, 0) as percentage")
            );

        return DB::table(DB::raw( "({$list->toSql()}) as r"))
            ->orderBy('percentage', 'DESC')
            ->mergeBindings($products)
            ->mergeBindings($total)
            ->get();
    }
}
