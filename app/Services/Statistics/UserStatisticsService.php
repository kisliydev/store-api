<?php

namespace App\Services\Statistics;

use App\Http\Requests\Request;
use App\Http\Requests\ChannelStatistics\UsersStatisticsGetRequest;
use App\Http\Requests\ChannelStatistics\UsersExportStatisticsRequest;
use App\Http\Requests\ChannelStatistics\UserStatisticsGetMobileRequest;

use App\Models\Dealer;
use App\Transformers\ChannelStatisticsTypeTransformer;

use App\Models\User;
use App\Models\Product;
use App\Models\PointsStatistic;
use App\Models\Channel;
use App\Models\UserStatistics;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;
use Excel;
use PHPExcel_Cell;

class UserStatisticsService extends ChannelStatisticsService
{
    /**
     * @var array
     */
    protected $filterable = [
        "dealer_id",
        "district_id",
        "position_id",
        "league_id",
        "id",
    ];

    /**
     * @var array
     */
    protected $sortable = [
        "full_name",
        "email",
        "rank",
        "total_points",
        "product_points",
        "queries_points",
        "other_points",
        "sold_products",
        "passed_queries",
        "added_manually",
        "achieved_levels",
        "created_at",
    ];

    /**
     * @param \App\Http\Requests\ChannelStatistics\UsersStatisticsGetRequest $request
     * @param \App\Models\User $user
     * @return mixed
     */
    public function getUsersStatistics(UsersStatisticsGetRequest $request, User $user)
    {
        $this->searchable = [
            'full_name',
            'email',
        ];

        return $this->getByUsers($request, $user);
    }

    /**
     * @param \App\Http\Requests\ChannelStatistics\UsersStatisticsGetRequest $request
     * @param \App\Models\User $user
     * @return mixed
     */
    public function getUsersMobileStatistics(UsersStatisticsGetRequest $request, User $user)
    {
        $users = $user->select(
            "{$this->u}.id",
            "{$this->m}.id as avatar_id",
            "{$this->m}.path as avatar_path",
            "{$this->m}.thumbnails as thumbnails",
            DB::raw("TRIM(CONCAT({$this->u}.first_name, ' ', {$this->u}.last_name)) as full_name"),

            DB::raw("SUM({$this->ps}.earned_points) as total_points")
        );
        $this->onlyChannelUsers($users, $request, $this->u, $this->ucs, 'id');

        $users->leftJoin($this->ps, function($join) {
                $join->on("{$this->ps}.user_id", '=', "{$this->u}.id")
                    ->on("{$this->ps}.channel_id", '=', "{$this->ucs}.channel_id");
            })
            ->leftJoin($this->m, "{$this->m}.id", '=', "{$this->u}.avatar_id")
            ->when($request->league_id, function ($q) use ($request) {
                $q->whereHas('dealers', function ($q) use ($request) {
                    $q->where('dealers.channel_id', $request->get('channel_id'))
                        ->where('dealers.league_id', $request->get('league_id'));
                });
            })
            ->groupBy("{$this->u}.id")
            ->orderBy('total_points', 'desc');

        $rank = DB::table(DB::raw("({$users->toSql()}) as users, (SELECT @rank := 0) as init"))
            ->addBinding($users->getQuery()->getBindings())
            ->select(
                DB::raw('users.*'),
                DB::raw('@rank := @rank + 1 as rank')
            );

        $query = DB::table(DB::raw( "({$rank->toSql()}) as r"))
            ->mergeBindings($users->getQuery());

        return $query->get();
    }

    /**
     * @param \App\Http\Requests\ChannelStatistics\UserStatisticsGetMobileRequest $request
     * @param \App\Models\User $user
     * @return mixed
     */
    public function getUserMobileStatistics(UserStatisticsGetMobileRequest$request, User $user)
    {
        $dealer = $user
            ->dealers->where('channel_id', $request->channel_id)
            ->first();
        $leagueId = optional($dealer)->league_id;
        $users = $this->getByUsers($request, $user, false, $leagueId);
        return $this->addUserData($users, $request->channel_id, true, $leagueId);
    }

    /**
     * @param \App\Http\Requests\ChannelStatistics\UserStatisticsGetMobileRequest $request
     * @param \App\Models\User $user
     * @return mixed
     */
    public function getAdminStatistics(UserStatisticsGetMobileRequest $request, User $user)
    {
        $users = $user->removeAppends($user->select(
                "{$this->u}.*",
                DB::raw("SUM({$this->ps}.earned_points) as total_points"),
                DB::raw("SUM(CASE WHEN {$this->ps}.object_type='product' THEN {$this->ps}.earned_points ELSE NULL END) as product_points"),
                DB::raw("SUM(CASE WHEN {$this->ps}.object_type='quiz' THEN {$this->ps}.earned_points ELSE NULL END) as quizzes_points"),
                DB::raw("SUM(CASE WHEN {$this->ps}.object_type='survey' THEN {$this->ps}.earned_points ELSE NULL END) as surveys_points"),
                DB::raw("SUM(CASE WHEN {$this->ps}.object_type='manual' THEN {$this->ps}.earned_points ELSE NULL END) as manual_points"),
                DB::raw("SUM(CASE WHEN {$this->ps}.object_type='level' THEN {$this->ps}.earned_points ELSE NULL END) as level_points"),
                DB::raw("SUM(CASE WHEN {$this->ps}.object_type IN ('quiz','survey') THEN {$this->ps}.earned_points ELSE NULL END) as queries_points"),
                DB::raw("SUM(CASE WHEN {$this->ps}.object_type IN ('manual','level') THEN {$this->ps}.earned_points ELSE NULL END) as other_points"),

                DB::raw("SUM(CASE WHEN {$this->ps}.object_type='product' THEN {$this->ps}.units_number ELSE NULL END) as sold_products"),
                DB::raw("COUNT(CASE WHEN {$this->ps}.object_type='quiz' THEN 1 ELSE NULL END) as passed_quizzes"),
                DB::raw("COUNT(CASE WHEN {$this->ps}.object_type='survey' THEN 1 ELSE NULL END) as passed_surveys"),
                DB::raw("COUNT(CASE WHEN {$this->ps}.object_type='manual' THEN 1 ELSE NULL END) as added_manually"),
                DB::raw("COUNT(CASE WHEN {$this->ps}.object_type='level' THEN 1 ELSE NULL END) as achieved_levels"),

                DB::raw("COUNT(CASE WHEN {$this->ps}.object_type IN ('quiz','survey') THEN 1 ELSE NULL END) as passed_queries")
            )
            ->leftJoin($this->ps, function($join) use ($request) {
                $join->on("{$this->ps}.user_id", '=', "{$this->u}.id")
                    ->where("{$this->ps}.channel_id", '=', $request->channel_id);
            })
            ->where("{$this->u}.id", $request->user_id)
            ->groupBy("{$this->u}.id")
            ->with(['avatar'])->get(), ['full_name']);

        return $this->addUserData($users, $request->channel_id, false);
    }

    /**
     * @param \App\Http\Requests\ChannelStatistics\UserStatisticsGetMobileRequest $request
     * @return array
     */
    public function getUserDetailedMobileStatistics(UserStatisticsGetMobileRequest $request)
    {
        $data = [
            'product' => DB::table($this->pr)
                ->select(
                    "{$this->pr}.id",
                    "{$this->pr}.name",
                    DB::raw("IFNULL(SUM({$this->ps}.units_number), 0) as count"),
                    DB::raw("IFNULL(SUM({$this->ps}.earned_points), 0) as points")
                )
                ->leftJoin($this->ps, function($join) use ($request) {
                    $join->on("{$this->ps}.object_id", "{$this->pr}.id")
                        ->where("{$this->ps}.object_type", 'product')
                        ->where("{$this->ps}.user_id", $request->user_id);
                })
                ->where("{$this->pr}.channel_id", $request->channel_id)
                ->groupBy("{$this->pr}.id")
                ->get(),
        ];
        $keys = [
            'quiz' => $this->ai,
            'survey' => $this->ai,
            'manual' => $this->mp,
            'level' => $this->aml,
        ];

        foreach($keys as $type => $table) {
            $data[$type] = DB::table($table)
                ->select(
                    "{$table}.id",
                    "{$table}.title as name",
                    DB::raw("IFNULL(COUNT({$this->ps}.units_number), 0) as count"),
                    DB::raw("IFNULL(SUM({$this->ps}.earned_points), 0) as points")
                )
                ->leftJoin($this->ps, function($join) use ($table, $type, $request) {
                    $join->on("{$this->ps}.object_id", "{$table}.id")
                        ->where("{$this->ps}.object_type", $type)
                        ->where("{$this->ps}.user_id", $request->user_id);
                })
                ->where("{$table}.channel_id", $request->channel_id)
                ->groupBy("{$table}.id")
                ->get();
        }
        return $data;
    }


    /**
     * @param \App\Http\Requests\ChannelStatistics\UsersExportStatisticsRequest $request
     * @param \App\Models\User $user
     * @return mixed
     */
    public function exportUsers(UsersExportStatisticsRequest $request, User $user)
    {
        $users = $this->getByUsers($request, $user, false);
        $channel = Channel::find($request->channel_id);

        return Excel::create('file', function($excel) use ($users, $channel, $request) {
            $excel->sheet("Contestants", function($sheet) use ($users, $channel, $request) {
                $i = $this->addSheetSignatures($request, $sheet, 2, 'Users' );

                $i+=2;
                $sheet->row($i, [
                    'Rank',
                    'Name',
                    'Email',
                    'Phone',
                    'Position',
                    'Dealer',
                    'Registered',
                ]);

                $sheet->row($i, function ($row) {
                    $this->addHeaderStyles($row);
                });
                $i++;

                $users->each(function($user) use ($sheet, &$i) {
                    $sheet->row($i, [
                        $user->rank,
                        $user->full_name,
                        $user->email,
                        str_replace('+', '', $user->phone),
                        $user->position_name,
                        $user->dealer_name,
                        Carbon::parse($user->created_at)->format('Y-m-d'),
                    ]);
                    $i++;
                });

                $this->setTextWrapper($sheet, 7, $i);
            });
        })->string('xlsx');
    }

    /**
     * @param \App\Http\Requests\ChannelStatistics\UsersExportStatisticsRequest $request
     * @param \App\Models\User $user
     * @param \App\Models\Product $product
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @param \App\Transformers\ChannelStatisticsTypeTransformer $transformer
     * @return mixed
     */
    public function exportUsersDetailed(
        UsersExportStatisticsRequest $request,
        User $user,
        Product $product,
        PointsStatistic $pointsStatistic,
        ChannelStatisticsTypeTransformer $transformer
    )
    {
        return Excel::create('users', function($excel) use ($request, $user, $product, $pointsStatistic, $transformer) {
            $excel->sheet("Contestants", function($sheet) use ($request, $user, $product, $pointsStatistic, $transformer) {
                $users = $this->getByUsers($request, $user, false);
                $products = $this->getChannelProducts($request, $product);
                $productsStat = $this->getProductsByUsers($request, $user, $product);

                $i = $this->addSheetSignatures($request, $sheet, 2, 'Users' );
                $i+=2;

                $headers = [
                    'Personal Details' => 5,
                    'Total' => 0,
                    'Quizzes' => 1,
                    'Surveys' => 1,
                    'Products' => 1,
                    'Manual' => 1,
                    'Level' => 1,
                ];
                $productsTitles = array_map(
                    function() {return 1;},
                    array_flip($products->pluck('name')->toArray())
                );
                $i = $this->addTableHeaders($sheet, $i, array_merge($headers, $productsTitles));
                $i++;

                $subHeaders = [
                    'Rank',
                    'Name',
                    'Email',
                    'Phone',
                    'Position',
                    'Dealer',
                    '',
                ];
                $temp = [
                    'Reg',
                    'Points',
                ];
                $lastColumnIndex = $products->count() * 2  + 16;
                for ($j = 0; $j < $products->count() + 5; $j++ ) {
                    $subHeaders = array_merge($subHeaders, $temp);
                }

                $sheet->row($i, $subHeaders);
                $sheet->row($i, function ($row) {
                    $this->addHeaderStyles($row);
                });
                $i++;

                /**
                 * Fill main table with data
                 */
                $firstInRange = $i;
                $users->each(function($user) use ($sheet, &$i, $products, $productsStat) {
                    $data = [
                        $user->rank,
                        $user->full_name,
                        $user->email,
                        str_replace('+', '', $user->phone),
                        $user->position_name,
                        $user->dealer_name,

                        intval($user->total_points),

                        intval($user->passed_quizzes),
                        intval($user->quizzes_points),

                        intval($user->passed_surveys),
                        intval($user->surveys_points),

                        intval($user->sold_products),
                        intval($user->product_points),

                        intval($user->added_manually),
                        intval($user->manual_points),

                        intval($user->achieved_levels),
                        intval($user->level_points),
                    ];

                    $selection = $productsStat->where('id', $user->id)->sortBy('product_id', SORT_NUMERIC);
                    $productsStat->forget($selection->keys()->toArray());

                    $selection->each(function($item) use (&$data){
                        $data[] = $item->sold_count;
                        $data[] = $item->sold_points;
                    });

                    $sheet->row($i, $data);
                    $i++;
                });

                if ($firstInRange !== $i) {
                    $lastInRange = $i - 1;
                    $data = [];
                    foreach (range(0, 5) as $index) {
                        $data[] = "";
                    }
                    foreach (range(6, $lastColumnIndex) as $index) {
                        $column = PHPExcel_Cell::stringFromColumnIndex($index);
                        $data[] = "=SUM({$column}{$firstInRange}:{$column}{$lastInRange})";
                    }

                    $sheet->row($i, $data);
                    $sheet->row($i, function ($row) {
                        $this->addFooterStyle($row);
                    });
                }
                $i += 3;

                /**
                 * Points type statistics
                 */
                $i = $this->addTableSignature($sheet, $i, 'Statistics By Points Categories') + 2;
                $i = $this->addPointsStatistics($sheet, $i, $request, $pointsStatistic, $transformer) + 3;

                /**
                 * Products statistics
                 */
                $i = $this->addTableSignature($sheet, $i, 'Statistics By Products') + 2;
                $i = $this->addProductsStatistics($sheet, $i, $request, $pointsStatistic);

                $this->setTextWrapper($sheet, $lastColumnIndex, $i);
            });
        })->string('xlsx');
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param \App\Models\User $user
     * @param bool $withPagination
     * @param int|null $leagueId
     * @return mixed
     */
    protected function getByUsers(Request $request, User $user, bool $withPagination = true, ?int $leagueId = null)
    {
        $users = $user->select(
            "{$this->u}.id",
            "{$this->u}.email",
            "{$this->u}.phone",
            "{$this->u}.skype",
            "{$this->u}.created_at",
            "{$this->ucs}.status as status",
            "{$this->ucs}.id as status_id",
            "{$this->d}.id as dealer_id",
            "{$this->d}.district_id as district_id",
            "{$this->dis}.name as district_name",
            "{$this->d}.league_id as league_id",
            "{$this->l}.name as league_name",
            "{$this->d}.name as dealer_name",
            "{$this->d}.location as dealer_location",
            "{$this->p}.id as position_id",
            "{$this->p}.name as position_name",
            "{$this->m}.id as avatar_id",
            "{$this->m}.path as avatar_path",
            "{$this->m}.thumbnails as thumbnails",
            DB::raw("TRIM(CONCAT({$this->u}.first_name, ' ', {$this->u}.last_name)) as full_name"),

            DB::raw("SUM({$this->ps}.earned_points) as total_points"),
            DB::raw("SUM(CASE WHEN {$this->ps}.object_type='product' THEN {$this->ps}.earned_points ELSE NULL END) as product_points"),
            DB::raw("SUM(CASE WHEN {$this->ps}.object_type='quiz' THEN {$this->ps}.earned_points ELSE NULL END) as quizzes_points"),
            DB::raw("SUM(CASE WHEN {$this->ps}.object_type='survey' THEN {$this->ps}.earned_points ELSE NULL END) as surveys_points"),
            DB::raw("SUM(CASE WHEN {$this->ps}.object_type='manual' THEN {$this->ps}.earned_points ELSE NULL END) as manual_points"),
            DB::raw("SUM(CASE WHEN {$this->ps}.object_type='level' THEN {$this->ps}.earned_points ELSE NULL END) as level_points"),
            DB::raw("SUM(CASE WHEN {$this->ps}.object_type IN ('quiz','survey') THEN {$this->ps}.earned_points ELSE NULL END) as queries_points"),
            DB::raw("SUM(CASE WHEN {$this->ps}.object_type IN ('manual','level') THEN {$this->ps}.earned_points ELSE NULL END) as other_points"),

            DB::raw("SUM(CASE WHEN {$this->ps}.object_type='product' THEN {$this->ps}.units_number ELSE NULL END) as sold_products"),
            DB::raw("COUNT(CASE WHEN {$this->ps}.object_type='quiz' THEN 1 ELSE NULL END) as passed_quizzes"),
            DB::raw("COUNT(CASE WHEN {$this->ps}.object_type='survey' THEN 1 ELSE NULL END) as passed_surveys"),
            DB::raw("COUNT(CASE WHEN {$this->ps}.object_type='manual' THEN 1 ELSE NULL END) as added_manually"),
            DB::raw("COUNT(CASE WHEN {$this->ps}.object_type='level' THEN 1 ELSE NULL END) as achieved_levels"),
            DB::raw("COUNT(CASE WHEN {$this->ps}.object_type IN ('quiz','survey') THEN 1 ELSE NULL END) as passed_queries")
        );
        $this->onlyChannelUsers($users, $request, $this->u, $this->ucs, 'id');

        $users->leftJoin($this->ud, function($join) use ($request) {
                /* that is really necessary to avoid duplicate users in the lists when they are bound to more than one channel */
                $join->on("{$this->ud}.user_id", '=', "{$this->u}.id")
                    ->whereIn("{$this->ud}.dealer_id", function($clause) use ($request) {
                        $clause->select('id')
                            ->from($this->d)
                            ->where('channel_id', $request->channel_id);
                    });
            })
            ->leftJoin($this->d, function($join) use ($request) {
                $join->on("{$this->d}.id", '=', "{$this->ud}.dealer_id")
                    ->where("{$this->d}.channel_id", '=', $request->channel_id);
            })
            ->leftJoin($this->dis, function($join) use ($request) {
                $join->on("{$this->dis}.id", '=', "{$this->d}.district_id")
                    ->where("{$this->dis}.channel_id", '=', $request->channel_id);
            })
            ->leftJoin($this->l, function($join) use ($request) {
                $join->on("{$this->l}.id", '=', "{$this->d}.league_id")
                    ->where("{$this->l}.channel_id", '=', $request->channel_id);
            })
            ->leftJoin($this->up, function($join) use ($request) {
                $join->on("{$this->up}.user_id", '=', "{$this->u}.id")
                    ->whereIn("{$this->up}.position_id", function($clause) use ($request) {
                        $clause->select('id')
                            ->from($this->p)
                            ->where('channel_id', $request->channel_id);
                    });
            })
            ->leftJoin($this->p, function($join) use ($request) {
                $join->on("{$this->p}.id", '=', "{$this->up}.position_id")
                    ->where("{$this->p}.channel_id", '=', $request->channel_id);
            })
            ->leftJoin($this->ps, function($join) use ($request) {
                $join->on("{$this->ps}.user_id", '=', "{$this->u}.id")
                    ->where("{$this->ps}.channel_id", '=', $request->channel_id);

                $this->addDateRange($join, $request);
            })
            ->leftJoin($this->m, "{$this->m}.id", '=', "{$this->u}.avatar_id")
            ->when($leagueId, function ($q) use ($leagueId) {
                $q->where("{$this->d}.league_id", $leagueId);
            })
            ->groupBy(
                "{$this->u}.id",
                "{$this->ucs}.id",
                "{$this->d}.id",
                "{$this->p}.id"
            )
            ->orderBy('total_points', 'desc');

        $rank = DB::table(DB::raw("({$users->toSql()}) as users, (SELECT @rank := 0) as init"))
            ->addBinding($users->getQuery()->getBindings())
            ->select(
                DB::raw('users.*'),
                DB::raw('@rank := @rank + 1 as rank')
            );

        $query = DB::table(DB::raw( "({$rank->toSql()}) as r"))
            ->addBinding($users->getQuery()->getBindings());

        return $this->getList($query, $request, $withPagination);
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param \App\Models\User $user
     * @param \App\Models\Product $product
     * @return \Illuminate\Support\Collection
     */
    protected function getProductsByUsers(Request $request, User $user, Product $product)
    {
        $subQuery = $product->select(
            "{$this->pr}.id as product_id",
            "{$this->ps}.user_id as user_id",
            DB::raw("SUM({$this->ps}.units_number) as count"),
            DB::raw("SUM({$this->ps}.earned_points) as points")
        )
            ->join($this->ps, function($join) use ($request) {
                $join->on("{$this->ps}.object_id", '=', "{$this->pr}.id")
                    ->where("{$this->ps}.object_type", '=', 'product');

                $this->addDateRange($join, $request);
            })
            ->where("{$this->pr}.channel_id", $request->channel_id)
            ->groupBy(
                "{$this->pr}.id",
                "{$this->ps}.user_id"
            );

        /**
         * dealer and districts joins are used in order
         * to filter records if such parameters are persist in $request
         */
        $query = $user->select(
            "{$this->u}.id",
            "{$this->ud}.dealer_id as dealer_id",
            "{$this->d}.district_id as district_id",
            "{$this->d}.league_id as league_id",
            "{$this->up}.id as position_id",
            "{$this->pr}.id as product_id",
            DB::raw("IFNULL(sold.count, 0) as sold_count"),
            DB::raw("IFNULL(sold.points, 0) as sold_points")
        )
            ->join($this->ucs, function($join) use ($request) {
                $join->on("{$this->ucs}.user_id", '=', "{$this->u}.id")
                    ->where("{$this->ucs}.channel_id", $request->channel_id);
            })
            ->leftJoin($this->pr, "{$this->pr}.channel_id", '=', "{$this->ucs}.channel_id")
            ->leftJoin($this->ud, function ($join) use ($request) {
                $join->on("{$this->ud}.user_id", '=', "{$this->u}.id")
                    ->whereIn("{$this->ud}.dealer_id", function($clause) use ($request) {
                        $clause->select('id')
                            ->from($this->d)
                            ->where('channel_id', $request->channel_id);
                    });
            })
            ->leftJoin($this->d, "{$this->d}.id", '=', "{$this->ud}.dealer_id")
            ->leftJoin($this->up, function($join) use ($request) {
                $join->on("{$this->up}.user_id", '=', "{$this->u}.id")
                    ->whereIn("{$this->up}.position_id", function($clause) use ($request) {
                        $clause->select('id')
                            ->from($this->p)
                            ->where('channel_id', $request->channel_id);
                    });
            })
            ->leftJoin(
                DB::raw("({$subQuery->toSql()}) as sold"),
                function($join) {
                    $join->on('sold.user_id', '=', "{$this->u}.id")
                        ->on('sold.product_id', '=', "{$this->pr}.id");
                }
            )
            ->mergeBindings($subQuery->getQuery());

        return $user->removeAppends($this->getList($query, $request, false));
    }

    /**
     * @param $users
     * @param int $channelID
     * @param bool $withDealerRank
     * @param int|null $leagueId
     * @return mixed
     */
    protected function addUserData($users, int $channelID, $withDealerRank = true, ?int $leagueId = null)
    {
        $counts = $this->getChannelItemsCount($channelID, $leagueId);

        $users->each(function($user) use ($counts, $channelID, $withDealerRank) {
            $user->surveys_count = $counts['survey'];
            $user->quizzes_count = $counts['quiz'];
            $user->users_count = $counts['users'];
            $user->dealer_rank = 0;
            $user->dealer_users_count = 0;

            if ( ! $withDealerRank) {
                return;
            }

            $uModel = User::hydrate(collect([$user])->toArray())->first();
            $relation = $uModel->dealer($channelID)->first();

            if (! $relation) {
                return;
            }
            // @todo consider removing this line
            // $data = $this->getUserInDealerRank($relation->dealer_id, $user->id);

            $service = app(DealerStatisticsService::class);

            $data = $service->getDealersMobileList(
                new FormRequest([
                    'channel_id' => $channelID,
                    'league_id' => $user->league_id,
                ]),
                new Dealer,
                new PointsStatistic
            );

            foreach ($data as $i => $item) {
                if ($item->name === $user->dealer_name) {
                    $user->dealer_rank = $i + 1;
                }
            }

            $user->dealer_users_count = count($data);
        });

        return $users;
    }

    /**
     * @param int $dealerID
     * @param int $userID
     * @return array
     */
    protected function getUserInDealerRank(int $dealerID, int $userID)
    {
        $users = DB::table($this->u)
            ->select(
                "{$this->u}.id",
                DB::raw("SUM({$this->ps}.earned_points) as total_points")
            )
            ->join($this->ud, "{$this->ud}.user_id", "=", "{$this->u}.id")
            ->join($this->d, "{$this->d}.id", "=", "{$this->ud}.dealer_id" )
            ->leftJoin($this->ps, function($join) {
                $join->on("{$this->ps}.user_id", '=', "{$this->u}.id")
                    ->on("{$this->ps}.channel_id", '=', "{$this->d}.channel_id");
            })
            ->where("{$this->ud}.dealer_id", '=', $dealerID)
            ->groupBy("{$this->u}.id")
            ->orderBy('total_points', 'desc');

        $rank = DB::table(DB::raw("({$users->toSql()}) as {$this->u}, (SELECT @rank := 0) as init"))
            ->mergeBindings($users)
            ->select(
                DB::raw("{$this->u}.*"),
                DB::raw('@rank := @rank + 1 as rank')
            );

        $query = DB::table(DB::raw( "({$rank->toSql()}) as r"))
            ->mergeBindings($users)
            ->where("id", $userID);

        return [
            'rank' => $query->first()->rank,
            'users_count' => $users->get()->count(),
        ];
    }

}
