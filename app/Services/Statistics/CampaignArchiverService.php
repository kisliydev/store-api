<?php

namespace App\Services\Statistics;

use App\Models\Campaign;
use Illuminate\Support\Facades\DB;

class CampaignArchiverService extends ChannelStatisticsService
{
    /**
     * @param \App\Models\Campaign $campaign
     */
    public function makeArchive(Campaign $campaign)
    {
        $points = DB::table($this->ps)
            ->select(
                "{$this->ps}.user_id",
                "{$this->ps}.object_id",
                "{$this->ps}.object_type",
                DB::raw("{$campaign->id} as campaign_id"),
                DB::raw("SUM({$this->ps}.units_number) as units_number"),
                DB::raw("SUM({$this->ps}.earned_points) as earned_points")
            );

        $this->onlyChannelUsers($points, $campaign->channel_id);

        $points->where("{$this->ps}.channel_id", $campaign->channel_id)
            ->groupBy(
                "{$this->ps}.user_id",
                "{$this->ps}.object_id",
                "{$this->ps}.object_type"
            );

        /* Copy points data to archive */
        DB::insert("INSERT INTO {$this->ca} (user_id, object_id, object_type, campaign_id, units_number, earned_points) {$points->toSql()}", $points->getBindings());
    }
}
