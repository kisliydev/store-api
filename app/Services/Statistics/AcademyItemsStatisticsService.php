<?php

namespace App\Services\Statistics;

use App\Http\Requests\Request;
use App\Http\Requests\ChannelStatistics\ExportSurveyAnswersRequest;
use App\Http\Requests\ChannelStatistics\AcademyItemsExportStatisticsRequest;

use App\Traits\AcademyItemsStatisticsQueryBuilder;

use App\Models\AcademyItem;
use App\Models\User;
use App\Models\UsersDealers;
use App\Models\Dealer;
use App\Models\District;

use Illuminate\Support\Facades\DB;

use Carbon\Carbon;
use Excel;
use PHPExcel_Cell;

class AcademyItemsStatisticsService extends ChannelStatisticsService
{
    use AcademyItemsStatisticsQueryBuilder;
    /**
     * @var array
     */
    protected $filterable = [
        "dealer_id",
        "district_id",
        "position_id",
    ];

    /**
     * @var array
     */
    protected $sortable = [];

    /**
     * @param ExportSurveyAnswersRequest $request
     * @return array
     */
    public function getCounters(ExportSurveyAnswersRequest $request)
    {
        $products     = DB::table($this->pra)->select(DB::raw('COUNT(*) as count'))->where('channel_id', $request->channel_id);
        $totalSurveys = $this->getAcademyItemsTotalCountQuery('survey', $request->channel_id);
        $totalQuizzes = $this->getAcademyItemsTotalCountQuery('quiz', $request->channel_id);
        $completeSurveys = $this->getAcademyItemsPassedCountQuery('survey', $request->channel_id);
        $completeQuizzes = $this->getAcademyItemsPassedCountQuery('quiz', $request->channel_id);

        return [
            'completed_quizes' => $completeQuizzes->first()->count,
            'total_quizes' => $totalQuizzes->first()->count,
            'completed_surveys' => $completeSurveys->first()->count,
            'total_surveys' => $totalSurveys->first()->count,
            'products' => $products->first()->count
        ];
    }

    /**
     * @param \App\Http\Requests\ChannelStatistics\ExportSurveyAnswersRequest $request
     * @param \App\Models\User $user
     * @param \App\Models\AcademyItem $academyItem
     * @return mixed
     */
    public function exportSurveyAnswers(ExportSurveyAnswersRequest $request, User $user, AcademyItem $academyItem)
    {
        return Excel::create('answers', function($excel) use ($request, $user, $academyItem) {
            $excel->sheet("Surveys", function($sheet) use ($request, $user, $academyItem) {
                $surveys = $this->getAcademyItems($request, $academyItem, "survey", true);
                $users = $this->getUsers($request, $user);

                $i = 2;

                $sheet->row($i, ["Report", "STORE Survey Answers"]);
                $sheet->row($i, function($row) {
                    $row->setFontWeight('bold');
                });
                $i++;

                $sheet->row($i, ["Generated", Carbon::today()->format('Y-m-d')]);
                $sheet->row($i, function($row) {
                    $row->setFontWeight('bold');
                });
                $i += 2;

                /**
                 * Set column headers
                 */
                $headers = [
                    'Name',
                    'Email',
                    'Phone',
                    'Dealer',
                    'Position',
                ];
                $sheet->row($i, $headers);
                $colNumber = count($headers);
                $surveys->each(function($survey) use (&$colNumber, $sheet, $i) {
                    if (! $survey->questions) {
                        return;
                    }

                    $colOffset = count($survey->questions) - 1;
                    $columnStartIndex = PHPExcel_Cell::stringFromColumnIndex($colNumber);

                    $sheet->cell("{$columnStartIndex}{$i}", function($cell) use ($survey) {
                        $cell->setValue($survey->title);
                    });

                    if ($colOffset > 1) {
                        $columnFinishIndex = PHPExcel_Cell::stringFromColumnIndex($colNumber + $colOffset);
                        $range = "{$columnStartIndex}{$i}:{$columnFinishIndex}{$i}";
                        $sheet->mergeCells($range);
                        $colOffset++;
                    } else {
                        $colOffset = 1;
                    }

                    $colNumber += $colOffset ;
                });

                $sheet->row($i, function ($row) {
                    $this->addHeaderStyles($row);
                });
                $i++;

                $colNumber = count($headers);
                $surveys->each(function($survey) use (&$colNumber, $sheet, $i) {
                    $survey->questions->each(function($question) use (&$colNumber, $sheet, $i) {

                        $columnIndex = PHPExcel_Cell::stringFromColumnIndex($colNumber);

                        $sheet->cell("{$columnIndex}{$i}", function($cell) use ($question) {
                            $cell->setValue($question->text);
                        });
                        $colNumber++;

                    });
                });
                $sheet->row($i, function ($row) {
                    $this->addHeaderStyles($row);
                });
                $i++;

                /**
                 * Fill in Table
                 */
                $users->each(function($user) use (&$i, $surveys, $sheet, $headers) {

                    /**
                     * User's personal details and bindings
                     */
                    $sheet->row($i, [
                        $user->full_name,
                        $user->email,
                        $user->phone,
                        $user->dealer_name,
                        $user->potion_name,
                    ]);

                    /**
                     * User's answers
                     */
                    if ($user->surveyAnswers) {
                        $colNumber = count($headers);
                        $surveys->each(function($survey) use (&$colNumber, $user, $sheet, $i) {
                            $survey->questions->each(function($question) use (&$colNumber, $user, $sheet, $i) {
                                $answer = $user->surveyAnswers->firstWhere('question_id', $question->id);
                                if ($answer) {
                                    $columnIndex = PHPExcel_Cell::stringFromColumnIndex($colNumber);

                                    $sheet->cell("{$columnIndex}{$i}", function($cell) use ($answer) {
                                        $cell->setValue($answer->text);
                                    });
                                }
                                $colNumber++;
                            });
                        });
                    }
                    $i++;
                });

                $this->setTextWrapper($sheet, $colNumber, $i);
            });
        })->string('xlsx');
    }

    /**
     * @param \App\Http\Requests\ChannelStatistics\AcademyItemsExportStatisticsRequest $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\District $district
     * @param \App\Models\AcademyItem $academyItem
     * @param \App\Models\UsersDealers $usersDealers
     * @return mixed
     */
    public function exportAcademyItems(AcademyItemsExportStatisticsRequest $request, Dealer $dealer, District $district, AcademyItem $academyItem, UsersDealers $usersDealers)
    {

        $dealerData = [
            'Quizzes' => [
                $this->getAcademyItems($request, $academyItem),
                $this->getAcademyItemInDealers($request, $dealer, $academyItem),
                $this->getAcademyItemInDealersDetailed($request, $dealer, $academyItem),
            ],
            'Surveys' => [
                $this->getAcademyItems($request, $academyItem, 'survey'),
                $this->getAcademyItemInDealers($request, $dealer, $academyItem, 'survey'),
                $this->getAcademyItemInDealersDetailed($request, $dealer, $academyItem, 'survey')
            ]
        ];

        $districtData = [
            'Quizzes' => $this->getAcademyItemInDistricts($request, $district, $academyItem, $usersDealers),
            'Surveys' => $this->getAcademyItemInDistricts($request, $district, $academyItem, $usersDealers, 'survey'),
        ];

        return Excel::create('answers', function($excel) use ($request, $dealerData, $districtData) {
            $excel->sheet("Academy Items Statistics", function($sheet) use ($request, $dealerData, $districtData) {

                $i = $this->addSheetSignatures($request, $sheet, 2, 'Academy Items');
                $i += 2;

                /***********************
                 * Fill dealers tables *
                 ***********************/
                $i = $this->addTableSignature($sheet, $i, 'Dealers') + 2;

                $isSurveys = false;
                $headers = [
                    "Dealer",
                    "District",
                    "Users",
                    "Items",
                    "Done",
                    "Done (Percentage)",
                    "Pending",
                    "Pending (Percentage)",
                ];
                foreach($dealerData as $collections) {
                    /**
                     * Set table title
                     */
                    $sectionTitle = $isSurveys ? 'Surveys' : 'Quizzes';
                    $isSurveys = true;
                    $i = $this->addTableSignature($sheet, $i, $sectionTitle, false) + 2;

                    /**
                     * Set table headers
                     */
                    $sheet->row($i, $headers);
                    $colNumber = count($headers);
                    $collections[0]->each(function($item) use (&$colNumber, $sheet, $i) {
                        /**
                         * Add item title to headers
                         */
                        $startIndex = PHPExcel_Cell::stringFromColumnIndex($colNumber);
                        $finishIndex = PHPExcel_Cell::stringFromColumnIndex($colNumber + 1);
                        $sheet->cell("{$startIndex}{$i}", function($cell) use ($item) {
                            $cell->setValue($item->title);
                        });
                        $sheet->mergeCells("{$startIndex}{$i}:{$finishIndex}{$i}");
                        /**
                         * Add 'Done' and 'Pending' sub-headers
                         */
                        $nextRow = $i + 1;
                        $nextIndex = PHPExcel_Cell::stringFromColumnIndex($colNumber + 1);
                        $sheet->cell("{$startIndex}{$nextRow}", function($cell){
                            $cell->setValue('Done');
                        });
                        $sheet->cell("{$nextIndex}{$nextRow}", function($cell){
                            $cell->setValue('Pending');
                        });

                        $colNumber += 2;
                    });
                    $sheet->row($i, function($row) {
                        $this->addHeaderStyles($row);
                    });
                    $i++;
                    $sheet->row($i, function($row) {
                        $this->addHeaderStyles($row);
                    });
                    $i++;

                    /**
                     * Fill table with data
                     */
                    $firstInRange = $i;
                    $lastColumn = 0;
                    $collections[1]->each(function($dealerItem) use ($sheet, &$i, $collections, &$lastColumn) {
                        $data = [
                            $dealerItem->dealer_name,
                            $dealerItem->district_name,
                            $dealerItem->users_count,
                            $dealerItem->items_count,
                            $dealerItem->done_count,
                            $this->getPercentage($dealerItem->items_count, $dealerItem->done_count),
                            ($dealerItem->items_count - $dealerItem->done_count),
                            $this->getPercentage($dealerItem->items_count, $dealerItem->items_count - $dealerItem->done_count),
                        ];
                        $this->setColors($sheet, $i, [
                            'E' => 'ok',
                            'F' => 'ok',
                            'G' => 'alert',
                            'H' => 'alert',
                        ]);
                        $this->setPercentFormat($sheet, $i, ['F', 'H']);

                        /**
                         * Add detailed data by each academy item
                         */
                        $collections[0]->each(function($academyItem) use (&$data, $dealerItem, $collections) {

                            $doneCount = $collections[2]
                                    ->where('dealer_id', $dealerItem->dealer_id)
                                    ->where('item_id', $academyItem->id)
                                    ->first()
                                    ->done_count ?? 0;

                            $pendingCount = $dealerItem->users_count - $doneCount;
                            $data[] = $doneCount;
                            $data[] = $pendingCount < 0 ? 0 : $pendingCount;
                        });

                        $sheet->row($i, $data);
                        $i++;
                        $lastColumn = count($data);
                    });

                    if ($firstInRange !== $i) {
                        /* set summary for columns that contain general data */
                        $lastInRange = $i - 1;
                        $data = [
                            '',
                            '',
                            "=SUM(C{$firstInRange}:C{$lastInRange})",
                            "=SUM(D{$firstInRange}:D{$lastInRange})",
                            "=SUM(E{$firstInRange}:E{$lastInRange})",
                            "=AVERAGE(F{$firstInRange}:F{$lastInRange})",
                            "=SUM(G{$firstInRange}:G{$lastInRange})",
                            "=AVERAGE(H{$firstInRange}:H{$lastInRange})",
                        ];

                        /* set summary for columns that contains given academyItem's data relative to the dealer */
                        if ($lastColumn > 8) {
                            foreach (range(9, $lastColumn) as $colNumber) {
                                $column = PHPExcel_Cell::stringFromColumnIndex($colNumber);
                                $data[] = "=SUM({$column}{$firstInRange}:{$column}{$lastInRange})";
                            }
                        }
                        $sheet->row($i, $data);
                        $sheet->row($i, function ($row) {
                            $this->addFooterStyle($row);
                        });
                        $this->setColors($sheet, $i, [
                            'E' => 'ok',
                            'F' => 'ok',
                            'G' => 'alert',
                            'H' => 'alert',
                        ]);
                    }
                    $this->setPercentFormat($sheet, $i, ['F', 'H']);
                    $i+=3;
                }

                /***********************
                 * Fill Districts tables *
                 ***********************/
                $i = $this->addTableSignature($sheet, $i, 'Districts') + 2;

                $isSurveys = false;
                $headers = [
                    "District",
                    "Users",
                    "Items",
                    "Done",
                    "Done (Percentage)",
                    "Pending",
                    "Pending (Percentage)",
                ];

                foreach($districtData as $collection) {
                    $sectionTitle = $isSurveys ? 'Surveys' : 'Quizzes';
                    $isSurveys = true;
                    $i = $this->addTableSignature($sheet, $i, $sectionTitle, false) + 2;

                    /**
                     * Set table headers
                     */
                    $sheet->row($i, $headers);
                    $sheet->row($i, function($row) {
                        $this->addHeaderStyles($row);
                    });
                    $i++;

                    $firstInRange = $i;
                    $collection->each(function($districtItem) use ($sheet, &$i) {
                        $sheet->row($i, [
                            $districtItem->district_name,
                            $districtItem->users_count,
                            $districtItem->items_count,
                            $districtItem->done_count,
                            $this->getPercentage($districtItem->items_count, $districtItem->done_count),
                            ($districtItem->items_count - $districtItem->done_count),
                            $this->getPercentage($districtItem->items_count, $districtItem->items_count - $districtItem->done_count),
                        ]);
                        $this->setColors($sheet, $i, [
                            'D' => 'ok',
                            'E' => 'ok',
                            'F' => 'alert',
                            'G' => 'alert',
                        ]);
                        $this->setPercentFormat($sheet, $i, ['E', 'G']);
                        $i++;
                    });

                    if ($firstInRange !== $i) {
                        $lastInRange = $i - 1;
                        $sheet->row($i, [
                            '',
                            "=SUM(B{$firstInRange}:B{$lastInRange})",
                            "=SUM(C{$firstInRange}:C{$lastInRange})",
                            "=SUM(D{$firstInRange}:D{$lastInRange})",
                            "=AVERAGE(E{$firstInRange}:E{$lastInRange})",
                            "=SUM(F{$firstInRange}:F{$lastInRange})",
                            "=AVERAGE(G{$firstInRange}:G{$lastInRange})",
                        ]);
                        $sheet->row($i, function ($row) {
                            $this->addFooterStyle($row);
                        });
                        $this->setColors($sheet, $i, [
                            'D' => 'ok',
                            'E' => 'ok',
                            'F' => 'alert',
                            'G' => 'alert',
                        ]);
                    }
                    $this->setPercentFormat($sheet, $i, ['E', 'G']);
                    $i += 3;
                }

                $this->setTextWrapper($sheet, $lastColumn, $i);
            });

        })->string('xlsx');
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param \App\Models\User $user
     * @return \Illuminate\Support\Collection
     */
    protected function getUsers(Request $request, User $user)
    {
        $query = $user->select(
            "{$this->u}.first_name",
            "{$this->u}.id",
            "{$this->u}.last_name",
            "{$this->u}.email",
            "{$this->u}.phone",
            "{$this->d}.name as dealer_name",
            "{$this->p}.name as position_name"
        );
        $this->onlyChannelUsers($query, $request, $this->u, $this->ucs, 'id');
        $query->leftJoin($this->ud, "{$this->ud}.user_id", '=', "{$this->u}.id")
            ->leftJoin($this->d, "{$this->d}.id", '=', "{$this->ud}.dealer_id")
            ->leftJoin($this->up, "{$this->up}.user_id", '=', "{$this->u}.id")
            ->leftJoin($this->p, "{$this->p}.id", '=', "{$this->up}.position_id")
            ->with(['surveyAnswers' => function ($query) use ($request) {
                $query->join("{$this->q}", "{$this->q}.id", '=', "{$this->ua}.question_id")
                    ->join("{$this->ai}", "{$this->ai}.id", '=', "{$this->q}.item_id")
                    ->where("{$this->ai}.channel_id", '=', $request->channel_id)
                    ->where("{$this->ai}.type", '=', 'survey')
                    ->select(
                        "{$this->ua}.id",
                        "{$this->ua}.user_id",
                        "{$this->ua}.question_id",
                        "{$this->ua}.text"
                    );
            }]);

        return $user->removeAppends($query->get(), ['full_name']);
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param \App\Models\AcademyItem $academyItem
     * @param string $type
     * @param bool $withQuestions
     * @return mixed
     */
    protected function getAcademyItems(Request $request, AcademyItem $academyItem, string $type = "quiz", bool $withQuestions = false)
    {
        $academyItems = $academyItem->where('channel_id', $request->channel_id)
            ->where('type', $type)
            ->where('status', 'published');

        if ($withQuestions) {
            $academyItems->with('questions');
        }

        return $academyItems->get(['id', 'title']);
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param \App\Models\District $district
     * @param \App\Models\AcademyItem $academyItem
     * @param \App\Models\UsersDealers $usersDealers
     * @param string $type
     * @return mixed
     */
    protected function getAcademyItemInDistricts(Request $request, District $district, AcademyItem $academyItem, UsersDealers $usersDealers, string $type = "quiz")
    {
        $doneItemsCount = $academyItem->select(
                DB::raw("COUNT({$this->dai}.user_id) as count"),
                "{$this->d}.district_id as district_id"
            )
            ->join($this->dai, "{$this->dai}.item_id", '=', "{$this->ai}.id")
            ->leftJoin($this->ud, "{$this->ud}.user_id", '=', "{$this->dai}.user_id")
            ->leftJoin($this->d, "{$this->d}.id", '=', "{$this->ud}.dealer_id")
            ->where("{$this->ai}.type", $type)
            ->where("{$this->ai}.status", 'published')
            ->where("{$this->ai}.channel_id", $request->channel_id)
            ->groupBy("{$this->d}.district_id");

        $usersCount = $usersDealers->select(
                DB::raw("COUNT({$this->ud}.user_id) as count"),
                "{$this->d}.district_id as district_id"
            )
            ->leftJoin($this->d, "{$this->d}.id", '=', "{$this->ud}.dealer_id")
            ->where("{$this->d}.channel_id", $request->channel_id)
            ->groupBy("{$this->d}.district_id");

        $dealers = $district->select(
                "{$this->dis}.name as district_name",
                DB::raw("users.count AS users_count"),
                DB::raw("done.count as done_count")
            )
            ->leftJoin(
                DB::raw("({$doneItemsCount->toSql()}) as done"),
                "done.district_id", '=', "{$this->dis}.id"
            )
            ->leftJoin(
                DB::raw("({$usersCount->toSql()}) as users"),
                "users.district_id", '=', "{$this->dis}.id"
            )
            ->where("{$this->dis}.channel_id", $request->channel_id);

        $this->addDateRange($dealers, $request, $this->dai);
        $this->addDealerFilters($dealers, $request);

        $itemsInChannelCount = $academyItem->select(
                DB::raw("COUNT(id) as count")
            )
            ->where('channel_id', $request->channel_id)
            ->where('status', 'published')
            ->where('type', $type);

        $query = DB::table(DB::raw("({$dealers->toSql()}) as districts, ({$itemsInChannelCount->toSql()}) as items"))
            ->mergeBindings($doneItemsCount->getQuery())
            ->mergeBindings($usersCount->getQuery())
            ->mergeBindings($dealers->getQuery())
            ->mergeBindings($itemsInChannelCount->getQuery())
            ->select(
                DB::raw('districts.*'),
                DB::raw('items.count * districts.users_count as items_count')
            )
            ->orderBy('users_count', 'desc');

        return $query->get();
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\AcademyItem $academyItem
     * @param string $type
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    protected function getAcademyItemInDealers(Request $request, Dealer $dealer, AcademyItem $academyItem, string $type = "quiz")
    {
        $doneItemsCount = $academyItem->select(
                DB::raw("COUNT({$this->dai}.user_id) as count"),
                "{$this->ud}.dealer_id as dealer_id"
            )
            ->join($this->dai, "{$this->dai}.item_id", '=', "{$this->ai}.id")
            ->leftJoin($this->ud, "{$this->ud}.user_id", '=', "{$this->dai}.user_id")
            ->where("{$this->ai}.type", $type)
            ->where("{$this->ai}.status", 'published')
            ->where("{$this->ai}.channel_id", $request->channel_id)
            ->groupBy("{$this->ud}.dealer_id");

        $dealers = $dealer->select(
                "{$this->d}.id as dealer_id",
                "{$this->d}.name as dealer_name",
                "{$this->dis}.id as district_id",
                "{$this->dis}.name as district_name",
                DB::raw("COUNT({$this->ud}.user_id) AS users_count"),
                DB::raw("done.count as done_count")
            )
            ->leftJoin($this->dis, "{$this->dis}.id", '=', "{$this->d}.district_id")
            ->leftJoin($this->ud, "{$this->ud}.dealer_id", '=', "{$this->d}.id")
            ->leftJoin(
                DB::raw("({$doneItemsCount->toSql()}) as done"),
                "done.dealer_id", '=', "{$this->d}.id"
            )
            ->where("{$this->d}.channel_id", $request->channel_id)
            ->groupBy(
                "{$this->d}.id",
                "{$this->dis}.id",
                "done.count"
            );

        $this->addDateRange($dealers, $request, $this->dai);
        $this->addDealerFilters($dealers, $request);

        $itemsInChannelCount = $academyItem->select(
                DB::raw("COUNT(id) as count")
            )
            ->where('channel_id', $request->channel_id)
            ->where('status', 'published')
            ->where('type', $type);

        $query = DB::table(DB::raw("({$dealers->toSql()}) as dealers, ({$itemsInChannelCount->toSql()}) as items"))
            ->mergeBindings($doneItemsCount->getQuery())
            ->mergeBindings($dealers->getQuery())
            ->mergeBindings($itemsInChannelCount->getQuery())
            ->select(
                DB::raw('dealers.*'),
                DB::raw('items.count * dealers.users_count as items_count')
            )
            ->orderBy('users_count', 'desc');

        return $query->get();
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\AcademyItem $academyItem
     * @param string $type
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    protected function getAcademyItemInDealersDetailed(Request $request, Dealer $dealer, AcademyItem $academyItem, string $type = 'quiz')
    {
        $subQuery = $academyItem->select(
                "{$this->dai}.item_id as item_id",
                "{$this->ud}.dealer_id as dealer_id",
                DB::raw("COUNT({$this->dai}.item_id) as count")
            )
            ->join($this->dai, "{$this->dai}.item_id", '=', "{$this->ai}.id")
            ->join($this->ud, "{$this->ud}.user_id", '=', "{$this->dai}.user_id")
            ->where("{$this->ai}.channel_id", $request->channel_id)
            ->where("{$this->ai}.type", $type)
            ->where("{$this->ai}.status", 'published')
            ->groupBy(
                "{$this->dai}.item_id",
                "{$this->ud}.dealer_id"
            );

        $query = $dealer->select(
            "{$this->d}.id as dealer_id",
            "{$this->ai}.id as item_id",
            DB::raw("IFNULL(taken.count, 0) AS done_count")
        )
            ->join($this->ai, function($join) use ($type) {
                $join->on("{$this->ai}.channel_id", '=', "{$this->d}.channel_id")
                    ->where("{$this->ai}.type", '=', $type)
                    ->where("{$this->ai}.status", '=', 'published');
            })
            ->leftJoin($this->dis, "{$this->dis}.id", '=', "{$this->d}.district_id")
            ->leftJoin(
                DB::raw("({$subQuery->toSql()}) as taken"),
                function($join) {
                    $join->on('taken.dealer_id', '=', "{$this->d}.id")
                        ->on('taken.item_id', '=', "{$this->ai}.id");
                }
            )
            ->mergeBindings($subQuery->getQuery())
            ->where("{$this->d}.channel_id", $request->channel_id);


        $this->addDealerFilters($query, $request);

        return $query->get();
    }

    /**
     * @param $query
     * @param \App\Http\Requests\Request $request
     */
    protected function addDealerFilters($query, Request $request)
    {
        if (!empty($request->filter)) {
            $filters = $this->getQueriedFilters($request);
            foreach ($filters as $criteria => $value) {
                $searchedId = abs(intval($value));
                if (! $searchedId) {
                    continue;
                }
                switch($criteria) {
                    case "dealer_id":
                        $query->where("{$this->d}.id", $searchedId);
                        break;
                    case "district_id":
                        $query->where("{$this->d}.district_id", $searchedId);
                        break;
                    case "position_id":
                        $query->whereIn("{$this->d}.id", function($subQuery) use ($searchedId) {
                            $subQuery->select("{$this->ud}.dealer_id")
                                ->from($this->ud)
                                ->join($this->up, function($join) use ($searchedId) {
                                    $join->on("{$this->up}.user_id", '=', "{$this->ud}.user_id")
                                        ->where("{$this->up}.position_id", $searchedId);
                                });
                        });
                        break;
                }
            }
        }
    }
}
