<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CampaignArchive extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'campaign_id',
        'object_type',
        'object_id',
        'units_number',
        'earned_points',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}
