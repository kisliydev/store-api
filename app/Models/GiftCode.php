<?php

namespace App\Models;

use App\Http\Requests\GiftCodes\DestroyRequest;
use App\Http\Requests\GiftCodes\ShowRequest;
use App\Http\Requests\GiftCodes\StoreRequest;
use App\Traits\Shortcodes;

class GiftCode extends Model
{
    use Shortcodes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'channel_id',
        'title',
        'description',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['title_short_code', 'description_short_code'];

    /**
     * @return string
     */
    public function getTitleShortCodeAttribute()
    {
        return "%%gift_code_title id={$this->id}%%";
    }

    /**
     * @return string
     */
    public function getDescriptionShortCodeAttribute()
    {
        return "%%gift_code_description id={$this->id}%%";
    }

    /**
     * @see \App\Traits\Shortcodes
     * @param array $args
     * @return string
     */
    public function getGiftCodeTitleShortcodeContent(array $args)
    {
        return $this->find($args['id'])->title ?? '';
    }

    /**
     * @see \App\Traits\Shortcodes
     * @param array $args
     * @return string
     */
    public function getGiftCodeDescriptionShortcodeContent(array $args)
    {
        return $this->find($args['id'])->description ?? '';
    }

    /**
     * @param \App\Http\Requests\GiftCodes\ShowRequest $request
     * @return mixed
     */
    public function getList(ShowRequest $request)
    {
        $query = $this->where('channel_id', $request->channel_id);

        return $this->addPagination($query, $request->query(), $request->per_page,
            filter_var($request->simple_paginate, FILTER_VALIDATE_BOOLEAN));
    }

    /**
     * @param \App\Http\Requests\GiftCodes\StoreRequest $request
     * @return mixed
     */
    public function createEntity(StoreRequest $request)
    {
        return $this->create($request->validated());
    }

    /**
     * @param \App\Http\Requests\GiftCodes\DestroyRequest $request
     * @return mixed
     */
    public function destroyEntities(DestroyRequest $request)
    {
        return $this->whereIn('id', json_decode($request->codes))->delete();
    }
}
