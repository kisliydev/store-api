<?php

namespace App\Models;

use App\Http\Requests\Point\PointGetAllRequest;
use App\Http\Requests\Point\PointGetRequest;
use App\Http\Requests\Request;
use App\Http\Requests\Sales\UpdateSaleRequest;
use App\Http\Requests\Point\PointSaveProductsRequest;
use App\Http\Requests\Point\PointSaveFromWebRequest;
use App\Http\Requests\Point\PointUpdateRequest;
use App\Http\Requests\Point\PointBulkActionRequest;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\DB;
use App\Traits\FullTextSearch;

class PointsStatistic extends Model
{
    use FullTextSearch;

    const WEIGHTPOINTS_ITEMS_TYPE_LIST = [
        'product',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'channel_id',
        'object_type',
        'object_id',
        'earned_points',
        'units_number',
        'created_at',
        'event_id',
        'points_per_item',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'object_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function quiz()
    {
        return $this->belongsTo(AcademyItem::class, 'object_id', 'id')->where('type', 'quiz');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function survey()
    {
        return $this->belongsTo(AcademyItem::class, 'object_id', 'id')->where('type', 'survey');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function manual()
    {
        return $this->belongsTo(ManualPoint::class, 'object_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function level()
    {
        return $this->belongsTo(Level::class, 'object_id', 'id');
    }

    /**
     * @param $query
     * @param \App\Http\Requests\Request $request
     * @return Builder
     */
    public function scopeGetItemsByUserLeague($query, Request $request)
    {
        return $query->when($request->league_id, function ($q) use ($request) {
            $user = new User;
            $dealerUsers = $user->whereHas('dealers', function ($q) use ($request) {
                $q->where('league_id', $request->league_id);
            });

            return $q->whereIn("{$this->getTable()}.user_id", $dealerUsers->get()->pluck('id')->all());
        });
    }

    /**
     * @param \App\Http\Requests\Point\PointGetAllRequest $request
     * @return \Illuminate\Contracts\Pagination\Paginator|\Illuminate\Database\Eloquent\Builder
     */
    public function getList(PointGetAllRequest $request)
    {
        return $this->_getListByChannel($request, [
            'user' => function($query) {
                $query->select('id', 'first_name', 'last_name')->with(['avatar'])->get();
            }
        ], ['user_name', 'user_email', 'object_name']);
    }

    /**
     * @param \App\Http\Requests\Point\PointGetRequest $request
     * @return \App\Models\PointsStatistic|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     */
    public function getItem(PointGetRequest $request)
    {
        return $this->find($request->id);
    }

    /**
     * @param \App\Http\Requests\Point\PointSaveProductsRequest $request
     * @param \App\Models\AppStatistic $appStatistic
     * @param \App\Models\Product $product
     * @return bool|int
     */
    public function addProducts(PointSaveProductsRequest $request, AppStatistic $appStatistic, Product $product) {

        $data = $request->validated();
        $total = 0;
        $user_id = auth()->user()->id;
        $event_id = $appStatistic->create([
            'event' => 'sale',
            'user_id' => $user_id,
            'channel_id' => $request->channel_id,
        ])->id;
        $remove_event = true;

        foreach ($data['products'] as $item) {
            $product_item = $product->find($item['object_id']);

            if (! $product_item) {
                continue;
            }

            $remove_event = false;
            $point = $this->addStatItem(
                $product_item,
                $user_id,
                'product',
                $data['channel_id'],
                abs(intval($item['units_number'])),
                $event_id
            );
            $total += $point->earned_points;
        }

        if ($remove_event) {
            $appStatistic->destroy($event_id);
        }

        return $total;
    }

    /**
     * @param \App\Http\Requests\Sales\UpdateSaleRequest $request
     * @return bool
     */
    public function updateProductsSale(UpdateSaleRequest $request)
    {
        $user_id = auth()->user()->id;
        $points = $this
            ->where('user_id', $user_id)
            ->where('event_id', $request->event_id)
            ->with(['product'])
            ->get();

        $request_data = collect($request->products);
        $updated = false;
        $points->each(function($point) use ($request_data, &$updated) {
            $data = $request_data->where('object_id', $point->object_id)->first();

            if (! $data) {
                return;
            }

            $earned_points = $this->_calculateEarnedPoints($data['units_number'], $point->product, $point->points_per_item, null, 'product');
            $point->units_number = $data['units_number'];
            $point->earned_points = $earned_points;
            $point->save();
            $updated = true;
        });

        if (! $updated) {
            return false;
        }

        return (int)$this->select(DB::raw('SUM(earned_points) as total'))
           ->where('user_id', $user_id)
           ->where('event_id', $request->event_id)
           ->first()->total;
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param string $item_type
     * @return bool
     */
    public function addAcademyItem(Request $request, string $item_type) {
        $user_id = auth()->user()->id;

        $item = AcademyItem::where('id', $request->object_id)
            ->where('channel_id', $request->channel_id)
            ->where('type', $item_type)
            ->where('status', ['published'])
            ->first();

        $point = $this->addStatItem($item, $user_id, $item->type, $request->channel_id);

        switch( $item_type ) {
            case 'survey':
                foreach($request['answers'] as $answer) {
                    UserAnswer::create([
                        'user_id' => $user_id,
                        'question_id' => $answer['question_id'],
                        'point_statistics_id' => $point->id,
                        'text' => $answer['text'],
                    ]);
                }
                break;
            case 'quiz':
            default:
                break;
        }

        DoneAcademyItem::create([
            'user_id' => $user_id,
            'item_id' => $item->id,
        ]);

        return $point->earned_points;
    }

    /**
     * @param \App\Models\Model $item
     * @param int $user_id
     * @param string $type
     * @param int $channel_id
     * @param int $units_number
     * @param int|null $event_id
     * @return mixed
     */
    public function addStatItem(Model $item, int $user_id, string $type, int $channel_id, int $units_number = 1, int $event_id = null) {
        $units_number = $units_number ?? 1;
        $item = $this->create([
            'user_id' => $user_id,
            'channel_id' => $channel_id,
            'event_id' => $event_id,
            'object_type' => $type,
            'object_id' => $item->id,
            'units_number' => $units_number,
            'earned_points' => $this->_calculateEarnedPoints($units_number, $item, null, $user_id, $type),
            'points_per_item' => $item->points,
        ]);

        return $item;
    }

    /**
     * @param \App\Http\Requests\Point\PointSaveFromWebRequest $request
     * @return \App\Models\PointsStatistic|bool|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     */
    public function add(PointSaveFromWebRequest $request)
    {
        $point = $this->create($request->validated());
        return $this->where('id', $point->id)->with([
            'user' => function($query) {
                $query->select('id', 'first_name', 'last_name')->with(['avatar'])->get();
            }
        ])->get();
    }

    /**
     * @param \App\Http\Requests\Point\PointUpdateRequest $request
     * @return mixed
     */
    public function resave(PointUpdateRequest $request)
    {
        $point = $this->find($request->id);

        if ($point) {
            $point->user_id = $request->user_id;
            $point->object_type = $request->object_type;
            $point->object_id = $request->object_id;
            $point->earned_points = $request->earned_points;
            $point->units_number = $request->units_number;
            $point->save();
        }

        return $this->where('id', $point->id)->with([
            'user' => function($query) {
                $query->select('id', 'first_name', 'last_name')->with(['avatar'])->get();
            }
        ])->get();
    }

    /**
     * @param \App\Http\Requests\Point\PointGetRequest $request
     * @return \App\Models\PointsStatistic|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     */
    public function remove(PointGetRequest $request)
    {
        $point = $this->find($request->id);
        if ($point) {
            DB::table('user_answers')
                ->where('point_statistics_id', $request->id)
                ->update(['point_statistics_id' => 0]);
            $this->destroy($request->id);
        }

        return $point;
    }

    /**
     * @param int $units_number
     * @param \App\Models\Model $object
     * @param int|null $points_per_unit
     * @param int|null $userID
     * @param string $type
     * @return int
     */
    private function _calculateEarnedPoints(int $units_number, Model $object, int $points_per_unit = null, int $userID = null, string $type)
    {
        $points = is_null($points_per_unit) ? $units_number * $object->points : $units_number * $points_per_unit;
        $user   = auth()->user();

        if (! $user) {
            $user = User::find($userID);
        }

        if (in_array($type, self::WEIGHTPOINTS_ITEMS_TYPE_LIST)) {
            //if ($object->use_weight_points) {
            //
            //    $position = $user->position($object->channel_id)->first();
            //    if ($position) {
            //        $points *= $position->weight_points;
            //    }

            $dealer = $user->dealer($object->channel_id)->first();
            if ($dealer) {
                $points *= $dealer->weight_points;
            }
            //}
        }

        return round($points);
    }

    /**
     * @param \App\Http\Requests\Point\PointBulkActionRequest $request
     * @return bool
     */
    public function bulkAction(PointBulkActionRequest $request) {
        $points = $this->whereIn('id', $request->id)->get();

        if (! $points->all()) {
            return false;
        }

        switch($request->action) {
            case 'delete':
                DB::table('user_answers')
                    ->whereIn('point_statistics_id', $request->id)
                    ->update(['point_statistics_id' => 0]);
                $this->destroy($request->id);
                break;
            default:
                return false;
        }

        return $points;
    }

    /**
     * @return array
     */
    public function getObjectTypesList()
    {
        return ['product', 'quiz', 'survey', 'manual', 'level'];
    }


    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string|null $search
     * @param array $search_in_columns
     */
    public function addSearch(Builder $query, string $search = null, array $search_in_columns = [])
    {
        if (empty($search) || empty($search_in_columns)) {
            return;
        }

        $query->where(function($where) use($search) {
            $where->whereIn('user_id', function($clause) use ($search) {
                $clause->select('id')
                    ->from('users')
                    ->where('users.first_name', 'like', "%{$search}%")
                    ->orWhere('users.last_name', 'like', "%{$search}%")
                    ->orWhere('users.email', 'like', "%{$search}%");
                $this->addFullTextSearch($clause, $search);
            }, 'or')
                ->orWhere(function($clause) use ($search) {
                    $clause->where('object_type', 'manual')
                        ->whereIn('object_id', function($subQuery) use ($search) {
                            $subQuery->select('id')
                                ->from('manual_points')
                                ->where('title', 'like', "%{$search}%");
                        });
                })
                ->orWhere(function($clause) use ($search) {
                    $clause->where('object_type', 'level')
                        ->whereIn('object_id', function($subQuery) use ($search) {
                            $subQuery->select('id')
                                ->from('levels')
                                ->where('title', 'like', "%{$search}%");
                        });
                })
                ->orWhere(function($clause) use ($search) {
                    $clause->where('object_type', 'product')
                        ->whereIn('object_id', function($subQuery) use ($search) {
                            $subQuery->select('id')
                                ->from('products')
                                ->where('name', 'like', "%{$search}%");
                        });
                })
                ->orWhere(function($clause) use ($search) {
                    $clause->where('object_type', 'quiz')
                        ->whereIn('object_id', function($subQuery) use ($search) {
                            $subQuery->select('id')
                                ->from('academy_items')
                                ->where('title', 'like', "%{$search}%")
                                ->where('type', '=', "quiz");
                        });
                })
                ->orWhere(function($clause) use ($search) {
                    $clause->where('object_type', 'survey')
                        ->whereIn('object_id', function($subQuery) use ($search) {
                            $subQuery->select('id')
                                ->from('academy_items')
                                ->where('title', 'like', "%{$search}%")
                                ->where('type', '=', "survey");
                        });
                });
        });
    }

    /**
     * @used to set custom elastic search document structure
     * @return array
     */
    function getIndexDocumentData()
    {
        $relation = $this->object_type;
        if (!empty($this->$relation->name)) {
            $object_name = $this->$relation->name;
        } elseif (!empty($this->$relation->title)) {
            $object_name = $this->$relation->title;
        } else {
            $object_name = '';
        }

        return [
            'id'          => $this->id,
            'user_name'   => $this->user->full_name ?? '',
            'user_email'  => $this->user->email ?? '',
            'object_name' => $object_name
        ];
    }

    /**
     * @return string
     */
    function getIndexName()
    {
        return $this->getTable();
    }
}
