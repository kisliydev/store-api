<?php
namespace App\Models;

use App\Http\Requests\Campaign\GetByChannelRequest;
use App\Http\Requests\Campaign\CampaignUpdateRequest;
use App\Http\Requests\Campaign\CampaignGetRequest;
use App\Http\Requests\Campaign\CampaignSaveAppLayoutRequest;
use App\Http\Requests\Campaign\CampaignDeleteRequest;
use App\Services\Base64ImageReplacer\Base64ImageReplacer;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Traits\ArrayData;

class Campaign extends Model
{
    use ArrayData, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'channel_id',
        'name',
        'started_at',
        'finished_at',
        'sale_started_at',
        'sale_finished_at',
        'app_layout',
        'status',
    ];

    /**
     * @var array
     */
    protected $statuses = [
        'scheduled',
        'pre_active',
        'active',
        'post_active',
        'finished',
        'archived',
        'draft',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'logo_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'app_layout',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'app_layout' => 'json',
    ];

    protected $dates = [
        'deleted_at',
    ];

    /**
     * @var string
     */
    private $_media_type = 'app_layout';

    const ACTIVE_CAMPAIGN_STATUSES = [
        'pre_active',
        'active',
        'post_active',
    ];

    const PAST_CAMPAIGN_STATUSES = [
        'finished',
        'archived',
    ];

    /**
     * @return array
     */
    public function getStatuses()
    {
        return $this->statuses;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function channel()
    {
        return $this->belongsTo('App\Models\Channel');
    }

    /**
     * @return string
     */
    public function getLogoIDAttribute()
    {
        return $this->app_layout['home_items']['products']['background_image'] ?? null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function logo()
    {
        return $this->HasOne(Media::class, 'id', 'logo_id');
    }

    /**
     * @param $query
     * @param int $channelID
     * @param int $currentID
     * @return mixed
     */
    public function scopePrevious($query, int $channelID, int $currentID)
    {
        return $query->whereIn('status', [
            'pre_active',
            'active',
            'post_active',
            'finished'
        ])->where('channel_id', $channelID)->where('id', '<', $currentID)->orderBy('id', 'desc');
    }

    /**
     * The campaign is considered to be the current if:
     * 1. It has one of the following statuses: 'pre_active', 'active', 'post_active';
     * 2. It has status 'scheduled' in case if there is no campaign(s) with the
     * statuses described in p.1;
     * 3. It has status 'finished' in case if there is no campaign(s) with the
     * statuses described in p.p.1,2;
     *
     * In a fact this method returns query that allows to get not just only the current campaign
     * (according to the description above).
     * If current campaign is active ('pre_active', 'active', 'post_active') or finished it also returns
     * the closest scheduled campaign.
     *
     * @param $query
     * @return mixed
     */
    public function scopeCurrent($query)
    {
        $table = $this->getTable();

        $subQuery = DB::table($table)->select('status', DB::raw("channel_id as campaign_channel_id"), DB::raw("MIN(id) as id"))->whereRaw("status <> 'archived'")->groupBy('campaign_channel_id', 'status');

        $query->select("{$table}.*")->from(DB::raw("({$subQuery->toSql()}) as base"))->mergeBindings($subQuery)->join($table, function (
                $join
            ) use ($table) {
                $join->on('base.status', '=', "{$table}.status")->on('base.id', '=', "{$table}.id");
            })->orderByRaw("FIELD({$table}.status,'pre_active','active','post_active','finished','scheduled', 'draft')");

        return $query;
    }

    /**
     * @param $query
     * @param string $dateTime
     * @return mixed
     */
    public function scopeReachedDatePoints(Builder $query, string $dateTime)
    {
        return $query->where(function ($query) use ($dateTime) {

            /* Those campaigns that starts now */
            $query->where('started_at', '<=', $dateTime)->where('status', 'scheduled');
        })->orwhere(function ($query) use ($dateTime) {

            /* Those campaigns which sales starts now */
            $query->where('sale_started_at', '<=', $dateTime)->where('status', 'pre_active');
        })->orwhere(function ($query) use ($dateTime) {

            /* Those campaigns which sales finishes now */
            $query->where('sale_finished_at', '<=', $dateTime)->where('status', 'active');
        })->orwhere(function ($query) use ($dateTime) {

            /* Those campaigns that finishes now */
            $query->where('finished_at', '<=', $dateTime)->where('status', 'post_active');
        });
    }

    /**
     * @param \App\Http\Requests\Campaign\GetByChannelRequest $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getAll(GetByChannelRequest $request)
    {
        return $this->_getListByChannel($request, ['logo'], ['name']);
    }

    /**
     * @param \App\Http\Requests\Campaign\CampaignUpdateRequest $request
     * @return mixed
     */
    public function resave(Request $request)
    {
        $campaign = $this->find($request->id);
        switch($campaign->status) {
            case 'draft':
            case 'scheduled':
                $allowedFields = ['name', 'started_at', 'sale_started_at', 'sale_finished_at', 'finished_at', 'status'];
                break;
            case 'pre_active':
                $allowedFields = ['name', 'sale_started_at', 'sale_finished_at', 'finished_at'];
                break;
            case 'active':
                $allowedFields = ['name', 'sale_finished_at', 'finished_at'];
                break;
            case 'post_active':
                $allowedFields = ['name', 'finished_at'];
                break;
            case 'finished':
            /*
             * Actually, an ability to edit archived campaign's data is banned.
             * But, just for some case...
             */
            case 'archived':
            default:
                $allowedFields = ['name'];
                break;
        }

        foreach ($allowedFields as $field) {
            $campaign->$field = $request->$field ?? $campaign->$field;
        }

        $campaign->save();

        return $campaign;
    }

    /**
     * @param \App\Http\Requests\Campaign\CampaignSaveAppLayoutRequest $request
     * @param \App\Models\Media $media
     * @param \App\Services\Base64ImageReplacer $imageReplacer
     * @return bool
     */
    public function saveAppLayout(CampaignSaveAppLayoutRequest $request, Media $media, Base64ImageReplacer $imageReplacer)
    {
        $campaign = $this->makeVisible(['app_layout'])->find($request->id);
        $newMeta = array_intersect_key($request->data, $this->getDefaultAppLayout());

        if (!$newMeta) {
            return false;
        }

        $oldMeta = $campaign->app_layout ?? $this->getDefaultAppLayout();
        $newMeta = $campaign->_saveMedia($newMeta, $oldMeta, $media);
        $newMeta = $campaign->_clearMediaOptions($newMeta);
        if (isset($newMeta['prizes_and_rules']['content'])) {
            $newMeta['prizes_and_rules']['content'] = $imageReplacer->setContent(
                $newMeta['prizes_and_rules']['content']
            )->replaceImages();
        }
        $campaign->app_layout = $this->replaceRecursive($oldMeta, $newMeta);
        $campaign->app_layout = $this->fillRecursive($campaign->app_layout, $this->getDefaultAppLayout());

        $campaign->save();

        return $campaign;
    }

    /**
     * @param \App\Http\Requests\Campaign\CampaignDeleteRequest $request
     * @return mixed
     */
    public function remove(CampaignDeleteRequest $request)
    {
        $campaign = $this->withTrashed()->find($request->id)->forceDelete();

        return $campaign;
    }

    /**
     * @param string|null $status
     * @return bool|mixed|string
     */
    public function getNextStatus(string $status = null)
    {
        $status = $status ?? $this->status;
        $nextDatePoint = $nextStatus = false;

        switch ($status) {
            case 'scheduled':
                $nextStatus = 'pre_active';
                $nextDatePoint = 'sale_started_at';
                break;
            case 'pre_active':
                $nextStatus = 'active';
                $nextDatePoint = 'sale_finished_at';
                break;
            case 'active':
                $nextStatus = 'post_active';
                $nextDatePoint = 'finished_at';
                break;
            case 'post_active':
                $nextStatus = 'finished';
                break;
            case 'finished':
                $nextStatus = 'archived';
                break;
            case 'archived':
            case 'draft':
            default:
                break;
        }

        if (! $nextStatus) {
            return $status;
        }

        /* check whether the next date point came */
        if ($nextDatePoint && Carbon::now()->gte(Carbon::createFromFormat('Y-m-d H:i:s', $this->$nextDatePoint))) {
            return $this->getNextStatus($nextStatus);
        }

        return $nextStatus;
    }

    /**
     * @param array|null $args
     * @return array
     */
    public function getAppLayoutImagesIDs(array $args = null) {
        $ids = [];
        $args = $args ?? $this->app_layout;
        foreach($args as $key => $value) {
            if (is_array($value)) {
                $ids = array_merge($ids, $this->getAppLayoutImagesIDs($value));
            } elseif ($this->isImageOption($key) && ! empty($value)) {
                $ids[] = $value;
            }
        }
        return $ids;
    }

    /**
     * @param $option
     * @param $old
     * @param \App\Models\Media $media
     * @param string|null $key
     * @param string|null $filename
     * @return array|bool
     * @throws \Exception
     */
    private function _saveMedia($option, $old, Media $media, string $key = null, string $filename = null)
    {
        /* file postfix in order to get unique file name */
        if (is_null($filename)) {
            $filename = $key;
        } else {
            $key = is_numeric($key) ? $this->_toOrdinalWord(abs(intval($key))) : $key;
            $filename = "{$filename}_{$key}";
        }

        if (is_array($option)) {
            foreach($option as $optionKey => $value) {
                $imageOptionKey = $this->getImageOption($optionKey);
                if ($imageOptionKey && ! empty($value) && is_numeric($value)) {
                    /* re-use image from the media library */
                    $option[$imageOptionKey] = $media->copyImage(
                        $this->_media_type,
                        $value,
                        "{$filename}_{$this->id}",
                        $this->channel_id,
                        (empty($old[$imageOptionKey]) ? 0 : abs(intval($old[$imageOptionKey])))
                    );
                } else {
                    /* process currently managed item */
                    $option[$optionKey] = $this->_saveMedia(
                        $value,
                        ($old[$optionKey] ?? ''),
                        $media,
                        strval($optionKey),
                        $filename
                    );

                }
            }

        } elseif ($this->isImageOption($key) && is_object($option)) {
            /* save new image to the library and bind it to the currently managed option */
            $option = $media->updateImage(
                $this->_media_type,
                $option,
                "{$filename}_{$this->id}",
                $this->channel_id,
                abs(intval($old))
            );
        } elseif ($this->isColorOption($key)) {
            $option = strtolower($option);
        }

        return $option;
    }

    /**
     * @param $key
     * @return bool|mixed
     */
    public function getImageOption($key)
    {
        return 0 === strpos($key, 'media_') ? str_replace('media_', '', $key) : false;
    }

    /**
     * Get current or past campaign app layout for quizes
     *
     * @param int $channelId
     * @param bool $isCurrent
     * @return Collection
     */
    public function getCampaignLayout(int $channelId, bool $isCurrent = true)
    {
        $status = $isCurrent ? self::ACTIVE_CAMPAIGN_STATUSES : self::PAST_CAMPAIGN_STATUSES;

        return $this->where('channel_id', $channelId)->whereIn('status', $status)->get()->makeVisible(['app_layout']);
    }

    /**
     * @param array $options
     * @return array
     */
    private function _clearMediaOptions(array $options)
    {
        foreach($options as $key => $option) {
            if (is_array($option)) {
                $options[$key] = $this->_clearMediaOptions($option);
            } elseif (0 === strpos($key, 'media_')) {
                $options[$key] = null;
            }
        }
        return $options;
    }

    /**
     * @param int $number
     * @return mixed|string
     */
    private function _toOrdinalWord(int $number) {
        $firstWord = [
            'zero','first','second','third','fourth','fifth',
            'sixth','seventh','eighth','ninth','tenth','eleventh',
            'twelfth','thirteenth','fourteenth','fifteenth','sixteenth',
            'seventeenth','eighteenth','nineteenth','twentieth'
        ];
        $secondWord = ['', '', 'twenty','thirty','forty','fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

        if($number <= 20)
            return $firstWord[$number];

        $firstNum = substr($number,-1,1);
        $secondNum = substr($number,-2,1);
        return "{$secondWord[$secondNum]}_{$firstWord[$firstNum]}";
    }

    /**
     * @return array
     */
    public function getDefaultAppLayout()
    {
        return [
            /* images */
            "default_dealer_image" => null,
            "login_background_image" => null,
            "register_background_image" => null,
            "login_logo_image" => null,
            "brand_logo_image" => null,
            "brand_academy_quiz_background_image" => null,
            "brand_academy_survey_background_image" => null,
            "brand_academy_products_background_image" => null,
            "user_default_profile_image" => null,

            /* numerics */
            "logo_height" => "20",
            "footer_border_height" => "1",
            "completed_sales_today_font_size" => "",

            /* colors */
            "h1_color" => "#ffffff",
            "app_background_color" => "#1a1919",
            "app_selected_content_background_color" => "#33383d",
            "nasted_content_background_color" => "#616770",
            "brand_main_color" => "#d8223c",
            "brand_secondary_color" => "#62ccb7",
            "checkbox_border_color" => "#5c5f66",
            "text_main_color" => "#fff",
            "text_secondary_color" => "rgba(255, 255, 255, 0.8)",
            "input_text_color" => "#fff",
            "input_text_title_color" => "#ffffff80",
            "input_border_bottom_color" => "#fff3",
            "active_input_border_bottom" => "#fffc",
            "list_item_text_color" => "#fff",
            "button_text_color" => "#fff",
            "button_background_color" => "#d8223c",
            "header_border_bottom_color" => "#ffffff1a",
            "user_left_count_background_color" => "#33383d",
            "modal_background_color" => "#33383d",
            "completed_points_color" => "",
            "text_inupt_field_background_color" => "#FFFFFF",
            "unread_message_background_color" => "#f7ecd7",
            "completed_list_label_text_color" => "",
            "completed_congratulations_color" => "",
            "welcome_block_background_color" => "#282c37",
            "notification_circle_text_color" => "#FFFFFF",
            "secondary_button_text_color" => "#a8a8a8",
            "completed_sales_today_color" => "",
            "overlay_text_heading_color" => "#2e2e2e",
            "completed_new_points_color" => "",
            "stats_survey_points_color" => "#de0000",
            "notification_circle_color" => "#ff0000",
            "completed_total_font_size" => "",
            "login_welcome_text_color" => "#FFFFFF",
            "required_color" => "#FF0000",
            "popup_head_color" => "#000000",
            "heading_text_color" => "#0a0a0a",
            "header_border_color" => "#000000",
            "header_background_color" => "#1a1919",
            "footer_background_color" => "rgba(26,25,25,0.98)",
            "footer_border_color" => "rgba(255, 255, 255, 0.1)",
            "footer_text_color" => "#515459",
            "footer_highlight_text_color" => "#25292c",
            "header_icons_color" => "#ffffff",
            "footer_icon_color" => "#ffffff",
            "line_color" => "#FFFFFF",
            "menu_icon_color" => "#FFFFFF",
            "menu_text_color" => "#FFFFFF",
            "menu_background_color" => "#00020e",
            "stats_contest_points_color" => "#62ccb7",
            "stats_quiz_points_color" => "#de0000",
            "stats_positive_color" => "#00be98",
            "stats_negative_color" => "#e00101",
            "completed_total_color" => "",
            "stats_other_points_color" => "#787878",
            "stats_query_points_color" => "#de0000",

            "welcome_screens" => [
                [
                    "header" => "",
                    "position" => "top",
                    "content" => "Praesent sapien massa, convallis a pellentesque nec, egestas non nisi.",
                    "video" => "",
                    "background_image" => null, // image ID,
                ],
                [
                    "header" => "",
                    "position" => "top",
                    "content" => "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
                    "video" => "",
                    "background_image" => null, // image ID,
                ],
                [
                    "header" => "",
                    "position" => "top",
                    "content" => "Curabitur non nulla sit amet nisl tempus convallis quis ac lectus.",
                    "video" => "",
                    "background_image" => null, // image ID,
                    'button_title' => "Let's go",
                ],
            ],
            "prizes_and_rules" => [
                'header' => 'Suspendisse nisl elit',
                'subheader' => 'Pellentesque auctor neque nec urna.',
                'date_from' => '',
                'date_till' => '',
                'content' => 'Phasellus blandit leo ut odio. Aenean posuere, tortor sed cursus feugiat.',
            ],
            "home_items" => [
                'products' => [
                    "headline_color" => "#fff",
                    "background_image" => null, //image ID,
                    "text_color" => "#000000",
                    "content" => '',
                    'header' => '',
                ],
                "academy" => [
                    "headline_color" => "#fff",
                    "background_image" => null, //image ID,
                    "text_color" => "#000000",
                    "content" => '',
                    'header' => '',
                ],
                "messages" => [
                    "headline_color" => "#fff",
                    "background_image" => null, //image ID,
                    "text_color" => "#000000",
                    "content" => '',
                    'header' => '',
                ],
            ],

            "help_tooltip" => [ // Results help tooltip
                'header'    => 'Suspendisse nisl elit',
                'content'   => 'Phasellus blandit leo ut odio. Aenean posuere, tortor sed cursus feugiat.',
            ],

            "manage_store_help_tooltip" => [
                'header'    => 'Suspendisse nisl elit',
                'content'   => 'Phasellus blandit leo ut odio. Aenean posuere, tortor sed cursus feugiat.',
            ],

            /**
             *
             * These fields are used in order to re-use images from media library.
             * If it is necessary to add more options with images it is also necessary
             * to add one more option in format media_{option_name}
             * @see self::_saveMedia
             */
            "media_background_image" => null,
            "media_default_dealer_image" => null,
            "media_login_background_image" => null,
            "media_register_background_image" => null,
            "media_login_logo_image" => null,
            "media_brand_logo_image" => null,
            "media_brand_academy_quiz_background_image" => null,
            "media_brand_academy_survey_background_image" => null,
            "media_brand_academy_products_background_image" => null,
            "media_user_default_profile_image" => null,
            "store_rank_results_global_tab_common" => "Global",
            "store_rank_results_store_tab_average" => "Average"
        ];
    }

    /**
     * @param string $option
     * @return false|int
     */
    public function isColorOption(string $option)
    {
        return preg_match('/_color$/', $option);
    }

    /**
     * @param string $option
     * @return false|int
     */
    public function isNumericOption(string $option)
    {
        return preg_match('/(
            font_size
            |height
        )$/ixu', $option);
    }

    /**
     * @param string $option
     * @return bool
     */
    public function isStringOption(string $option)
    {
        return in_array($option, [
            'header',
            'subheader',
            'content',
            'video',
            'position',
            'button_title',
        ]);
    }

    /**
     * @param string $option
     * @return bool
     */
    public function isDate(string $option)
    {
        return in_array($option, [
            'date_from',
            'date_till',
        ]);
    }

    /**
     * @param string $option
     * @return bool
     */
    public function isImageOption(string $option)
    {
        return preg_match("/^(?!media_)(.*?)_image$/", $option);
    }

    /**
     * @return bool
     */
    public function isRunning()
    {
        return in_array($this->status, ['pre_active', 'active', 'post_active', 'finished']);
    }

    /**
     * @return bool
     */
    public function isAllowedStatus()
    {
        return in_array($this->status, $this->statuses);
    }

    /**
     * @param \App\Http\Requests\Campaign\CampaignDeleteRequest $request
     * @return int
     */
    public function toTrash(CampaignDeleteRequest $request)
    {
        $this->findOrFail($request->id)->delete();

        return (int) $request->id;
    }

    /**
     * @param \App\Http\Requests\Campaign\CampaignGetRequest $request
     * @return int
     */
    public function restoreCampaign(CampaignGetRequest $request)
    {
        $this->onlyTrashed()->findOrFail($request->id)->restore();

        return (int) $request->id;
    }
}
