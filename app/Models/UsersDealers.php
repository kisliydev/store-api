<?php

namespace App\Models;

use App\Transformers\Messages\UserIdTransformer;
use Illuminate\Database\Eloquent\Model;

class UsersDealers extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'dealer_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
    ];

    /**
     * @param int $dealerId
     * @return mixed
     */
    public function getAllUserIdsFromDealer(int $dealerId)
    {
        return $this->select('user_id')
            ->where('dealer_id', $dealerId)
            ->get()
            ->pluck('user_id')
            ->toArray();
    }
}
