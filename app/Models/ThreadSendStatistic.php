<?php

namespace App\Models;

use App\Http\Requests\ThreadSendStatistics\LevelsListRequest;
use Illuminate\Support\Facades\DB;

class ThreadSendStatistic extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'thread_id',
        'sent_to',
    ];

    /**
     * @param \App\Http\Requests\ThreadSendStatistics\LevelsListRequest $request
     * @return mixed
     */
    public function getLevelsList(LevelsListRequest $request)
    {
        $t   = 'threads';
        $u   = 'users';
        $aml = 'levels';
        $s   = $this->getTable();

        return $this->select(
                "{$s}.created_at",
                "{$t}.status as thread_status",
                "{$aml}.title as level_title",
                DB::raw("TRIM(CONCAT({$u}.first_name, ' ', {$u}.last_name)) as user_full_name")
            )
            ->join($t, function($join) use ($t, $s, $request) {
                $join->on("{$t}.id", "{$s}.thread_id")
                    ->where("{$t}.channel_id", $request->channel_id)
                    ->whereRaw("json_extract({$t}.data, '$.level') IS NOT NULL");
            })
            ->leftJoin($aml, function($join) use ($aml, $t) {
                $join->on("{$aml}.id", '=', DB::raw("json_extract({$t}.data, '$.level')"));
            })
            ->leftJoin($u, "{$u}.id", '=', "{$s}.sent_to")
            ->paginate($request->per_page ?? $this->per_page);
    }
}
