<?php

namespace App\Models;

use App\Http\Requests\Brand\BrandGetAllRequest;
use App\Http\Requests\Brand\BrandSaveRequest;
use App\Http\Requests\Brand\BrandGetRequest;
use App\Http\Requests\Brand\BrandUpdateRequest;
use App\Http\Requests\Brand\BrandSaveSettingsRequest;

/**
 * Class Brand
 *
 * @package App\Models
 */
class Brand extends Model
{
    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'settings' => 'json',
    ];

    /**
     * @var array
     */
    public $settings_keys = [
        'onesignal_app_id',
        'onesignal_rest_api_key',
        'slug',
        'app_name',
        'app_url_apple',
        'app_url_google',
    ];

    /**
     * @var string
     */
    private $_media_type = 'brand_logos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'settings',
        'logo_id',
        'created_at',
        'description',
        'legal_name',
        'owner',
    ];

    /**
     * @var string
     */
    protected $default_sort = 'name';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function countryBrands()
    {
        return $this->hasMany('App\Models\CountryBrand');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function brandUsers()
    {
        return $this->hasMany('App\Models\BrandUser');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function channels()
    {
        return $this->hasManyThrough('App\Models\Channel', 'App\Models\CountryBrand');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function logo()
    {
        return $this->hasOne('App\Models\Media', 'id', 'logo_id');
    }

    /**
     * @param \App\Http\Requests\Brand\BrandGetAllRequest $request
     * @return mixed
     */
    public function getList(BrandGetAllRequest $request)
    {
        return $this->_getList($this->query(), $request, ['logo'], ['name']);
    }

    /**
     * @param \App\Http\Requests\Brand\BrandGetRequest $request
     * @return mixed
     */
    public function get(BrandGetRequest $request)
    {
        return $this->_getBrand($request->id);
    }

    /**
     * @param \App\Http\Requests\Brand\BrandSaveRequest $request
     * @param \App\Models\Media $media
     * @return \App\Models\Brand|\App\Models\Brand[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     * @throws \Exception
     */
    public function add(BrandSaveRequest $request, Media $media)
    {
        $data  = $request->validated();
        $brand = $this->create($data);

        if (! empty($data['logo'])) {
            $brand->logo_id = $media->saveImage($this->_media_type, $data['logo'], $brand->id);
            $brand->save();
        } elseif (! empty($data['media_id'])) {
            $brand->logo_id = $media->copyImage($this->_media_type, $data['media_id'], $brand->id);
            $brand->save();
        }

        return $this->_getBrand($brand->id);
    }

    /**
     * @param \App\Http\Requests\Brand\BrandUpdateRequest $request
     * @param \App\Models\Media $media
     * @return \App\Models\Brand|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     * @throws \Exception
     */
    public function resave(BrandUpdateRequest $request, Media $media)
    {
        $brand = $this->find($request->id);
        if ($brand) {
            $brand->name        = $request->name;
            $brand->description = $request->description ?? null;
            $brand->legal_name  = $request->legal_name ?? null;
            $brand->owner       = $request->owner ?? null;
            if (! empty($request->remove_logo) && filter_var($request->remove_logo, FILTER_VALIDATE_BOOLEAN)) {
                $media->removeMedia([$brand->logo_id]);
                $brand->logo_id = null;
            } elseif (! empty($request->logo)) {
                $brand->logo_id = $media->updateImage($this->_media_type, $request->logo, $brand->id, null, $brand->logo_id);
            } elseif(! empty($request->media_id)) {
                $brand->logo_id = $media->copyImage($this->_media_type, $request->media_id, $brand->id, null, $brand->logo_id);
            }
            $brand->save();
        }

        return $this->_getBrand($brand->id);
    }

    /**
     * @param \App\Http\Requests\Brand\BrandSaveSettingsRequest $request
     * @return mixed
     */
    public function saveSettings(BrandSaveSettingsRequest $request)
    {
        $brand = $this->find($request->id);
        if ($brand) {
            $data = [];
            foreach ($this->settings_keys as $key) {
                if (isset($request->$key)) {
                    $data[$key] = $request->$key;
                }
            }
            $brand->settings = $data;
            $brand->save();
        }

        return $this->_getBrand($brand->id);
    }

    /**
     * @param \App\Http\Requests\Brand\BrandGetRequest $request
     * @return \App\Models\Brand|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     */
    public function remove(BrandGetRequest $request)
    {
        $brand = $this->_getBrand($request->id);

        if ($brand) {
            $this->destroy($request->id);
        }

        return $brand;
    }

    /**
     * @param int $brand_id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     */
    private function _getBrand(int $brand_id)
    {
        return $this->with(['logo'])->find($brand_id);
    }

    /**
     * @return mixed
     */
    public function getSettings()
    {
        return $this->select('settings', 'name', 'id')->get();
    }
}
