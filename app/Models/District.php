<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Http\Requests\District\DistrictGetAllRequest;
use App\Http\Requests\District\DistrictGetRequest;
use App\Http\Requests\District\DistrictSaveRequest;
use App\Http\Requests\District\DistrictUpdateRequest;
use App\Http\Requests\District\DistrictBulkActionRequest;

class District extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'channel_id',
        'logo_id',
        'created_at',
    ];

    /**
     * @var string
     */
    private $_media_type = 'logos';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function channel() {
        return $this->belongsTo( 'App\Models\Channel' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function logo()
    {
        return $this->hasOne('App\Models\Media', 'id', 'logo_id');
    }

    /**
     * @param DistrictGetAllRequest $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getList(DistrictGetAllRequest $request)
    {
        $d = 'dealers';
        $di = 'districts';
        $u = 'users';
        $ud = 'users_dealers';

        $dealersCount = Dealer::select(
                "district_id",
                DB::raw("COUNT(id) as dealers_count")
            )
            ->where("channel_id", $request->channel_id)
            ->groupBy("district_id");

        $usersCount = UsersDealers::select(
                "district_id",
                DB::raw("COUNT(user_id) as users_count")
            )
            ->join($d, function($join) use ($d, $ud, $request) {
                $join->on("{$d}.id", '=', "{$ud}.dealer_id")
                    ->where("{$d}.channel_id", $request->channel_id);
            })
            ->groupBy("{$d}.district_id");

        $query = $this->query()
            ->leftJoin(
                DB::raw("({$dealersCount->toSql()}) as {$d}"),
                "{$d}.district_id", '=', "{$di}.id"
            )
            ->leftJoin(
                DB::raw("({$usersCount->toSql()}) as {$u}"),
                "{$u}.district_id", '=', "{$di}.id"
            )
            ->where("{$di}.channel_id", $request->channel_id)
            ->mergeBindings($dealersCount->getQuery())
            ->mergeBindings($usersCount->getQuery());

        return $this->_getList($query, $request, ['logo'], ['name']);
    }

    /**
     * @param \App\Http\Requests\District\DistrictGetRequest $request
     * @return mixed
     */
    public function get(DistrictGetRequest $request)
    {
        return $this->_getDistrict($request->id);
    }

    /**
     * @param DistrictSaveRequest $request
     * @param Media $media
     * @return District|District[]|bool|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     * @throws \Exception
     */
    public function add(DistrictSaveRequest $request, Media $media)
    {
        $data = $request->validated();

        if (! Channel::find($data['channel_id'])) {
            return false;
        }

        $district = $this->create($data);

        if (! empty($data['logo'])) {
            $district->logo_id = $media->saveImage($this->_media_type, $data['logo'], $district->id, $district->channel_id);
            $district->save();
        } elseif (! empty($data['media_id'])) {
            $district->logo_id = $media->copyImage($this->_media_type, $data['media_id'], $district->id, $district->channel_id);
            $district->save();
        }

        return $this->_getDistrict($district->id);
    }

    /**
     * @param \App\Http\Requests\District\DistrictUpdateRequest $request
     * @param \App\Models\Media $media
     * @return \App\Models\District|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     * @throws \Exception
     */
    public function resave(DistrictUpdateRequest $request, Media $media)
    {
        $district = $this->find($request->id);
        if ($district) {
            $district->name = $request->name;
            if (! empty($request->remove_logo) && filter_var($request->remove_logo, FILTER_VALIDATE_BOOLEAN)) {
                $media->removeMedia([$district->logo_id]);
                $district->logo_id = null;
            } elseif (! empty($request->logo)) {
                $district->logo_id = $media->updateImage($this->_media_type, $request->logo, $district->id, $district->channel_id, $district->logo_id);
            } elseif(! empty($request->media_id)) {
                $district->logo_id = $media->copyImage($this->_media_type, $request->media_id, $district->id, $district->channel_id, $district->logo_id);
            }
            $district->save();
        }

        return $this->_getDistrict($district->id);
    }

    /**
     * @param \App\Http\Requests\District\DistrictGetRequest $request
     * @param \App\Models\Media $media
     * @return \App\Models\District|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     * @throws \Exception
     */
    public function remove(DistrictGetRequest $request, Media $media)
    {
        $district = $this->_getDistrict($request->id);
        if ($district) {
            if ( $district->logo_id ) {
                $media->removeMedia([$district->logo_id]);
            }
            $this->destroy($request->id);
        }

        return $district;
    }

    /**
     * @param \App\Http\Requests\District\DistrictBulkActionRequest $request
     * @param \App\Models\Media $media
     * @return bool
     */
    public function bulkAction(DistrictBulkActionRequest $request, Media $media) {
        $districts = $this->whereIn('id', $request->id)->with('logo')->get();
        if (! $districts->all()) {
            return false;
        }

        switch($request->action) {
            case 'delete':
                $image_ids = array_filter(array_column($districts->toArray(), 'logo_id'));
                if ($image_ids) {
                    $media->removeMedia($image_ids);
                }
                DB::table('dealers')->whereIn('district_id', $request->id)->update(['district_id' => 0]);
                $this->destroy($request->id);
                break;
            default:
                return false;
        }

        return $districts;
    }

    /**
     * @param int $district_id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     */
    private function _getDistrict(int $district_id)
    {
        return $this->with('logo')->find($district_id);
    }
}
