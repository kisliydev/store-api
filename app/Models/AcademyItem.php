<?php

namespace App\Models;

use App\Events\QuizReleased;
use App\Http\Requests\BrandAcademy\BrandAcademyExportRequest;
use App\Http\Requests\BrandAcademy\BrandAcademyGetAllRequest;
use App\Http\Requests\BrandAcademy\BrandAcademyGetRequest;
use App\Http\Requests\BrandAcademy\BrandAcademyLoadRequest;
use App\Http\Requests\BrandAcademy\BrandAcademySaveRequest;
use App\Http\Requests\BrandAcademy\BrandAcademyUpdateRequest;
use App\Http\Requests\BrandAcademy\BrandAcademyBulkActionRequest;
use App\Http\Requests\BrandAcademy\BrandAcademyQuickEditRequest;
use App\Http\Requests\GeneralReport\AcademyListRequest;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Maatwebsite\Excel\Collections\RowCollection;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;
use Excel;
use Mockery\Exception;
use PHPExcel_Cell;
use App\Traits\ExcelStyling;

class AcademyItem extends Model
{
    use ExcelStyling;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'channel_id',
        'title',
        'description',
        'short_description',
        'complete_message',
        'fail_message',
        'start_button_text',
        'video_url',
        'points',
        'use_weight_points',
        'started_at',
        'finished_at',
        'thumbnail_id',
        'background_id',
        'created_at',
        'type',
        'article_title',
        'article_link',
        'status',
        'app_layout',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'started_at',
        'finished_at',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'app_layout' => 'json',
    ];

    /**
     * @var string
     */
    private $_thumb_media_type = 'brand_items';

    /**
     * @var string
     */
    private $_background_media_type = 'brand_items_background';

    /**
     * @var array
     */
    private $_excel_titles = [
        'property' => 'Property',

        'title'             => 'Quiz Name',
        'short_description' => 'Short Description',

        'description'       => 'Intro Text',

        'complete_message'  => 'Success Message',
        'fail_message'      => 'Fail Message',
        'start_button_text' => 'Start Button Text',
        'article_title'     => 'Article Name',
        'article_link'      => 'Article Link',

        'video_url'         => 'Video Link',

        'points'            => 'Points',
        'use_weight_points' => 'Use weight points (1-true/0-false)',
        'started_at'        => 'Start Date (YYYYMMDD)',
        'finished_at'       => 'End Date (YYYYMMDD)',
    ];

    const EXCEL_ANSWERS_FIELDS_COUNT = 6;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function channel()
    {
        return $this->belongsTo('App\Models\Channel');
    }

    /**
     * @return array
     */
    public function getDefaultAppLayout()
    {
        return [
            'quiz_name_color' => '#FFFFFF',
            'short_description_color' => '#C9CED6',
            'description_color' => '#C9CED6',
            'start_button_color' => '#FFFFFF',
            'start_button_background' => '#E51A3C',
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function background()
    {
        return $this->hasOne('App\Models\Media', 'id', 'background_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function thumbnail()
    {
        return $this->hasOne('App\Models\Media', 'id', 'thumbnail_id');
    }

    /**
     * @param AcademyListRequest $request
     * @param string $type
     * @return \Illuminate\Contracts\Pagination\Paginator|\Illuminate\Database\Eloquent\Builder
     */
    public function getReportList(AcademyListRequest $request, string $type)
    {
        $query = $this->where('channel_id', $request->channel_id)->where('type', '=', $type);

        return $this->addPagination(
            $query,
            $request->query(),
            $request->per_page,
            filter_var($request->simple_paginate, FILTER_VALIDATE_BOOLEAN)
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function questions()
    {
        return $this->hasMany('App\Models\Question', 'item_id', 'id')->orderBy('order');
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademyGetAllRequest $request
     * @param string $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function mobileList(BrandAcademyGetAllRequest $request, string $type = '')
    {
        $ai  = $this->getTable();
        $dai = 'done_academy_items';
        $query_builder = $this->select(
                "{$ai}.*",
                "{$dai}.id as is_done_by_user"
            )
            ->leftJoin($dai, function($join) use ($ai, $dai) {
                $join->on("{$dai}.item_id", '=', "{$ai}.id")
                    ->where("{$dai}.user_id", '=', auth()->user()->id);
            })
            ->where("{$ai}.channel_id", $request->channel_id)
            ->where("{$ai}.type", '=', $type)
            ->where("{$ai}.status", 'published');

        $request->merge(['sort' => 'is_done_by_user,-id']);
        return $this->_getList($query_builder, $request, ['thumbnail'], ['title']);
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademyGetAllRequest $request
     * @param string $type
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getList(BrandAcademyGetAllRequest $request, string $type = '')
    {
        $query = $this->where('channel_id', $request->channel_id)->where('type', '=', $type);
        return $this->_getList($query, $request, ['questions', 'thumbnail', 'background'], ['title']);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string|null $sort
     */
    public function addMultiSort(Builder $query, string $sort = null)
    {
        if (! $sort) {
            if (empty($this->default_sort)) {
                return;
            }
            $sort = $this->default_sort;
        }

        $sorts = explode(',', $sort);

        foreach ($sorts as $sortCol) {
            $sortDir = starts_with($sortCol, '-') ? 'desc' : 'asc';
            $sortCol = ltrim($sortCol, '-');

            if ('status' == $sortCol) {
                $clause = 'desc' == $sortDir
                    ? "FIELD(status,'finished', 'draft', 'published', 'scheduled')"
                    : "FIELD(status,'scheduled', 'published', 'draft', 'finished')";
                $query->orderByRaw($clause);
            } else {
                $query->orderBy($sortCol, $sortDir);
            }
        }
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademyGetRequest $request
     * @param bool $check_if_done
     * @return \App\Models\AcademyItem|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     */
    public function getItem(BrandAcademyGetRequest $request, bool $check_if_done = false)
    {
        $item = $this->_getItem($request->id, $check_if_done);

        return $item;
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademySaveRequest $request
     * @param string $type
     * @param \App\Models\Question $question
     * @param \App\Models\Media $media
     * @param \App\Models\Answer $answer
     * @return mixed
     * @throws \Exception
     */
    public function add(
        BrandAcademySaveRequest $request,
        string $type = '',
        Question $question,
        Media $media,
        Answer $answer
    ) {
        $data = $request->validated();

        $data['type'] = $type;
        $data['use_weight_points'] = $this->boolval($data['use_weight_points']);
        $data['app_layout'] = $this->_getAppLayout($data['app_layout'] ?? []);
        $need_update = false;

        $item = $this->create($data);

        if (! empty($data['thumbnail'])) {
            $item->thumbnail_id = $media->saveImage(
                $this->_thumb_media_type,
                $data['thumbnail'],
                $item->id,
                $item->channel_id
            );
            $need_update = true;
        } elseif (! empty($data['media_thumbnail_id'])) {
            $item->thumbnail_id = $media->copyImage(
                $this->_thumb_media_type,
                $data['media_thumbnail_id'],
                $item->id,
                $item->channel_id
            );
            $need_update = true;
        }

        if (! empty($data['background'])) {
            $item->background_id = $media->saveImage(
                $this->_background_media_type,
                $data['background'],
                $item->id,
                $item->channel_id
            );
            $need_update = true;
        } elseif (! empty($data['media_background_id'])) {
            $item->background_id = $media->copyImage(
                $this->_background_media_type,
                $data['media_background_id'],
                $item->id,
                $item->channel_id
            );
            $need_update = true;
        }

        if ($need_update) {
            $item->save();
        }

        if (! empty($data['questions'])) {
            $question->add($item->id, $type, $data['questions'], $media, $answer, $item->channel_id);
        }

        return $this->_getItem($item->id);
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademyUpdateRequest $request
     * @param \App\Models\Question $question
     * @param \App\Models\Media $media
     * @param \App\Models\Answer $answer
     * @return \App\Models\AcademyItem|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     * @throws \Exception
     */
    public function resave(BrandAcademyUpdateRequest $request, Question $question, Media $media, Answer $answer)
    {
        $item = $this->_getItem($request->id);
        if ($item) {
            $data = $request->validated();
            $item->title             = $data['title'];
            $item->description       = $data['description'] ?? '';
            $item->short_description = $data['short_description'] ?? '';
            $item->complete_message  = $data['complete_message'] ?? '';
            $item->fail_message      = $data['fail_message'] ?? '';
            $item->start_button_text = $data['start_button_text'];
            $item->video_url         = $data['video_url'] ?? '';
            $item->article_title     = $data['article_title'] ?? '';
            $item->article_link      = $data['article_link'] ?? '';
            $item->app_layout        = $this->_getAppLayout($data['app_layout'] ?? []);

            if (! empty($data['remove_thumbnail']) && $this->boolval($data['remove_thumbnail'])) {
                $media->removeMedia([$item->thumbnail_id]);
                $item->thumbnail_id = null;
            } elseif (! empty($data['thumbnail'])) {
                $item->thumbnail_id = $media->updateImage(
                    $this->_thumb_media_type,
                    $data['thumbnail'],
                    $item->id,
                    $item->channel_id,
                    $item->thumbnail_id
                );
            } elseif (! empty($data['media_thumbnail_id'])) {
                $item->thumbnail_id = $media->copyImage(
                    $this->_thumb_media_type,
                    $data['media_thumbnail_id'],
                    $item->id,
                    $item->channel_id,
                    $item->thumbnail_id
                );
            }

            if ($this->boolval($data['remove_background'])) {
                $media->removeMedia([$item->background_id]);
                $item->background_id = null;
            } elseif (! empty($data['background'])) {
                $item->background_id = $media->updateImage(
                    $this->_background_media_type,
                    $data['background'],
                    $item->id,
                    $item->channel_id,
                    $item->background_id
                );
            } elseif (! empty($data['media_background_id'])) {
                $item->background_id = $media->copyImage(
                    $this->_background_media_type,
                    $data['media_background_id'],
                    $item->id,
                    $item->channel_id,
                    $item->background_id
                );
            }

            $item->save();

            if (! empty($data['new_questions'])) {
                $question->add($item->id, $item->type, $data['new_questions'], $media, $answer, $item->channel_id);
            }

            if (! empty($data['questions'])) {
                $question->resave($item->type, $data['questions'], $media, $answer, $item->channel_id);
            }

            app()['cache']->forget("{$item->type}_{$item->id}");
        }

        return $this->_getItem($item->id);
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademyQuickEditRequest $request
     * @return mixed
     */
    public function quickEdit(BrandAcademyQuickEditRequest $request)
    {
        $data = $request->validated();
        $item = $this->find($data['id']);

        $willBePublished = $data['status'] != $item->status && $data['status'] == 'published';

        if ($willBePublished) {
            $item->started_at = Carbon::now()->toDateTimeString();
        } else {
            $item->started_at = empty($data['started_at']) ? null : $data['started_at'];
        }

        $item->finished_at       = empty($data['finished_at']) ? null : $data['finished_at'];
        $item->status            = $data['status'];
        $item->points            = $data['points'] ?? 0;
        $item->use_weight_points = empty($data['use_weight_points']) ? false : $this->boolval($data['use_weight_points']);

        if ($item->save() && $willBePublished) {
            event(new QuizReleased($item->toArray()));
        }

        return $this->_getItem($item->id);
    }

    /**
     * @return void
     */
    public function publish() {
        $this->status = 'published';
        if ($this->save()) {
            event(new QuizReleased($this->toArray()));
        }
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademyGetRequest $request
     * @param \App\Models\Media $media
     * @param \App\Models\Question $question
     * @param \App\Models\Answer $answer
     * @return \App\Models\AcademyItem|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     * @throws \Exception
     */
    public function remove(BrandAcademyGetRequest $request, Media $media, Question $question, Answer $answer)
    {
        $item = $this->_getItem($request->id);
        if ($item) {
            if ($item->thumbnail_id) {
                $media->removeMedia([$item->thumbnail_id]);
            }
            if ($item->background_id) {
                $media->removeMedia([$item->background_id]);
            }

            $question->bulkDelete([$request->id], $media, $answer);
            $this->destroy($request->id);
        }

        return $item;
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademyBulkActionRequest $request
     * @param \App\Models\Media $media
     * @param \App\Models\Question $question
     * @param \App\Models\Answer $answer
     * @return mixed
     */
    public function bulkAction(BrandAcademyBulkActionRequest $request, Media $media, Question $question, Answer $answer) {
        $items = $this->whereIn('id', $request->id)->with('questions.answers.image')->get();

        if (! $items->all()) {
            return false;
        }

        switch($request->action) {
            case 'delete':
                $image_ids = [];
                $array_items = $items->toArray();
                foreach(['thumbnail_id', 'background_id'] as $column ) {
                    $image_ids = array_merge($image_ids, array_filter(array_column($array_items, $column)));
                }

                if ($image_ids) {
                    $media->removeMedia($image_ids);
                }
                $question->bulkDelete($request->id, $media, $answer);
                $this->destroy($request->id);
                break;
            default:
                return false;
        }

        return $items;
    }

    /**
     * @param \App\Models\Channel $channel
     */
    public function copyToChannel(Channel $channel)
    {
       $newItem = $this->replicate();
       $newItem->channel_id = $channel->id;
       $newItem->save();

       $questions = $this->questions()->get();
       if($questions) {
           $questions->each(function($question) use ($newItem) {
               $question->copyToItem($newItem);
           });
       }
    }

    /**
     * @param BrandAcademyLoadRequest $request
     * @param string $type
     * @return bool
     * @throws \Exception
     */
    public function loadFromExcel(BrandAcademyLoadRequest $request, string $type = '')
    {
        /*
         * amount of rows that need to be skipped
         */
        Config::set('excel.import.startRow', 3);
        $raw = [];

        Excel::load($request->items->getRealPath(), function ($reader) use (&$raw, $request, $type) {
            $reader->limitRows(config('services.excel.rows_limit'));
            $reader->ignoreEmpty();
            /* if the document has only one sheet */
            if ($reader->all() instanceof RowCollection) {
                $data = $this->_parseExcelRows($reader, $request, $type);

                if ($data) {
                    $raw[$reader->getTitle()] = $data;
                }
            } else {
                $reader->each(function ($sheet) use (&$raw, $request, $type) {
                    $data = $this->_parseExcelRows($sheet, $request, $type);
                    if ($data) {
                        $raw[$sheet->getTitle()] = $data;
                    }
                });
            }
        });

        if (! $raw) {
            return false;
        }

        $invalidSheets = $this->_getInvalidSheetTitles($raw);

        if ($invalidSheets) {
            throw new \Exception('Please correct data from following tabs: ' . implode(', ', $invalidSheets));
        }

        $items = [];
        $requiredFields = [
            'title',
            'short_description',
            'description',
            'complete_message',
            'fail_message'
        ];
        $requiredAmount = count($requiredFields);

        foreach ($raw as $i_data) {

            if (empty($i_data['data']) || $requiredAmount !== count(array_intersect($requiredFields, array_keys($i_data['data'])))) {
                continue;
            }

            $item = $this->create($i_data['data']);
            $items[] = $item->id;

            if (empty($i_data['questions'])) {
                continue;
            }

            foreach ($i_data['questions'] as $q_data) {

                $q_data['data']['item_id'] = $item->id;

                if (empty($q_data['data']['text'])) {
                    continue;
                }

                $question = Question::create($q_data['data']);

                if (empty($q_data['answers'])) {
                    continue;
                }

                foreach ($q_data['answers'] as $a_data) {
                    if (empty($a_data['text'])) {
                        continue;
                    }

                    $a_data['question_id'] = $question->id;
                    Answer::create($a_data);
                }
            }
        }

        if (! $items) {
            throw new Exception('Wrong document structure');
        }

        Config::set('excel.import.startRow', 1);
        return $this->whereIn('id', $items)->with('questions.answers.image')->get();
    }

    /**
     * @param array $items
     * @return array
     */
    private function _getInvalidSheetTitles(array $items)
    {
        $invalidSheets = [];

        function tooLong(string $str = null) {
            return strlen((string)$str) > 255;
        }

        foreach($items as $sheetTitle => $item) {

            if (empty($item['questions'])) {
                continue;
            }

            $invalidQuestion = !!array_filter($item['questions'], function($question) {
                $invalidAnswers = false;

                if (!empty($question['answers']) && is_array($question['answers'])) {
                    $invalidAnswers = !!array_filter($question['answers'], function($answer) {
                        return tooLong($answer['text']);
                    });
                }

                return tooLong($question['data']['text']) || $invalidAnswers;
            });

            if ($invalidQuestion) {
                $invalidSheets[] = $sheetTitle;
            }
        }

        return $invalidSheets;
    }

    /**
     * @param \App\Http\Requests\BrandAcademy\BrandAcademyExportRequest $request
     * @param string $type
     * @return bool|\Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public function exportToExcel(BrandAcademyExportRequest $request, string $type = '')
    {
        $items = $this->where('channel_id', $request->channel_id)->where('type', '=', $type)->with('questions.answers')->get();

        if (! $items->count()) {
            return false;
        }

        switch ($type) {
            case 'quiz':
                return Excel::create('quizzes', function ($excel) use ($items) {
                    $i = 0;
                    foreach ($items as $quiz) {
                        $i++;
                        $styles = [
                            'rgb-color' => '7F7F7F',
                            'font-size' => 11,
                            'text' => [
                                'background' => '#d8d8d8',
                                'rgb-color'  => '000000',
                                'color'      => '#000000',
                            ],
                            'border-color' => '#a5a5a5',
                            'notes' => [
                                'background' => '#fef2cb',
                                'color'      => '#ff0000',
                            ],
                            'header' => [
                                'font-size' => 16
                            ],
                        ];

                        $excel->sheet("Quiz {$i}", function ($sheet) use ($quiz, $i, $styles) {
                            $sheet->setStyle([
                                'font' => [
                                    'size'   => $styles['font-size'],
                                    'color'  => ['rgb' => $styles['rgb-color']]
                                ],
                            ]);

                            $sheet->setWidth([
                                'A' => 20,
                                'B' => 2,
                                'C' => 50,
                            ]);


                            $sheet->row(1, [ 'STORE', '', "Quiz {$i}"]);
                            $sheet->row(1, function ($row) use ($styles) {
                                $row->setFontSize($styles['header']['font-size']);
                                $row->setFontWeight('bold');
                            });

                            $sheet->row(3, [$this->_excel_titles['property'], '', 'Value']);
                            $sheet->row(3, function ($row) use ($styles) {
                                $row->setFontSize($styles['header']['font-size']);
                                $row->setFontColor($styles['text']['color']);
                            });

                            $sheet->row(4, [$this->_excel_titles['title'], '', $quiz->title, '=LEN(C4) & "/100"']);

                            $sheet->row(6, [$this->_excel_titles['short_description'], '', $quiz->short_description, '=LEN(C6) & "/100"']);

                            $sheet->row(8, [$this->_excel_titles['description'], '', $quiz->description, '=LEN(C8) & "/500"']);

                            $sheet->row(10, [$this->_excel_titles['complete_message'], '', $quiz->complete_message, '=LEN(C10) & "/100"']);
                            $sheet->row(11, [$this->_excel_titles['fail_message'], '', $quiz->fail_message, '=LEN(C11) & "/100"']);
                            $sheet->row(12, [$this->_excel_titles['start_button_text'], '', $quiz->start_button_text, '=LEN(C12) & "/20"']);
                            $sheet->row(13, [$this->_excel_titles['article_title'], '', $quiz->article_title, '=LEN(C13) & "/100"']);
                            $sheet->row(14, [$this->_excel_titles['article_link'], '', $quiz->article_link, '=LEN(C14) & "/100"']);

                            $sheet->row(16, [$this->_excel_titles['video_url'], '', $quiz->video_url, '=LEN(C16) & "/100"']);

                            foreach([ 'C4','C6', 'C8', 'C10:C14', 'C16', ] as $range) {
                                $sheet->cells($range, function ($cells) use ($styles) {
                                    $cells->setBackground($styles['text']['background']);
                                    $cells->setFontColor($styles['text']['color']);
                                });

//                                $sheet->setBorder($range, 'thin');
                            }

                            /**
                             * unnecessary
                             * @todo Remove after some while in case if these rows won't be needed
                             */
//                            $sheet->row(16, [$this->_excel_titles['points'], $quiz->points]);
//                            $sheet->row(17, [$this->_excel_titles['use_weight_points'], $quiz->use_weight_points]);
//                            $sheet->row(18, [$this->_excel_titles['started_at'], $quiz->started_at]);
//                            $sheet->row(19, [$this->_excel_titles['finished_at'], $quiz->finished_at]);

//                            $sheet->row(18, ['', '', 'Mark correct answer with * symbol (e.g.: *correct answer)']);
//                            $sheet->row(18, function ($row) {
//                                $row->setFontColor('#ff0000');
//                            });

//                            if (! $quiz->questions) {
//                                return;
//                            }
                            $countDiff = 10 - $quiz->questions->count();

                            if ($countDiff > 0) {
                                $blankQuestion = (object)[
                                    'text' => '',
                                    'answers' => [
                                        (object)['text' => '', 'correct' => false],
                                        (object)['text' => '', 'correct' => false],
                                        (object)['text' => '', 'correct' => false],
                                        (object)['text' => '', 'correct' => false],
                                        (object)['text' => '', 'correct' => false],
                                        (object)['text' => '', 'correct' => false],
                                    ]
                                ];
                                for($iter = 0; $iter < $countDiff; $iter ++) {
                                    $quiz->questions->push($blankQuestion);
                                }
                            }

                            $j = 17;
                            $n = $m = 0;
                            foreach ($quiz->questions as $question) {
                                $n++;
                                $j++;
                                $sheet->row($j, ["Question {$n}", '', $question->text]);
                                $sheet->cell("A{$j}", function ($cell) {
                                    $cell->setFontWeight('bold');
                                });

                                $sheet->cell("c{$j}", function ($cell) use ($styles) {
                                    $cell->setBackground($styles['text']['background']);
                                    $cell->setFontColor($styles['text']['color']);
                                });

                                if (! $question->answers) {
                                    continue;
                                }

                                $answers = $question->answers;
                                $answersCount = count($answers);
                                for($ai = 0; $ai < self::EXCEL_ANSWERS_FIELDS_COUNT; $ai++) {
                                    $m++;
                                    $j++;
                                    $text = null;
                                    if ($ai < $answersCount) {
                                        $text = ($answers[$ai]->correct ? '*' : '').$answers[$ai]->text;
                                    }
                                    $sheet->row($j, ["answer {$m}", '', $text ?? '']);
                                    $sheet->cell("c{$j}", function ($cell) use ($styles) {
                                        $cell->setFontColor($styles['text']['color']);
                                    });
                                }

                                $m = 0;
                            }


                            $sheet->cell("A1:A{$j}", function ($cell) {
                                $cell->setAlignment('right');
                            });

                            $sheet->getStyle("C1:C{$j}")->getAlignment()->setWrapText(true);
//                            $sheet->setBorder("C17:C{$j}", 'thin');

                        });

                        $excel->setActiveSheetIndex(0);
                    }
                })->string('xlsx');
            default:
                return false;
        }
    }

    /**
     * @param int $id
     * @param bool $countRequired
     * @return int
     */
    public function getQuestionsCount(int $id, bool $countRequired = false): int
    {
        return $this->find($id)->questions()->when($countRequired, function ($q) {
            $q->where('data->required', true);
        })->count();
    }

    /**
     * @param int $channel_id
     * @param string $type
     * @return array
     */
    public function getItemsIdByChannel(int $channel_id, string $type): array
    {
        return $this->where(['channel_id' => $channel_id, 'type' => $type])->get()->pluck('id')->values()->all();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function getQuestionsList(int $id)
    {
        return $this->where('id', $id)->with(['questions.userAnswers.user', 'questions.answers'], function ($q) {
            $q->groupBy('email');
        })->get();
    }

    /**
     * @param object $item
     * @param \App\Http\Requests\BrandAcademy\BrandAcademyLoadRequest $request
     * @param string $type
     * @return array
     */
    private function _parseExcelRows($item, BrandAcademyLoadRequest $request, string $type)
    {
        $data = [
            'data' => [
                'channel_id' => $request->channel_id,
                'type' => $type,
            ],
            'questions' => [],
        ];
        $question_id = false;

        $item->each(function ($row) use (&$data, &$question_id) {
            $row = $row->all();

            if (empty($row['property'])) {
                return;
            }

            $property = array_search($row['property'], $this->_excel_titles);

            if (false !== $property) {
                $data['data'][$property] = $row['value'];
            } elseif (preg_match("/question[\s]+([\d]+)/i", $row['property'], $matches)) {
                $question_id = $matches[1];
                $data['questions'][$question_id] = [
                    'data' => [
                        'text' => $row['value'],
                        'type' => 'single_choice',
                    ],
                    'answers' => [],
                ];
            } elseif (preg_match("/answer[\s]+[\d]+/i", $row['property']) && $question_id) {
                $data['questions'][$question_id]['answers'][] = [
                    'text' => preg_replace("/^([\*]{1})?(.*?)$/", "$2", $row['value']),
                    'correct' => preg_match("/^[\*]{1}[\s\S]*/", $row['value']),
                ];
            }
        });

        return array_filter($data);
    }

    /**
     * @param $data
     * @return mixed
     */
    private function _getAppLayout(array $data = [])
    {
        $defaults = $this->getDefaultAppLayout();

        /* remove all data that shouldn't be saved */
        $valid = array_intersect_key($data, $defaults);

        /* add default values in case if some of fields are missed in the incoming request */
        return array_map( function(string $color) {
            return strtolower($color);
        }, array_merge($defaults, $valid));
    }

    /**
     * @param int $item_id
     * @param bool $check_if_done
     * @return \App\Models\AcademyItem|\App\Models\AcademyItem[]|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    private function _getItem(int $item_id, bool $check_if_done = false)
    {
        if ($check_if_done) {
            $ai  = $this->getTable();
            $dai = 'done_academy_items';
            $query = $this->select(
                    "{$ai}.*",
                    "{$dai}.id as is_done_by_user"
                )
                ->leftJoin($dai, function($join) use ($ai, $dai) {
                    $join->on("{$dai}.item_id", '=', "{$ai}.id")
                        ->where("{$dai}.user_id", '=', auth()->user()->id);
                });
        } else {
            $query = $this;
        }
        return $query->with(['questions.answers.image', 'background', 'thumbnail'])->find($item_id);
    }
}
