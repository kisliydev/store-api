<?php

namespace App\Models;

use App\Http\Requests\Media\ChannelItemsRequest;
use App\Http\Requests\Media\StoreRequest;
use App\Http\Requests\Media\MediaUpdateRequest;
use App\Http\Requests\Media\BulkActionRequest;
use App\Http\Requests\Media\ChannelItemsListRequest;
use App\Http\Requests\Media\GetRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Jobs\MediaDestroy;
use Illuminate\Http\UploadedFile;
use GuzzleHttp\Client;
use Image;

/**
 * Class Media
 *
 * @package App\Models
 */
class Media extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'path',
        'type',
        'file_type',
        'channel_id',
        'thumbnails',
        'capture',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'thumbnails' => 'json',
    ];

    /**
     * @var array
     */
    private $_config = [];

    /**
     * @var string
     */
    private $_media_type = 'uploads';

    /**
     * @param string|null $path
     * @return string
     */
    public function getPathAttribute(string $path = null)
    {
        return $path && ('testing' != env('APP_ENV') || config('media.force_media_url') ) ? $this->getMediaUrl($path) : $path;
    }

    /**
     * @param null $values
     * @return null
     */
    public function getThumbnailsAttribute($values = null)
    {
        if (! $values || 'testing' == env('APP_ENV')) {
            return $values;
        }

        try {
            $urls = [];
            foreach(json_decode($values) as $key => $path) {
                $urls[$key] = $this->getMediaUrl($path);
            }
            return $urls;
        } catch( \Exception $exception) {
            return $values;
        }
    }

    /**
     * @param \App\Http\Requests\Media\ChannelItemsListRequest $request
     * @return array
     */
    public function getChannelMediaFolders(ChannelItemsListRequest $request)
    {
        $counts = $this->select(
                DB::raw("count(CASE WHEN type='attachments' THEN 1 ELSE NULL END) as attachments_count"),
                DB::raw("count(CASE WHEN type='app_layout' THEN 1 ELSE NULL END) as app_layout_count"),
                DB::raw("count(CASE WHEN type='brand_items' THEN 1 ELSE NULL END) as brand_items_count"),
                DB::raw("count(CASE WHEN type='logos' THEN 1 ELSE NULL END) as logos_count"),
                DB::raw("count(CASE WHEN type='uploads' THEN 1 ELSE NULL END) as uploads_count"),
                DB::raw("count(CASE WHEN type='products' THEN 1 ELSE NULL END) as products_count")
            )
            ->where('channel_id', $request->channel_id)
            ->get()->first()->toArray();

        return [
            [
                'verb' => 'channel',
                'url' => "type=app_layout&channel_id={$request->channel_id}",
                'name' => 'app layout',
                'type' => 'folder',
                'items_count' => $counts['app_layout_count'],
                'next' => false,
                'previous' => 'channels',
            ],
            [
                'verb' => 'channel',
                'url' => "type=brand_items&channel_id={$request->channel_id}",
                'name' => 'brand academy items',
                'type' => 'folder',
                'items_count' => $counts['brand_items_count'],
                'next' => false,
                'previous' => 'channels',
            ],
            [
                'verb' => 'channel',
                'url' => "type=logos&channel_id={$request->channel_id}",
                'name' => 'logos',
                'type' => 'folder',
                'items_count' => $counts['logos_count'],
                'next' => false,
                'previous' => 'channels',
            ],
            [
                'verb' => 'channel',
                'url' => "type=attachments&channel_id={$request->channel_id}",
                'name' => 'messages attachments',
                'type' => 'folder',
                'items_count' => $counts['attachments_count'],
                'next' => false,
                'previous' => 'channels',
            ],
            [
                'verb' => 'channel',
                'url' => "type=products&channel_id={$request->channel_id}",
                'name' => 'products',
                'type' => 'folder',
                'items_count' => $counts['products_count'],
                'next' => false,
                'previous' => 'channels',
            ],
            [
                'verb' => 'channel',
                'url' => "type=uploads&channel_id={$request->channel_id}",
                'name' => 'uploads',
                'type' => 'folder',
                'items_count' => $counts['uploads_count'],
                'next' => false,
                'previous' => 'channels',
            ],
        ];
    }

    /**
     * @param \App\Http\Requests\Media\ChannelItemsRequest $request
     * @param bool $isChannel
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getList(ChannelItemsRequest $request, bool $isChannel = true)
    {
        $query = $this->where('type', $request->type);

        if ($isChannel) {
            $query->where('channel_id', $request->channel_id);
        }

        return $this->addPagination(
            $query,
            $request->query(),
            $request->per_page,
            filter_var($request->simple_paginate, FILTER_VALIDATE_BOOLEAN)
        );
    }

    /**
     * @param \App\Http\Requests\Media\StoreRequest $request
     * @return array
     */
    public function storeUploads(StoreRequest $request)
    {
        $ids = [];
        $postfix = time();
        $this->_setConfig($this->_media_type);
        foreach ($request->images as $id => $image) {
            $ids[] = $this->_saveImage($image, "{$request->channel_id}_{$id}_{$postfix}", $request->channel_id, true);
        }

        return $ids;
    }

    /**
     * @param \App\Http\Requests\Media\MediaUpdateRequest $request
     * @return bool
     * @throws \Exception
     */
    public function resave(MediaUpdateRequest $request)
    {
        $this->_setConfig($this->_media_type);
        return $this->_updateImage($request->image, $request->id, true);
    }

    /**
     * @param \App\Http\Requests\Media\BulkActionRequest $request
     * @return mixed
     */
    public function bulkAction(BulkActionRequest $request)
    {
        $media = $this->whereIn('id', $request->id)->get();

        if (! $media->all()) {
            return false;
        }

        switch($request->action) {
            case 'delete':
                $this->removeMedia(array_column($media->toArray(), 'id'));
                break;
            default:
                return false;
        }

        return $media;
    }

    /**
     * @param string $type
     * @param \Illuminate\Http\UploadedFile $file
     * @param string $filename
     * @param int|null $channelId
     * @return bool
     */
    public function saveImage(string $type, UploadedFile $file, string $filename, int $channelId = null)
    {
        $this->_setConfig($type);
        return $this->_saveImage($file, $filename, $channelId);
    }

    /**
     * @param string $type
     * @param string $url
     * @param string $filename
     * @param int|null $channelId
     * @return bool
     */
    public function saveImageFromUrl(string $type, string $url, string $filename, int $channelId = null)
    {
        $data = $this->_getFromUrl($url);

        if (! $data) {
            return false;
        }
        $this->_setConfig($type);
        $id = $this->_saveImage($data['file'], $filename, $channelId);
        unlink($data['temp_file']);

        return $id;
    }

    /**
     * @param string $url
     * @return mixed
     */
    public function getContent(string $url)
    {
        return $this->_getFromUrl($url);
    }

    /**
     * @param string $type
     * @param \Illuminate\Http\UploadedFile $file
     * @param string $filename
     * @param int|null $channelId
     * @param int|null $old_id
     * @return bool
     * @throws \Exception
     */
    public function updateImage(string $type, UploadedFile $file, string $filename, int $channelId = null, int $old_id = null)
    {

        $this->_setConfig($type);
        if (empty($old_id)) {
            return $this->_saveImage($file, $filename, $channelId);
        }

        return $this->_updateImage($file, $old_id);
    }

    /**
     * @param string $type
     * @param int $origin_id
     * @param string $filename
     * @param int|null $channelId
     * @param int|null $old_id
     * @return bool
     * @throws \Exception
     */
    public function copyImage(string $type, int $origin_id, string $filename, int $channelId = null, int $old_id = null)
    {
        $origin = $this->find($origin_id);

        $fileExists = 'testing' == env('APP_ENV') ? file_exists($origin->getOriginal('path')) : Storage::exists($origin->getOriginal('path'));

        if (!$fileExists) {
            return false;
        }

        $content = 'testing' == env('APP_ENV') ? file_get_contents($origin->getOriginal('path')) : Storage::get($origin->getOriginal('path'));
        $fileInfo = pathinfo($origin->getOriginal('path'));
        $data = $this->_getTempFile($content, $fileInfo['filename'], $fileInfo['extension'] ?? null);

        if (empty($data['file'])) {
            return false;
        }

        $id = $this->updateImage($type, $data['file'], $filename, $channelId, $old_id);
        unlink($data['temp_file']);

        return $id;
    }

    /**
     * @param string $path
     * @return string
     */
    public function getMediaUrl(string $path)
    {
        return Storage::url($path);
    }


    /**
     * @param string $type
     * @param \Illuminate\Http\UploadedFile $file
     * @param string $filename
     * @param int $channelId
     * @return bool
     */
    public function saveAttachment(string $type, UploadedFile $file, string $filename, int $channelId)
    {
        if ($this->isFile($file, 'image')) {
            return $this->saveImage($type, $file, $filename, $channelId);
        }

        $this->_setConfig($type);
        return $this->_saveFile($file, $filename, $channelId);
    }

    /**
     * @param string $type
     * @param \Illuminate\Http\UploadedFile $file
     * @param string $filename
     * @param int $channelId
     * @param int|null $old_id
     * @return bool
     * @throws \Exception
     */
    public function updateAttachment(string $type, UploadedFile $file, string $filename, int $channelId, int $old_id = null)
    {
        if (empty($old_id)) {
            return $this->saveAttachment($type, $file, $filename, $channelId);
        }

        $this->_setConfig($type);

        if ($this->isFile($file, 'image')) {
            return $this->_updateImage($file, $old_id);
        }

        return $this->_updateFlie($file, $old_id);
    }

    /**
     * @param array $ids
     * @return int
     */
    public function removeMedia(array $ids)
    {
        $ids = array_filter($ids);

        if (!$ids) {
            return 0;
        }

        $removed = 0;
        $this->whereIn('id', $ids)->get()->each(function($item) use (&$removed) {
            $this->destroy($item->id);
            $this->_removeFromStorage($item);
            $removed++;
        });
        return $removed;
    }

    /**
     * @param array $channelIDs
     */
    public function removeChannelMedia(array $channelIDs)
    {
        if (! $channelIDs) {
            return;
        }

        $this->whereIn('channel_id', $channelIDs)->get()->each(function($item) {
            $this->destroy($item->id);
            $this->_removeFromStorage($item);
        });
    }

    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @param string $type
     * @return bool
     */
    public function isFile(UploadedFile $file, $type = 'image')
    {
        $type = in_array($type, array_keys(config('media.mimes'))) ? $type : 'image';
        return in_array($file->getClientOriginalExtension(), config("media.mimes.{$type}"));
    }

    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @return bool
     */
    public function getFileType(UploadedFile $file)
    {
        foreach(array_keys(config('media.mimes')) as $type) {
            if ($this->isFile($file, $type)) {
                return $type;
            }
        }

        return false;
    }

    /**
     * @param string $url
     * @return array|bool
     */
    private function _getFromUrl(string $url)
    {
        try {
            $client = new Client();
            $res = $client->request('GET', $url);
            $fileInfo = pathinfo($url);
            return $this->_getTempFile($res->getBody(), $fileInfo['filename'], $fileInfo['extension'] ?? null);
        } catch(\GuzzleHttp\Exception\GuzzleException $exception) {
            return false;
        }
    }

    /**
     * @param string $content
     * @param string $filename
     * @param string $extension
     * @return array|bool
     */
    private function _getTempFile(string $content, string $filename, string $extension = null) {
        $tempFile = tempnam(sys_get_temp_dir(), 'image_');
        file_put_contents($tempFile, $content);

        $isImage = empty($extension) || in_array($extension, config('media.mimes.image'));

        if ($isImage && !exif_imagetype($tempFile)) {
            unlink($tempFile);
            return false;
        }

        $image = "{$tempFile}.{$extension}";
        rename($tempFile, $image);

        return [
            'file' => new UploadedFile($image, $filename, mime_content_type($image), filesize($image)),
            'temp_file' => $image,
        ];
    }

    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @param string $filename
     * @param int|null $channelId
     * @param bool $return_model
     * @return bool
     */
    private function _saveImage(
        UploadedFile $file,
        string $filename,
        int $channelId = null,
        bool $return_model = false
    ) {
        $imageName = "{$this->_config['prefix']}{$filename}.{$file->getClientOriginalExtension()}";

        $path = $file->storeAs($this->_config['folder'], $imageName);

        if (! $path) {
            return false;
        }

        $data = [
            'path'    => $path,
            'type'    => $this->_config['type'],
            'capture' => $file->getClientOriginalName(),
        ];

        if ($channelId) {
            $data['channel_id'] = $channelId;
        }

        $data['thumbnails'] = $this->_saveImageThumbnails($file, $filename);
        $image = $this->create($data);

        return $return_model ? $image : $image->id;
    }

    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @param string $filename
     * @param int $channelId
     * @return bool
     */
    private function _saveFile(
        UploadedFile $file,
        string $filename,
        int $channelId
    ) {
        $attachmentName = "{$this->_config['prefix']}{$filename}.{$file->getClientOriginalExtension()}";

        $path = $file->storeAs($this->_config['folder'], $attachmentName);

        if (! $path) {
            return false;
        }

        $attachment = $this->create([
            'path'       => $path,
            'type'       => $this->_config['type'],
            'file_type'  => $this->getFileType($file),
            'channel_id' => $channelId,
            'capture'    => $file->getClientOriginalName(),
        ]);

        return $attachment->id;
    }

    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @param int $file_id
     * @return bool
     * @throws \Exception
     */
    private function _updateFlie(UploadedFile $file, int $file_id)
    {
        $mediaFile = $this->find($file_id);
        if (! $mediaFile) {
            return false;
        }

        $folder = dirname($mediaFile->getOriginal('path'));
        $filename = pathinfo(basename($mediaFile->path), PATHINFO_FILENAME);
        $path = $file->storeAs($folder, $filename.'.'.$file->getClientOriginalExtension());

        if (! $path) {
            return false;
        }

        /* if file with another extension was loaded */
        if ($path !== $mediaFile->path) {
            $this->_removeFromStorage($mediaFile);
        }

        $mediaFile->path    = $path;
        $mediaFile->capture = $file->getClientOriginalName();
        $mediaFile->save();

        return $mediaFile->id;
    }

    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @param int $image_id
     * @param bool $return_model
     * @return bool
     * @throws \Exception
     */
    private function _updateImage(
        UploadedFile $file,
        int $image_id,
        bool $return_model = false
    ) {
        $image = $this->find($image_id);
        if (! $image) {
            return false;
        }

        $folder = dirname($image->getOriginal('path'));
        $filename = $this->_getUpdatedImageName(pathinfo(basename($image->path), PATHINFO_FILENAME));

        $path = $file->storeAs($folder, $filename.'.'.$file->getClientOriginalExtension());

        if (! $path) {
            return false;
        }

        $this->_removeFromStorage($image);

        $image->thumbnails = $this->_saveImageThumbnails($file, str_replace_first($this->_config['prefix'], '', $filename));
        $image->path       = $path;
        $image->capture    = $file->getClientOriginalName();
        $image->save();

        return $return_model ? $image : $image->id;
    }

    /**
     * @param string $raw
     * @return null|string|string[]
     */
    private function _getUpdatedImageName(string $raw)
    {
        return preg_replace_callback(
            "/^(?P<name>[a-zA_Z-_]+?)(?P<copy>_[\d]+)(?P<objectID>_[\d]+)?$/",
            function($matches) {
                $name = $matches['name'];
                $copy = intval(trim($matches['copy'], '_'));
                $objID = empty($matches['objectID']) ? '' : intval(trim($matches['objectID'], '_'));

                if (empty($objID)) {
                    return "{$name}_1_{$copy}";
                }
                $copy++;
                return "{$name}_{$copy}_{$objID}";

            },
            $raw
        );
    }

    /**
     * @param \Illuminate\Http\UploadedFile $data
     * @param string $filename
     * @return array|bool
     */
    private function _saveImageThumbnails(UploadedFile $data, string $filename)
    {
        if (! $this->_config['thumbnails']) {
            return false;
        }

        $thumbnails = [];
        $folder     = $this->_config['folder'];
        $filename   = "{$this->_config['prefix']}{$filename}";
        $extension  = $data->getClientOriginalExtension();

        foreach($this->_config['thumbnails'] as $key => $dimension) {
            $thumb = Image::make($data);

            $thumb->fit($dimension['width'], $dimension['height'], function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->encode($extension);

            $path = "{$folder}/{$filename}_{$dimension['width']}x{$dimension['height']}.{$extension}";
            $thumbnails[$key] = $path;
            Storage::put($path, (string)$thumb);
        }

        return $thumbnails;
    }

    /**
     * @param \App\Models\Media $item
     * @throws \Exception
     */
    private function _removeFromStorage(Media $item)
    {
        dispatch((new MediaDestroy([
            'path' => $item->getOriginal('path'),
            'thumbnails' =>  $item->getOriginal('thumbnails')])
        )->onQueue(config('queue.tube.default')));
    }

    /**
     * @param string $type
     */
    private function _setConfig(string $type)
    {

       $this->_config = config("media.{$type}");

       if (empty($this->_config['type'])) {
           $this->_config['type'] = $type;
       }
    }
}
