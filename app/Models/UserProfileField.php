<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\User\UserSaveAdditionalDataRequest;


class UserProfileField extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'field_id',
        'value',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'user_id',
    ];

    /**
     * @param \App\Http\Requests\User\UserSaveAdditionalDataRequest $request
     * @return array
     */
    public function saveUserData(UserSaveAdditionalDataRequest $request)
    {
        $data = $request->validated();
        $fields = [];

        $in_channel = ProfileField::whereIn('id', array_column($data['additional_data'], 'field_id'))->where('channel_id', $data['channel_id'])->get()->pluck('id')->toArray();

        foreach($data['additional_data'] as $field) {
            if (in_array($field['field_id'], $in_channel)) {
                $fields[] = $this->updateOrCreate(
                    [
                        'user_id' => $data['id'],
                        'field_id' => $field['field_id'],
                    ],
                    [
                        'value' => $field['value'],
                    ]
                );
            }
        }

        return $fields;
    }
}



