<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as EModel;
use Illuminate\Database\Eloquent\Builder;
use App\Http\Requests\Request;
use \Illuminate\Support\Collection;

class Model extends EModel
{
    /**
     * @var int
     */
    public $per_page = 20;

    /**
     * @var bool
     */
    protected $with_appends = true;

    /**
     * @param $value
     * @return mixed
     */
    public function boolval(&$value)
    {
        return filter_var($value, FILTER_VALIDATE_BOOLEAN);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string|null $sort
     */
    public function addMultiSort(Builder $query, string $sort = null)
    {
        if (! $sort) {
            if (empty($this->default_sort)) {
                return;
            }
            $sort = $this->default_sort;
        }

        $sorts = explode(',', $sort);

        foreach ($sorts as $sortCol) {
            $sortDir = starts_with($sortCol, '-') ? 'desc' : 'asc';
            $sortCol = ltrim($sortCol, '-');

            $query->orderBy($sortCol, $sortDir);
        }
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string|null $filters
     */
    public function addMultiFilters(Builder $query, string $filters = null)
    {
        if (! $filters) {
            return;
        }

        $filters = explode(',', $filters);

        foreach ($filters as $filter) {
            list($criteria, $value) = explode(':', $filter);
            $query->where($criteria, $value);
        }
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string|null $search
     * @param array $search_in_columns
     */
    public function addSearch(Builder $query, string $search = null, array $search_in_columns = [])
    {
        if (! empty($search) && $search_in_columns) {
            $query->where(function($subQuery) use ($search_in_columns, $search) {
                $i = 0;
                foreach ($search_in_columns as $column) {
                    $where = $i ? 'orWhere' : 'where';
                    $subQuery = $subQuery->$where($column, 'like', "%{$search}%");
                    $i++;
                }
            });
        }
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param array $params
     * @param int|null $per_page
     * @param bool $use_simple_pagination
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function addPagination(
        Builder $query,
        array $params,
        int $per_page = null,
        bool $use_simple_pagination = true
    ) {
        $paginate = $use_simple_pagination ? 'simplePaginate' : 'paginate';
        $query = $query->$paginate($per_page ?? $this->per_page);

        foreach ($params as $paramKey => $paramValue) {
            $query->appends($paramKey, $paramValue);
        }

        return $query;
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param array $relation
     * @param array $search_in_columns
     * @param bool $use_simple_pagination
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function _getListByChannel(Request $request, array $relation = [], array $search_in_columns = [], bool $use_simple_pagination = false)
    {
        return $this->_getList($this->where('channel_id', $request->channel_id), $request, $relation, $search_in_columns, $use_simple_pagination);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param \App\Http\Requests\Request $request
     * @param array $relation
     * @param array $search_in_columns
     * @param bool $use_simple_pagination
     * @return \Illuminate\Database\Eloquent\Builder
     */
    protected function _getList(Builder $query, Request $request, array $relation = [], array $search_in_columns = [], bool $use_simple_pagination = false)
    {
        $this->addMultiFilters($query, $request->filter);
        $this->addMultiSort($query, $request->sort);
        $this->addSearch($query, $request->search, $search_in_columns);

        if ($relation) {
            $query = $query->with($relation);
        }

        if (! empty($request->trashed) && filter_var($request->trashed, FILTER_VALIDATE_BOOLEAN)) {
            $query = $query->onlyTrashed();
        }

        return $this->addPagination($query, $request->query(), $request->per_page, $use_simple_pagination);
    }

    /**
     * @param \Illuminate\Support\Collection $collection
     * @param array $except
     * @return \Illuminate\Support\Collection
     */
    public function removeAppends(Collection $collection, array $except = []): Collection
    {
        return $collection->each(function ($query) use ($except) {
            $query->setAppends($except);
        });
    }

    /**
     * @param bool $flag
     */
    public function withAppends(bool $flag)
    {
        $this->with_appends = $flag;
    }
}
