<?php

namespace App\Models;

use App\Http\Requests\UserProfileField\FieldsSaveRequest;

class ProfileField extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'channel_id',
        'type',
        'label',
        'options',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'options' => 'json',
    ];

    /**
     * @param \App\Http\Requests\UserProfileField\FieldsSaveRequest $request
     * @return array
     */
    public function resave(FieldsSaveRequest $request)
    {
        $data = $request->validated();
        $fields = [];

        foreach($data['fields'] as $item) {
            if (empty($item['id'])) {
                $item['channel_id'] = $data['channel_id'];
                $field = $this->create($item);
            } elseif ($this->boolval($item['remove'])) {
                $this->destroy($item['id']);
                continue;
            } else {
                $field = $this->find($item['id']);

                if (! $field) {
                    continue;
                }

                $field->type = $item['type'];
                $field->label = $item['type'];
                $field->options = $item['options'];
                $field->save();
            }

            $fields[] = $field;
        }

        return $fields;
    }
}
