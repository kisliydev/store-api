<?php

namespace App\Models;

use App\Http\Requests\Localization\LocalizationSaveRequest;
use App\Http\Requests\Localization\LocalizationUpdateRequest;
use App\Http\Requests\Localization\LocalizationAddToChannelRequest;
use App\Http\Requests\Localization\LocalizationGetAllRequest;
use App\Http\Requests\Localization\LocalizationGetRequest;
use Excel;

class Localization extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'translations',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'translations' => 'json',
    ];

    const DEFAULT_LOCALIZATION_NAME = 'English';

    /**
     * @param \App\Http\Requests\Localization\LocalizationGetAllRequest $request
     * @return \Illuminate\Contracts\Pagination\Paginator|\Illuminate\Database\Eloquent\Builder
     */
    public function getList(LocalizationGetAllRequest $request)
    {
        return $this->_getList($this->select('id', 'name'), $request, [], ['name', 'local']);
    }

    /**
     * @param \App\Http\Requests\Localization\LocalizationSaveRequest $request
     * @param \App\Models\Channel $channel
     * @return mixed
     */
    public function add(LocalizationSaveRequest $request, Channel $channel)
    {
        $data = $request->validated();
        $translations = $channel->parseLocalizationFile($data['translations']);
        return $translations ? $this->create([
            'name' => $data['name'],
            'translations' => $translations,
        ]) : false;
    }

    /**
     * @param \App\Http\Requests\Localization\LocalizationUpdateRequest $request
     * @param \App\Models\Channel $channel
     * @return bool
     */
    public function resave(LocalizationUpdateRequest $request, Channel $channel)
    {
        $localization = $this->find($request->id);
        $data = $request->validated();
        $newTranslation = array_intersect_key($data['translations'], $channel->getDefaultAppLocalization());

        if ($newTranslation) {
            $localization->translations = array_replace_recursive($localization->translations, $newTranslation);
            $localization->save();
            return $localization;
        }

        return false;
    }

    /**
     * @param \App\Http\Requests\Localization\LocalizationGetRequest $request
     * @return mixed
     */
    public function remove(LocalizationGetRequest $request)
    {
        $localization = $this->find($request->id);
        $this->destroy($request->id);
        return $localization;
    }

    /**
     * @param \App\Http\Requests\Localization\LocalizationAddToChannelRequest $request
     * @param \App\Models\Channel $channel
     * @return mixed
     */
    public function addToChannel(LocalizationAddToChannelRequest $request, Channel $channel)
    {
        $localization = $this->find($request->id);

        $channel->where('id', $request->channel_id)
            ->update(['app_localization' => json_encode($localization->translations)]);

        return $localization->translations;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function getLocalizationByLanguage(string $name)
    {
        return $this->where('name', $name)->get(['id', 'name']);
    }
}
