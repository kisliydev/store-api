<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Language extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'local',
    ];

    /**
     * @var array
     */
    private $_languauges = [
        ["name" => "Abkhaz", "local" => "Аҧсуа"],
        ["name" => "Afar", "local" => "Afaraf"],
        ["name" => "Afrikaans", "local" => "Afrikaans"],
        ["name" => "Akan", "local" => "Akan"],
        ["name" => "Albanian", "local" => "Shqip"],
        ["name" => "Amharic", "local" => "አማርኛ"],
        ["name" => "Arabic", "local" => "العربية"],
        ["name" => "Aragonese", "local" => "Aragonés"],
        ["name" => "Armenian", "local" => "Հայերեն"],
        ["name" => "Assamese", "local" => "অসমীয়া"],
        ["name" => "Avaric", "local" => "Авар"],
        ["name" => "Avestan", "local" => "avesta"],
        ["name" => "Aymara", "local" => "Aymar"],
        ["name" => "Azerbaijani", "local" => "Azərbaycanca"],
        ["name" => "Bambara", "local" => "Bamanankan"],
        ["name" => "Bashkir", "local" => "Башҡортса"],
        ["name" => "Basque", "local" => "Euskara"],
        ["name" => "Belarusian", "local" => "Беларуская"],
        ["name" => "Bengali", "local" => "বাংলা"],
        ["name" => "Bihari", "local" => "भोजपुरी"],
        ["name" => "Bislama", "local" => "Bislama"],
        ["name" => "Bosnian", "local" => "Bosanski"],
        ["name" => "Breton", "local" => "Brezhoneg"],
        ["name" => "Bulgarian", "local" => "Български"],
        ["name" => "Burmese", "local" => "မြန်မာဘာသာ"],
        ["name" => "Castilian", "local" => "Castellano"],
        ["name" => "Catalan", "local" => "Català"],
        ["name" => "Chamorro", "local" => "Chamoru"],
        ["name" => "Chechen", "local" => "Нохчийн"],
        ["name" => "Chichewa", "local" => "Chichewa"],
        ["name" => "Chinese", "local" => "中文"],
        ["name" => "Chuvash", "local" => "Чӑвашла"],
        ["name" => "Cornish", "local" => "Kernewek"],
        ["name" => "Corsican", "local" => "Corsu"],
        ["name" => "Cree", "local" => "ᓀᐦᐃᔭᐍᐏᐣ"],
        ["name" => "Croatian", "local" => "Hrvatski"],
        ["name" => "Czech", "local" => "Čeština"],
        ["name" => "Danish", "local" => "Dansk"],
        ["name" => "Divehi", "local" => "Divehi"],
        ["name" => "Dutch", "local" => "Nederlands"],
        ["name" => "Dzongkha", "local" => "རྫོང་ཁ"],
        ["name" => "English", "local" => "English"],
        ["name" => "Esperanto", "local" => "Esperanto"],
        ["name" => "Estonian", "local" => "Eesti"],
        ["name" => "Ewe", "local" => "Eʋegbe"],
        ["name" => "Faroese", "local" => "Føroyskt"],
        ["name" => "Fijian", "local" => "Na Vosa Vaka-Viti"],
        ["name" => "Finnish", "local" => "Suomi"],
        ["name" => "French", "local" => "Français"],
        ["name" => "Fula", "local" => "Fulfulde"],
        ["name" => "Galician", "local" => "Galego"],
        ["name" => "Georgian", "local" => "ქართული"],
        ["name" => "German", "local" => "Deutsch"],
        ["name" => "Greek", "local" => "Ελληνικά"],
        ["name" => "Guaraní", "local" => "Avañe'ẽ"],
        ["name" => "Gujarati", "local" => "ગુજરાતી"],
        ["name" => "Haitian", "local" => "Kreyòl Ayisyen"],
        ["name" => "Hausa", "local" => "هَوُسَ"],
        ["name" => "Hebrew", "local" => "עברית"],
        ["name" => "Herero", "local" => "Otjiherero"],
        ["name" => "Hindi", "local" => "हिन्दी"],
        ["name" => "Hiri Motu", "local" => "Hiri Motu"],
        ["name" => "Hungarian", "local" => "Magyar"],
        ["name" => "Interlingua", "local" => "Interlingua"],
        ["name" => "Indonesian", "local" => "Bahasa Indonesia"],
        ["name" => "Interlingue", "local" => "Interlingue"],
        ["name" => "Irish", "local" => "Gaeilge"],
        ["name" => "Igbo", "local" => "Igbo"],
        ["name" => "Inupiaq", "local" => "Iñupiak"],
        ["name" => "Ido", "local" => "Ido"],
        ["name" => "Icelandic", "local" => "Íslenska"],
        ["name" => "Italian", "local" => "Italiano"],
        ["name" => "Inuktitut", "local" => "ᐃᓄᒃᑎᑐᑦ"],
        ["name" => "Japanese", "local" => "日本語"],
        ["name" => "Javanese", "local" => "Basa Jawa"],
        ["name" => "Kalaallisut", "local" => "Kalaallisut"],
        ["name" => "Kannada", "local" => "ಕನ್ನಡ"],
        ["name" => "Kanuri", "local" => "Kanuri"],
        ["name" => "Kashmiri", "local" => "كشميري"],
        ["name" => "Kazakh", "local" => "Қазақша"],
        ["name" => "Khmer", "local" => "ភាសាខ្មែរ"],
        ["name" => "Kikuyu", "local" => "Gĩkũyũ"],
        ["name" => "Kinyarwanda", "local" => "Kinyarwanda"],
        ["name" => "Kyrgyz", "local" => "Кыргызча"],
        ["name" => "Komi", "local" => "Коми"],
        ["name" => "Kongo", "local" => "Kongo"],
        ["name" => "Korean", "local" => "한국어"],
        ["name" => "Kurdish", "local" => "Kurdî"],
        ["name" => "Kwanyama", "local" => "Kuanyama"],
        ["name" => "Latin", "local" => "Latina"],
        ["name" => "Luxembourgish", "local" => "Lëtzebuergesch"],
        ["name" => "Ganda", "local" => "Luganda"],
        ["name" => "Limburgish", "local" => "Limburgs"],
        ["name" => "Lingala", "local" => "Lingála"],
        ["name" => "Lao", "local" => "ພາສາລາວ"],
        ["name" => "Lithuanian", "local" => "Lietuvių"],
        ["name" => "Luba-Katanga", "local" => "Tshiluba"],
        ["name" => "Latvian", "local" => "Latviešu"],
        ["name" => "Manx", "local" => "Gaelg"],
        ["name" => "Macedonian", "local" => "Македонски"],
        ["name" => "Malagasy", "local" => "Malagasy"],
        ["name" => "Malay", "local" => "Bahasa Melayu"],
        ["name" => "Malayalam", "local" => "മലയാളം"],
        ["name" => "Maltese", "local" => "Malti"],
        ["name" => "Māori", "local" => "Māori"],
        ["name" => "Marathi", "local" => "मराठी"],
        ["name" => "Marshallese", "local" => "Kajin M̧ajeļ"],
        ["name" => "Mongolian", "local" => "Монгол"],
        ["name" => "Nauru", "local" => "Dorerin Naoero"],
        ["name" => "Navajo", "local" => "Diné Bizaad"],
        ["name" => "Northern Ndebele", "local" => "isiNdebele"],
        ["name" => "Nepali", "local" => "नेपाली"],
        ["name" => "Ndonga", "local" => "Owambo"],
        ["name" => "Norwegian Bokmål", "local" => "Norsk (Bokmål)"],
        ["name" => "Norwegian Nynorsk", "local" => "Norsk (Nynorsk)"],
        ["name" => "Norwegian", "local" => "Norsk"],
        ["name" => "Nuosu", "local" => "ꆈꌠ꒿ Nuosuhxop"],
        ["name" => "Southern Ndebele", "local" => "isiNdebele"],
        ["name" => "Occitan", "local" => "Occitan"],
        ["name" => "Ojibwe", "local" => "ᐊᓂᔑᓈᐯᒧᐎᓐ"],
        ["name" => "Old Church Slavonic", "local" => "Словѣ́ньскъ"],
        ["name" => "Oromo", "local" => "Afaan Oromoo"],
        ["name" => "Oriya", "local" => "ଓଡି଼ଆ"],
        ["name" => "Ossetian", "local" => "Ирон æвзаг"],
        ["name" => "Panjabi", "local" => "ਪੰਜਾਬੀ"],
        ["name" => "Pāli", "local" => "पाऴि"],
        ["name" => "Persian", "local" => "فارسی"],
        ["name" => "Polish", "local" => "Polski"],
        ["name" => "Pashto", "local" => "پښتو"],
        ["name" => "Portuguese", "local" => "Português"],
        ["name" => "Quechua", "local" => "Runa Simi"],
        ["name" => "Romansh", "local" => "Rumantsch"],
        ["name" => "Kirundi", "local" => "Kirundi"],
        ["name" => "Romanian", "local" => "Română"],
        ["name" => "Russian", "local" => "Русский"],
        ["name" => "Sanskrit", "local" => "संस्कृतम्"],
        ["name" => "Sardinian", "local" => "Sardu"],
        ["name" => "Sindhi", "local" => "سنڌي‎"],
        ["name" => "Northern Sami", "local" => "Sámegiella"],
        ["name" => "Samoan", "local" => "Gagana Sāmoa"],
        ["name" => "Sango", "local" => "Sängö"],
        ["name" => "Serbian", "local" => "Српски"],
        ["name" => "Gaelic", "local" => "Gàidhlig"],
        ["name" => "Shona", "local" => "ChiShona"],
        ["name" => "Sinhala", "local" => "සිංහල"],
        ["name" => "Slovak", "local" => "Slovenčina"],
        ["name" => "Slovene", "local" => "Slovenščina"],
        ["name" => "Somali", "local" => "Soomaaliga"],
        ["name" => "Southern Sotho", "local" => "Sesotho"],
        ["name" => "Spanish", "local" => "Español"],
        ["name" => "Sundanese", "local" => "Basa Sunda"],
        ["name" => "Swahili", "local" => "Kiswahili"],
        ["name" => "Swati", "local" => "SiSwati"],
        ["name" => "Swedish", "local" => "Svenska"],
        ["name" => "Tamil", "local" => "தமிழ்"],
        ["name" => "Telugu", "local" => "తెలుగు"],
        ["name" => "Tajik", "local" => "Тоҷикӣ"],
        ["name" => "Thai", "local" => "ภาษาไทย"],
        ["name" => "Tigrinya", "local" => "ትግርኛ"],
        ["name" => "Tibetan Standard", "local" => "བོད་ཡིག"],
        ["name" => "Turkmen", "local" => "Türkmençe"],
        ["name" => "Tagalog", "local" => "Tagalog"],
        ["name" => "Tswana", "local" => "Setswana"],
        ["name" => "Tonga", "local" => "faka Tonga"],
        ["name" => "Turkish", "local" => "Türkçe"],
        ["name" => "Tsonga", "local" => "Xitsonga"],
        ["name" => "Tatar", "local" => "Татарча"],
        ["name" => "Twi", "local" => "Twi"],
        ["name" => "Tahitian", "local" => "Reo Mā’ohi"],
        ["name" => "Uyghur", "local" => "ئۇيغۇرچه"],
        ["name" => "Ukrainian", "local" => "Українська"],
        ["name" => "Urdu", "local" => "اردو"],
        ["name" => "Uzbek", "local" => "O‘zbek"],
        ["name" => "Venda", "local" => "Tshivenḓa"],
        ["name" => "Vietnamese", "local" => "Tiếng Việt"],
        ["name" => "Volapük", "local" => "Volapük"],
        ["name" => "Walloon", "local" => "Walon"],
        ["name" => "Welsh", "local" => "Cymraeg"],
        ["name" => "Wolof", "local" => "Wolof"],
        ["name" => "Western Frisian", "local" => "Frysk"],
        ["name" => "Xhosa", "local" => "isiXhosa"],
        ["name" => "Yiddish", "local" => "ייִדיש"],
        ["name" => "Yoruba", "local" => "Yorùbá"],
        ["name" => "Zhuang", "local" => "Cuengh"],
        ["name" => "Zulu", "local" => "isiZulu"]
    ];

    /**
     * @return void
     */
    public function setup()
    {
        foreach( $this->_languauges as $language) {
            $this->firstOrCreate($language);
        }
    }

    /**
     * @param \App\Models\Localization $localization
     * @return mixed
     */
    public function getList(Localization $localization)
    {
        $langTable = $this->getTable();
        $locTable = $localization->getTable();
        return $this->select(
                "{$langTable}.name",
                "{$langTable}.id",
                DB::raw("COALESCE({$locTable}.id, 0) as in_use")
            )
            ->leftJoin($locTable, "{$locTable}.name", '=', "{$langTable}.name")
            ->get();
    }
}
