<?php

namespace App\Models;

use App\Http\Requests\ChannelStatistics\DealerStatisticsGetRequest;
use App\Http\Requests\ChannelStatistics\DealersExportStatisticsRequest;
use App\Http\Requests\ChannelStatistics\DealerStatisticsGetMobileRequest;
use App\Http\Requests\ChannelStatistics\DealerStatisticsGetListMobileRequest;
use App\Transformers\ChannelStatisticsTypeTransformer;
use App\Transformers\UserStatisticsTransformer;
use App\Transformers\CollectionTransformer;
use App\Http\Requests\Request;
use Illuminate\Support\Facades\DB;


use Excel;
use PHPExcel_Cell;

class DealerStatistics extends ChannelStatistics
{
    /**
     * @var array
     */
    protected $filterable = [
        "district_id",
    ];

    /**
     * @var array
     */
    protected $sortable = [
        "name",
        "location",
        "rank",
        "users",
        "total_points",
        "sold_products",
        "passed_quizzes",
        "passed_surveys",
        "added_manually",
        "achieved_levels",
    ];

    /**
     * @param \App\Http\Requests\ChannelStatistics\DealerStatisticsGetRequest $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    public function getDealersStatistics(DealerStatisticsGetRequest $request, Dealer $dealer, PointsStatistic $pointsStatistic)
    {
        $this->searchable = [
            'name',
            'location',
        ];

        return $this->getDealersList($request, $dealer, $pointsStatistic);
    }

    /**
     * @param \App\Http\Requests\ChannelStatistics\DealerStatisticsGetListMobileRequest $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @return mixed
     */
    public function getDealersMobileStatistics(DealerStatisticsGetListMobileRequest $request, Dealer $dealer, PointsStatistic $pointsStatistic)
    {
        return $this->getDealersMobileList($request, $dealer, $pointsStatistic);
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @return mixed
     */
    protected function getDealersMobileList(Request $request, Dealer $dealer, PointsStatistic $pointsStatistic)
    {
        $usersInDealersQuery = $dealer->select(
                "{$this->d}.id",
                DB::raw("COUNT({$this->ud}.user_id) as users_count")
            )
            ->leftJoin($this->ud, "{$this->ud}.dealer_id", '=', "{$this->d}.id")
            ->where("{$this->d}.channel_id", $request->channel_id)
            ->groupBy("{$this->d}.id");

        $pointsStatisticQuery = $pointsStatistic->select(
                "{$this->ps}.user_id",
                "{$this->ud}.dealer_id",
                DB::raw("SUM({$this->ps}.earned_points) as total_points")
            )
            ->leftJoin($this->ud, "{$this->ud}.user_id", '=', "{$this->ps}.user_id")
            ->where("{$this->ps}.channel_id", $request->channel_id)
            ->groupBy(
                "{$this->ps}.user_id",
                "{$this->ud}.dealer_id"
            );

        $orderBy = empty($request->get_average) ? 'total_points' : 'average_points';
        $dealers = $dealer->select(
                "{$this->d}.id",
                "{$this->d}.name",
                "{$this->d}.logo_id",
                "{$this->m}.path as logo_path",
                "{$this->m}.thumbnails as thumbnails",
                DB::raw("uid.users_count"),
                DB::raw("SUM(stat.total_points) as total_points"),
                DB::raw("CASE WHEN uid.users_count IS NULL THEN 0 ELSE ROUND(SUM(stat.total_points)/uid.users_count, 2) END as average_points")
            )
            ->leftJoin($this->m, "{$this->m}.id", '=', "{$this->d}.logo_id")
            ->leftJoin(DB::raw("({$usersInDealersQuery->toSql()}) as uid"), "uid.id", "=", "{$this->d}.id")
            ->leftJoin(DB::raw("({$pointsStatisticQuery->toSql()}) as stat"),  "stat.dealer_id", "=", "{$this->d}.id")
            ->where("{$this->d}.channel_id", $request->channel_id)
            ->groupBy("{$this->d}.id")
            ->orderBy($orderBy, 'desc');

        $rank = DB::table(DB::raw("({$dealers->toSql()}) as dealers, (SELECT @rank := 0) as init"))
            ->mergeBindings($dealers->getQuery())
            ->select(
                DB::raw('dealers.*'),
                DB::raw('@rank := @rank + 1 as rank')
            );

        return DB::table(DB::raw( "({$rank->toSql()}) as r"))
            ->mergeBindings($usersInDealersQuery->getQuery())
            ->mergeBindings($pointsStatisticQuery->getQuery())
            ->mergeBindings($dealers->getQuery())
            ->get();
    }

    /**
     * @param \App\Http\Requests\ChannelStatistics\DealersExportStatisticsRequest $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @param \App\Models\Product $product
     * @param \App\Transformers\ChannelStatisticsTypeTransformer $transformer
     * @return mixed
     */
    public function exportDealersStatistics(DealersExportStatisticsRequest $request, Dealer $dealer, PointsStatistic $pointsStatistic, Product $product, ChannelStatisticsTypeTransformer $transformer)
    {
        return Excel::create('dealers', function($excel) use ($request, $dealer, $pointsStatistic, $product, $transformer) {
            $excel->sheet("Dealers", function($sheet) use ($request, $dealer, $pointsStatistic, $product, $transformer) {

                $products = $this->getChannelProducts($request, $product);
                $productsStat = $this->getProductsInDealers($request, $dealer, $product);
                $dealers = $this->getDealersList($request, $dealer, $pointsStatistic, false);

                $i = $this->addSheetSignatures($request, $sheet, 2, 'Dealers' );
                $i += 2;

                $headers = [
                    'Data' => 3,
                    'Total' => 0,
                    'Quizzes' => 1,
                    'Surveys' => 1,
                    'Products' => 1,
                    'Manual' => 1,
                    'Levels' => 1
                ];
                $productsTitles = array_map(
                    function() {return 1;},
                    array_flip($products->pluck('name')->toArray())
                );

                $i = $this->addTableHeaders($sheet, $i, array_merge($headers, $productsTitles));
                $i++;

                $subHeaders = [
                    'Rank',
                    'Dealer',
                    'District',
                    'Users',
                    '',
                ];
                $temp = [
                    'Reg',
                    'Points',
                ];
                $lastColumnIndex = $products->count() * 2  + 14;
                for ($j = 0; $j < $products->count() + 5; $j++ ) {
                    $subHeaders = array_merge($subHeaders, $temp);
                }

                $sheet->row($i, $subHeaders);
                $sheet->row($i, function ($row) {
                    $this->addHeaderStyles($row);
                });
                $i++;

                /**
                 * Fill main table with data
                 */
                $firstInRange = $i;
                $dealers->each(function($dealer) use ($sheet, &$i, $products, $productsStat, $lastColumnIndex) {
                    $data = [
                        $dealer->rank,
                        $dealer->name,
                        $dealer->district_name,
                        $dealer->users_count,

                        intval($dealer->total_points),

                        intval($dealer->passed_quizzes),
                        intval($dealer->quizzes_points),

                        intval($dealer->passed_surveys),
                        intval($dealer->surveys_points),

                        intval($dealer->sold_products),
                        intval($dealer->product_points),

                        intval($dealer->added_manually),
                        intval($dealer->manual_points),

                        intval($dealer->achieved_levels),
                        intval($dealer->level_points),
                    ];

                    $selection = $productsStat->where('id', $dealer->id)->sortBy('product_id', SORT_NUMERIC);
                    $productsStat->forget($selection->keys()->toArray());

                    $selection->each(function($item) use (&$data){
                        $data[] = $item->sold_count;
                        $data[] = $item->sold_points;
                    });

                    $sheet->row($i, $data);

                    if (! $dealer->users_count) {
                        foreach( range(0, $lastColumnIndex) as $index) {
                            $column = PHPExcel_Cell::stringFromColumnIndex($index);
                            $this->setColors($sheet, $i, [$column => 'alert']);
                        }
                    }
                    $i++;
                });

                if ($firstInRange !== $i) {
                    $lastInRange = $i - 1;
                    $data = [];
                    foreach(range(0, 2) as $index) {
                        $data[] = "";
                    }

                    foreach(range(3, $lastColumnIndex) as $index) {
                        $column = PHPExcel_Cell::stringFromColumnIndex($index);
                        $data[] = "=SUM({$column}{$firstInRange}:{$column}{$lastInRange})";
                    }

                    $sheet->row($i, $data);
                    $sheet->row($i, function($row) {
                        $this->addFooterStyle($row);
                    });
                }
                $i += 3;

                /**
                 * Points statistics by type (category)
                 */
                $i = $this->addTableSignature($sheet, $i, 'Statistics By Points Category') + 2;
                $i = $this->addPointsStatistics($sheet, $i, $request, $pointsStatistic, $transformer) + 3;

                /**
                 * Products statistics
                 */
                $i = $this->addTableSignature($sheet, $i, 'Statistics By Products') + 2;
                $this->addProductsStatistics($sheet, $i, $request, $pointsStatistic);

                $this->setTextWrapper($sheet, $lastColumnIndex, ++$i);
            });
        })->string('xlsx');
    }

    /**
     * @param \App\Http\Requests\ChannelStatistics\DealerStatisticsGetMobileRequest $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @param \App\Transformers\UserStatisticsTransformer $transformer
     * @return mixed
     */
    public function getDealerMobileStatistics(DealerStatisticsGetMobileRequest $request, Dealer $dealer, PointsStatistic $pointsStatistic, UserStatisticsTransformer $transformer)
    {
        $dealer = $this->getDealersList($request, $dealer, $pointsStatistic, false);
        return $this->addDealerData($dealer, $request->channel_id, $transformer);
    }

    /**
     * @param \App\Http\Requests\ChannelStatistics\DealerStatisticsGetMobileRequest $request
     * @return array
     */
    public function getDealerDetailedMobileStatistics(DealerStatisticsGetMobileRequest $request)
    {
        $data = [
            'product' => DB::table($this->pr)
                ->select(
                    "{$this->pr}.id",
                    "{$this->pr}.name",
                    DB::raw("IFNULL(SUM({$this->ps}.units_number), 0) as count"),
                    DB::raw("IFNULL(SUM({$this->ps}.earned_points), 0) as points")
                )
                ->leftJoin($this->ps, function($join) {
                    $join->on("{$this->ps}.object_id", "{$this->pr}.id")
                        ->where("{$this->ps}.object_type", 'product');
                })
                ->join($this->ud, function($join) use ($request) {
                    $join->on("{$this->ud}.user_id", "{$this->ps}.user_id")
                        ->where("{$this->ud}.dealer_id", $request->dealer_id);
                })
                ->where("{$this->pr}.channel_id", $request->channel_id)
                ->groupBy("{$this->pr}.id")
                ->get(),
        ];
        $keys = [
            'quiz' => $this->ai,
            'survey' => $this->ai,
            'manual' => $this->mp,
            'level' => $this->aml,
        ];

        foreach($keys as $type => $table) {
            $data[$type] = DB::table($table)
                ->select(
                    "{$table}.id",
                    "{$table}.title as name",
                    DB::raw("IFNULL(COUNT({$this->ps}.units_number), 0) as count"),
                    DB::raw("IFNULL(SUM({$this->ps}.earned_points), 0) as points")
                )
                ->leftJoin($this->ps, function($join) use ($table, $type) {
                    $join->on("{$this->ps}.object_id", "{$table}.id")
                        ->where("{$this->ps}.object_type", $type);
                })
                ->join($this->ud, function($join) use ($request) {
                    $join->on("{$this->ud}.user_id", "{$this->ps}.user_id")
                        ->where("{$this->ud}.dealer_id", $request->dealer_id);
                })
                ->where("{$table}.channel_id", $request->channel_id)
                ->groupBy("{$table}.id")
                ->get();
        }
        return $data;
    }

    /**
     * @param \App\Http\Requests\ChannelStatistics\DealerStatisticsGetMobileRequest $request
     * @return mixed
     */
    public function getDealerUsersMobileStatistics(DealerStatisticsGetMobileRequest $request)
    {
        $users = $this->getDealerUsersQuery($request->dealer_id);

        $rank = DB::table(DB::raw("({$users->toSql()}) as users, (SELECT @rank := 0) as init"))
            ->mergeBindings($users)
            ->select(
                DB::raw('users.*'),
                DB::raw('@rank := @rank + 1 as rank')
            );

        $query = DB::table(DB::raw( "({$rank->toSql()}) as r"))
            ->mergeBindings($users);

        return $query->get();
    }

    /**
     * @param $query
     * @param $request
     * @param string|null $table
     * @param null $whereTable
     * @param string|null $column
     */
    public function onlyChannelUsers($query, $request, string $table = null, $whereTable = null, string $column = null)
    {
        $query->whereIn("{$this->ps}.user_id", function($subQuery) use ($request) {
            $subQuery->select("{$this->ud}.user_id")
                ->from($this->ud)
                ->join($this->d, function($join) use ($request) {
                    $join->on("{$this->d}.id", '=', "{$this->ud}.dealer_id")
                        ->where("{$this->d}.channel_id", $request->channel_id);
                });
        })
            ->where("{$this->ps}.channel_id", '=', $request->channel_id);
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @param bool $withPagination
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    protected function getDealersList(Request $request, Dealer $dealer, PointsStatistic $pointsStatistic, bool $withPagination = true)
    {
        $usersInDealersQuery = $dealer->select(
            "{$this->d}.id",
            DB::raw("COUNT({$this->ud}.user_id) as users_count")
        )
            ->leftJoin($this->ud, "{$this->ud}.dealer_id", '=', "{$this->d}.id")
            ->where("{$this->d}.channel_id", $request->channel_id)
            ->groupBy("{$this->d}.id");

        $pointsStatisticQuery = $pointsStatistic->select(
            "{$this->ps}.user_id",
            "{$this->ud}.dealer_id",
            DB::raw("SUM({$this->ps}.earned_points) as total_points"),
            DB::raw("SUM(CASE WHEN {$this->ps}.object_type='product' THEN {$this->ps}.earned_points ELSE NULL END) as product_points"),
            DB::raw("SUM(CASE WHEN {$this->ps}.object_type='quiz' THEN {$this->ps}.earned_points ELSE NULL END) as quizzes_points"),
            DB::raw("SUM(CASE WHEN {$this->ps}.object_type='survey' THEN {$this->ps}.earned_points ELSE NULL END) as surveys_points"),
            DB::raw("SUM(CASE WHEN {$this->ps}.object_type='manual' THEN {$this->ps}.earned_points ELSE NULL END) as manual_points"),
            DB::raw("SUM(CASE WHEN {$this->ps}.object_type='level' THEN {$this->ps}.earned_points ELSE NULL END) as level_points"),

            DB::raw("SUM(CASE WHEN {$this->ps}.object_type='product' THEN {$this->ps}.units_number ELSE NULL END) as sold_products"),
            DB::raw("COUNT(CASE WHEN {$this->ps}.object_type='quiz' THEN 1 ELSE NULL END) as passed_quizzes"),
            DB::raw("COUNT(CASE WHEN {$this->ps}.object_type='survey' THEN 1 ELSE NULL END) as passed_surveys"),
            DB::raw("COUNT(CASE WHEN {$this->ps}.object_type='manual' THEN 1 ELSE NULL END) as added_manually"),
            DB::raw("COUNT(CASE WHEN {$this->ps}.object_type='level' THEN 1 ELSE NULL END) as achieved_levels")
        )
            ->leftJoin($this->ud, "{$this->ud}.user_id", '=', "{$this->ps}.user_id")
            ->where("{$this->ps}.channel_id", $request->channel_id)
            ->groupBy(
                "{$this->ps}.user_id",
                "{$this->ud}.dealer_id"
            );

        $this->addDateRange($pointsStatisticQuery, $request);

        $dealers = $dealer->select(
            "{$this->d}.*",
            "{$this->d}.logo_id",
            "{$this->dis}.name as district_name",
            "{$this->m}.path as logo_path",
            "{$this->m}.thumbnails as thumbnails",
            DB::raw("uid.users_count"),
            DB::raw("SUM(stat.total_points) as total_points"),
            DB::raw("SUM(stat.product_points) as product_points"),
            DB::raw("SUM(stat.quizzes_points) as quizzes_points"),
            DB::raw("SUM(stat.surveys_points) as surveys_points"),
            DB::raw("SUM(stat.manual_points) as manual_points"),
            DB::raw("SUM(stat.level_points) as level_points"),
            DB::raw("SUM(stat.sold_products) as sold_products"),
            DB::raw("SUM(stat.passed_quizzes) as passed_quizzes"),
            DB::raw("SUM(stat.passed_surveys) as passed_surveys"),
            DB::raw("SUM(stat.added_manually) as added_manually"),
            DB::raw("SUM(stat.achieved_levels) as achieved_levels")
        )
            ->leftJoin($this->dis, "{$this->dis}.id", '=', "{$this->d}.district_id")
            ->leftJoin($this->m, "{$this->m}.id", '=', "{$this->d}.logo_id")
            ->leftJoin(DB::raw("({$usersInDealersQuery->toSql()}) as uid"), "uid.id", "=", "{$this->d}.id")
            ->leftJoin(DB::raw("({$pointsStatisticQuery->toSql()}) as stat"),  "stat.dealer_id", "=", "{$this->d}.id")
            ->where("{$this->d}.channel_id", $request->channel_id)
            ->groupBy("{$this->d}.id")
            ->orderBy('total_points', 'desc');

        $rank = DB::table(DB::raw("({$dealers->toSql()}) as dealers, (SELECT @rank := 0) as init"))
            ->mergeBindings($dealers->getQuery())
            ->select(
                DB::raw('dealers.*'),
                DB::raw('@rank := @rank + 1 as rank')
            );

        $query = DB::table(DB::raw( "({$rank->toSql()}) as r"))
            ->mergeBindings($usersInDealersQuery->getQuery())
            ->mergeBindings($pointsStatisticQuery->getQuery())
            ->mergeBindings($dealers->getQuery());

        return $this->getList($query, $request, $withPagination);
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\Product $product
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Support\Collection
     */
    protected function getProductsInDealers(Request $request, Dealer $dealer, Product $product)
    {
        $subQuery = $product->select(
            "{$this->pr}.id as product_id",
            "{$this->ud}.dealer_id as dealer_id",
            DB::raw("SUM({$this->ps}.units_number) as count"),
            DB::raw("SUM({$this->ps}.earned_points) as points")
        )
            ->join($this->ps, function($join) use ($request) {
                $join->on("{$this->ps}.object_id", '=', "{$this->pr}.id")
                    ->where("{$this->ps}.object_type", '=', 'product');

                $this->addDateRange($join, $request);
            })
            ->join($this->ud, "{$this->ud}.user_id", '=', "{$this->ps}.user_id")
            ->where("{$this->pr}.channel_id", $request->channel_id)
            ->groupBy(
                "{$this->pr}.id",
                "{$this->ud}.dealer_id"
            );

        $query = $dealer->select(
            "{$this->d}.*",
            "{$this->pr}.id as product_id",
            DB::raw("IFNULL(sold.count, 0) as sold_count"),
            DB::raw("IFNULL(sold.points, 0) as sold_points")
        )
            ->join($this->pr, "{$this->pr}.channel_id", '=', "{$this->d}.channel_id")
            ->leftJoin(
                DB::raw("({$subQuery->toSql()}) as sold"),
                function($join) {
                    $join->on('sold.dealer_id', '=', "{$this->d}.id")
                        ->on('sold.product_id', '=', "{$this->pr}.id");
                }
            )
            ->where("{$this->d}.channel_id", $request->channel_id)
            ->mergeBindings($subQuery->getQuery());

        return $this->getList($query, $request, false);
    }

    /**
     * @param $dealers
     * @param int $channelID
     * @param \App\Transformers\CollectionTransformer $transformer
     * @return mixed
     */
    protected function addDealerData($dealers, int $channelID, CollectionTransformer $transformer)
    {
        $counts = $this->getChannelItemsCount($channelID);

        $dealers->each(function($dealer) use ($transformer, $counts) {
            $data = $this->getDealerUsersQuery($dealer->id)->limit(3)->get();

            $dealer->top_users = $transformer->transform(collect($data));
            $dealer->surveys_count = $counts['survey'] * $dealer->users_count;
            $dealer->quizzes_count = $counts['quiz'] * $dealer->users_count;
            $dealer->managers = DB::table($this->u)
                ->select(
                    "{$this->u}.id",
                    DB::raw("TRIM(CONCAT({$this->u}.first_name, ' ', {$this->u}.last_name)) as full_name")
                )
                ->join($this->ud, "{$this->ud}.user_id", '=', "{$this->u}.id")
                ->join($this->up, "{$this->up}.user_id", '=', "{$this->u}.id")
                ->join($this->p, function($join) {
                    $join->on("{$this->p}.id", '=', "{$this->up}.position_id")
                        ->where("{$this->p}.is_manager", '=', true);
                })
                ->where("{$this->ud}.dealer_id", $dealer->id)
                ->get();
        });

        return $dealers;
    }

    /**
     * @param int $dealerID
     * @return mixed
     */
    protected function getDealerUsersQuery(int $dealerID)
    {
        return DB::table($this->u)
            ->select(
                "{$this->u}.id",
                "{$this->m}.path as avatar_path",
                "{$this->m}.thumbnails as thumbnails",
                DB::raw("TRIM(CONCAT({$this->u}.first_name, ' ', {$this->u}.last_name)) as full_name"),
                DB::raw("SUM({$this->ps}.earned_points) as total_points")
            )
            ->join($this->ud, "{$this->ud}.user_id", "=", "{$this->u}.id")
            ->join($this->d, "{$this->d}.id", "=", "{$this->ud}.dealer_id" )
            ->leftJoin($this->m, "{$this->m}.id", '=', "{$this->u}.avatar_id")
            ->leftJoin($this->ps, function($join) {
                $join->on("{$this->ps}.user_id", '=', "{$this->u}.id")
                    ->on("{$this->ps}.channel_id", '=', "{$this->d}.channel_id");
            })
            ->where("{$this->ud}.dealer_id", '=', $dealerID)
            ->groupBy("{$this->u}.id")
            ->orderBy('total_points', 'desc');
    }
}
