<?php

namespace App\Models;

class Question extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'item_id',
        'text',
        'data',
        'order',
        'answers',
        'type',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'image',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'json',
    ];

    /**
     * @var string
     */
    private $_media_type = 'question_images';

    /**
     * @var array
     */
    private $_no_answer_types = [
        'text_input',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function answers() {
        return $this->hasMany( 'App\Models\Answer')->orderBy('order');
    }

    public function userAnswers()
    {
        return $this->hasMany(UserAnswer::class);
    }

    /**
     * @return bool
     */
    public function getImageAttribute() {
        return ! empty($this->data["image_id"]) ? Media::find($this->data["image_id"]) : false;
    }

    /**
     * @param int $item_id
     * @param string $item_type
     * @param array $q_data
     * @param \App\Models\Media $media
     * @param \App\Models\Answer $answer
     * @param int $channelId
     * @throws \Exception
     */
    public function add(int $item_id, string $item_type, array $q_data, Media $media, Answer $answer, int $channelId)
    {
        foreach($q_data as $raw) {
            $question = $this->create([
                'item_id' => $item_id,
                'type' => $raw['type'] ?? 'single_choice',
                'text' => $raw['text'],
                'data' => [],
                'order' => ($raw['order'] ?? 0),
            ]);
            $question->data = $question->_prepareData($raw, $item_type, $media, $channelId);
            $question->save();

            if (! empty($raw['new_answers']) && $question->_canHaveAnswers() ) {
                $answer->add($question, $raw['new_answers'], $media, $channelId);
            }
        }
    }

    /**
     * @param string $item_type
     * @param array $q_data
     * @param \App\Models\Media $media
     * @param \App\Models\Answer $answer
     * @param int $channelId
     * @throws \Exception
     */
    public function resave(string $item_type, array $q_data, Media $media, Answer $answer, int $channelId)
    {
        foreach( $q_data as $raw ) {

            $question = $this->find($raw['id']);

            if (! $question) {
                continue;
            }

            if ($this->boolval($raw['remove_question'])) {
                if ($question->image_id) {
                    $media->removeMedia([$question->image_id]);
                }

                $answer->bulkDelete([$question->id], $media);
                $this->destroy($raw['id']);
            } else {
                if (! empty($raw['type'])) {
                    $question->type = $raw['type'];
                }

                $question->text = $raw['text'];
                $question->order = $raw['order'] ?? 0;

                $question->data = $question->_prepareData($raw, $item_type, $media, $channelId);
                $question->save();

                if (! $question->_canHaveAnswers()) {
                    $answer->bulkDelete([$question->id], $media);
                    continue;
                }

                if (! empty($raw['new_answers'])) {
                    $answer->add($question, $raw['new_answers'], $media, $channelId);
                }

                if (! empty($raw['answers'])) {
                    $answer->resave($raw['answers'], $media, $channelId);
                }

            }
        }
    }

    /**
     * @param array $item_ids
     * @param \App\Models\Media $media
     * @param \App\Models\Answer $answer
     */
    public function bulkDelete(array $item_ids, Media $media, Answer $answer)
    {
        $questions = $this->whereIn('item_id', $item_ids)->get(['id', 'data'])->toArray();

        if (! $questions) {
            return;
        }

        $question_ids = array_column($questions, 'id');
        $image_ids = array_filter(array_map(function($item) {
            return $item['data']['image_id'] ?? '';
        }, $questions));

        if ($image_ids) {
            $media->removeMedia($image_ids);
        }

        $answer->bulkDelete($question_ids, $media);

        $this->destroy($question_ids);
    }

    /**
     * @param \App\Models\AcademyItem $item
     */
    public function copyToItem(AcademyItem $item)
    {
        $newQuestion = $this->replicate();
        $newQuestion->item_id = $item->id;
        $newQuestion->save();

        $answers = $this->answers()->get()->toArray();
        if($answers) {
            $data = array_map(function($answer) use ($newQuestion) {
                unset($answer['id']);
                $answer['question_id'] = $newQuestion->id;
                return $answer;
            }, $answers);
            $newQuestion->answers()->createMany($data);
        }
    }

    /**
     * @return bool
     */
    public function _canHaveAnswers() {
        return !in_array( $this->type, $this->_no_answer_types);
    }

    /**
     * @param array $q_data
     * @param string $item_type
     * @param \App\Models\Media $media
     * @param int $channelId
     * @return array
     * @throws \Exception
     */
    private function _prepareData(array $q_data, string $item_type, Media $media, int $channelId)
    {
        $data = [];

        $old_image_id = $this->data["image_id"] ?? 0;
        if($this->boolval($q_data['remove_image'])) {
            $data['image_id'] = null;
            $media->removeMedia([$old_image_id]);
        } elseif (! empty($q_data['image'])) {
            $data['image_id'] = $media->updateImage(
                $this->_media_type,
                $q_data['image'],
                $this->id,
                $channelId,
                $old_image_id
            );
        } elseif (! empty($q_data['media_image_id'])) {
            $data['image_id'] = $media->copyImage($this->_media_type, $q_data['media_image_id'], $this->id, $channelId, $old_image_id);
        } else {
            $data['image_id'] = $old_image_id;
        }

        if ('survey' == $item_type) {
            $data['required'] = $this->boolval($q_data['required']);
            if ($this->_canHaveAnswers()) {
                $data['show_alternative'] = $this->boolval($q_data['show_alternative']);
                $data['alternative_label'] = $q_data['alternative_label'] ?? '';
            } else {
                $data['show_alternative'] = false;
                $data['alternative_label'] = '';
            }
        }

        return $data;
    }
}
