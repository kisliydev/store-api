<?php

namespace App\Models;

use App\Http\Requests\Message\ListRequest;
use App\Http\Requests\Message\StoreRequest;
use App\Traits\MessageShortcodes;
use Illuminate\Support\Facades\Auth;
use App\Traits\ThreadListQueryBuilder;
use App\Traits\Notifications;

class Message extends Model
{
    use ThreadListQueryBuilder, MessageShortcodes, Notifications;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'thread_id',
        'content',
        'status',
        'author_id',
        'type',
        'publish_at',
    ];

    /**
     * @var string
     */
    private $_media_type = 'attachments';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function statuses()
    {
        return $this->hasMany(MessageStatus::class);
    }

    /**
     * @param \App\Http\Requests\Message\ListRequest $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getList(ListRequest $request)
    {
        $thread = Thread::find($request->thread_id);
        $query = $this->where('thread_id', $thread->id)->with([
            'attachments.media',
            'author' => $this->getAuthorClosure($thread->channel_id)
        ]);

        $this->addMultiFilters($query, $request->filter);
        $this->addMultiSort($query, $request->sort);

        if ($request->with_pagination) {
            return $this->addPagination($query, $request->query(), $request->per_page);
        }

        return $query->get();
    }

    /**
     * @param \App\Http\Requests\Message\StoreRequest $request
     * @return mixed
     */
    public function createEntity(StoreRequest $request)
    {
        $thread = Thread::find($request->thread_id);
        $authorId = 'admin' == $request->from ? $this->_setMessageAuthorID($thread->channel_id) : Auth::user()->id;
        if ($authorId === 'null' || $authorId === null) {
            return;
        }
        $message = app()->call(self::class . '@add',[[
            'thread_id'   => $request->thread_id,
            'author_id'   => $authorId,
            'content'     => $request->input('content'),
            'from'        => $request->from,
            'attachments' => $request->attachments ?? null
        ]]);

        if ('admin' == $request->from) {
            $thread->sendPushNotification($thread->getRecipientsIDs(), $thread, $message->content);
        } else {
            $thread->sendEmailNotificationToChannelReceivers('open', $thread->channel_id);
        }

        return $this->where('id', $message->id)->with([
            'attachments.media',
            'author' => $this->getAuthorClosure()
        ])->get();
    }

    /**
     * @param \App\Models\Media $media
     * @param \App\Models\GiftCode $giftCode
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function add(Media $media, GiftCode $giftCode, array $data)
    {
        $content = $data['content'];

        if ('admin' == $data['from']) {
            $content = $this->parseShortcodes($giftCode->parseShortcodes($content), $data);
        }

        $message = $this->create([
            'thread_id' => $data['thread_id'],
            'content'   => $content,
            'author_id' => $data['author_id'],
            'type'      => "from_{$data['from']}",
            'publish_at' => $data['publish_at'] ?? null,
        ]);

        $message->statuses()->create([
            'user_id' => $data['author_id'],
            'read_by' => $data['from']
        ]);

        $thread = $message->thread;
        $thread->touch();

        if (!$data['attachments']) {
            return $message;
        }

        $this->_handleAttachments($media, $thread, $message, $data['attachments']);

        return $message;
    }

    /**
     * @param \App\Models\Media $media
     * @param array $data
     * @return mixed
     * @throws \Exception
     */
    public function resave(Media $media, array $data)
    {
        $message = $this->where('thread_id', $data['thread_id'])
            ->orderBy('id', 'desc')
            ->first();

        $message->content = $data['message_content'];
        $message->save();

        $thread = $message->thread;
        $thread->touch();

        if (empty($data['attachments'])) {
            return $message;
        }

        $this->_handleAttachments($media, $thread, $message, $data['attachments']);

        return $message;
    }

    /**
     * @param \App\Models\Media $media
     * @param \App\Models\Thread $thread
     * @param \App\Models\Message $message
     * @param array $attachments
     * @throws \Exception
     */
    private function _handleAttachments(Media $media, Thread $thread, Message $message, array $attachments)
    {
        $new         = [];
        $toBeEdited  = [];
        $tobeRemoved = [];
        $prefix      = time();

        foreach($attachments as $attachment) {
            if (empty($attachment['id'])) {
                $new[] = $attachment;
            } elseif(!empty($attachment['remove'])) {
                $tobeRemoved[] = $attachment['id'];
            } else {
                $toBeEdited[] = $attachment;
            }
        }

        if ($tobeRemoved) {
            Attachment::destroy($tobeRemoved);
        }

        if ($toBeEdited) {
            foreach ($toBeEdited as $key => $attachment) {
                $mediaID = $data = null;

                $instance = Attachment::find($attachment['id']);

                if (!empty($attachment['media'])) {
                    $mediaID = $media->updateAttachment(
                        $this->_media_type,
                        $attachment['media'],
                        "{$prefix}_{$key}_{$message->id}",
                        $thread->channel_id,
                        $instance->media_id ?? null
                    );
                } elseif (!empty($attachment['link'])) {
                    $data = ['link' => $attachment['link']];
                }

                /*
                 * commented due to nuances of frontend logic
                 */
                //if (!$mediaID && !$data) {
                //    Attachment::destroy($attachment['id']);
                //    if ($instance->media_id) {
                //        $media->removeMedia([$instance->media_id]);
                //    }
                //}

                if ($mediaID || $data) {
                    $instance->media_id = $mediaID;
                    $instance->data = $data;
                    $instance->save();
                }
            }
        }

        if ($new) {
            foreach ($new as $key => $attachment) {
                $mediaID = $data = null;
                if (!empty($attachment['media'])) {
                    $mediaID = $media->saveAttachment(
                        $this->_media_type,
                        $attachment['media'],
                        "{$prefix}_{$key}_{$message->id}",
                        $thread->channel_id
                    );
                } elseif (!empty($attachment['link'])) {
                    $data = ['link' => $attachment['link']];
                }

                $message->attachments()->createMany([
                    [
                        'media_id'   => $mediaID,
                        'message_id' => $message->id,
                        'data'       => $data,
                    ],
                ]);
            }
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo('App\Models\User', 'author_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attachments()
    {
        return $this->hasMany('App\Models\Attachment');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function thread()
    {
        return $this->belongsTo('App\Models\Thread');
    }

    /**
     * @param array $messageId
     */
    public function deleteById(array $messageIds)
    {
        $messages = $this->whereIn('id', $messageIds)->get();
        foreach ($messages as $message) {
            $message->delete();
        }

        return $messages;
    }
}
