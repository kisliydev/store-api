<?php

namespace App\Models;

use App\Http\Requests\CountryBrand\CountryBrandGetAllRequest;
use App\Http\Requests\CountryBrand\CountryBrandGetRequest;
use App\Http\Requests\CountryBrand\CountryBrandUpdateRequest;
use App\Http\Requests\Media\CountryBrandListRequest;
use Illuminate\Support\Facades\DB;
use App\Traits\ThreadListQueryBuilder;

/**
 * Class CountryBrand
 *
 * @package App\Models
 */
class CountryBrand extends Model
{
    use ThreadListQueryBuilder;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'brand_id',
        'created_at',
        'name',
    ];

    /**
     * @var string
     */
    protected $default_sort = 'name';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function brand()
    {
        return $this->belongsTo('App\Models\Brand');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function channels()
    {
        return $this->hasMany('App\Models\Channel');
    }

    /**
     * @param \App\Http\Requests\CountryBrand\CountryBrandGetAllRequest $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getList(CountryBrandGetAllRequest $request)
    {
        return $this->_getList($this->where('brand_id', $request->brand_id), $request, [], ['name']);
    }

    /**
     * @param \App\Http\Requests\CountryBrand\CountryBrandGetRequest $request
     * @return mixed
     */
    public function get(CountryBrandGetRequest $request)
    {
        $brand = $this->find($request->id);

        if ($brand) {
            $t = 'threads';
            $m = 'messages';
            $c = 'channels';
            $brand->unread_messages_count = Message::select(DB::raw("COUNT({$m}.id) as count"))
                    ->join($t, "{$t}.id", '=', "{$m}.thread_id")
                    ->join($c, function($join) use ($brand, $c, $t) {
                        $join->on("{$c}.id", "{$t}.channel_id")
                            ->where("{$c}.country_brand_id", $brand->id);
                    })
                    ->whereNotExists($this->getNotReadByAdminClosure())
                    ->groupBy("{$c}.country_brand_id")
                    ->first()->count ?? 0;
        }

        return $brand;
    }

    /**
     * @param \App\Http\Requests\CountryBrand\CountryBrandUpdateRequest $request
     * @return \App\Models\CountryBrand|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     */
    public function resave(CountryBrandUpdateRequest $request)
    {
        $brand = $this->find($request->id);
        $brand->name = $request->name;
        $brand->save();

        return $brand;
    }

    /**
     * @param \App\Http\Requests\CountryBrand\CountryBrandGetRequest $request
     * @return \App\Models\CountryBrand|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     */
    public function remove(CountryBrandGetRequest $request)
    {
        $brand = $this->find($request->id);
        if ($brand) {
            $this->destroy($request->id);
        }

        return $brand;
    }

    /**
     * @param \App\Http\Requests\Media\CountryBrandListRequest $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getListForMedia(CountryBrandListRequest $request)
    {
        return $this->addPagination(
            $this->getMediaListQuery($request),
            $request->query(),
            $request->per_page,
            filter_var($request->simple_paginate, FILTER_VALIDATE_BOOLEAN)
        );
    }

    /**
     * @param \App\Http\Requests\Media\CountryBrandListRequest $request
     * @return mixed
     */
    private function getMediaListQuery(CountryBrandListRequest $request)
    {
        $b = $this->getTable();
        $m = with(new Media)->getTable();
        $c = with(new Channel)->getTable();

        return $this->where("{$b}.brand_id", $request->brand_id)->select(
                "{$b}.name",
                DB::raw("'channels' as next"),
                DB::raw("'brands' as previous"),
                DB::raw("'folder' as type"),
                DB::raw("concat('".url('/api/media/channels?country_brand_id=')."', {$b}.id) as url"),
                DB::raw("count({$m}.id) as items_count")
            )
            ->leftJoin($c, "{$c}.country_brand_id", '=', "{$b}.id")
            ->leftJoin($m, "{$m}.channel_id", '=', "{$c}.id")
            ->groupBy("{$b}.id");
    }
}
