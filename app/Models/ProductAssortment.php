<?php

namespace App\Models;

use App\Http\Requests\ProductAssortment\GetAllRequest;
use App\Http\Requests\ProductAssortment\GetRequest;
use App\Http\Requests\ProductAssortment\LoadRequest;
use App\Http\Requests\ProductAssortment\ExportRequest;
use App\Http\Requests\ProductAssortment\SaveRequest;
use App\Http\Requests\ProductAssortment\UpdateRequest;
use App\Http\Requests\ProductAssortment\BulkActionRequest;
use Excel;
use Maatwebsite\Excel\Collections\RowCollection;
use Illuminate\Support\Facades\Storage;

class ProductAssortment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'channel_id',
        'category_id',
        'name',
        'description',
        'thumbnail_id',
        'sku',
        'link',
        'order',
    ];

    /**
     * @var string
     */
    private $_media_type = 'products';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(ProductAssortmentCategory::class, 'category_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function thumbnail()
    {
        return $this->hasOne('App\Models\Media', 'id', 'thumbnail_id');
    }

    /**
     * @param \App\Http\Requests\ProductAssortment\GetAllRequest $request
     * @return \Illuminate\Contracts\Pagination\Paginator|\Illuminate\Database\Eloquent\Builder
     */
    public function getList(GetAllRequest $request)
    {
        return $this->_getList($this->where('channel_id', $request->channel_id)->orderBy('order', 'asc'), $request, ['category', 'thumbnail'], ['name', 'description', 'sku', 'link']);
    }

    /**
     * @param \App\Http\Requests\ProductAssortment\GetRequest $request
     * @return \App\Models\ProductAssortment|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     */
    public function getItem(GetRequest $request)
    {
        return $this->_getItem($request->id);
    }

    /**
     * @param \App\Http\Requests\ProductAssortment\LoadRequest $request
     * @param \App\Models\Media $media
     * @return array
     */
    public function loadFromExcel(LoadRequest $request, Media $media)
    {
        $ids = [];
        Excel::load($request->items->getRealPath(), function($reader) use (&$ids, $request, $media) {
            $reader->limitRows(config('services.excel.rows_limit'));
            $reader->ignoreEmpty();
            /* if the document has only one sheet */
            if ($reader->all() instanceof RowCollection) {
                $ids = $this->_parseExcelRows($reader, $request, $media);
            } else {
                $reader->each(function ($sheet) use (&$ids, $request, $media) {
                    $data = $this->_parseExcelRows($sheet, $request, $media);
                    if ($data) {
                        $ids = array_merge($ids, $data);
                    }
                });
            }
        });
        return $this->whereIn('id', $ids)->with(['category', 'thumbnail'])->get();
    }

    /**
     * @param \App\Http\Requests\ProductAssortment\ExportRequest $request
     * @return mixed
     */
    public function export(ExportRequest $request)
    {
        return Excel::create('file', function ($excel) use ($request) {
            $excel->sheet('Product Assortment', function ($sheet) use ($request) {
                $i = 1;
                $sheet->row($i, [
                    'name', 'category',
                    'description', 'thumbnail',
                    'sku', 'link', 'order'
                ]);
                $sheet->row($i, function ($row) {
                    $row->setBackground('#cccccc');
                });
                $i++;

                $data = $this->with(['category', 'thumbnail'])->get();
                $data->each(function($product) use (&$i, $sheet) {
                    $sheet->row($i, [
                        $product->name,
                        ($product->category ? $product->category->name : ''),
                        $product->description,
                        ($product->thumbnail ? Storage::url($product->thumbnail->path) : ''),
                        $product->sku,
                        $product->link,
                        $product->order,
                    ]);
                    $i++;
                });
            });
        })->string('xlsx');
    }

    /**
     * @param \App\Http\Requests\ProductAssortment\SaveRequest $request
     * @param \App\Models\Media $media
     * @return \App\Models\ProductAssortment|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     */
    public function add(SaveRequest $request, Media $media)
    {
        $data = $request->validated();
        $item = $this->create($data);

        if (! empty($data['thumbnail'])) {
            $item->thumbnail_id = $media->saveImage($this->_media_type, $data['thumbnail'], $item->id, $item->channel_id);
            $item->save();
        } elseif (! empty($data['media_id'])) {
            $item->thumbnail_id = $media->copyImage($this->_media_type, $data['media_id'], $item->id, $item->channel_id);
            $item->save();
        }

        return $this->_getItem($item->id);
    }

    /**
     * @param \App\Http\Requests\ProductAssortment\UpdateRequest $request
     * @param \App\Models\Media $media
     * @return \App\Models\ProductAssortment|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     */
    public function resave(UpdateRequest $request, Media $media)
    {
        $data = $request->validated();
        $item = $this->find($data['id']);

        $item->name = $data['name'];
        $item->description = $data['description'] ?? '';
        $item->link = $data['link'] ?? '';
        $item->sku = $data['sku'] ?? '';
        $item->order = $data['order'] ?? 0;
        $item->category_id = $data['category_id'];

        if ($this->boolval($data['remove_thumbnail'])) {
            $media->removeMedia([$item->thumbnail_id]);
            $item->thumbnail_id = null;
        } elseif (! empty($data['thumbnail'])) {
            $item->thumbnail_id = $media->updateImage($this->_media_type, $data['thumbnail'], $item->id, $item->channel_id, $item->thumbnail_id);
        } elseif(! empty($data['media_id'])) {
            $item->thumbnail_id = $media->copyImage($this->_media_type, $data['media_id'], $item->id, $item->channel_id, $item->thumbnail_id);
        }
        $item->save();

        return $this->_getItem($item->id);
    }

    /**
     * @param \App\Http\Requests\ProductAssortment\GetRequest $request
     * @param \App\Models\Media $media
     * @return mixed
     */
    public function remove(GetRequest $request, Media $media)
    {
        $item = $this->_getItem($request->id);
        if ($item) {
            if ($item->thumbnail_id) {
                $media->removeMedia([$item->thumbnail_id]);
            }
            $this->destroy($request->id);
        }

        return $item;
    }

    /**
     * @param \App\Http\Requests\ProductAssortment\BulkActionRequest $request
     * @param \App\Models\Media $media
     * @return bool
     */
    public function bulkAction(BulkActionRequest $request, Media $media)
    {
        $items = $this->whereIn('id', $request->id)->with('thumbnail')->get();

        if (! $items->all()) {
            return false;
        }

        switch($request->action) {
            case 'delete':
                $imageIDs = array_filter(array_column($items->toArray(), 'thumbnail_id'));
                if ($imageIDs) {
                    $media->removeMedia($imageIDs);
                }
                $this->destroy($request->id);
                break;
            default:
                return false;
        }

        return $items;
    }

    /**
     * @param int $itemID
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     */
    private function _getItem(int $itemID)
    {
        return $this->with(['thumbnail', 'category'])->find($itemID);
    }

    /**
     * @param $item
     * @param \App\Http\Requests\ProductAssortment\LoadRequest $request
     * @param \App\Models\Media $media
     * @return array
     */
    private function _parseExcelRows($item, LoadRequest $request, Media $media)
    {
        $ids = $categories = [];
        $item->each(function($row) use (&$ids, &$categories, $request, $media) {
            $productData = $row->all();

            if (empty($productData['name']) || $this->where('channel_id', $request->channel_id)->where('name', $productData['name'])->first()) {
                return;
            }

            $category = $productData['category'] ?? 'uncategorized';
            if (! array_key_exists($category, $categories)) {
                $categories[$category] = ProductAssortmentCategory::firstOrCreate([
                    'channel_id' => $request->channel_id,
                    'name' => $category,
                ]);
            }

            $product = $this->create([
                'channel_id' => $request->channel_id,
                'category_id' => $categories[$category]->id,
                'name' => $productData['name'],
                'description' => $productData['description'] ?? '',
                'sku' => $productData['sku'] ?? '',
                'link' => $productData['link'] ?? '',
                'order' => abs(intval($productData['order'])) ?? 0,
            ]);

            if (! empty($productData['thumbnail'])) {
                $product->thumbnail_id = $media->saveImageFromUrl($this->_media_type, $productData['thumbnail'], $product->id, $product->channel_id);
                $product->save();
            }
            $ids[] = $product->id;
        });
        return $ids;
    }
}
