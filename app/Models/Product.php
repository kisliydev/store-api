<?php

namespace App\Models;

use App\Http\Requests\Product\ProductGetAllRequest;
use App\Http\Requests\Product\ProductGetRequest;
use App\Http\Requests\Product\ProductLoadRequest;
use App\Http\Requests\Product\ProductSaveRequest;
use App\Http\Requests\Product\ProductUpdatePositionRequest;
use App\Http\Requests\Product\ProductUpdateRequest;
use App\Http\Requests\Product\ProductBulkActionRequest;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Collections\RowCollection;
use Maatwebsite\Excel\Facades\Excel;

class Product extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'channel_id',
        'text_before_amount',
        'text_after_amount',
        'default_amount',
        'points',
        'use_weight_points',
        'position',
        'order',
        'created_at',
    ];

    /**
     * @var string
     */
    private $_media_type = 'assortment';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function channel()
    {
        return $this->belongsTo('App\Models\Channel');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function thumbnail()
    {
        return $this->hasOne('App\Models\Media', 'id', 'thumbnail_id');
    }

    /**
     * @param \App\Http\Requests\Product\ProductGetAllRequest $request
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function getList(ProductGetAllRequest $request)
    {
        return $this->_getListByChannel($request, ['thumbnail'], ['name']);
    }

    /**
     * @param \App\Http\Requests\Product\ProductGetRequest $request
     * @return mixed
     */
    public function get(ProductGetRequest $request)
    {
        return $this->_getProduct($request->id);
    }

    /**
     * @param \App\Http\Requests\Product\ProductSaveRequest $request
     * @return \App\Models\Product|bool|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     */
    public function add(ProductSaveRequest $request)
    {
        $data = $request->validated();

        if (! Channel::find($data['channel_id'])) {
            return false;
        }

        $data['use_weight_points'] = ! empty($data['use_weight_points']) && filter_var($data['use_weight_points'], FILTER_VALIDATE_BOOLEAN);

        $product = $this->create($data);

        return $this->_getProduct($product->id);
    }

    /**
     * @param \App\Http\Requests\Product\ProductUpdateRequest $request
     * @return \App\Models\Product|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     */
    public function resave(ProductUpdateRequest $request)
    {
        $product = $this->_getProduct($request->id);
        if ($product) {
            $product->name = $request->name;
            $product->text_before_amount = $request->text_before_amount;
            $product->text_after_amount = $request->text_after_amount;
            $product->default_amount = $request->default_amount;
            $product->points = $request->points;
            $product->use_weight_points = !empty($request->use_weight_points) && filter_var($request->use_weight_points, FILTER_VALIDATE_BOOLEAN);
            $product->order = abs(intval($request->order));
            $product->save();
        }

        app()['cache']->forget("product_{$product->id}");

        return $product;
    }

    /**
     * @param \App\Http\Requests\Product\ProductGetRequest $request
     * @return \App\Models\Product|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     */
    public function remove(ProductGetRequest $request)
    {
        $product = $this->_getProduct($request->id);
        if ($product) {
            $this->destroy($request->id);
        }

        return $product;
    }

    /**
     * @param \App\Http\Requests\Product\ProductBulkActionRequest $request
     * @return bool
     */
    public function bulkAction(ProductBulkActionRequest $request) {
        $products = $this->whereIn('id', $request->id)->with('thumbnail')->get();

        if (! $products->all()) {
            return false;
        }

        switch($request->action) {
            case 'delete':
                DB::table('points_statistics')->whereIn('object_id', $request->id)->update(['object_id' => 0]);
                $this->destroy($request->id);
                break;
            default:
                return false;
        }

        return $products;
    }

    /**
     * @param \App\Http\Requests\Product\ProductUpdatePositionRequest $request
     */
    public function updateProductPosition(ProductUpdatePositionRequest $request)
    {
        foreach ($request->products as $item) {
            $this->where([
                'id' => $item['id'],
                'channel_id' => $request->channel_id
            ])->update(['position' => $item['position']]);
        }
    }

    /**
     * @param \App\Http\Requests\Product\ProductLoadRequest $request
     * @return array|bool
     */
    public function excelUpload(ProductLoadRequest $request)
    {
        $products = [];
        Excel::load($request->products->getRealPath(), function ($reader) use (&$products, $request) {
            $reader->limitRows(config('services.excel.rows_limit'));
            $reader->ignoreEmpty();
            if ($reader->all() instanceof RowCollection) {
                $products = $this->parseExcel($reader, $request);
            } else {
                $reader->each(function ($sheet) use (&$products, $request) {
                    $data = $this->parseExcel($sheet, $request);
                    if ($data) {
                        $products = array_merge($products, $data);
                    }
                });
            }
        });

        return $products;
    }

    /**
     * @param $item
     * @param \App\Http\Requests\Product\ProductLoadRequest $request
     * @return array
     */
    private function parseExcel($item, ProductLoadRequest $request)
    {
        $productIds = [];
        $item->each(function ($row) use (&$productIds, $request) {
            $productsData = $row->all();
            if (empty($productsData['name']) ||
                $this->where(['channel_id' => $request->channel_id, 'name' => $productsData['name']])->first()
            ) {
                return;
            }
            $points = isset($productsData['points']) && is_numeric($productsData['points']) ? $productsData['points'] : 0;
            $data = [
                'channel_id' => $request->channel_id,
                'name' => $productsData['name'] ?? null,
                'text_before_amount' => $productsData['description'] ?? null,
                'points' => $points,
            ];
            $product = $this->create($data);
            $productIds[] = $product->id;
        });

        return $this->_getProduct($productIds);
    }

    /**
     * @param int|array $product_id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     */
    private function _getProduct($product_id)
    {
        return $this->with('thumbnail')->find($product_id);
    }
}
