<?php

namespace App\Models;

use App\Http\Requests\ManualPoint\ManualPointGetListRequest;
use App\Http\Requests\ManualPoint\ManualPointGetRequest;
use App\Http\Requests\ManualPoint\ManualPointBulkActionRequest;

class ManualPoint extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'channel_id',
        'title',
        'description',
        'points',
        'recipients',
    ];

    /**
     * @param \App\Http\Requests\ManualPoint\ManualPointGetListRequest $request
     * @return \Illuminate\Contracts\Pagination\Paginator|\Illuminate\Database\Eloquent\Builder
     */
    public function getList(ManualPointGetListRequest $request)
    {
        return $this->_getList($this->where('channel_id', $request->channel_id), $request, [], ['title', 'description']);
    }

    /**
     * @param \App\Http\Requests\ManualPoint\ManualPointGetRequest $request
     * @return mixed
     */
    public function remove(ManualPointGetRequest $request)
    {
        $point = $this->find($request->id);
        $this->destroy($request->id);

        $this->_clearStatistics([$request->id]);

        return $point;
    }

    /**
     * @param \App\Http\Requests\ManualPoint\ManualPointBulkActionRequest $request
     * @return bool
     */
    public function bulkAction(ManualPointBulkActionRequest $request)
    {
        $points = $this->whereIn('id', $request->id)->get();

        if (! $points->all()) {
            return false;
        }

        switch($request->action) {
            case 'delete':
                $this->destroy($request->id);
                $this->_clearStatistics($request->id);
                break;
            default:
                return false;
        }

        return $points;
    }

    /**
     * @param array $ids
     * @return mixed
     */
    private function _clearStatistics(array $ids)
    {
        return PointsStatistic::where('object_type', 'manual')->whereIn('object_id', $ids)->delete();
    }
}
