<?php

namespace App\Models;

use App\Http\Requests\Channel\ChannelGetByCountryBrandRequest;
use App\Http\Requests\Channel\ChannelGetRequest;
use App\Http\Requests\Channel\ChannelSaveRequest;
use App\Http\Requests\Channel\ChannelUpdateRequest;
use App\Http\Requests\Channel\ChannelSaveMetaRequest;
use App\Http\Requests\Channel\ChannelLoadLocalizationRequest;
use App\Http\Requests\GeneralReport\InfoRequest;
use App\Http\Requests\Media\ChannelsListRequest;
use App\Http\Requests\MessagesSettings\StoreRequest;
use App\Http\Requests\MessagesSettings\ShowRequest;
use App\Transformers\User\ChannelUsersTransformer;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use App\Traits\ThreadListQueryBuilder;
use App\Traits\ArrayData;
use Excel;

/**
 * Class Channel
 *
 * @package App\Models
 *
 */
class Channel extends Model
{
    use SoftDeletes;
    use ThreadListQueryBuilder;
    use ArrayData;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'app_settings',
        'app_localization',
        'messages_settings',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'app_settings' => 'json',
        'app_localization' => 'json',
        'messages_settings' => 'json',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'country_brand_id',
        'invite_code',
        'created_at',
        'started_at',
        'finished_at',
        'deleted_at',
        'app_settings',
        'app_localization',
        'messages_settings',
        'store_name',
        'store_legal_name',
        'store_owner',
    ];

    /**
     * @param \App\Http\Requests\GeneralReport\InfoRequest $request
     * @param \App\Transformers\User\ChannelUsersTransformer $transformer
     * @return array
     */
    public function getChannelInfo(InfoRequest $request, ChannelUsersTransformer $transformer)
    {
        return [
            'channel' => $this->getPushSettings(['channel_id' => $request->channel_id]),
            'admins' => $transformer->transform($this->getChannelModeratorsIDs($request))->first(),
        ];
    }

    /**
     * @param \App\Http\Requests\GeneralReport\InfoRequest $request
     * @return mixed
     */
    private function getChannelModeratorsIDs(InfoRequest $request)
    {
        return $this->where('id', $request->channel_id)->with([
            'countryBrand.brand.brandUsers' => function ($query) {
                $query->select('brand_users.brand_id', 'users.id', 'users.first_name', 'users.last_name',
                    'users.email', 'users.phone', 'users.skype');
                $query->leftJoin('model_has_roles', 'brand_users.user_id', '=', 'model_has_roles.model_id');
                $query->leftJoin('roles', 'model_has_roles.role_id', '=', 'roles.id');
                $query->where(function ($subQuery) {
                    $subQuery->where('roles.name', 'global_manager');
                    $subQuery->orWhere('roles.name', 'brand_admin');
                    $subQuery->orWhere('roles.name', 'brand_manager');
                });
                $query->leftJoin('users', 'brand_users.user_id', '=', 'users.id');
                $query->distinct();
            }
        ])->get();
    }

    /**
     * @param \App\Http\Requests\Media\ChannelsListRequest $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getListForMedia(ChannelsListRequest $request)
    {
        return $this->addPagination(
            $this->getMediaListQuery($request),
            $request->query(),
            $request->per_page,
            filter_var($request->simple_paginate, FILTER_VALIDATE_BOOLEAN)
        );
    }

    /**
     * @param \App\Http\Requests\Media\ChannelsListRequest $request
     * @return mixed
     */
    private function getMediaListQuery(ChannelsListRequest $request)
    {
        $c = $this->getTable();
        $m = with(new Media)->getTable();

        return $this->select(
            "{$c}.name",
            DB::raw("'channel' as next"),
            DB::raw("'country_brands' as previous"),
            DB::raw("'folder' as type"),
            DB::raw("concat('".url('/api/media/channel?channel_id=')."', {$c}.id) as url"),
            DB::raw("count({$m}.id) as items_count")
        )
            ->where('country_brand_id', $request->country_brand_id)
            ->leftJoin($m, "{$m}.channel_id", '=', "{$c}.id")
            ->groupBy("{$c}.id");
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function countryBrand()
    {
        return $this->belongsTo(CountryBrand::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function districts()
    {
        return $this->hasMany(District::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function dealers()
    {
        return $this->hasMany(Dealer::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function positions()
    {
        return $this->hasMany(Position::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function quizzes()
    {
        return $this->hasMany(AcademyItem::class)->where('type', 'quiz');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function surveys()
    {
        return $this->hasMany(AcademyItem::class)->where('type', 'survey');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function productAssortment()
    {
        return $this->hasMany(ProductAssortment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function levels()
    {
        return $this->hasMany(Level::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function threads()
    {
        return $this->hasMany(Thread::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * @see self::getList()
     */
    public function currentCampaign()
    {
        return $this->hasMany(Campaign::class)->current()
            ->toHasOne('channel_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function unreadMessages()
    {
        return $this->hasManyThrough(Message::class,Thread::class)
            ->whereNotExists($this->getNotReadByAdminClosure());
    }

    /**
     * @param array $push
     * @return mixed
     */
    public function getPushSettings(array $push)
    {
        return $this->where('id', $push['channel_id'])->with('countryBrand.brand')->first();
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelGetByCountryBrandRequest $request
     * @return \Illuminate\Contracts\Pagination\Paginator|\Illuminate\Database\Eloquent\Builder
     */
    public function getList(ChannelGetByCountryBrandRequest $request)
    {
        $query = $this->where('country_brand_id', $request->country_brand_id);
        $with = [
            'unreadMessages' => function($query) {
                $query->select(DB::raw("COUNT(`messages`.`id`) as `count`"))
                    ->groupBy('threads.channel_id');
            },
            'currentCampaign'
        ];
        return $this->_getList($query, $request, $with, ['name']);
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelGetRequest $request
     * @return mixed
     */
    public function getItem(ChannelGetRequest $request)
    {
        return $this->with([
            'currentCampaign',
            'countryBrand.brand.logo',
            'unreadMessages' => function($query) {
            $query->select(DB::raw("COUNT(`messages`.`id`) as `count`"))
                ->groupBy('threads.channel_id');
        }])->where('id', $request->id)->first();
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelSaveMetaRequest $request
     * @return array|bool
     */
    public function saveMeta(ChannelSaveMetaRequest $request)
    {
        $channel = $this->find($request->id);

        $key = $request->key;

        /* remove those fields that don't supposed to be saved  */
        $newMeta = array_intersect_key($request->data, $this->getDefaultMeta($key));

        if (!$newMeta) {
            return false;
        }
        $oldMeta = $channel->$key ?? [];

        $channel->$key = $this->replaceRecursive($oldMeta, $newMeta);
        $channel->$key = $this->fillRecursive($channel->$key, $this->getDefaultMeta($key));
        $channel->save();

        $data = [$key => $channel->$key];

        return $data;
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelSaveRequest $request
     * @param \App\Models\User $user
     * @param \App\Models\Campaign $campaign
     * @return mixed
     * @throws \Exception
     */
    public function add(ChannelSaveRequest $request, User $user, Campaign $campaign)
    {
        $data = $request->validated();
        if (empty($data['localization_id'])) {
            $localization = $this->getDefaultAppLocalization();
        } else {
            $localization = Localization::find($data['localization_id'])->translations;
            unset($data['localization_id']);
        }
        $data['app_localization'] = $localization;

        $channel = $this->create([
            'country_brand_id' => $data['country_brand_id'],
            'name' => $data['name'],
            'invite_code' => $data['invite_code'],
            'app_settings' => $this->getDefaultAppSettings(),
            'app_localization' => $localization,
            'messages_settings' => $this->getDefaultMessagesSettings(),
            'store_name' => $data['store_name'] ?? '',
            'store_legal_name' => $data['store_legal_name'] ?? '',
            'store_owner' => $data['store_owner'] ?? '',
        ]);
        $user->addHeadQuorterUsers($channel->id);
        $campaign->create([
            'channel_id' => $channel->id,
            'name' => $data['campaign_name'],
            'started_at' => $data['started_at'],
            'sale_started_at' => $data['sale_started_at'],
            'sale_finished_at' => $data['sale_finished_at'],
            'finished_at' => $data['finished_at'],
            'app_layout' => $campaign->getDefaultAppLayout(),
        ]);

        return $this->where('id', $channel->id)->with('currentCampaign')->first();
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelGetRequest $request
     * @return mixed
     */
    public function duplicate(ChannelGetRequest $request)
    {
        $channel = $this->withTrashed()->find($request->id);
        $newChannel = $channel->replicate();
        $newChannel->name = preg_replace('/^((.?)*)(\sCOPY)$/', '$1', $newChannel->name)." COPY";
        $newChannel->invite_code = $this->getInviteCode();
        $newChannel->save();

        if ($newChannel->trashed()) {
            $newChannel->restore();
        }

        $relations = [
            'products',
            'quizzes',
            'surveys',
            'productAssortment',
            'levels',
            'currentCampaign'
        ];

        foreach($relations as $relation) {
            $items = $channel->$relation()->get();
            if ($items) {
                if (in_array($relation, ['quizzes','surveys'])) {
                    $items->each(function($item) use ($newChannel) {
                        $item->copyToChannel($newChannel);
                    });
                } else {
                    $data = array_map(function($item) use ($newChannel) {
                        unset($item['id']);
                        $item['channel_id'] = $newChannel->id;
                        return $item;
                    }, $items->toArray());
                    $newChannel->$relation()->createMany($data);
                }
            }
        }

        return $newChannel;
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelUpdateRequest $request
     * @return mixed
     */
    public function resave(ChannelUpdateRequest $request)
    {
        $channel = $this->find($request->id);
        if ($channel) {
            $channel->name             = $request->name;
            $channel->store_name       = $request->store_name ?? null;
            $channel->store_legal_name = $request->store_legal_name ?? null;
            $channel->store_owner       = $request->store_owner ?? null;
            if (strcmp($channel->invite_code, $request->invite_code) !== 0) {
                $channel->invite_code = $this->_getUniqueInviteCode($request->invite_code);
            }
            $channel->save();
        }

        return $channel;
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelLoadLocalizationRequest $request
     * @return bool
     */
    public function loadLocalization(ChannelLoadLocalizationRequest $request)
    {
        $channel = $this->find($request->id);
        if (! $channel) {
            return false;
        }

        $data = $this->parseLocalizationFile($request->localization);
        if (! $data) {
            return false;
        }

        $old_data = $channel->app_localization;
        $channel->app_localization = array_merge($old_data, $data);
        $channel->save();

        return $channel->app_localization;
    }

    /**
     * @param \Illuminate\Http\UploadedFile $file
     * @return array|bool
     */
    public function parseLocalizationFile(UploadedFile $file)
    {
        $rows = Excel::load($file->getRealPath(), function ($reader) {
            $reader->limitRows(config('services.excel.rows_limit'));
            $reader->ignoreEmpty();
        })->get();

        if (! $rows) {
            return false;
        }

        $data = [];
        foreach ($rows as $item) {

            $item_data = $item->all();

            if (empty($item_data['key'])) {
                continue;
            }

            $data[$item_data['key']] = $item_data['value'];
        }

        /* remove those fields that don't supposed to be saved  */
        return array_intersect_key($data, $this->getDefaultAppLocalization());
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelGetRequest $request
     * @return mixed
     */
    public function exportLocalization(ChannelGetRequest $request)
    {
        $channel = $this->find($request->id)->makeVisible(['app_localization']);
        $data = $channel->app_localization;
        return Excel::create('file', function ($excel) use ($data) {
            $excel->sheet('Localization', function ($sheet) use ($data) {
                $i = 1;
                $sheet->row($i, ['key', 'value']);
                $sheet->row($i, function ($row) {
                    $row->setBackground('#cccccc');
                });
                $i++;

                foreach ($data as $key => $value) {
                    $sheet->row($i, [$key, $value]);
                    $i++;
                }
            });
        })->string('xlsx');
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelGetRequest $request
     * @return mixed
     */
    public function trash(ChannelGetRequest $request)
    {
        $channel = $this->find($request->id);

        if ($channel) {
            $channel->delete();
        }

        app()['cache']->forget("channel:{$channel->id}");

        return $channel;
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelGetRequest $request
     * @return mixed
     */
    public function restoreFromTrash(ChannelGetRequest $request)
    {
        $channel = $this->withTrashed()->find($request->id);

        if ($channel) {
            $channel->restore();
        }

        return $channel;
    }

    /**
     * @param \App\Http\Requests\Channel\ChannelGetRequest $request
     * @return mixed
     */
    public function remove(ChannelGetRequest $request)
    {
        $channel = $this->withTrashed()->find($request->id);
        if ($channel) {
            if ($channel->trashed()) {
                $channel->forceDelete();
            } else {
                $this->destroy($request->id);
            }
        }

        app()['cache']->forget("channel:{$channel->id}");

        return $channel;
    }

    /**
     * @return string
     */
    public function getInviteCode() {
        return $this->_getUniqueInviteCode(str_random(6));
    }

    /**
     * @param string $code
     * @return string
     */
    private function _getUniqueInviteCode(string $code) {
        $codes = $this->pluck('invite_code')->all();

        $found = false;

        while (! $found) {
            if (! in_array($code, $codes)) {
                $found = true;
            } else {
                $code = str_random(6);
            }
        }

        return $code;
    }

    /**
     * @param string $key
     * @return mixed
     */
    public function getDefaultMeta(string $key)
    {
        $callback = 'getDefault' . str_replace( " ", "",ucwords(str_replace("_", " " , $key)));
        return $this->$callback();
    }

    /**
     * @param int $id
     * @return string
     */
    public function getChannelName(int $id): string
    {
        return $this->find($id)->name;
    }

    /**
     * @return array
     */
    public function getDefaultAppSettings()
    {
        return [
            "in_app_popup_push_notification_window" => false,
            "messages_write_message_to_name" => "DEMO APP", // не используется в приложении
            "messages_write_message_to_email" => "store@store.no", // возможность отправки сообщений в приложении забанена
            "push_prompt_msg_limit" => "250",
            "stats_your_overview_surveys_append" => "surveys",
            "stats_your_overview_queries_append" => "queries",
            "dual_store_rankings" => "avp",
            "enable_sorting_of_dealers_by_average_points" => false,
            "disable_push_notification_in_app" => false,

            /* Хрень какая-то для вывода статистики */
            "stats_your_overview_other_append" => "extra",
            "stats_your_overview_points" => "points",
            "your_overview_quizzes" => "quizes",
            "stats_your_overview_quizzes_append" => "quizes",
        ];
    }

    /**
     * @return array
     * @todo  !!!!!!!!! in case if you need to add new key to the app localization !!!!!!!!!!!!!
     *        !!!!!!!!! it is necessary to add appropriate field with the label to !!!!!!!!!!!!!
     *        !!!!!!!!!    \App\Transformers\AppLocalizationListTransformer        !!!!!!!!!!!!!
     */
    public function getDefaultAppLocalization()
    {
        return [
            /*** Invite Code Input Page ***/
            'invite_code_page_title' => 'Code',
            'invite_code_page_text' => 'Please enter your code',
            'invite_code_page_button_text' => 'Continue',

            /*** Select Login Method Page ***/
            'sign_in_method_page_title' => 'Log in',
            'sign_in_method_page_text' => 'Please sign in to get the full experience',
            'sign_in_method_page_welcome_text' => 'Welcome',
            'sign_in_method_page_facebook_button_text' => 'Log in with Facebook',
            'sign_in_method_page_google_button_text' => 'Log in with Google',
            'sign_in_method_page_email_button_text' => 'Log in with Email',
            'sign_in_method_page_sign_up_text' => "I don't have an account.",
            'sign_in_method_page_sign_up_link_text' => "Sign up",

            /*** Email/Password Login Page ***/
            'sign_in_page_title' => 'Log in',
            'sign_in_page_text' => 'Please log in with your email',
            'sign_in_page_email_label' => 'Email',
            'sign_in_page_password_label' => 'Password',
            'sign_in_page_forgot_password_link_text' => 'Forgot your password?',
            'sign_in_page_button_text' => 'Log in',
            'sign_in_page_no_email_verification' => 'You haven\'t verified your account yet. But you can send the verification email again.',

            /***  **/
            'signin_error_non_existing_social_account' => "There is no an account with such credential",

            /*** Sign Up Page ***/
            'sign_up_method_page_title' => 'Sign up',
            'sign_up_method_page_text' => 'Please sign up to get the full experience',
            'sign_up_method_page_facebook_button_text' => 'Sign up with Facebook',
            'sign_up_method_page_google_button_text' => 'Sign up with Google',
            'sign_up_method_page_email_button_text' => 'Sign up with Email',
            'sign_up_method_page_sign_in_text' => "Already have an account?",
            'sign_up_method_page_sign_in_link_text' => "Log in",
            'no_dealers_and_positions' => 'There is no set up dealer and position yet. Please contact your dealer or manager.',

            /*** Sign Up: Email/Password Page ***/
            'sign_up_email_password_page_title' => 'Sign up',
            'sign_up_email_password_page_email_label' => 'Email',
            'sign_up_email_password_page_password_label' => 'Password',
            'sign_up_email_password_page_confirm_password_label' => 'Confirm password',
            'sign_up_email_password_page_button_test' => 'Next step',
            'sign_up_email_password_page_text' => 'Please sign up with your email',

            /*** Sign Up: Set Avatar page ***/
            'sign_up_set_avatar_page_text' => 'Please upload your profile photo',
            'sign_up_set_avatar_page_or' => 'or',
            'sign_up_set_avatar_page_use_camera_link_text' => 'I want to use my camera to take new photo',
            'sign_up_set_avatar_page_button_text' => 'Next step',

            /*** Sign Up: User Data Page ***/
            'sign_up_user_data_page_title' => 'Please enter your information',
            'sign_up_user_data_page_eula_text' => 'Yes, I agree to',
            'sign_up_user_data_page_eula_link_text' => 'terms and conditions',
            'sign_up_user_data_page_button_text' => 'Next step',

            /*** Sign Up: Congratulations Page ***/
            'sign_up_congratulations_page_title' => 'Congratulations!',
            'sign_up_congratulations_page_text' => 'You are registered now. To confirm your account please follow the link in the email that you will receive form us',
            'sign_up_congratulations_page_button_text' => 'Continue!',

            /*** Sign Out ***/
            'sign_out_button_text' => "Log Out",
            'sign_out_confirm_text' => "You are signing out, correct?",

            /*** EULA Page ***/
            'eula_page_title' => 'End User License Agreement',
            'eula_page_buton_text' => 'Ok, I got it!',

            /*** Forgot Password Page ***/
            'forgot_password_page_title' => 'Forgot password?',
            'forgot_password_page_text' => "Please enter your email address and we'll get you back on track",
            'forgot_password_page_email_label' => "Email",
            'forgot_password_page_button_text' => "Submit",
            'forgot_password_page_sign_in_link_text' => 'Log in',
            'forgot_password_page_success_title' => "Congratulations!",
            'forgot_password_page_success_text' => "We've sent you a confirm code on your email. Please enter it to reset your password",
            'forgot_password_page_sign_in_text' => 'Return to',

            /*** New Password page ***/
            'new_password_page_title' => 'New Password',
            'new_password_page_text' => 'Please enter your new pasword',
            'new_password_page_password_label' => 'New password',
            'new_password_page_confirm_password_label' => 'Confirm password',
            'new_password_page_button_text' => 'Set new password',
            'new_password_page_success_button_text' => 'Log in',
            'new_password_page_success_text' => 'Your new password was saved. Please log in in the application with your new password',
            'new_password_page_success_title' => 'Congratulations!',

            /*** Main Page ***/
            'main_page_contest_sup_link_text' => 'Register your sale',
            'main_page_contest_link_text' => 'Contest',
            'main_page_contest_description' => 'Win great prizes',
            'main_page_brand_academy_link_text' => 'Brand academy',
            'main_page_brand_academy_description' => 'Be a smartest sale person',
            'main_page_messages_link_text' => 'Messages',
            'main_page_messages_description' => 'Contact your marketing department',

            /*** Header ***/
            'header_tooltip_title' => 'Help',
            'header_tooltip_text' => 'We need your help',
            'header_tooltip_close_button_text' => 'Ok',

            /*** Main Menu ***/
            "main_menu_home_link_text" => "Home",
            "main_menu_rules_link_text" => "Prizes & rules",
            "main_menu_contest_link_text" => "Contest",
            "main_menu_brand_academy_link_text" => "Brand academy",
            "main_menu_messages_link_text" => "Messages",
            "main_menu_manage_store_link_text" => "Manage store",
            "main_menu_statistics_link_text" => "Results",
            "main_menu_profile_link_text" => "My profile",
            "join_channel_button_text" => "Join new Channel",

            /*** Footer ***/
            'my_points_label' => 'My points',

            /*** Contest Page ***/
            'contest_page_sale_scheduled_text' => "Contest starts in [{1} days]",
            'contest_page_sale_started_text' => "Contest finishes in [{1} days]",
            'contest_page_sale_finished_text' => "Contest finished [{1} days] ago",

            'contest_page_title' => "Contest",
            'contest_page_new_points_label' => "New points",
            'contest_page_points_measure' => "points per sale",
            'contest_page_register_button_text' => "Register sale",
            'contest_page_update_button_text' => "Update",
            'contest_page_archive_button_text' => 'Archive',
            'contest_success_register_points_title' => "Congratulation",
            'contest_success_register_points_text' => 'New points:',
            'yes_button_text' => "yes",
            'no_button_text' => "no",

            /*** Contest Page: archive ***/
            'contest_page_archive_button_text' => 'Archive',
            'contest_page_archive_edit_link_text' => "Edit",
            'contest_page_archive_points_measure' => "points",
            'contest_item_is_removed' => "items has been removed",

            /*** Manage Store Page **/
            'manage_store_page_points_measure' => 'p',
            'manage_store_page_dealer_rank_label' => 'Store rank',
            'manage_store_page_users_count_label' => 'Employees',
            'manage_store_page_quizzes_stat_label' => 'Quiz',
            'manage_store_page_surveys_stat_label' => 'Survey',
            'manage_store_page_manual_stat_label' => 'Manuals',
            'manage_store_page_products_stat_label' => 'Sales',
            'manage_store_page_extra_stat_label' => 'Extra',
            'manage_store_page_level_stat_label' => 'Levels',
            'manage_store_page_invite_users_button_text' => 'Invite users',
            'manage_store_page_new_message_button_text' => 'New Message',
            'manage_store_page_remove_users_button_text' => 'Remove users',
            'manage_store_page_users_list_label' => 'My employees',
            'manage_store_page_remove_users_success_text' => 'Thank you, your request was sent to the Admin. He will contact you shortly.',
            'manage_store_page_success_removing_users_button_text' => 'OK',

            /*** Brand Academy Page ***/
            'brand_academy_page_quiz_label' => 'Quiz',
            'brand_academy_page_survey_label' => 'Survey',
            'brand_academy_page_product_assortment_label' => 'Products',
            'brand_academy_page_progress_label' => 'Progress',
            'brand_academy_page_back_button_label' => 'Back to brand academy',
            'brand_academy_page_retry_button_text' => 'Retry Quiz',
            'brand_academy_page_check_answer_button_text' => 'Check Your Answer',
            'brand_academy_page_survey_answer_input_placeholder' => 'Enter your answer',
            'brand_academy_page_back_to_survey_button_text' => 'Back to Survey',
            'brand_academy_page_back_to_quiz_button_text'   => 'Back to Quiz',
            'brand_academy_page_right_answer' => 'Right Answer',
            'brand_academy_page_wrong_answer' => 'Wrong Answer',
            'brand_academy_page_finish_quiz_button_text' => 'Finish',

            /*** Product Assortment Page ***/
            'product_assortment_page_sku_label' => 'Art, ID',
            'product_assortment_page_read_more_link_text' => 'Read More',

            /*** Users Removal pop-up ***/
            'users_removal_pop_up_title' => 'Choose the reason for removal',
            'users_removal_pop_up_reason_quit_text' => 'The person has quit',
            'users_removal_pop_up_reason_unknown_text' => "I don't know this person",
            'users_removal_pop_up_reason_other_text' => "Other",
            'users_removal_pop_up_reason_other_placeholder' => "Enter your reason",
            'users_removal_pop_up_confirm_button_text' => 'Remove',
            'users_removal_pop_up_cancel_button_text' => 'Cancel',

            /*** Invite Users pop-up ***/
            'invite_users_pop_up_title' => 'Invite users',
            'invite_users_pop_up_text' => 'Make sure all your staff are in.',
            'invite_users_pop_up_copy_button_text' => 'Copy SMS text',
            'invite_users_pop_up_cancel_button_text' => 'Cancel',

            /*** Users List Page ***/
            'users_list_page_channel_tab_label' => 'Global',
            'users_list_page_dealer_tab_label' => 'In my store',

            /*** Dealers List Page ***/
            'dealers_list_page_total_tab_label' => 'Total',
            'dealers_list_page_average_tab_label' => 'Average',

            "my_rank_results_global_tab"    => "[Global {position} of {total}]",
            "my_rank_results_store_tab"     => "[In My Store {position} of {total}]",
            "store_rank_results_global_tab" => "[Total {position} of {total}]",
            "store_rank_results_store_tab"  => "[Average {position} of {total}]",
            "store_rank_results_global_tab_common" => "Global",
            "store_rank_results_store_tab_average" => "Average",

            /*** User's Stat Page ***/
            'user_stat_page_my_rank_label' => 'My rank',
            'user_stat_page_my_points_label' => 'My points',
            'user_stat_page_users_list_link_text' => 'My rank',
            'user_stat_page_dealers_list_link_text' => 'Store rank',
            'user_stat_page_rank_label' => 'Rank',
            'user_stat_page_points_label' => 'Points',
            'user_stat_page_quizzes_stat_label' => 'Quizzes',
            'user_stat_page_surveys_stat_label' => 'Surveys',
            'user_stat_page_manual_stat_label' => 'Manuals',
            'user_stat_page_products_stat_label' => 'Sales',
            'user_stat_page_level_stat_label' => 'Levels',
            'user_stat_page_extra_stat_label' => 'Extra',

            /*** Dealer's Page ***/
            'dealer_page_rank_label' => 'Store rank',
            'dealer_page_points_label' => 'Store points',
            'dealer_page_users_count_label' => 'Employees',
            'dealer_page_quizzes_stat_label' => 'Quiz',
            'dealer_page_surveys_stat_label' => 'Survey',
            'dealer_page_extra_stat_label' => 'Extra',
            'dealer_page_manual_stat_label' => 'Manuals',
            'dealer_page_products_stat_label' => 'Sales',
            'dealer_page_level_stat_label' => 'Levels',

            /*** User Profile Page ****/
            'user_profile_page_first_name_label' => 'First Name',
            'user_profile_page_last_name_label' => 'Last Name',
            'user_profile_page_email_label' => 'Your Email',
            'user_profile_page_phone_label' => 'Phone',
            'user_profile_page_dealer_label' => 'Select Your Store',
            'user_profile_page_position_label' => 'Select Your Position',
            'user_profile_page_change_password_button_text' => 'Change Password',
            'user_profile_page_password_label' => 'New Password',
            'user_profile_page_confirm_password_label' => 'Confirm Password',
            'user_profile_page_save_button_text' => 'Save settings',
            'user_profile_page_remove_account_button_text' => 'Remove my account',
            'user_profile_page_successfully_updated_profile_text' => 'Profile was successfully updated',
            'user_profile_page_ok_button_text' => 'Ok',
            'user_profile_page_camera_button_text' => 'Camera',
            'user_profile_page_gallery_button_text' => 'Gallery',
            'user_profile_page_cancel_button_text' => 'Cancel',
            'user_profile_page_remove_account_title' => 'You are about to delete your registered user account and all it’s following data',
            'user_profile_page_remove_account_text' => 'Are you sure you want to permanently delete your account?',
            'user_profile_page_successfully_updated_password_text' => 'Password was successfully updated',
            'settings_eula_button_link_text' => 'Terms&Conditions',
            'user_profile_settings_page_dealer_label' => "Store",
            'user_profile_settings_page_position_label' => "Position",

            /*** Messages Page ***/
            'messages_page_new_message_button_text' => 'Write new message',
            'messages_page_subject_label' => 'Subject',
            'messages_page_message_text_label' => 'Enter your message',
            'messages_page_url_label' => 'Paste your URL',
            'messages_page_send_button_text' => 'Send',
            'messages_page_search_placeholder' => 'Search',
            'messages_page_today_message_text' => 'today',
            "messages_page_new_message_label" =>  "NEW",
            "messages_page_upload_photo_title" => "Upload Photo",
            "messages_page_upload_attachment_title" => "Upload Attachment",
            "messages_page_sending_message" => "Sending",
            "messages_page_message_is_sent" => "Sent.",

            /*** Others ***/
            'invite_message' => 'Join us in contest',
            'error_other_text' => 'Sorry, it looks like there was an error. Please try again.',
            'cancel' => 'cancel',
            'error' => 'error',
            'send' => 'send',
        ];
    }

    /**
     * @return array
     */
    public function getDefaultMessagesSettings()
    {
        return [
            /**
             * The list of users that will receive notifications about new incoming messages
             * @var array   The list of users IDs
             */
            'send_to' => [],

            /**
             * From who users will receive email notifications
             * @var int    The user ID
             */
            'reply_from' => null,

            /**
             * Autoresponse message text
             * @var string
             */
            'auto_reply_message' => "Thank you for letting us know! We'll be in touch shortly.",

            /**
             * From whose email autoresponse messages will be send
             * @var int    The user ID
             */
            'auto_reply_from' => null,

            /**
             * Whether to send email notifications after user sent new message or not
             * @var boolean
             */
            "send_email_notification_to_moderators" => true,

            /**
             * Whether to send different kind of email notifications to the users.
             * !!! This option doesn't effect on registration/lost_password emails. !!!
             * @var boolean
             */
            "send_email_notification_to_users" => true,
        ];
    }
}
