<?php

namespace App\Models;

use App\Http\Requests\Dealer\DealerGetAllRequest;
use App\Http\Requests\Dealer\DealerGetRequest;
use App\Http\Requests\Dealer\DealerSaveRequest;
use App\Http\Requests\Dealer\DealerLoadRequest;
use App\Http\Requests\Dealer\DealerUpdateRequest;
use App\Http\Requests\Dealer\DealersBulkActionRequest;
use App\Http\Requests\Dealer\DealerGetRedListRequest;
use App\Http\Requests\Dealer\DealerQuickEditRequest;
use App\Http\Requests\Dealer\DealerUpdateImageRequest;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Collections\RowCollection;
use Excel;

class Dealer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'channel_id',
        'district_id',
        'league_id',
        'weight_points',
        'logo_id',
        'location',
        'created_at',
    ];

    /**
     * @var string
     */
    private $_media_type = 'logos';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function channel()
    {
        return $this->belongsTo('App\Models\Channel');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function district()
    {
        return $this->belongsTo('App\Models\District');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function logo()
    {
        return $this->hasOne('App\Models\Media', 'id', 'logo_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany('App\Models\User', 'users_dealers', 'dealer_id', 'user_id');
    }

    /**
     * @param \App\Http\Requests\Dealer\DealerGetAllRequest $request
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function getList(DealerGetAllRequest $request)
    {
        return $this->_getListByChannel($request, ['logo'], ['name']);
    }

    /**
     * @param \App\Http\Requests\Dealer\DealerGetRedListRequest $request
     * @return mixed
     */
    public function getRedList(DealerGetRedListRequest $request)
    {
        $channel_id = $request->channel_id;
        $dealers = with(new self())->getTable();
        $districts = with(new District())->getTable();
        $leagues = with(new League())->getTable();
        $user_dealers = with(new UsersDealers())->getTable();
        $points = with(new PointsStatistic())->getTable();
        return $this->select(
            "{$dealers}.id",
            "{$dealers}.logo_id",
            "{$dealers}.name AS dealer_name",
            "{$districts}.name AS district_name",
            DB::raw("COUNT({$user_dealers}.id) AS users_count"),
            DB::raw("SUM(COALESCE({$points}.earned_points,0)) AS dealer_points")
        )->from($dealers)
        ->leftJoin($districts, "{$districts}.id", '=', "{$dealers}.district_id")
        ->leftJoin($leagues, "{$leagues}.id", '=', "{$dealers}.league_id")
        ->leftJoin($user_dealers, "{$user_dealers}.dealer_id", '=', "{$dealers}.id")
        ->leftJoin($points, function($join) use ($user_dealers, $points, $channel_id) {
            $join->on("{$points}.user_id", '=', "{$user_dealers}.user_id")
                ->where("{$points}.channel_id", $channel_id);
        })->where("{$dealers}.channel_id", $channel_id)
        ->when($request->league_id, function ($q) use ($request, $dealers) {
            return $q->where("{$dealers}.league_id", $request->league_id);
        })
        ->groupBy("{$dealers}.id")
        ->havingRaw('users_count=0')
        ->orderBy('dealer_points', 'desc')
        ->with(['logo'])
        ->get();
    }

    /**
     * @param \App\Http\Requests\Dealer\DealerGetRequest $request
     * @return mixed
     */
    public function get(DealerGetRequest $request)
    {
        return $this->_getDealer($request->id);
    }

    /**
     * @param \App\Http\Requests\Dealer\DealerSaveRequest $request
     * @param \App\Models\Media $media
     * @return \App\Models\Dealer|\App\Models\Dealer[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     * @throws \Exception
     */
    public function add(DealerSaveRequest $request, Media $media)
    {
        $data = $request->validated();

        $dealer = $this->create($data);

        if (! empty($data['logo'])) {
            $dealer->logo_id = $media->saveImage($this->_media_type, $data['logo'], $dealer->id, $dealer->channel_id);
            $dealer->save();
        } elseif (! empty($data['media_id'])) {
            $dealer->logo_id = $media->copyImage($this->_media_type, $data['media_id'], $dealer->id, $dealer->channel_id);
            $dealer->save();
        }

        return $this->_getDealer($dealer->id);
    }

    /**
     * @param \App\Http\Requests\Dealer\DealerLoadRequest $request
     * @return array|bool
     */
    public function loadFromExcel(DealerLoadRequest $request)
    {
        $dealers = [];
        Excel::load($request->dealers->getRealPath(), function ($reader) use (&$dealers, $request) {
            $reader->limitRows(config('services.excel.rows_limit'));
            $reader->ignoreEmpty();
            /* if the document has only one sheet */
            if ($reader->all() instanceof RowCollection) {
                $dealers = $this->_parseExcelRows($reader, $request);
            } else {
                $reader->each(function ($sheet) use (&$dealers, $request) {
                    $data = $this->_parseExcelRows($sheet, $request);
                    if ($data) {
                        $dealers = array_merge($dealers, $data);
                    }
                });
            }
        });

        return $dealers;
    }

    /**
     * @param \App\Http\Requests\Dealer\DealerUpdateRequest $request
     * @param \App\Models\Media $media
     * @return \App\Models\Dealer|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     * @throws \Exception
     */
    public function resave(DealerUpdateRequest $request, Media $media)
    {
        $dealer = $this->find($request->id);
        if ($dealer) {
            $dealer->name = $request->name;
            $dealer->location = $request->location;
            $dealer->district_id = $request->district_id ?? null;
            $dealer->league_id = $request->league_id ?? null;
            if (! empty($request->remove_logo) && filter_var($request->remove_logo, FILTER_VALIDATE_BOOLEAN)) {
                $media->removeMedia([$dealer->logo_id]);
                $dealer->logo_id = null;
            } elseif (! empty($request->logo)) {
                $dealer->logo_id = $media->updateImage($this->_media_type, $request->logo, $dealer->id, $dealer->channel_id, $dealer->logo_id);
            } elseif(! empty($request->media_id)) {
                $dealer->logo_id = $media->copyImage($this->_media_type, $request->media_id, $dealer->id, $dealer->channel_id, $dealer->logo_id);
            }
            $dealer->save();
        }

        return $this->_getDealer($dealer->id);
    }

    /**
     * @param \App\Http\Requests\Dealer\DealerQuickEditRequest $request
     * @return mixed
     */
    public function quickEdit(DealerQuickEditRequest $request)
    {
        return $this->where('id', $request->id)->update(['weight_points' => $request->weight_points]);
    }

    /**
     * @param \App\Http\Requests\Dealer\DealerUpdateImageRequest $request
     * @param \App\Models\Media $media
     * @return \App\Models\Dealer|\App\Models\Dealer[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     * @throws \Exception
     */
    public function updateImage(DealerUpdateImageRequest $request, Media $media)
    {
        $dealer = $this->find($request->id);

        if (auth()->user()->inDealer($dealer->id) && auth()->user()->isStoreManager($dealer->channel_id)) {
            $dealer->logo_id = $media->updateImage($this->_media_type, $request->logo, $dealer->id, $dealer->channel_id, $dealer->logo_id);
            $dealer->save();
            return $this->_getDealer($dealer->id);
        }
        throw new \Exception('Forbidden');

    }

    /**
     * @param \App\Http\Requests\Dealer\DealerGetRequest $request
     * @param \App\Models\Media $media
     * @return \App\Models\Dealer|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     * @throws \Exception
     */
    public function remove(DealerGetRequest $request, Media $media)
    {
        $dealer = $this->_getDealer($request->id);
        if ($dealer) {
            if ($dealer->logo_id) {
                $media->removeMedia([$dealer->logo_id]);
            }
            $this->destroy($request->id);
        }

        return $dealer;
    }

    /**
     * @param \App\Http\Requests\Dealer\DealersBulkActionRequest $request
     * @param \App\Models\Media $media
     * @return bool
     */
    public function bulkAction(DealersBulkActionRequest $request, Media $media) {
        $dealers = $this->whereIn('id', $request->id)->with('logo')->get();

        if (! $dealers->all()) {
            return false;
        }

        switch($request->action) {
            case 'delete':
                $image_ids = array_filter(array_column($dealers->toArray(), 'logo_id'));
                if ($image_ids) {
                    $media->removeMedia($image_ids);
                }
                $this->destroy($request->id);
                break;
            default:
                return false;
        }

        return $dealers;
    }

    /**
     * @param int $dealer_id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     */
    private function _getDealer(int $dealer_id)
    {
        return $this->with('logo')->find($dealer_id);
    }

    /**
     * @param object $item
     * @param \App\Http\Requests\Dealer\DealerLoadRequest $request
     * @return array
     */
    private function _parseExcelRows($item, DealerLoadRequest $request)
    {   $dealers = $districts = $leagues = [];
        $item->each(function ($row) use (&$dealers, &$districts, &$leagues, $request) {
            $dealer_data = $row->all();

            if (empty($dealer_data['name']) || $this->where('channel_id', $request->channel_id)->where('name', $dealer_data['name'])->first()) {
                return;
            }

            $data = [
                'channel_id' => $request->channel_id,
                'name' => $dealer_data['name'],
                'weight_points' => $dealer_data['weight_points'] ?? 1.0,
                'location' => $dealer_data['location'] ?? '',
            ];

            if (! empty($dealer_data['district'])) {
                if (! array_key_exists($dealer_data['district'], $districts)) {
                    $districts[$dealer_data['district']] = District::firstOrCreate([
                        'channel_id' => $request->channel_id,
                        'name' => $dealer_data['district'],
                    ]);
                }
                $data['district_id'] = $districts[$dealer_data['district']]->id;
            }

            if (! empty($dealer_data['league'])) {
                if (! array_key_exists($dealer_data['league'], $leagues)) {
                    $leagues[$dealer_data['league']] = League::firstOrCreate([
                        'channel_id' => $request->channel_id,
                        'name' => $dealer_data['league'],
                    ]);
                }
                $data['league_id'] = $leagues[$dealer_data['league']]->id;
            }

            $dealer = $this->create($data);
            $dealers[] = $this->_getDealer($dealer->id);
        });

        return $dealers;
    }
}
