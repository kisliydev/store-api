<?php

namespace App\Models;

use App\Http\Requests\ProductAssortmentCategory\GetAllRequest;
use App\Http\Requests\ProductAssortmentCategory\GetForMobileRequest;
use App\Http\Requests\ProductAssortmentCategory\GetRequest;
use App\Http\Requests\ProductAssortmentCategory\UpdateRequest;
use App\Http\Requests\ProductAssortmentCategory\BulkActionRequest;

class ProductAssortmentCategory extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'channel_id',
        'name',
        'order',
    ];

    /**
     * @return mixed
     */
    public function products()
    {
        return $this->hasMany(ProductAssortment::class, 'category_id', 'id')->orderBy('order', 'asc');
    }

    /**
     * @param \App\Http\Requests\ProductAssortmentCategory\GetAllRequest $request
     * @return \Illuminate\Contracts\Pagination\Paginator|\Illuminate\Database\Eloquent\Builder
     */
    public function getList(GetAllRequest $request)
    {
        return $this->_getList($this->where('channel_id', $request->channel_id)->orderBy('order', 'asc'), $request, [], ['name']);
    }

    /**
     * @param \App\Http\Requests\ProductAssortmentCategory\GetForMobileRequest $request
     * @return \Illuminate\Contracts\Pagination\Paginator|\Illuminate\Database\Eloquent\Builder
     */
    public function getForMobile(GetForMobileRequest $request)
    {
        return $this->where('channel_id', $request->channel_id)
            ->orderBy('order', 'asc')->with(['products.thumbnail'])->get();
    }

    /**
     * @param \App\Http\Requests\ProductAssortmentCategory\UpdateRequest $request
     * @return mixed
     */
    public function resave(UpdateRequest $request)
    {
        $category = $this->find($request->id);
        $category->name = $request->name;
        $category->order = $request->order ?? 0;
        $category->save();
        return $category;
    }

    /**
     * @param \App\Http\Requests\ProductAssortmentCategory\GetRequest $request
     * @return mixed
     */
    public function remove(GetRequest $request)
    {
        $category = $this->find($request->id);
        $this->destroy($request->id);
        return $category;
    }

    /**
     * @param \App\Http\Requests\ProductAssortmentCategory\BulkActionRequest $request
     * @return bool
     */
    public function bulkAction(BulkActionRequest $request) {
        $categories = $this->whereIn('id', $request->id)->get();

        if (! $categories->all()) {
            return false;
        }

        switch($request->action) {
            case 'delete':
                $this->destroy($request->id);
                break;
            default:
                return false;
        }

        return $categories;
    }
}
