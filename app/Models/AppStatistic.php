<?php

namespace App\Models;

use App\Http\Requests\Sales\GetUserSalesRequest;
use Illuminate\Support\Facades\DB;

class AppStatistic extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'channel_id',
        'user_id',
        'event',
        'data',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'json',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function points() {
        return $this->hasMany(PointsStatistic::class, 'event_id', 'id')
            ->where('object_type', 'product');
    }

    /**
     * @param \App\Http\Requests\Sales\GetUserSalesRequest $request
     * @return mixed
     */
    public function getByUserChannel(GetUserSalesRequest $request)
    {
        return $this->where('channel_id', $request->channel_id)
            ->where('user_id', auth()->user()->id)
            ->with(['points' => function($query) {
                $query->select(
                        'id',
                        'object_id',
                        'earned_points',
                        'units_number',
                        'points_per_item',
                        'event_id'
                    )
                    ->with(['product' => function($subQuery) {
                        $subQuery->select(
                            'id',
                            'name',
                            'points',
                            'use_weight_points',
                            'text_before_amount',
                            'text_after_amount'
                        )->get();
                    }])
                    ->get();
            }])
            ->select(
                'id',
                DB::raw("DATE_FORMAT(created_at, '%Y-%m-%d') as day"),
                DB::raw("DATE_FORMAT(created_at, '%H:%i') as time")
            )
            ->get();
    }
}
