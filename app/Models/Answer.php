<?php

namespace App\Models;

class Answer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question_id',
        'order',
        'text',
        'image_id',
        'correct',
    ];

    /**
     * @var string
     */
    private $_media_type = 'answer_images';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function image()
    {
        return $this->hasOne('App\Models\Media', 'id', 'image_id');
    }

    /**
     * @param \App\Models\Question $question
     * @param array $data
     * @param \App\Models\Media $media
     * @param int $channelId
     * @throws \Exception
     */
    public function add(Question $question, array $data, Media $media, int $channelId) {
        foreach($data as $raw) {
            $answer = $this->create([
                'question_id' => $question->id,
                'text' => $raw['text'],
                'correct' => $this->boolval($raw['correct']),
                'order' => ($raw['order'] ?? 0),
            ]);

            if (! empty($raw['image'])) {
                $answer->image_id = $media->saveImage($this->_media_type, $raw['image'], $answer->id, $channelId);
                $answer->save();
            } elseif (! empty($raw['media_image_id'])) {
                $answer->image_id = $media->copyImage($this->_media_type, $raw['media_image_id'], $answer->id, $channelId);
                $answer->save();
            }
        }
    }

    /**
     * @param array $data
     * @param \App\Models\Media $media
     * @param int|null $channelId
     * @throws \Exception
     */
    public function resave(array $data, Media $media, int $channelId=null)
    {
        foreach($data as $raw) {
            $answer = $this->find($raw['id']);

            if (! $answer) {
                continue;
            }

            if ($this->boolval($raw['remove_answer'])) {
                $media->removeMedia([$answer->image_id]);
                $this->destroy($raw['id']);
            } else {
                if ($this->boolval($raw['remove_image'])) {
                    $answer->image_id = null;
                    $media->removeMedia([$answer->image_id]);
                } elseif (! empty($raw['image'])) {
                    $answer->image_id = $media->updateImage($this->_media_type, $raw['image'], $answer->id, $channelId, $answer->image_id);
                } elseif (! empty($raw['media_image_id'])) {
                    $answer->image_id = $media->copyImage($this->_media_type, $raw['media_image_id'], $answer->id, $channelId, $answer->image_id);
                }
                $answer->text = $raw['text'];
                $answer->correct = $this->boolval($raw['correct']);
                $answer->order = $raw['order'] ?? 0;

                $answer->save();
            }
        }
    }

    /**
     * @param array $question_id
     * @param \App\Models\Media $media
     */
    public function bulkDelete(array $question_id, Media $media) {

        $answers = $this->whereIn('question_id', $question_id)->get(['id', 'image_id'])->toArray();

        if (! $answers) {
            return;
        }

        $answer_ids = array_column($answers, 'id');
        $image_ids = array_filter(array_column($answers, 'image_id'));

        if ($image_ids) {
            $media->removeMedia($image_ids);
        }

        $this->destroy($answer_ids);
    }
}
