<?php

namespace App\Models;

use App\Http\Requests\Recipients\GetListRequest;

class Recipient extends Model
{
    /**
     * The IDAttributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'thread_id',
        'object_id',
        'object_type',
    ];

    public $recipientTypes = [
        'all',
        'district_id',
        'league_id',
        'user_id',
        'dealer_id'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'all',
        'user_id',
        'dealer_id',
        'district_id',
        'league_id',
    ];

    /**
     * @return mixed|null
     */
    public function getAllAttribute()
    {
        return 'all' == $this->object_type;
    }

    /**
     * @return mixed|null
     */
    public function getUserIDAttribute()
    {
        return $this->_getObjectID('user_id');
    }

    /**
     * @return mixed|null
     */
    public function getDealerIDAttribute()
    {
        return $this->_getObjectID('dealer_id');
    }

    /**
     * @return mixed|null
     */
    public function getDistrictIDAttribute()
    {
        return $this->_getObjectID('district_id');
    }

    /**
     * @return mixed|null
     */
    public function getLeagueIDAttribute()
    {
        return $this->_getObjectID('league_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function dealer()
    {
        return $this->belongsTo(Dealer::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function district()
    {
        return $this->belongsTo(District::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function league()
    {
        return $this->belongsTo(League::class);
    }

    /**
     * @param GetListRequest $request
     * @return mixed
     */
    public function getList(GetListRequest $request) {
        $type = preg_replace("/^(.*)?s$/", "$1", $request->type);
        $relation = $type .( 'user' == $type ? '.avatar' : '.logo');

        $query = $this->where('thread_id', $request->thread_id)
            ->where('object_type', "{$type}_id");

        $request->merge(['per_page' => 10]);

        return $this->_getList($query, $request, [$relation]);
    }

    /**
     * @param string $objectType
     * @return mixed|null
     */
    private function _getObjectID(string $objectType)
    {
        return $objectType == $this->object_type ? $this->object_id : null;
    }
}
