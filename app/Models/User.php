<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Symfony\Component\HttpFoundation\Response as HttpResponse;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Tymon\JWTAuth\JWTAuth;
use Maatwebsite\Excel\Collections\RowCollection;

use App\Mail\UserFirstLoginNotification;

use App\Traits\ThreadListQueryBuilder;
use App\Traits\HasRoles;
use App\Traits\Tick;
use App\Traits\Notifications;
use App\Notifications\OneSignalNotification;
use App\Transformers\User\UsersBindingsTransformer;

use App\Http\Requests\Request;
use App\Http\Requests\Auth\ForgotPasswordResetRequest;
use App\Http\Requests\Auth\SwitchChannelRequest;
use App\Http\Requests\Auth\JoinChannelRequest;
use App\Http\Requests\Auth\PreliminaryForgotTokenCheck;
use App\Http\Requests\User\UserGetForMobileRequest;
use App\Http\Requests\User\UserUpdateMobileRequest;

use App\Http\Requests\User\UserGetAllRequest;
use App\Http\Requests\User\UserGetInBrandRequest;
use App\Http\Requests\User\UserGetInCountryBrandRequest;
use App\Http\Requests\User\UserGetInChannelRequest;
use App\Http\Requests\User\UserGetInDealerRequest;
use App\Http\Requests\User\UserSaveRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Http\Requests\User\UserGetRequest;
use App\Http\Requests\User\UsersGetRequest;
use App\Http\Requests\User\UserLoadRequest;
use App\Http\Requests\User\UserGetChannelsRequest;
use App\Http\Requests\User\UserManageBindingsRequest;
use App\Http\Requests\User\UserAddHQRequest;

use Mail;
use Auth;
use Excel;
use Faker\Factory as Faker;

use App\Traits\FullTextSearch;

class User extends UserModel
{
    use Notifiable;
    use HasRoles;
    use SoftDeletes;
    use Tick;
    use Notifications;
    use ThreadListQueryBuilder;
    use FullTextSearch;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'email_token',
        'forgot_password_token',
        'first_name',
        'last_name',
        'avatar_id',
        'phone',
        'skype',
        'verified',
        'facebook_credentials',
        'google_credentials',
        'status',
        'country_id',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'full_name',
        'role',
        'acl',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'forgot_password_token',
        'verified',
        'email_token',
        'facebook_credentials',
        'google_credentials',
        'data',
    ];

    /**
     * @var array
     */
    private $_search_columns = [
//        'first_name',
//        'last_name',
        'email',
        'phone',
        'skype',
    ];

    private $notificationUsers;
    private $notificationAppName;

    /**
     * @var string
     */
    private $_media_type = 'avatars';

    /**
     * @param array $usersIds
     * @param array $notificationData
     * @param string|null $appName
     */
    public function sendPushNotification(array $usersIds, array $notificationData, string $appName = null)
    {
        $this->notificationUsers   = $usersIds;
        $this->notificationData    = $notificationData;
        $this->notificationAppName = $appName;

        $this->notify(new OneSignalNotification($usersIds, $notificationData));
    }

    /**
     * @used to set custom elastic search document structure
     * @return array
     */
    function getIndexDocumentData()
    {
        return [
            'id'         => $this->id,
            'first_name' => $this->first_name,
            'last_name'  => $this->last_name,
            'email'      => $this->email,
            'phone'      => $this->phone,
            'skype'      => $this->skype,
        ];
    }

    /**
     * @return mixed
     */
    public function routeNotificationForOneSignal()
    {
        if (! is_array($this->notificationUsers) || ! $this->notificationUsers) {
            $this->notificationUsers = Auth::check() ? [Auth::user()->id] : [];
        }

        $query = UserChannelStatus::select('player_ids.player_id')
            ->leftJoin('player_ids', 'user_channel_statuses.id', '=', 'player_ids.status_id')
            ->where('status', 'active')
            ->where('channel_id', $this->notificationData['channel_id'])
            ->whereNotNull('player_ids.player_id')
            ->whereIn('user_id', $this->notificationUsers);

        if (!config('mail.intercept')) {
            if ($this->notificationAppName) {
                $query->where('player_ids.app_name', $this->notificationAppName);
            } else {
                $query->whereNull('player_ids.app_name');
            }
        }

        return $query->get()->pluck('player_id')->toArray();
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return trim("{$this->first_name} {$this->last_name}");
    }

    /**
     * @return string
     */
    public function getRoleAttribute()
    {
        return $this->with_appends ? app()['cache']->tags(['acl', "user_acl:{$this->id}"])->remember( "role:{$this->id}", config('cache.lifetime'), function () {
            return Role::where('id', function ($query) {
                    $query->select('role_id')
                        ->from(config('permission.table_names.model_has_roles'))
                        ->where('model_id', $this->id);
                })
                    ->get()
                    ->pluck('name')
                    ->toArray()[0] ?? false;
        })  : null;
    }

    /**
     * @return mixed
     */
    public function getAclAttribute()
    {
        return $this->with_appends ? app()['cache']->tags(['acl', "user_acl:{$this->id}"])->remember( "acl:{$this->id}", config('cache.lifetime'), function () {
            return Permission::whereIn('id', function ($query) {
                    $query->select('permission_id')
                        ->from(config('permission.table_names.model_has_permissions'))
                        ->where('model_id', $this->id);
                })->whereIn('id', function ($query) {
                    $query->select('permission_id')
                        ->from(config('permission.table_names.role_has_permissions'))
                        ->whereIn('role_id', function ($sub) {
                            $sub->select('role_id')->from(config('permission.table_names.model_has_roles'))->where('model_id', $this->id);
                        });
                }, 'or')->whereNotIn('name', function ($query) {
                    $query->select('permission')
                        ->from('denied_permissions')
                        ->where('user_id', $this->id);
                })
                ->get()
                ->pluck('name')
                ->toArray();
        }) : null;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function avatar()
    {
        return $this->hasOne('App\Models\Media', 'id', 'avatar_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function channelStatus()
    {
        return $this->hasMany('App\Models\UserChannelStatus', 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function surveyAnswers()
    {
        return $this->hasMany('App\Models\UserAnswer', 'user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function dealers()
    {
        return $this->belongsToMany(Dealer::class, 'users_dealers', 'user_id', 'dealer_id');
    }

    /**
     * @param int $channel_id
     * @return $this|\Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function dealer(int $channel_id = 0)
    {
        $relation = $this->belongsToMany('App\Models\Dealer', 'users_dealers', 'user_id', 'dealer_id');

        if ($channel_id) {
            $relation->where('channel_id', '=', $channel_id);
        }

        return $relation->toHasOne('user_id', 'id');
    }

    /**
     * @param int $channel_id
     * @return $this|\Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function position(int $channel_id = 0)
    {
        $relation = $this->belongsToMany('App\Models\Position', 'user_positions', 'user_id', 'position_id');

        if ($channel_id) {
            $relation->where('channel_id', '=', $channel_id);
        }

        return $relation->toHasOne('user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function statistics()
    {
        return $this->hasMany(PointsStatistic::class, 'user_id', 'id')->toHasOne('user_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function additionalData()
    {
        return $this->hasMany('App\Models\UserProfileField');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function channels()
    {
        return $this->belongsToMany(
            'App\Models\Channel',
            'user_channel_statuses',
            'user_id',
            'channel_id'
        );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function countryBrands()
    {
        return $this->morphedByMany(
            'App\Models\CountryBrand',
            'brand',
            'brand_users',
            'user_id',
            'brand_id'
        )->with(['brand', 'brand.logo']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphToMany
     */
    public function brands()
    {
        return $this->morphedByMany(
            'App\Models\Brand',
            'brand',
            'brand_users',
            'user_id',
            'brand_id'
        )->with('logo');
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param \App\Models\Media $media
     * @return mixed
     * @throws \Exception
     */
    public function register(Request $request, Media $media)
    {
        $data = $request->validated();

        /*
         * SOCIAL REGISTER CASE
         */
        $isSocialRegister = ! empty($data['social_vendor']);
        if ($isSocialRegister) {
            $data["{$request->social_vendor}_credentials"] = $request->social_id;
            $data['verified'] = 1;
            unset($data['social_vendor']);
            unset($data['social_id']);

            if (! empty($data['email']) && $this->where('email', $data['email'])->withTrashed()->count()) {
                unset($data['email']);
            }
        }

        $user = $this->_add($data, $media);

        $user->bindToChannel($request->channel_id);
        $user->bindToDealer($request->dealer_id);
        $user->bindToPosition($request->position_id);

        if ($isSocialRegister) {
            $this->_handleMobileLogin($request, $user, Channel::find($request->channel_id));

            return [
                'token' => app()->make(JWTAuth::class)->fromUser($user),
                'channel_id' => $request->channel_id
            ];
        }

        return $this->with(['avatar'])->find($user->id);
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param \Tymon\JWTAuth\JWTAuth $JWTAuth
     * @return array
     */
    public function login(Request $request, JWTAuth $JWTAuth)
    {
        $user = $this
            ->where('email', $request->email)
            ->first();
        $token = $JWTAuth->attempt($request->only('email', 'password'));

        if (! $user || ! $user->hasStatus('active') || ! $user->verified || 'app_user' == $user->role || ! $token) {
            return [
                'message' => 'Unauthorized',
                'code' => HttpResponse::HTTP_UNAUTHORIZED,
            ];
        }
        Auth::login($user);
        $this->setOnlineStatus(1);

        return ['token' => $token];
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param \Tymon\JWTAuth\JWTAuth $JWTAuth
     * @return array
     */
    public function mobileLogin(Request $request, JWTAuth $JWTAuth)
    {
        $is_social_login = ! empty($request->social_vendor);
        $error = [
            'message' => 'Unauthorized',
            'code' => HttpResponse::HTTP_UNAUTHORIZED,
        ];

        if ($is_social_login) {
            $user = $this->getBySocialData($request);

            if (! $user) {
                return [
                    'need' => 'register',
                    'code' => HttpResponse::HTTP_ACCEPTED,
                ];
            }

            $token = $JWTAuth->fromUser($user);
        } else {
            $user = $this->where('email', $request->email)->first();

            if (! $user) {
                return $error;
            }

            $token = $JWTAuth->attempt($request->only('email', 'password'));
        }

        if (! $user->hasStatus('active') || ! $user->verified || ! $token) {
            $error = array_merge($error, [
                'verified' => $user->verified,
            ]);

            return $error;
        }

        $channel = Channel::with(['countryBrand.brand.channels', 'currentCampaign'])->find($request->channel_id);

        if (! $channel->currentCampaign || ! $channel->currentCampaign->isRunning()) {
            return $error;
        }

        if (! $user->hasAccessToChannel($channel)) {
            if ('app_user' == $user->role) {
                return [
                    'need' => 'bind_to_channel',
                    'code' => HttpResponse::HTTP_ACCEPTED,
                ];
            }

            return $error;
        }

        $this->_handleMobileLogin($request, $user, $channel);

        $result = [
            'token' => $token,
            'channel_id' => $channel->id
        ];

        if ('auth.ms.login.general' == $request->route()->getName()) {
            $result['brand_id'] = $channel->countryBrand->brand_id;
        }

        return $result;
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param \Tymon\JWTAuth\JWTAuth $JWTAuth
     * @return array
     */
    public function mobileJoinChannelAndLogin(Request $request, JWTAuth $JWTAuth)
    {
        $is_social_login = ! empty($request->social_vendor);
        $error = [
            'message' => 'Unauthorized',
            'code' => HttpResponse::HTTP_UNAUTHORIZED,
        ];

        $user = $is_social_login ? $this->getBySocialData($request) : $this->where('email', $request->email)->first();

        if (! $user || 'app_user' != $user->role || ! $user->hasStatus('active') || ! $user->verified) {
            return $error;
        }

        $token = $is_social_login ? $JWTAuth->fromUser($user) : $JWTAuth->attempt($request->only('email', 'password'));

        if (! $token) {
            return $error;
        }

        $channel = Channel::with(['currentCampaign'])->find($request->channel_id);

        if (! $channel->currentCampaign || ! $channel->currentCampaign->isRunning()) {
            return $error;
        }

        $this->_handleMobileLogin($request, $user, $channel);
        $user->bindToDealer($request->dealer_id);
        $user->bindToPosition($request->position_id);

        return [
            'token' => $token,
            'channel_id' => $channel->id
        ];
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param \App\Models\User $user
     * @param \App\Models\Channel $channel
     */
    private function _handleMobileLogin(Request $request, User $user, Channel $channel)
    {
        PlayerId::updateOrCreate(
            [
                'player_id' => $request->player_id,
                'status_id' => $user->bindToChannel($request->channel_id, 'active')->id,
            ],
            [
                'app_name'  => $request->app_name,
            ]
        );

        $is_first_time_login = ! AppStatistic::where('channel_id', $channel->id)
            ->where('user_id', $user->id)
            ->where('event', 'first_login')
            ->first();

        if ($is_first_time_login) {
            AppStatistic::create([
                'channel_id' => $channel->id,
                'user_id' => $user->id,
                'event' => 'first_login',
            ]);

            $this->maybeNotifyModerators(UserFirstLoginNotification::class, $channel->id, [
                'user' => $user,
                'channelName' => $channel->name,
            ]);
        }

        Auth::login($user);
    }

    /**
     * @param \App\Http\Requests\Auth\SwitchChannelRequest $request
     * @return mixed
     */
    public function switchChannel(SwitchChannelRequest $request)
    {
        $user = auth()->user();
        if (! $user->hasAccessToChannel(Channel::find($request->channel_id))) {
            return false;
        }

        return PlayerId::updateOrCreate(
            [
                'player_id' => $request->player_id,
                'status_id' => $user->bindToChannel($request->channel_id, 'active')->id,
            ],
            [
                'app_name'  => $request->app_name,
            ]
        );
    }

    /**
     * @param \App\Http\Requests\Auth\JoinChannelRequest $request
     * @return mixed
     */
    public function joinChannel(JoinChannelRequest $request)
    {
        $user = auth()->user();

        /*
         * The user might be admin or some kind of manager, so that we don't need
         * to assign him to any dealer or position.
         * Update player id (just in case) if user is already in the channel.
         */
        if ($user->hasAccessToChannel(Channel::find($request->channel_id))) {

            return PlayerId::updateOrCreate(
                [
                    'player_id' => $request->player_id
                ],
                [
                    'status_id' => UserChannelStatus::where('user_id', auth()->user()->id)
                    ->where('channel_id', $request->channel_id)
                    ->first()->id,
                    'app_name'  => $request->app_name,
                ]
            );
        }

        $user->bindToDealer($request->dealer_id);
        $user->bindToPosition($request->position_id);

        return PlayerId::updateOrCreate(
            [
                'player_id' => $request->player_id
            ],
            [
                'status_id' => $user->bindToChannel($request->channel_id, 'active')->id,
                'app_name'  => $request->app_name,
            ]
        );
    }

    /**
     * Set the verified status to true and make the email token null
     */
    public function userVerified()
    {
        $this->verified = 1;
        $this->email_token = null;
        $this->save();

        $this->sendAuthEmail($this, 'verified');
    }

    /**
     * @param bool $isMobile
     * @return array
     */
    public function forgotPassword(bool $isMobile = false)
    {
        $this->forgot_password_token = $isMobile ? mt_rand(1000, 9999) : str_random(70);
        $this->save();

        $this->sendAuthEmail($this, 'forgotPassword' . ($isMobile ? "Mobile" : ''));

        return ['forgot_mail_token' => $this->base64url_encode($this->email)];
    }

    /**
     * @param \App\Http\Requests\Auth\PreliminaryForgotTokenCheck $request
     * @return mixed
     */
    public function getLostPasswordUser(PreliminaryForgotTokenCheck $request)
    {
        return $this->where('forgot_password_token', $request->forgot_password_token)
            ->where('email', $this->base64url_decode($request->forgot_mail_token))->first();
    }

    /**
     * @param \App\Http\Requests\Auth\ForgotPasswordResetRequest $request
     * @return mixed
     */
    public function resetForgotPassword(ForgotPasswordResetRequest $request)
    {
        return $this->where('forgot_password_token', $request->forgot_password_token)
            ->where('email', $this->base64url_decode($request->forgot_mail_token))
            ->update([
                'forgot_password_token' => null,
                'password' => bcrypt($request->password),
            ]);
    }

    /**
     * @return mixed
     */
    public function getAuthUserProfile()
    {
        return $this->where('id', auth()->user()->id)->with(['avatar'])->get();
    }

    /**
     * @param \App\Http\Requests\User\UserGetForMobileRequest $request
     * @return mixed
     */
    public function getAuthUserProfileForMobile(UserGetForMobileRequest $request)
    {
        return $this->where('id', auth()->user()->id)
            ->with([
                'avatar',
                'dealer' => function($query) use ($request) {
                    $query->where('channel_id', '=', $request->channel_id);
                },
                'position' => function($query) use ($request) {
                    $query->where('channel_id', '=', $request->channel_id);
                },
            ])
            ->get();
    }

    /**
     * @param \App\Http\Requests\User\UserGetForMobileRequest $request
     * @return array
     */
    public function getTotals(UserGetForMobileRequest $request)
    {
        $user = auth()->user();

        /**
         * fetch the number of unread Messages in the channel
         */
        $m = 'messages';
        $t = 'threads';
        $unreadMessagesQuery = Message::select(DB::raw("COUNT(`{$m}`.`id`) as count"))
            ->join($t, function($join) use ($t, $m) {
                $join->on("{$t}.id", '=', "{$m}.thread_id")
                    ->where("{$t}.status", '<>', 'scheduled');
            });
        $dealer = $user->dealer($request->channel_id);
        $unreadMessagesCount = $this->getRawUserThreadsQuery($unreadMessagesQuery, $request->channel_id, $dealer)
                ->whereNotExists($this->getNotReadByUserClosure())->first()->count ?? 0;

        /**
         * fetch the number of earned_points
         */
        $earnedPoints = $user->statistics()->select(
            'user_id',
            DB::raw("SUM(earned_points) as earned_points")
        )
            ->where('channel_id', '=', $request->channel_id)
            ->groupBy('user_id')->first()->earned_points ?? 0;

        /**
         * fetch the number of non-passed quizzes and surveys
         */
        $doa = 'done_academy_items';
        $ai  = 'academy_items';
        $nonPassedItems = AcademyItem::select(DB::raw("COUNT(`{$ai}`.`id`) as count"))
            ->whereNotExists(function($query) use ($doa, $ai, $user) {
                $query->select('*')
                    ->from($doa)
                    ->whereRaw("{$doa}.item_id={$ai}.id")
                    ->where('user_id', $user->id);
            })
            ->where('status', 'published')
            ->where("{$ai}.channel_id", $request->channel_id)
            ->count();

        return [
            'unread_messages_count'    => $unreadMessagesCount,
            'earned_points'            => $earnedPoints,
            'non_passed_academy_items' => $nonPassedItems,
        ];
    }

    /**
     * @param \App\Http\Requests\User\UserGetRequest $request
     * @return array|bool|\Illuminate\Support\Collection
     */
    public function getUser(UserGetRequest $request)
    {
        $user = $this->find($request->id);
        $relations = ['avatar'];
        $except = ['role', 'full_name'];

        if (auth()->user()->can('view_permissions')) {
            $except[] = 'acl';
        }

        switch ($user->role) {
            case 'global_manager':
            case 'brand_admin':
                $relations[] = 'brands';
                break;
            case 'brand_manager':
                $relations[] = 'countryBrands';
                break;
            case 'app_user':
                // if we get user data from SAP
                if (empty($request->channel_id)) {
                    $relations['channels'] = function ($query) use ($request) {
                        $this->channelBindingsQuery($query);
                    };
                }

                // if we get user data from channel dashboard
                if (! empty($request->channel_id)) {
                    $relations['dealer'] = function($query) use ($request) {
                        $query->where('channel_id', '=', $request->channel_id);
                    };

                    $relations['position'] = function($query) use ($request) {
                        $query->where('channel_id', '=', $request->channel_id);
                    };

                    $relations['additionalData'] = function ($query) use ($request) {
                        $query->whereIn('user_profile_fields.field_id', function ($sub_query) use ($request) {
                            $sub_query->select('id')
                                ->from('profile_fields')
                                ->where('channel_id', '=', $request->channel_id);
                        });
                    };
                }
                break;
            default:
                break;
        }

        return $this->removeAppends($this->where('id', $request->id)->with($relations)->get(), $except);
    }

    /**
     * @param $query
     * @param bool $forList
     */
    public function channelBindingsQuery($query, bool $forList = false)
    {
        $c = 'channels';
        $cb = 'country_brands';
        $b = 'brands';
        $d = 'dealers';
        $ud = 'users_dealers';
        $ucs = 'user_channel_statuses';
        $up = 'user_positions';
        $p = 'positions';

        $dealers = DB::table($ud)
            ->select(
                "{$ud}.dealer_id",
                "{$ud}.user_id",
                "{$d}.channel_id",
                "{$d}.name"
            )
            ->leftJoin($d, "{$d}.id", '=', "{$ud}.dealer_id");

        $positions = DB::table($up)
            ->select(
                "{$up}.position_id",
                "{$up}.user_id",
                "{$p}.channel_id"
            )
            ->leftJoin($p, "{$p}.id", '=', "{$up}.position_id");

        if ($forList) {
            $query->select(
                "{$c}.id as channel_id",
                "{$c}.name as name",
                "{$cb}.name as country_name",
                "{$b}.name as brand_name",
                DB::raw("d.name as dealer_name")
            );
        } else {
            $query->select(
                "{$c}.id as channel_id",
                "{$cb}.id as country_brand_id",
                "{$b}.id as brand_id",
                DB::raw("d.dealer_id as dealer_id"),
                DB::raw("p.position_id as position_id")
            );
        }

        $query->join($cb, "{$cb}.id", '=', "{$c}.country_brand_id")
        ->join($b, "{$b}.id", '=', "{$cb}.brand_id")
        ->leftJoin(
            DB::raw("({$dealers->toSql()}) as d"),
            function($join) use ($c, $ucs) {
                $join->on('d.user_id', "{$ucs}.user_id")
                    ->on('d.channel_id', "{$c}.id");
            })
        ->leftJoin(
            DB::raw("({$positions->toSql()}) as p"),
            function($join) use ($c, $ucs) {
                $join->on('p.user_id', "{$ucs}.user_id")
                    ->on('p.channel_id', "{$c}.id");
            });
    }

    /**
     * @param string|null $search
     * @return array|bool
     */
    public function getBindings(?string $search = null)
    {
        $user = auth()->user();
        switch ($user->role) {
            case 'global_manager':
            case 'brand_admin':
                return $user->brands()->when($search, function ($query, $search) {
                    return $query->where('name', 'like', "%$search%");
                })->orderBy('created_at', 'desc')->get()->makeHidden(['settings', 'pivot', 'created_at', 'updated_at']);
            case 'brand_manager':
                return $user->countryBrands()->when($search, function ($query, $search) {
                    return $query->where('name', 'like', "%$search%");
                })->get()->makeHidden(['pivot', 'created_at', 'updated_at']);
            default:
                return false;
        }
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getBoundChannels()
    {
        $unreadMessagesQuery = Message::select(DB::raw("COUNT(`messages`.`id`) as count"))
            ->join('threads', function ($join) {
                $join->on("threads.id", '=', "messages.thread_id")
                    ->where("threads.status", '<>', 'scheduled');
            });
        $channels = auth()->user()->getUserActiveChannels();
        $channels->map(function ($item) use ($unreadMessagesQuery) {
            $item->unread_messages = $this->getRawUserThreadsQuery(
                clone $unreadMessagesQuery,
                $item->id,
                auth()->user()->dealer($item->id)
            )->whereNotExists($this->getNotReadByUserClosure())->first()->count ?? 0;
        });

        return $channels;
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @return mixed
     */
    public function getBySocialData(Request $request)
    {
        return $this->where($request->social_vendor.'_credentials', $request->social_id)->first();
    }

    /**
     * @param \App\Http\Requests\User\UserGetAllRequest $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getAll(UserGetAllRequest $request) {
        $this->userRoles = [
            'super_admin',
            'internal_admin',
            'brand_admin',
            'global_manager',
            'brand_manager',
            'app_user',
        ];

        /* set table names */
        $cb = 'country_brands';
        $b = 'brands';

        $relations = [
            'avatar',
            'channels' => function($query) {
                $this->channelBindingsQuery($query, true);
            },
            'countryBrands' => function($query) use ($b, $cb) {
                $query->select(
                        "{$cb}.id as country_id",
                        "{$cb}.name as country_name",
                        "{$b}.name as brand_name"
                    )
                    ->leftJoin($b, "{$b}.id", '=', "{$cb}.brand_id");
            },
            'brands' => function($query) use ($b) {
                $query->select(
                    "{$b}.id as brand_id",
                    "{$b}.name as brand_name"
                );
            }
        ];

        $users = $this->_getList($this->query(), $request, $relations, $this->_search_columns);
        $this->removeAppends($users->getCollection(), ['role', 'full_name']);

        return $users;
    }

    /**
     * @param \App\Http\Requests\User\UserGetInBrandRequest $request
     * @return bool|\Illuminate\Contracts\Pagination\Paginator|\Illuminate\Database\Eloquent\Builder
     */
    public function getInBrand(UserGetInBrandRequest $request)
    {
        $brand_id = $request->brand_id;

        /* add all app_user that are bound to the given brand's channels */
        $channel_users = UserChannelStatus::whereIn('channel_id', function($query) use ($brand_id) {
            $query->select('id')
                ->from(with(new Channel)->getTable())
                ->whereIn('country_brand_id', function($sub_query) use ($brand_id) {
                    $sub_query->select('id')
                        ->from(with(new CountryBrand())->getTable())
                        ->where('brand_id', $brand_id);
                });
        })->select('user_id');

        $brand_users = BrandUser::where(function($query) use ($brand_id) {
            /* add all global_managers and brand_admins that are bound to the given brand */
            $query->where('brand_type', '=', Brand::class)
                ->where('brand_id', '=', $brand_id);
        })->orWhere(function($query) use ($brand_id) {
            /* add all brand_admins that are bound to the given brand's country brands */
            $query->where('brand_type', '=', CountryBrand::class)
                ->whereIn('brand_id', function($sub_query) use ($brand_id) {
                    $sub_query->select('id')
                        ->from(with(new CountryBrand)->getTable())
                        ->where('brand_id', '=', $brand_id);
                });
        })->select('user_id');

        $user_ids = array_unique(
            $brand_users->union($channel_users)
                ->get()->pluck(['user_id'])->toArray()
        );

        if (! $user_ids) {
            return false;
        }
        $roles = [
            'brand_admin',
            'global_manager',
            'brand_manager',
            'app_user',
        ];

        $users = $this->_getList($this->whereIn('id', $user_ids)->role($roles), $request, ['avatar'], $this->_search_columns);
        $this->removeAppends($users->getCollection(), ['role', 'full_name']);

        return $users;
    }

    /**
     * @param \App\Http\Requests\User\UserGetInCountryBrandRequest $request
     * @return bool|\Illuminate\Contracts\Pagination\Paginator|\Illuminate\Database\Eloquent\Builder
     */
    public function getInCountryBrand(UserGetInCountryBrandRequest $request)
    {
        $brand_id = $request->country_brand_id;
        $channel_users = UserChannelStatus::whereIn('channel_id', function($query) use ($brand_id) {
            $query->select('id')
                ->from(with(new Channel)->getTable())
                ->where('country_brand_id', '=', $brand_id);
        })->select('user_id');

        $brand_users = BrandUser::where('brand_type', '=', CountryBrand::class)
            ->where('brand_id', '=', $brand_id)->select('user_id');

        $user_ids = array_unique(
            $brand_users->union($channel_users)
                ->get()->pluck(['user_id'])->toArray()
        );

        if (! $user_ids) {
            return false;
        }

        $roles = [
            'brand_manager',
            'app_user',
        ];

        $users = $this->_getList($this->whereIn('id', $user_ids)->role($roles), $request, ['avatar'], $this->_search_columns);
        $this->removeAppends($users->getCollection(), ['role', 'full_name']);

        return $users;
    }

    /**
     * @param \App\Http\Requests\User\UserGetInChannelRequest $request
     * @return \Illuminate\Contracts\Pagination\Paginator|\Illuminate\Database\Eloquent\Builder
     */
    public function getInChannel(UserGetInChannelRequest $request)
    {
        $roles = [
            'app_user',
        ];
        $channel_id = $request->channel_id;
        $query_builder = $this->whereIn('id', function ($query) use ($channel_id) {
            $query->select('user_id')
                ->from(with(new UserChannelStatus())->getTable())
                ->where('channel_id', '=', $channel_id);
        })->role($roles)->with([
            'channelStatus' => function ($query) use ($channel_id) {
                $query->where('channel_id', '=', $channel_id);
            },
        ]);

        $users = $this->_getList($query_builder, $request, ['avatar'], $this->_search_columns);
        $this->removeAppends($users->getCollection(), ['role', 'full_name']);

        return $users;
    }

    /**
     * @param \App\Http\Requests\User\UserGetInDealerRequest $request
     * @return \Illuminate\Contracts\Pagination\Paginator|\Illuminate\Database\Eloquent\Builder
     */
    public function getInDealer(UserGetInDealerRequest $request)
    {
        $dealer_id = $request->dealer_id;
        $channel_id = $request->channel_id;
        $query_builder = $this->whereIn('id', function ($query) use ($dealer_id) {
            $query->select('users_dealers.user_id')->from('users_dealers')->where('users_dealers.dealer_id', '=', $dealer_id)->whereNull('deleted_at');
        })->with([
            'channelStatus' => function ($query) use ($channel_id) {
                $query->where('channel_id', '=', $channel_id);
            },
        ]);

        $users = $this->_getList($query_builder, $request, ['avatar'], $this->_search_columns);
        $this->removeAppends($users->getCollection(), ['role', 'full_name']);

        return $users;
    }

    /**
     * @param \App\Http\Requests\User\UserSaveRequest $request
     * @param \App\Models\Media $media
     * @return \App\Models\User|\App\Models\User[]|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     * @throws \Exception
     */
    public function add(UserSaveRequest $request, Media $media)
    {
        $data = $request->validated();
        $user = $this->_add($data, $media);

        switch ($data['role']) {
            case 'brand_admin':
            case 'global_manager':
                foreach($data['brands'] as $brand) {
                    $user->bindToBrand($brand['brand_id']);
                }
                break;
            case 'brand_manager':
                foreach($data['country_brands'] as $brand) {
                    $user->bindToCountryBrand($brand['country_brand_id']);
                }
                break;
            case 'app_user':
                foreach ($data['channels'] as $channel) {

                    $user->bindToChannel($channel['channel_id']);

                    if (! empty($channel['position_id'])) {
                        $user->bindToPosition($channel['position_id']);
                    }

                    if (! empty($channel['dealer_id'])) {
                        $user->bindToDealer($channel['dealer_id']);
                    }
                }
                break;
            default:
                break;
        }

        return $this->with('avatar')->find($user->id);
    }

    /**
     * @param \App\Http\Requests\User\UserUpdateRequest $request
     * @param \App\Models\Media $media
     * @return \App\Models\User|\App\Models\User[]|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function resave(UserUpdateRequest $request, Media $media)
    {
        $data = $request->validated();
        $user = $this->find($request->id);

        $user->_save($data, $media);
        return $this->with('avatar')->find($user->id);
    }

    /**
     * @param \App\Http\Requests\User\UserManageBindingsRequest $request
     * @param \App\Models\ACL $acl
     * @return \App\Models\User|\App\Models\User[]|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function resaveBindings(UserManageBindingsRequest $request, ACL $acl)
    {
        $data = $request->validated();
        $user = $this->find($request->id);
        $user->manageSystemBindings($data);
        $acl->manageUser($user->id, $data['role']);
        return $this->find($user->id);
    }

    /**
     * @param \App\Http\Requests\User\UserUpdateMobileRequest $request
     * @param \App\Models\Media $media
     * @return \App\Models\User|\App\Models\User[]|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function resaveMobile(UserUpdateMobileRequest $request, Media $media)
    {
        $data = $request->validated();
        $user = $this->find($request->id);
        $user->_save($data, $media);

        if ($this->isAppRole($user->role)) {
            $user->manageChannelBindings($data);
        }


        return $this->with([
            'avatar',
            'dealer' => function($query) use ($request) {
                $query->where('channel_id', '=', $request->channel_id);
            },
            'position' => function($query) use ($request) {
                $query->where('channel_id', '=', $request->channel_id);
            },
        ])->find($user->id);
    }

    /**
     * @param \App\Http\Requests\User\UsersGetRequest $request
     * @return bool
     */
    public function toTrash(UsersGetRequest $request)
    {
        $ids = $this->removeCurrentId($request->validated()['ids']);

        if (! $ids && $this->whereIn('id', $ids)->delete()) {
            return false;
        }

        $removed = [];

        foreach($ids as $id) {
            if ($this->where('id', $id)->delete()) {
                $removed[] = $id;
            }
        }

        return $removed;
    }

    /**
     * @param \App\Http\Requests\User\UsersGetRequest $request
     * @param \App\Models\Media $media
     * @return array|bool
     */
    public function deleteHeadQuorterUsers(UsersGetRequest $request, Media $media)
    {
        $users = [];

        $this->whereIn('id', $request->validated()['ids'])
            ->role('hq')
            ->get()
            ->each(function($user) use (&$users, $media) {
                $users[] = $user;
                $user->deletePermanently($media);
            });

        return $users ? $users : false;
    }

    /**
     * @param \App\Http\Requests\User\UsersGetRequest $request
     * @param \App\Models\Media $media
     * @return array|bool
     */
    public function remove(UsersGetRequest $request, Media $media)
    {
        $ids = $this->removeCurrentId($request->validated()['ids']);

        if (! $ids) {
            return false;
        }

        $users = [];

        foreach($ids as $id) {
            $user = $this->withTrashed()->find($id);
            $user->deletePermanently($media);
            $users[] = $user;
        }

        return $users;
    }

    /**
     * @param \App\Models\Media $media
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function removeOwnAccount(Media $media)
    {
        $user = auth()->user();
        $user->deletePermanently($media);
        return $user;
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param string $level
     * @return array|bool
     */
    public function removeFrom(Request $request, string $level)
    {
        $ids = $this->removeCurrentId($request->validated()['ids']);

        if (! $ids) {
            return false;
        }

        $users = [];

        foreach($ids as $id) {
            $user = $this->withTrashed()->find($id);
            if ($user->removeFromLevel($request, $level)) {
                $users[] = $user;
            }
        }

        return $users;
    }

    /**
     * @return array
     */
    public function getNewHeadQuorterUsers()
    {
        $faker = Faker::create();
        $users = [];
        for ($i = 0; $i < 5; $i++) {
            $firstName = strtolower(preg_replace('/[^a-zA-Z]/', '', $faker->firstName));
            $lastName = strtolower(preg_replace('/[^a-zA-Z]/', '', $faker->lastName));
            $email = $this->getUniqueHQEmail("{$firstName}.{$lastName}@store.com");
            $data = [
                'first_name' => $firstName,
                'last_name' => $lastName,
                'full_name' => "{$firstName} {$lastName}",
                'email' => $email,
                'password' => $this->getPasswordFrom($email),
            ];
            $users[] = $data;
        }
        return $users;
    }

    /**
     * @param \App\Http\Requests\User\UserAddHQRequest $request
     * @return bool
     * @throws \Exception
     */
    public function addHeadQuorters(UserAddHQRequest $request) {
        $data = $request->validated();
        $users = [];
        $emails = [];

        $usersData = array_filter($data['users'], function($item) use (&$emails) {
            if (! in_array($item['email'], $emails)) {
                $emails[] = $item['email'];
                return true;
            }
            return false;
        });

        foreach($usersData as $raw) {
            $user = $this->_add(array_merge($raw, ['role' => 'hq']));
            $users[] = $user->id;
            $user->bindToChannel($data['channel_id']);
        }

        return $users ? $this->whereIn('id', $users)->get() : false;
    }

    /**
     * @param int $channel_id
     * @return mixed
     * @throws \Exception
     */
    public function addHeadQuorterUsers(int $channel_id)
    {
        $faker = Faker::create();
        $users = [];
        for ($i = 1; $i < 6; $i++) {
            $name = strtolower(preg_replace('/[^a-zA-Z]/', '', $faker->firstName));
            $lastName = strtolower(preg_replace('/[^a-zA-Z]/', '', $faker->lastName));
            $email = $this->getUniqueHQEmail("{$name}.{$lastName}@store.com");
            $data = [
                'first_name' => $name,
                'last_name' => $lastName,
                'email' => $email,
                'password' => $this->getPasswordFrom($email),
                'role' => 'hq',
            ];

            $user = $this->_add($data);
            $user->bindToChannel($channel_id);

            $users[] = $user->id;
        }

        return $this->whereIn('id', $users)->get();
    }

    /**
     * @param \App\Http\Requests\User\UserGetInChannelRequest $request
     * @return \Illuminate\Contracts\Pagination\Paginator|\Illuminate\Database\Eloquent\Builder
     */
    public function getHeadQuorterUsers(UserGetInChannelRequest $request)
    {
        $ids = UserChannelStatus::where('channel_id', $request->channel_id)->get(['user_id'])->makeHidden([
            'dealer',
            'position'
        ])->pluck(['user_id']);

        $query = $this->select(
            'id',
            'email',
            DB::raw("trim(concat(first_name, ' ', last_name)) as full_name"),
            'first_name',
            'last_name'
        )->whereIn('id', $ids)->role('hq');

        $users = $this->_getList($query, $request, [], $this->_search_columns);
        $this->removeAppends($users->getCollection(),['role']);

        return $users;
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string|null $search
     * @param array $search_in_columns
     */
    public function addSearch(Builder $query, string $search = null, array $search_in_columns = [])
    {

        if (! empty($search) && $search_in_columns) {
            $query->where(function($subQuery) use ($search_in_columns, $search) {
                $i = 0;
                foreach ($search_in_columns as $column) {
                    $where = $i ? 'orWhere' : 'where';
                    $subQuery = $subQuery->$where($column, 'like', "%{$search}%");
                    $i++;
                }

                $this->addFullTextSearch($subQuery, $search);
            });
        }
    }

    /**
     * @param string $email
     * @return bool|string
     */
    public function getPasswordFrom(string $email)
    {
        return substr(md5($email), 0, 6);
    }

    /**
     * @param \App\Http\Requests\User\UserLoadRequest $request
     * @return array
     */
    public function loadFromExcel(UserLoadRequest $request)
    {
        $users = [];

        Excel::load($request->users->getRealPath(), function ($reader) use (&$users, $request) {
            $reader->limitRows(config('services.excel.rows_limit'));
            $reader->ignoreEmpty();
            /* if the document has only one sheet */
            if ($reader->all() instanceof RowCollection) {
                $users = $this->_parseExcelRows($reader, $request);
            } else {
                $reader->each(function ($sheet) use (&$users, $request) {
                    $data = $this->_parseExcelRows($sheet, $request);
                    if ($data) {
                        $users = array_merge($users, $data);
                    }
                });
            }
        });

        return $users;
    }

    /**
     * @param $item
     * @param \App\Http\Requests\User\UserLoadRequest $request
     * @return array
     */
    private function _parseExcelRows($item, UserLoadRequest $request)
    {   $users = $dealers = $districts = $positions = [];
        $item->each(function ($row) use (&$users, &$dealers, &$districts, &$positions, $request) {
            $user_data = $row->all();

            if (
                empty($user_data['dealer_name']) ||
                ! $this->isValidEmail($user_data['user_email']) ||
                ! $this->isValidPhone($user_data['user_phone']) ||
                ! $this->isAppRole($user_data['user_role'])
            ) {
                return;
            }

            $existed = $this->where('email', $user_data['user_email'])
                ->orWhere('phone', $user_data['user_phone'])->get();

            switch ($existed->count()) {
                case '0':
                    $user = $this->_add([
                        'first_name' => $user_data['first_name'] ?? '',
                        'last_name' => $user_data['last_name'] ?? '',
                        'phone' => $user_data['user_phone'] ?? '',
                        'email' => $user_data['user_email'] ?? '',
                        'password' => $user_data['user_pass'] ?? '',
                        'role' => $user_data['user_role'],
                    ]);
                    break;
                case '1':
                    $user = $existed->first();
                    if (! $this->isAppRole($user->role) || $user->trashed() || $user->inChannel($request->channel_id)) {
                        return;
                    }
                    break;
                default:
                    return;
            }

            $user->bindToChannel($request->channel_id);

            if (! array_key_exists($user_data['dealer_name'], $dealers)) {

                $dealer_data = [
                    'channel_id' => $request->channel_id,
                    'name' => $user_data['dealer_name'],
                    'location' => $user_data['dealer_location'] ?? '',
                    'weight_points' => 1.0,
                ];

                if (! empty($user_data['dealer_district'])) {
                    if (! array_key_exists($user_data['dealer_district'], $districts)) {
                        $districts[$user_data['dealer_district']] = District::firstOrCreate([
                            'channel_id' => $request->channel_id,
                            'name' => $user_data['dealer_district'],
                        ]);
                    }
                    $dealer_data['district_id'] = $districts[$user_data['dealer_district']]->id;
                }

                $dealers[$user_data['dealer_name']] = Dealer::firstOrCreate($dealer_data);
            }

            $user->bindToDealer($dealers[$user_data['dealer_name']]->id);

            if (! empty($user_data['user_position'])) {
                if (! array_key_exists($user_data['user_position'], $positions)) {
                    $positions[$user_data['user_position']] = Position::firstOrCreate([
                        'channel_id' => $request->channel_id,
                        'name' => $user_data['user_position'],
                    ]);
                }

                $user->bindToPosition($positions[$user_data['user_position']]->id);
            }

            $users[] = $user;
        });

        return $users;
    }

    /**
     * @param array $data
     * @param \App\Models\Media|null $media
     * @param bool $send_email
     * @return mixed
     * @throws \Exception
     */
    private function _add(array $data, Media $media = null, bool $send_email = true)
    {
        $data['password'] = empty($data['password']) ? bcrypt(str_random(10)) : bcrypt($data['password']);
        $data['email_token'] = str_random(10);
        $role = $data['role'] ?? 'app_user';
        unset($data['role']);

        if ($role == 'hq' || $this->boolval($data['skip_confirmation'])) {
            $data['verified'] = 1;
        }

        $user = $this->create($data);

        if ($media) {
            if(! empty($data['avatar_url'])) {
                $user->avatar_id = $media->saveImageFromUrl($this->_media_type, $data['avatar_url'], $user->id);
                $user->save();
            } elseif (! empty($data['avatar'])) {
                $user->avatar_id = $media->saveImage($this->_media_type, $data['avatar'], $user->id);
                $user->save();
            } elseif (! empty($data['media_id'])) {
                $user->avatar_id = $media->copyImage($this->_media_type, $data['media_id'], $user->id);
                $user->save();
            }
        }

        $this->_assignRole($role, $user);
        if (! $user->verified && $send_email) {
            $user->sendAuthEmail($user, 'register');
        }

        return $user;
    }

    /**
     * @param $data
     * @param \App\Models\Media $media
     * @throws \Exception
     */
    private function _save($data, Media $media)
    {
        $this->first_name = $data['first_name'] ?? '';
        $this->last_name = $data['last_name'] ?? '';
        if ($this->facebook_credentials || $this->google_credentials) {
            $this->email = $data['email'] ?? null;
        } elseif (! empty($data['email'])) {
            $this->email = $data['email'];
        }
        $this->phone = $data['phone'] ?? null;
        $this->skype = $data['skype'] ?? null;

        if (!empty($data['country_id'])) {
            $this->country_id = $data['country_id'];
        }

        if (!empty($data['role']) && $data['role'] == 'hq') {
            $this->verified = 1;
        }

        if (! empty($data['password'])) {
            $this->password = bcrypt($data['password']);
        }

        if ($this->boolval($data['remove_avatar'])) {
            $media->removeMedia([$this->avatar_id]);
            $this->avatar_id = null;
        } elseif (! empty($data['avatar'])) {
            $this->avatar_id = $media->updateImage($this->_media_type, $data['avatar'], $this->id, null, $this->avatar_id);
        } elseif (! empty($data['media_id'])) {
            $this->avatar_id = $media->copyImage($this->_media_type, $data['media_id'], $this->id, null, $this->avatar_id);
        }

        $this->save();
    }

    /**
     * @param \App\Models\Media $media
     * @throws \Exception
     */
    public function deletePermanently(Media $media)
    {
        if ($this->avatar_id) {
            $media->removeMedia([$this->avatar_id]);
        }
        $this->forceDelete();
    }

    /**
     * @param int $status
     * @return mixed
     */
    public function setOnlineStatus(int $status)
    {
        return $this->Where('id', auth()->user()->id)->update(['is_online' => $status]);
    }

    /**
     * @param string $data
     * @return string
     */
    public function base64url_encode(string $data)
    {
        return rtrim(strtr(base64_encode($data . $this->getCurrentTick()), '+/', '-_'), '=');
    }

    /**
     * @param string $data
     * @return bool|string
     */
    private function base64url_decode(string $data)
    {
        $string = base64_decode(str_pad(strtr($data, '-_', '+/'), strlen($data) % 4, '=', STR_PAD_RIGHT));
        $regexp = "/^(.*?)" . $this->getCurrentTick() . '|' . $this->getPreviousTick() . "$/";
        return preg_replace( $regexp, '$1', $string);
    }

    /**
     * @param string $role
     * @param \App\Models\User $user
     */
    private function _assignRole(string $role = null, User $user)
    {
        if (! $role || ! Role::where('name', $role)->count()) {
            $role = 'app_user';
        }

        $user->assignRole($role);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string|null $filters
     */
    public function addMultiFilters(Builder $query, string $filters = null)
    {
        if (! $filters) {
            if (is_array($this->userRoles)) {
                $query->role($this->userRoles);
            }
            return;
        }

        $filters = array_map(function($filter) {
            list($criteria, $value) = explode(':', $filter);
            return [
                'criteria' => $criteria,
                'value' => $value
            ];
        }, explode(',', $filters));

        if (!in_array('role', array_column($filters, 'criteria')) && is_array($this->userRoles)) {
            $query->role($this->userRoles);
        }

        foreach ($filters as $filter) {
            if ('role' == $filter['criteria']) {
                if (in_array($filter['value'], (array)$this->userRoles)) {
                    $query->role([$filter['value']]);
                }
            } else {
                $query->where($filter['criteria'], $filter['value']);
            }
        }
    }
}
