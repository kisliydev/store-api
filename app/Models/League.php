<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use App\Http\Requests\League\LeagueGetAllRequest;
use App\Http\Requests\League\LeagueGetRequest;
use App\Http\Requests\League\LeagueSaveRequest;
use App\Http\Requests\League\LeagueUpdateRequest;
use App\Http\Requests\League\LeagueBulkActionRequest;

class League extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'channel_id',
        'logo_id',
        'created_at',
    ];

    /**
     * @var string
     */
    private $_media_type = 'logos';

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function channel() {
        return $this->belongsTo( 'App\Models\Channel' );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function logo()
    {
        return $this->hasOne('App\Models\Media', 'id', 'logo_id');
    }

    /**
     * @param LeagueGetAllRequest $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getList(LeagueGetAllRequest $request)
    {
        $d = 'dealers';
        $di = 'leagues';
        $u = 'users';
        $ud = 'users_dealers';

        $dealersCount = Dealer::select(
            "league_id",
            DB::raw("COUNT(id) as dealers_count")
        )
            ->where("channel_id", $request->channel_id)
            ->groupBy("league_id");

        $usersCount = UsersDealers::select(
            "league_id",
            DB::raw("COUNT(user_id) as users_count")
        )
            ->join($d, function($join) use ($d, $ud, $request) {
                $join->on("{$d}.id", '=', "{$ud}.dealer_id")
                    ->where("{$d}.channel_id", $request->channel_id);
            })
            ->groupBy("{$d}.league_id");

        $query = $this->query()
            ->leftJoin(
                DB::raw("({$dealersCount->toSql()}) as {$d}"),
                "{$d}.league_id", '=', "{$di}.id"
            )
            ->leftJoin(
                DB::raw("({$usersCount->toSql()}) as {$u}"),
                "{$u}.league_id", '=', "{$di}.id"
            )
            ->where("{$di}.channel_id", $request->channel_id)
            ->mergeBindings($dealersCount->getQuery())
            ->mergeBindings($usersCount->getQuery());

        return $this->_getList($query, $request, ['logo'], ['name']);
    }

    /**
     * @param \App\Http\Requests\League\LeagueGetRequest $request
     * @return mixed
     */
    public function get(LeagueGetRequest $request)
    {
        return $this->_getLeague($request->id);
    }

    /**
     * @param LeagueSaveRequest $request
     * @param Media $media
     * @return League|League[]|bool|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     * @throws \Exception
     */
    public function add(LeagueSaveRequest $request, Media $media)
    {
        $data = $request->validated();

        if (! Channel::find($data['channel_id'])) {
            return false;
        }

        $league = $this->create($data);

        if (! empty($data['logo'])) {
            $league->logo_id = $media->saveImage($this->_media_type, $data['logo'], $league->id, $league->channel_id);
            $league->save();
        } elseif (! empty($data['media_id'])) {
            $league->logo_id = $media->copyImage($this->_media_type, $data['media_id'], $league->id, $league->channel_id);
            $league->save();
        }

        return $this->_getLeague($league->id);
    }

    /**
     * @param \App\Http\Requests\League\LeagueUpdateRequest $request
     * @param \App\Models\Media $media
     * @return \App\Models\League|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     * @throws \Exception
     */
    public function resave(LeagueUpdateRequest $request, Media $media)
    {
        $league = $this->find($request->id);
        if ($league) {
            $league->name = $request->name;
            if (! empty($request->remove_logo) && filter_var($request->remove_logo, FILTER_VALIDATE_BOOLEAN)) {
                $media->removeMedia([$league->logo_id]);
                $league->logo_id = null;
            } elseif (! empty($request->logo)) {
                $league->logo_id = $media->updateImage($this->_media_type, $request->logo, $league->id, $league->channel_id, $league->logo_id);
            } elseif(! empty($request->media_id)) {
                $league->logo_id = $media->copyImage($this->_media_type, $request->media_id, $league->id, $league->channel_id, $league->logo_id);
            }
            $league->save();
        }

        return $this->_getLeague($league->id);
    }

    /**
     * @param \App\Http\Requests\League\LeagueGetRequest $request
     * @param \App\Models\Media $media
     * @return \App\Models\League|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static[]
     * @throws \Exception
     */
    public function remove(LeagueGetRequest $request, Media $media)
    {
        $league = $this->_getLeague($request->id);
        if ($league) {
            if ( $league->logo_id ) {
                $media->removeMedia([$league->logo_id]);
            }
            $this->destroy($request->id);
        }

        return $league;
    }

    /**
     * @param \App\Http\Requests\League\LeagueBulkActionRequest $request
     * @param \App\Models\Media $media
     * @return bool
     */
    public function bulkAction(LeagueBulkActionRequest $request, Media $media) {
        $leagues = $this->whereIn('id', $request->id)->with('logo')->get();
        if (! $leagues->all()) {
            return false;
        }

        switch($request->action) {
            case 'delete':
                $image_ids = array_filter(array_column($leagues->toArray(), 'logo_id'));
                if ($image_ids) {
                    $media->removeMedia($image_ids);
                }
                DB::table('dealers')->whereIn('league_id', $request->id)->update(['league_id' => 0]);
                $this->destroy($request->id);
                break;
            default:
                return false;
        }

        return $leagues;
    }

    /**
     * @param int $league_id
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null|static|static[]
     */
    private function _getLeague(int $league_id)
    {
        return $this->with('logo')->find($league_id);
    }
}
