<?php

namespace App\Models;

use App\Transformers\Messages\UserIdTransformer;

class UserChannelStatus extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'channel_id',
        'user_id',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'created_at',
        'updated_at',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'dealer',
        'position'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function channel()
    {
        return $this->belongsTo('App\Models\Channel');
    }

    /**
     * @return string
     */
    public function getDealerAttribute()
    {
        return UsersDealers::where('user_id', $this->user_id)
            ->join('dealers', function($join) {
                $join->on('dealers.id', '=', 'users_dealers.dealer_id')
                    ->where('dealers.channel_id', '=', $this->channel_id);
            })->select('users_dealers.*')->get();
    }

    /**
     * @return string
     */
    public function getPositionAttribute()
    {
        return UserPosition::where('user_id', $this->user_id)
            ->join('positions', function($join) {
                $join->on('positions.id', '=', 'user_positions.position_id')
                    ->where('positions.channel_id', '=', $this->channel_id);
            })->select('user_positions.*')->get();
    }

    /**
     * @param int $channelId
     * @param \App\Transformers\Messages\UserIdTransformer $transformer
     * @return array
     */
    public function getChannelUserIds(int $channelId, UserIdTransformer $transformer): array
    {
        return $transformer->transform(
            $this->removeAppends($this->select('user_id')->where('channel_id', $channelId)->get())
        )->toArray();
    }
}
