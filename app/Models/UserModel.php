<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use \App\Http\Requests\Request;

class UserModel extends Model implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * @param string $email
     * @return mixed
     */
    public function isValidEmail(string $email)
    {
        return !! filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    /**
     * @param string $phone
     * @return bool
     */
    public function isValidPhone(string $phone)
    {
        return !! preg_match("/^\+[\d]{5,15}$/", $phone);
    }

    /**
     * @param string|null $role
     * @param bool $with_hq
     * @return bool
     */
    public function isAppRole(string $role = null, bool $with_hq = false)
    {
        $role = $role ?? $this->role;

        $roles = ['app_user'];
        if ($with_hq) {
            $roles[] = 'hq';
        }
        return in_array($role, $roles);
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return in_array(
            $this->role,
            [
                'super_admin',
                'internal_admin',
                'brand_admin',
                'global_manager',
                'brand_manager',
            ]
        );
    }

    /**
     * @return bool
     */
    public function isSystemAdmin()
    {
        return in_array($this->role, ['super_admin', 'internal_admin']);
    }

    /**
     * @param int $channelID
     * @return bool
     */
    public function isStoreManager(int $channelID)
    {
        if (! $this->isAppRole()) {
            return false;
        }

        $position = $this->position($channelID)->first();

        if (!$position) {
            return false;
        }

        return !!$position->is_manager;
    }

    /**
     * @param string $status
     * @return bool
     */
    public function hasStatus(string $status)
    {
        return 0 === strcmp($status, $this->status);
    }

    /**
     * @param int $brand_id
     * @param string $type
     * @return bool
     */
    public function inBrand(int $brand_id, string $type = null)
    {
        $type = $type ?? Brand::class;
        return !! BrandUser::where('brand_id', $brand_id)
            ->where('brand_type', $type)
            ->first();
    }

    /**
     * @param int $brand_id
     * @return bool
     */
    public function inCountryBrand(int $brand_id)
    {
        return $this->inBrand($brand_id, CountryBrand::class);
    }

    /**
     * @param int $channel_id
     * @return bool
     */
    public function inChannel(int $channel_id)
    {
        return !! UserChannelStatus::where('user_id', $this->id)
            ->where('channel_id', $channel_id)
            ->first();
    }

    /**
     * @param int $dealer_id
     * @return bool
     */
    public function inDealer(int $dealer_id)
    {
        return !! UsersDealers::where('user_id', $this->id)
            ->where('dealer_id', $dealer_id)
            ->first();
    }

    /**
     * @param $brand
     * @return bool
     */
    public function hasAccessToBrand($brand)
    {
        $brandID = $brand instanceof Brand ? $brand->id : abs(intval($brand));
        switch ($this->role) {
            case 'super_admin':
            case 'internal_admin':
                return !! Brand::find($brandID);
            case 'brand_admin':
            case 'global_manager':
                return $this->inBrand($brandID);
            case 'brand_manager':
            case 'app_user':
            case 'hq':
            default:
                return false;
        }
    }

    /**
     * @param $countryBrand
     * @return bool
     */
    public function hasAccessToCountryBrand($countryBrand)
    {
        if (! $countryBrand instanceof CountryBrand) {
            $countryBrand = CountryBrand::find(abs(intval($countryBrand))) ?? false;
            if (empty($countryBrand)) {
                return false;
            }
        }

        switch ($this->role) {
            case 'super_admin':
            case 'internal_admin':
                return !! Brand::find($countryBrand->brand_id);
            case 'brand_admin':
            case 'global_manager':
                return $this->inBrand($countryBrand->brand_id);
            case 'brand_manager':
                return $this->inCountryBrand($countryBrand->id);
            case 'app_user':
            case 'hq':
            default:
                return false;
        }
    }

    /**
     * @param $channel
     * @return bool
     */
    public function hasAccessToChannel($channel)
    {
        if (! $channel instanceof Channel) {
            $channelID = abs(intval($channel));
            $channel = app()['cache']->remember("channel:{$channelID}", config('cache.lifetime'), function() use ($channelID){
                return Channel::find(abs(intval($channelID)));
            });

            if (! $channel) {
                return false;
            }
        }

        //return app()['cache']->tags(["user_acl:{$this->id}"])->remember("user_has_access_to_channel:{$this->id},channel:{$channel->id}", config('cache.lifetime'), function() use ($channel) {
            switch ($this->role) {
                case 'super_admin':
                case 'internal_admin':
                    return !! Brand::find($channel->countryBrand->brand->id);
                case 'brand_admin':
                case 'global_manager':
                    return $this->inBrand($channel->countryBrand->brand->id);
                case 'brand_manager':
                    return $this->inCountryBrand($channel->countryBrand->id);
                case 'app_user':
                case 'hq':
                    return $this->inChannel($channel->id);
                default:
                    return false;
            }
        //});
    }

    /**
     * @param int $brand_id
     * @param string|null $type
     * @return mixed
     */
    public function bindToBrand(int $brand_id, string $type = null)
    {
        $type = $type ?? Brand::class;

        return BrandUser::updateOrCreate([
            'user_id' => $this->id,
            'brand_id' => $brand_id,
            'brand_type' => $type,
        ]);
    }

    /**
     * @param int $brand_id
     * @return mixed
     */
    public function bindToCountryBrand(int $brand_id)
    {
        return $this->bindToBrand($brand_id, CountryBrand::class);
    }

    /**
     * @param int $channel_id
     * @param string $status
     * @return mixed
     */
    public function bindToChannel(int $channel_id, string $status = 'on_hold')
    {
        return UserChannelStatus::updateOrCreate(
            [
                'user_id' => $this->id,
                'channel_id' => $channel_id,
            ], [
                'status' => $status
            ]
        )->setAppends([]);
    }

    /**
     * @param int $dealer_id
     * @return mixed
     */
    public function bindToDealer(int $dealer_id)
    {
        return UsersDealers::firstOrCreate([
            'user_id' => $this->id,
            'dealer_id' => $dealer_id,
        ]);
    }

    /**
     * @param int $position_id
     * @return mixed
     */
    public function bindToPosition(int $position_id)
    {
        return UserPosition::firstOrCreate([
            'user_id' => $this->id,
            'position_id' => $position_id,
        ]);
    }

    /**
     * @param array $data
     */
    public function manageSystemBindings(array $data)
    {
        switch ($data['role']) {
            case 'super_admin':
            case 'internal_admin':
                $this->removeAllBindings();
                break;
            case 'brand_admin':
            case 'global_manager':
                $this->removeAllBindings();
                foreach ($data['brands'] as $brand) {
                    $this->bindToBrand($brand['brand_id']);
                }
                break;
            case 'brand_manager':
                $this->removeAllBindings();
                foreach ($data['country_brands'] as $brand) {
                    $this->bindToCountryBrand($brand['country_brand_id']);
                }
                break;
            case 'app_user':
                BrandUser::where('user_id', $this->id)->delete();
                foreach ($data['channels'] as $channel_data) {
                    $channel     = Channel::with('dealers', 'positions')->find($channel_data['channel_id']);
                    $dealerIDs   = $channel->dealers->pluck('id')->toArray();
                    $positionIDs = $channel->positions->pluck('id')->toArray();

                    $this->removeFromDealer($dealerIDs);
                    $this->removeFromPosition($positionIDs);

                    if ($this->boolval($channel_data['remove_from_channel'])) {
                        $this->removeFromChannel([$channel->id]);
                    } else {
                        if (! $this->inChannel($channel_data['channel_id'])) {
                            $this->bindToChannel($channel_data['channel_id']);
                        }

                        if (! empty($channel_data['dealer_id']) && in_array($channel_data['dealer_id'], $dealerIDs)) {
                            $this->bindToDealer($channel_data['dealer_id']);
                        }

                        if (! empty($channel_data['position_id']) && in_array($channel_data['position_id'], $positionIDs)) {
                            $this->bindToPosition($channel_data['position_id']);
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    /**
     * @param array $data
     */
    public function manageChannelBindings(array $data)
    {

        if ($data['dealer_id']) {
            $dealer = Dealer::find($data['dealer_id']);
            $dealers = Dealer::where('channel_id', $dealer->channel_id)
                ->get()
                ->pluck('id')
                ->toArray();

            if ($dealers) {
                $this->removeFromDealer($dealers);
            }
            $this->bindToDealer($dealer->id);
        }

        if ($data['position_id']) {
            $position  = Position::find($data['position_id']);
            $positions = Position::where('channel_id', $position->channel_id)
                ->get()
                ->pluck('id')
                ->toArray();

            if ($positions) {
                $this->removeFromPosition($positions);
            }
            $this->bindToPosition($position->id);
        }
    }

    /**
     * @param \App\Http\Requests\Request $request
     * @param string $level
     * @return bool
     */
    public function removeFromLevel(Request $request, string $level) {
        switch($this->role) {
            case 'global_manager':
            case 'brand_admin':
                return $level == 'brand' ? $this->removeFromBrand([$request->brand_id]) : false;
            case 'brand_manager':
                switch ($level) {
                    case 'brand':
                        $ids = Brand::find($request->brand_id)
                            ->countryBrands()->pluck('id')->toArray();
                        break;
                    case 'country_brand':
                        $ids = [$request->brand_id];
                        break;
                    default:
                        return false;
                }
                return $this->removeFromCountryBrand($ids);
            case 'app_user':
            case 'hq':
                switch ($level) {
                    case 'brand':
                        $channels = Brand::find($request->brand_id)
                            ->channels()->get();
                        $channel_ids = $channels->pluck('id')->toArray();
                        break;
                    case 'country_brand':
                        $channels = CountryBrand::find($request->country_brand_id)
                            ->channels()->get();
                        $channel_ids = $channels->pluck('id')->toArray();
                        break;
                    case 'channel':
                        $channels = Channel::where('id', $request->channel_id)->get();
                        $channel_ids = [$request->channel_id];
                        break;
                    default:
                        return false;
                }

                $dealers_ids = $channels->flatMap(function ($channel) {
                    return $channel->dealers()->get()->pluck('id')->toArray();
                })->toArray();

                if ($dealers_ids) {
                    $this->removeFromDealer($dealers_ids);
                }

                $positions_ids = $channels->flatMap(function ($channel) {
                    return $channel->positions()->get()->pluck('id')->toArray();
                })->toArray();

                if ($positions_ids) {
                    $this->removeFromPosition($positions_ids);
                }

                return $this->removeFromChannel($channel_ids);
        }

        return false;
    }

    /**
     * @return void
     */
    public function removeAllBindings()
    {
        BrandUser::where('user_id', $this->id)->delete();
        UserChannelStatus::where('user_id', $this->id)->delete();
        UserPosition::where('user_id', $this->id)->delete();
        UsersDealers::where('user_id', $this->id)->forceDelete();
    }

    /**
     * @param array $brand_ids
     * @param string $type
     * @return bool
     */
    public function removeFromBrand(array $brand_ids, string $type = null)
    {
        $type = $type ?? Brand::class;
        return !! BrandUser::where('user_id', $this->id)
            ->whereIn('brand_id', $brand_ids)
            ->where('brand_type', $type)
            ->delete();
    }

    /**
     * @param array $brand_ids
     * @return bool
     */
    public function removeFromCountryBrand(array $brand_ids)
    {
        return $this->removeFromBrand($brand_ids, CountryBrand::class);
    }

    /**
     * @param array $channel_ids
     * @return bool
     */
    public function removeFromChannel(array $channel_ids)
    {
        return !! UserChannelStatus::where('user_id', $this->id)
            ->whereIn('channel_id', $channel_ids)
            ->delete();
    }

    /**
     * @param array $dealer_ids
     * @return bool
     */
    public function removeFromDealer(array $dealer_ids)
    {
        return !! UsersDealers::where('user_id', $this->id)
            ->whereIn('dealer_id', $dealer_ids)
            ->delete();
    }

    /**
     * @param array $position_ids
     * @return bool
     */
    public function removeFromPosition(array $position_ids)
    {
        return !! UserPosition::where('user_id', $this->id)
            ->whereIn('position_id', $position_ids)
            ->delete();
    }

    /**
     * @param string $email
     * @return string
     */
    public function getUniqueHQEmail(string $email) {
        static $emails;

        if ( ! $emails ) {
            $emails = $this->pluck('email')->all();
        }

        if (!in_array($email, $emails)) {
            $emails[] = $email;
            return $email;
        }

        $head = substr($email, 0, -9);
        $tail = substr($email, -9);

        $postfix = 0;
        $found = false;

        while (! $found) {
            $postfix++;
            $new_email = "{$head}.{$postfix}{$tail}";

            if (! in_array($new_email, $emails)) {
                $emails[] = $new_email;
                $email = $new_email;
                $found = true;
            }
        }
        return $email;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getUserActiveChannels()
    {
        $userId = auth()->user()->id;
        $channels = Channel::join('user_channel_statuses', function ($join) use ($userId) {
            $join->on('channels.id', '=', 'user_channel_statuses.channel_id')
                ->where(['user_channel_statuses.user_id' => $userId, 'user_channel_statuses.status' => 'active']);
        })->get(['channels.id', 'channels.name']);

        return $channels;
    }

    /**
     * @param int $brand_id
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getChannels(int $brand_id) {
        $channels = with(new Channel)->getTable();
        switch ($this->role) {
            case 'super_admin':
            case 'internal_admin':
                return Brand::with('channels')
                    ->find($brand_id)
                    ->channels()
                    ->get(["{$channels}.id", "{$channels}.name"]);
            case 'brand_admin':
            case 'global_manager':
                $brand_users = with(new BrandUser)->getTable();
                $brands = with(new Brand)->getTable();
                return Brand::where("{$brands}.id", $brand_id)
                    ->leftJoin($brand_users, function($join) use ($brand_users, $brands) {
                        $join->on("{$brand_users}.brand_id", "{$brands}.id")
                            ->where("{$brand_users}.brand_type", Brand::class)
                            ->where("{$brand_users}.user_id", $this->id);
                    })->select("{$brands}.id")
                    ->with('channels')
                    ->first()
                    ->channels()
                    ->get(["{$channels}.id", "{$channels}.name"]);
            case 'brand_manager':
                $brand_users = with(new BrandUser)->getTable();
                $brands = with(new CountryBrand)->getTable();
                return CountryBrand::where("{$brands}.brand_id", $brand_id)
                    ->leftJoin($brand_users, function($join) use ($brand_users, $brands) {
                        $join->on("{$brand_users}.brand_id", "{$brands}.id")
                            ->where("{$brand_users}.brand_type", CountryBrand::class)
                            ->where("{$brand_users}.user_id", $this->id);
                    })->select("{$brands}.id")
                    ->with('channels')
                    ->first()->channels()->get(["{$channels}.id", "{$channels}.name"]);
            default:
                $statuses = with(new UserChannelStatus)->getTable();
                return Channel::whereIn("{$channels}.id", Brand::find($brand_id)->channels->pluck('id'))
                    ->join($statuses, function($join) use ($channels, $statuses) {
                        $join->on("{$statuses}.channel_id", "{$channels}.id")
                            /**
                             * Commented due to user can't get the channels where he wasn't been logged in
                             */
//                            ->where("{$statuses}.status", 'active')
                            ->where("{$statuses}.user_id", $this->id);
                    })->get(["{$channels}.id", "{$channels}.name"]);
        }
    }

    /**
     * @param array $ids
     * @return array
     */
    protected function removeCurrentId(array $ids)
    {
        $key = array_search( auth()->user()->id, $ids);

        if ($key !== false) {
            unset($ids[$key]);
        }

        return $ids;
    }
}
