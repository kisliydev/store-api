<?php

namespace App\Models;

use App\Http\Requests\Media\MediaListRequest;
use Illuminate\Pagination\AbstractPaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BrandUser extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'brand_id',
        'user_id',
        'brand_type',
    ];

    /**
     * @param \App\Http\Requests\Media\MediaListRequest $request
     * @return mixed
     */
    public function getListForMedia(MediaListRequest $request)
    {
        $user = Auth::user();
        if (! $user->isAdmin()) {
            return false;
        }

        $callback = $user->role == 'brand_manager' ? 'getCountryBrandsByRole' : 'getBrandsByRole';

        $data = $this->addPagination(
            $this->$callback(),
            $request->query(),
            $request->per_page,
            filter_var($request->simple_paginate, FILTER_VALIDATE_BOOLEAN)
        );

        if ($user->isSystemAdmin() && (! $request->page || $request->page == 1)) {
            $this->addSuperAdminLevelItems($data);
        }

        return $data;
    }

    /**
     * @param \Illuminate\Pagination\AbstractPaginator $data
     */
    private function addSuperAdminLevelItems(AbstractPaginator $data)
    {
        $avatars = Media::where('type', 'avatars')->get()->count();
        $logos = Media::where('type', 'brand_logos')->get()->count();
        $data->setCollection(collect([
            [
                'verb' => 'admin',
                'url' => 'type=avatars',
                'name' => 'avatars',
                'type' => 'folder',
                'items_count' => $avatars,
                'next' => false,
                'previous' => false,
            ],
            [
                'verb' => 'admin',
                'url' => 'type=brand_logos',
                'name' => 'brand logos',
                'type' => 'folder',
                'items_count' => $logos,
                'next' => false,
                'previous' => false,
            ],
        ])->merge($data->getCollection()));
    }

    /**
     * @return mixed
     */
    private function getBrandsByRole()
    {
        /* database tables names */
        $b = with(new Brand)->getTable();
        $cb = with(new CountryBrand)->getTable();
        $c = with(new Channel)->getTable();
        $m = with(new Media)->getTable();
        $bu = $this->getTable();

        /* columns to be selected */
        $next = DB::raw("'country_brands' as next");
        $previous = DB::raw("false as previous");
        $folder = DB::raw("'folder' as type");
        $url = DB::raw("concat('".url('/api/media/country/brands?brand_id=')."', {$b}.id) as url");
        $count = DB::raw("count({$m}.id) as items_count");

        $user = Auth::user();
        $query = $user->isSystemAdmin() ? Brand::query() : $this->where('user_id', $user->id)->where('brand_type', '=', Brand::class)->leftJoin($b, "{$b}.id", '=', "{$bu}.brand_id");

        return $query->select(
                "{$b}.id",
                "{$b}.name",
                $folder,
                $url,
                $count,
                $next,
                $previous
            )
            ->leftJoin($cb, "{$cb}.brand_id", '=', "{$b}.id")
            ->leftJoin($c, "{$c}.country_brand_id", '=', "{$cb}.id")
            ->leftJoin($m, "{$m}.channel_id", '=', "{$c}.id")
            ->groupBy("{$b}.id");
    }

    /**
     * @return mixed
     */
    private function getCountryBrandsByRole()
    {
        /* database tables names */
        $b = with(new Brand)->getTable();
        $cb = with(new CountryBrand)->getTable();
        $c = with(new Channel)->getTable();
        $m = with(new Media)->getTable();
        $bu = $this->getTable();

        /* columns to be selected */
        $folder = DB::raw("'folder' as type");
        $url = DB::raw("concat('".url('/api/media/channels?country_brand_id=')."', {$cb}.id) as url");
        $count = DB::raw("count({$m}.id) as items_count");

        return $this->where('user_id', Auth::user()->id)
            ->where('brand_type', '=', CountryBrand::class)
            ->select(
                "{$cb}.name as country",
                "{$b}.name as brand",
                $folder,
                $url,
                $count
            )
            ->leftJoin($cb, "{$cb}.id", '=', "{$bu}.brand_id")
            ->leftJoin($b, "{$b}.id", '=', "{$cb}.brand_id")
            ->join($c, "{$c}.country_brand_id", '=', "{$cb}.id")
            ->join($m, "{$m}.channel_id", '=', "{$c}.id")
            ->groupBy("{$cb}.id");
    }
}
