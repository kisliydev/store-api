<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PlayerId extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'status_id',
        'player_id',
        'app_name',
    ];
}
