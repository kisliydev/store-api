<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\UserAnswer\UserAnswerGetRequest;

class UserAnswer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'text',
        'question_id',
        'point_statistics_id',
        'created_at',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param \App\Http\Requests\UserAnswer\UserAnswerGetRequest $request
     * @param \App\Models\Question $question
     * @return mixed
     */
    public function getSurveyAnswers(UserAnswerGetRequest $request, Question $question)
    {
        $q = $question->getTable();
        $ua = $this->getTable();
        return $this->select("{$q}.text as question", "{$ua}.text as answer")
            ->join($q, function($join) use ($request, $q, $ua){
                $join->on("{$q}.id", '=', "{$ua}.question_id")
                    ->where("{$q}.item_id", '=', $request->survey_id);
            })
            ->where('user_id', $request->user_id)
            ->orderBy("{$q}.order")->get();
    }
}
