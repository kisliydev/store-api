<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\ToDo\GetInChannelRequest;

class ToDoItem extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'text',
        'order',
    ];

    /**
     * @param \App\Http\Requests\ToDo\GetInChannelRequest $request
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getInChannel(GetInChannelRequest $request)
    {
        $todo = $this->getTable();
        $done = with(new ToDoItemChannel)->getTable();
        return $this->leftJoin($done, function($join) use ($request, $todo, $done) {
            $join->on("{$todo}.id", "{$done}.item_id")
                ->where("{$done}.channel_id", $request->channel_id);
        })->orderBy('order')->select("{$todo}.id", "{$todo}.text", "{$todo}.order", "{$done}.done_by")->get();
    }
}
