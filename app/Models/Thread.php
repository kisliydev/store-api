<?php

namespace App\Models;

use App\Http\Requests\GeneralReport\MessagesRequest;
use App\Http\Requests\Message\StoreRequest as MessageStoreRequest;
use App\Http\Requests\Thread\ThreadListRequest;
use App\Http\Requests\Thread\ThreadUpdateRequest;
use App\Http\Requests\Thread\ThreadCreateRequest;
use App\Http\Requests\Thread\ThreadUserCreateRequest;
use App\Http\Requests\Thread\DeleteUsersFromDealerRequest;
use App\Http\Requests\Thread\UpdateRequest;
use App\Http\Requests\Thread\PublishRequest;
use App\Jobs\MessageAutoResponse;
use App\Jobs\OneSignalPushNotifications;
use App\Notifications\OneSignalNotification;
use App\Transformers\Messages\UserIdTransformer;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use App\Mail\OpenThreadNotification;
use App\Mail\DeleteFromDealerNotification;
use App\Traits\Notifications;
use App\Traits\ThreadListQueryBuilder;
use Illuminate\Support\Facades\DB;
use Throwable;

class Thread extends Model
{
    use Notifications;
    use ThreadListQueryBuilder;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subject',
        'status',
        'data',
        'type',
        'channel_id',
        'author_id',
        'district_id',
        'dealer_id',
        'created_at',
        'publish_at',
        'published_at'
    ];

    /**
     * @var \App\Transformers\Messages\UserIdTransformer
     */
    private $userIdTransformer;

    /**
     * @var bool
     */
    private $skipPushNotification = false;

    /**
     * Thread constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->userIdTransformer = new UserIdTransformer();
    }

    /**
     * @param \App\Http\Requests\Thread\ThreadListRequest $request
     * @param string $status
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getAdminList(ThreadListRequest $request, string $status)
    {
        $relations = [
            'lastMessage' => $this->lastThreadMessageClosure($request->channel_id),
            'unreadByAdminMessages',
            "recipientsCount"
        ];

        if ('scheduled' == $status) {
            array_push($relations, "recipients");
        }

        $query = $this->select('threads.*')
            ->distinct()
            ->where('channel_id', $request->channel_id)
            ->where('status', $status)
            //->whereNull('parent_id')
            ->with($relations)
            ->orderBy('updated_at', 'desc');

        $this->addSearchToQuery($request, $query);
        $this->addMultiFilters($query, $request->filter);
        $this->addMultiSort($query, $request->sort);

        if ($request->with_pagination) {
            return $this->addPagination(
                $query, $request->query(),
                $request->per_page,
                filter_var($request->simple_paginate, FILTER_VALIDATE_BOOLEAN)
            );
        }

        return $query->get();
    }

    /**
     * @param \App\Http\Requests\Thread\ThreadListRequest $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getUserList(ThreadListRequest $request)
    {
        $query = $this->getUserThreadQuery($this->query(), $request)
            ->with([
                'lastMessage' => $this->lastThreadMessageClosure($request->channel_id),
                'unreadByUserMessages'
            ]);

        $this->addSearchToQuery($request, $query);

        if ($request->with_pagination) {
            return $this->addPagination(
                $query, $request->query(),
                $request->per_page,
                filter_var($request->simple_paginate, FILTER_VALIDATE_BOOLEAN)
            );
        }

        return $query->get();
    }

    /**
     * @param \App\Http\Requests\Message\StoreRequest $request
     * @return mixed
     */
    public function manage(MessageStoreRequest $request)
    {
        $thread = $this->find($request->thread_id);

        if (
            in_array($thread->type, ['manual', 'from_admin', 'auto_level'])
            && empty($thread->parent_id)
            && ! $this->_threadSentToOnlyUser($thread->recipients()->get())
        ) {
            /*
             * When user replies the thread that was created from admin panel
             * we need to create the new 1-by-1 (user-admin) thread.
             */
            $childThread = $thread->replicate();
            $childThread->subject   = "Re: {$thread->subject}";
            $childThread->status    = 'open';
            $childThread->type      = 'from_user';
            $childThread->parent_id = $thread->id;
            $childThread->save();

            $childThread->recipients()->createMany([[
                'object_type' => 'user_id',
                'object_id' => auth()->user()->id,
            ]]);

            /*
             * Copy the message from old thread
             * in order to make an ability for users
             * see the origin message in the thread
             */
            $messages = $thread->messages;
            if ($messages->count()) {
                $oldMessage = $messages->first();
                $message    = $oldMessage->replicate();
                $message->thread_id = $childThread->id;

                /*
                 * set the created/updated dates
                 * according to the message from the origin thread
                 * in order to keep the chronology of the correspondence
                 */
                $message->updated_at = $oldMessage->updated_at;
                $message->created_at = $oldMessage->created_at;
                $message->save();

                /*
                 * Some small hack. After the message was copied to the new thread
                 * it shouldn't bee in the number of unread messages.
                 */
                MessageStatus::create([
                    'message_id' => $message->id,
                    'user_id'    => auth()->user()->id,
                    'read_by'    => 'admin',
                ]);
            }

            return $childThread->id;
        }

        if ('open' !== $thread->status) {
            $thread->status = 'open';
            $thread->save();
        }

        return $thread->id;
    }

    /**
     * @param $recipients
     * @param int|null $userID
     * @return bool
     */
    private function _threadSentToOnlyUser($recipients, int $userID = null)
    {
        $count = $recipients->count();
        if (! $count || $count > 1) {
            return false;
        }

        $userID = $userID ?? auth()->user()->id;
        $recipient = $recipients->first();
        return $recipient->object_type == 'user_id' && $recipient->object_id == $userID;
    }

    /**
     * @param \App\Http\Requests\Thread\ThreadListRequest $request
     * @param \Illuminate\Database\Eloquent\Builder $query
     */
    private function addSearchToQuery(ThreadListRequest $request, Builder $query)
    {
        if (! $request->search) {
            return;
        }
        $query->where(function($where) use ($request) {
            $t = 'threads';
            $m = 'messages';
            $where->whereIn("{$t}.id" ,function($subQuery) use ($request, $m) {
                $subQuery->select("{$m}.thread_id")
                    ->distinct()
                    ->from($m)
                    ->where("{$m}.content", 'like', "%{$request->search}%");
                })
                ->orWhere("{$t}.subject", 'like', "%{$request->search}%");
       });
    }

    /**
     * @param \App\Http\Requests\Thread\DeleteUsersFromDealerRequest $request
     * @return bool|mixed
     */
    public function addUsersDeleteThread(DeleteUsersFromDealerRequest $request)
    {
        $u = 'users';
        $ud = 'users_dealers';

        $data = $request->validated();
        $dealer = auth()->user()->dealer($data['channel_id'])->first();

        if (! $dealer) {
            return false;
        }

        /* Get the list of app users that are bind to the store manager's dealer */
        $appUsers = $this->removeAppends(User::whereIn("{$u}.id", $data['users'])
            ->select(
                "{$u}.id",
                "{$u}.email",
                "{$u}.first_name",
                "{$u}.last_name"
            )
            ->role('app_user')
            ->join($ud, function($join) use ($dealer, $ud, $u) {
                $join->on("{$ud}.user_id", '=', "{$u}.id")
                    ->where("{$ud}.dealer_id", '=', $dealer->dealer_id);
            })
            ->get(), ['full_name']);

        if (! $appUsers->all()) {
            return false;
        }

        try {

            $viewData = [
                'dealer' => $dealer,
                'users' => $appUsers,
                'reason' => $data['reason']
            ];

            $saveData = (object) [
                'channel_id' => $data['channel_id'],
                'subject' => 'Delete Users Request',
                'message_content' => view('messages.delete_from_dealer')->with($viewData)->render(),
                'type' => 'from_user',
                'data' => null,
                'recipients' => 'user_id:'. auth()->user()->id,
            ];

        } catch (Throwable $e) {
            return false;
        }

        $thread = $this->_createEntity($saveData, 'open', auth()->user()->id, true);
        $thread->sendAutoResponse($request->channel_id);

        $this->maybeNotifyUsers($appUsers, DeleteFromDealerNotification::class, $data['channel_id'], $dealer);

        return $this->with([
            'lastMessage' => $this->lastThreadMessageClosure($thread->channel_id)
        ])->find($thread->id);
    }

    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param string|null $filters
     */
    public function addMultiFilters(Builder $query, string $filters = null)
    {
        if (! $filters) {
            return;
        }

        $filtersArgs = [];

        foreach (explode(',', $filters) as $filter) {
            list($criteria, $value) = explode(':', $filter);
            if (empty($filtersArgs[$criteria])) {
                $filtersArgs[$criteria] = [];
            }
            if ('keyword' == $criteria) {
                $filtersArgs[$criteria] = $value;
            } else {
                $filtersArgs[$criteria][] = $value;
            }
        }

        if (! empty($filtersArgs['keyword'])) {
            $query->where('subject', 'like', "%{$filtersArgs['keyword']}%");
            unset($filtersArgs['keyword']);
        }

        $query->join('recipients', function($subQuery) use ($filtersArgs) {
            $subQuery->on('recipients.thread_id', '=', 'threads.id')
                ->where(function($clause) use ($filtersArgs) {
                    $clause->where('object_type', 'all');
                    foreach ($filtersArgs as $object_type => $ids) {
                        $clause->orWhere(function($subClause) use ($object_type, $ids) {
                            $subClause->where('object_type', $object_type)->whereIn('object_id', $ids);
                        });
                    }
                });
        });
    }

    /**
     * @param \App\Http\Requests\Thread\ThreadUpdateRequest $request
     * @return bool
     */
    public function updateStatus(ThreadUpdateRequest $request)
    {
        $thread = $this->find($request->id);

        switch ($request->status) {
            /**
             * @deprecated status 'closed'
             * @todo remove
             */
            case 'closed':
            case 'done':
            case 'open':
                $readBy = 'admin';
                $thread->status = $request->status;
                $thread->save();
                break;
            case 'read_by_user':
                $readBy = 'user';
                $thread->touch();
                break;
            case 'read_by_admin':
                $readBy = 'admin';
                $thread->touch();
                break;
            default:
                return false;
        }

        $this->_readMessages($thread, $readBy);

        if ('admin' == $readBy) {
            $thread->channel_unread_messages_count = $thread->channel->unreadMessages->count();
            unset($thread->channel);
            unset($thread->unread_messages);
        }

        return $thread;
    }

    /**
     * @param ThreadCreateRequest $request
     * @param string $status
     * @return Thread|Thread[]|Builder|Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     * @throws \Exception
     */
    public function createEntity(ThreadCreateRequest $request, string $status)
    {
        return $this->_createEntity($request, $status);
    }

    /**
     * @param ThreadUserCreateRequest $request
     * @return Thread|Thread[]|Builder|Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     * @throws \Exception
     */
    public function userCreateEntity(ThreadUserCreateRequest $request)
    {
        $thread = $this->_createEntity($request, 'open', auth()->user()->id, true);
        $thread->sendAutoResponse($request->channel_id);
        return $this->with([
            'lastMessage' => $this->lastThreadMessageClosure($thread->channel_id)
        ])->find($thread->id);
    }

    /**
     * @param $request
     * @param string $status
     * @param int|null $authorID
     * @param bool $skipPush
     * @return Thread|Thread[]|Builder|Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     * @throws \Exception
     */
    public function _createEntity($request, string $status, int $authorID = null, bool $skipPush = false)
    {
        if (! $authorID) {
            $authorID = $this->_setMessageAuthorID($request->channel_id);
        }

        if (! $authorID) {
            throw new \Exception("Can't set the message author");
        }

        $this->skipPushNotification = $skipPush;

        $thread = $this->create([
            'channel_id'   => $request->channel_id,
            'author_id'    => $authorID,
            'subject'      => $request->subject,
            'type'         => $request->type,
            'status'       => $status,
            'data'         => $request->data,
            'publish_at'   => $request->publish_at ?? null,
            'published_at' => ('scheduled' === $status ? null : Carbon::now()->toDateTimeString()),
        ]);

        $message = app()->call(Message::class . '@add',[[
            'thread_id'   => $thread->id,
            'author_id'   => $authorID,
            'content'     => $request->message_content,
            'attachments' => $request->attachments ?? null,
            'from'        => 'from_user' == $request->type ? 'user' : 'admin',
            'publish_at' => $request->publish_at ?? null,
        ]]);

        $recipients = $this->getRecipientsArray($request->recipients);
        $thread->recipients()->createMany($recipients);

        if ('scheduled' !== $status && $request->message_content) {
            $this->sendPushNotification($thread->getRecipientsIDs(), $thread, $message->content);
        }

        $this->sendEmailNotificationToChannelReceivers($status, $request->channel_id);

        return $this->with([
                'lastMessage' => $this->lastThreadMessageClosure($request->channel_id)
            ])->find($thread->id);
    }

    /**
     * @param \App\Http\Requests\Thread\UpdateRequest $request
     * @return \App\Models\Thread|\App\Models\Thread[]|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function updateScheduled(UpdateRequest $request)
    {
        $data = $request->validated();
        $thread = $this->find($request->thread_id);

        $thread->subject    = $data['subject'];
        $thread->publish_at = $data['publish_at'];
        $thread->save();

        Recipient::where('thread_id', $thread->id)->delete();
        $recipients = $this->getRecipientsArray($data['recipients']);
        $thread->recipients()->createMany($recipients);

        app()->call(Message::class . '@resave',[$data]);

        return $this->with([
                'lastMessage' => $this->lastThreadMessageClosure($thread->channel_id)
            ])->find($thread->id);
    }

    /**
     * @param \App\Http\Requests\Thread\PublishRequest $request
     * @return mixed
     */
    public function deleteScheduled(PublishRequest $request)
    {
        $thread = $this->find($request->thread_id);

        $this->destroy($thread->id);

        return $thread;
    }

    /**
     * @param array $threadIds
     */
    public function deleteById(array $threadIds)
    {
        $threads = $this->find($threadIds);
        $recipients = [];
        foreach ($threads as $thread) {
            $recipients = array_merge($recipients, $thread->getRecipientsIDs());
            $thread->delete();
        }

        return compact('threads', 'recipients');
    }

    /**
     * @param \App\Http\Requests\Thread\PublishRequest $request
     * @return \App\Models\Thread|\App\Models\Thread[]|\Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function publishScheduled(PublishRequest $request)
    {
        return $this->find($request->thread_id)->publish();
    }

    /**
     * @param string $type
     * @return $this
     */
    public function publish(string $type = OneSignalNotification::NOTIFICATION_TYPE_CHAT_MESSAGE) {
        $this->status       = 'closed';
        $this->published_at = Carbon::now()->toDateTimeString();
        $this->save();

        $this->sendPushNotification($this->getRecipientsIDs(), $this, $this->lastMessage->content, $type);

        return $this;
    }

    /**
     * @param \App\Models\Thread $thread
     * @param string $readBy
     * @return array|void
     */
    private function _readMessages(Thread $thread, string $readBy = 'admin')
    {
        switch($readBy) {
            case 'admin':
                $closure = $this->getNotReadByAdminClosure();
                break;
            case 'user':
                $closure = $this->getNotReadByUserClosure();
                break;
            default:
                return;
        }

        $unreadMessages = $thread->messages()
            ->whereNotExists($closure)
            ->get(['id']);

        if (!$unreadMessages->count()) {
            return;
        }

        $userID = auth()->user()->id;

        $unreadMessages->each(function($message) use ($userID, $readBy) {
            MessageStatus::create([
                'message_id' => $message->id,
                'user_id'    => $userID,
                'read_by'    => $readBy,
            ]);
        });
    }

    /**
     * @param int $channelId
     */
    public function sendAutoResponse(int $channelId)
    {
        $settings = $this->_getChannelSettings($channelId);

        if (! $settings || ! $settings->auto_reply_from) {
            return;
        }

        dispatch((new MessageAutoResponse([
            'thread_id' => $this->id,
            'author_id' => $settings->auto_reply_from[0]->id,
            'content' => $settings->auto_reply_message,
            'attachments' => null,
            'from' => 'admin',
        ]))->onQueue(config('queue.tube.default')))
            ->delay(now()->addSeconds(5));
    }

    /**
     * @param string $status
     * @param int $channelId
     */
    public function sendEmailNotificationToChannelReceivers(string $status, int $channelId)
    {
        if ($status == 'open') {
            $this->maybeNotifyModerators( OpenThreadNotification::class, $channelId);
        }
    }

    public function getRecipientsIDs() {
        $usersIds = [];

        foreach ($this->recipients->toArray() as $recipient) {
            switch ($recipient['object_type']) {
                case 'league_id':
                    $usersIds = array_merge($usersIds, $this->getAllUserIdsFromLeague(intval($recipient['object_id'])));
                    break;
                case 'district_id':
                    $usersIds = array_merge($usersIds, $this->getAllUserIdsFromDistrict(intval($recipient['object_id'])));
                    break;
                case 'dealer_id':
                    if (empty($ud)) {
                        $ud = new UsersDealers();
                    }
                    $usersIds = array_merge($usersIds, $ud->getAllUserIdsFromDealer(intval($recipient['object_id'])));
                    break;
                case 'user_id':
                    $usersIds [] = $recipient['object_id'];
                    break;
                case 'all':
                    $usersIds = array_merge($usersIds, (new UserChannelStatus())->getChannelUserIds(
                        $this->channel_id,
                        $this->userIdTransformer
                    ));
                    break;
            }
        }
        return $usersIds;
    }

    /**
     * @param \App\Http\Requests\GeneralReport\MessagesRequest $request
     * @return mixed
     */
    public function getChannelAutoThreads(MessagesRequest $request)
    {
        $query = $this->where('channel_id', $request->channel_id)->where('type', 'auto_level')->where('status', 'closed');

        return $this->addPagination(
            $query, $request->query(),
            $request->per_page,
            filter_var($request->simple_paginate, FILTER_VALIDATE_BOOLEAN)
        );
    }

    /**
     * @param int $leagueID
     * @return mixed
     */
    public function getAllUserIdsFromLeague(int $leagueID)
    {
        return $this->_getFromDealers('league_id', $leagueID);
    }

    /**
     * @param int $districtId
     * @return mixed
     */
    public function getAllUserIdsFromDistrict(int $districtId)
    {
        return  $this->_getFromDealers('district_id', $districtId);
    }

    /**
     * @param string $field
     * @param int $id
     * @return mixed
     */
    private function _getFromDealers(string $field, int $id)
    {
        $ud = 'users_dealers';
        $d  = 'dealers';
        return UsersDealers::select("{$ud}.user_id")
            ->join($d, function($join) use ($d, $ud, $field, $id) {
                $join->on("{$d}.id", '=', "{$ud}.dealer_id")
                    ->where("{$d}.{$field}", $id);
            })
            ->get(["{$ud}.user_id"])
            ->pluck('user_id')
            ->toArray();
    }

    /**
     * @param string $requestRecipients
     * @return array
     */
    private function getRecipientsArray(string $requestRecipients)
    {
        $recipients = [];

        if (! $requestRecipients) {
            return $recipients;
        }

        $requestRecipients = explode(',', $requestRecipients);

        foreach ($requestRecipients as $recipient) {
            list($objectType, $objectId) = explode(':', $recipient);
            $recipients[] = [
                'object_type' => $objectType,
                'object_id' => $objectId,
            ];
        }

        return $recipients;
    }

    /**
     * @param array $usersIds
     * @param Thread $thread
     * @param string $message
     * @param string $type
     */
    public function sendPushNotification(
        array $usersIds,
        Thread $thread,
        string $message,
        $type = OneSignalNotification::NOTIFICATION_TYPE_CHAT_MESSAGE
    ) {
        if($this->skipPushNotification) {
            return;
        }

        $title = $thread->channel->name.', '.$thread->subject;

        dispatch((new OneSignalPushNotifications(
            array_unique($usersIds),
            [
                'subject' => $title,
                'body' => $message,
                'data' => OneSignalNotification::getPushData($type, $thread),
                'channel_id' => $thread->channel_id
            ],
            get_class($this)
        ))->onQueue(config('queue.tube.push-notifications')));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recipients()
    {
        return $this->hasMany('App\Models\Recipient');
    }

    /**
     * @return $this
     */
    public function recipientsCount()
    {
        return $this->recipients()->select(
            DB::raw("COUNT(object_id) as count"),
            'thread_id',
            'object_type'
        )
            ->groupBy(
                'object_type',
                'thread_id'
            );
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany('App\Models\Message');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function lastMessage()
    {
        return $this->hasMany('App\Models\Message')->toHasOne('thread_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function unreadByAdminMessages()
    {
        return $this->hasMany('App\Models\Message')
            ->whereNotExists($this->getNotReadByAdminClosure());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany|\Illuminate\Database\Query\Builder
     */
    public function unreadByUserMessages()
    {
        return $this->hasMany('App\Models\Message')
            ->whereNotExists($this->getNotReadByUserClosure());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sentStatistics()
    {
        return $this->hasMany('App\Models\ThreadSendStatistic');
    }
}
