<?php

namespace App\Models;


use App\Http\Requests\Request;
use App\Http\Requests\Level\GetListRequest;
use App\Http\Requests\Level\CreateRequest;
use App\Http\Requests\Level\UpdateRequest;
use App\Http\Requests\Level\QuickEditRequest;
use App\Http\Requests\Level\DeleteRequest;

class Level extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'channel_id',
        'title',
        'description',
        'type',
        'type_data',
        'points',
        'use_weight_points',
    ];

    /**
     * @return array
     */
    public  function getTypesList()
    {
        return [
            'total_points',
            'time_points',
            'total_sales',
            'total_sales_by_product',
            'total_sales_by_dealer',
            'total_quizzes',
            'total_surveys',
        ];
    }

    /**
     * @return array
     */
    public function getTypesSchema()
    {
        $daysOption = [
            ['value' => '1', 'title' => 'Monday'],
            ['value' => '2', 'title' => 'Tuesday'],
            ['value' => '3', 'title' => 'Wednesday'],
            ['value' => '4', 'title' => 'Thursday'],
            ['value' => '5', 'title' => 'Friday'],
            ['value' => '6', 'title' => 'Saturday'],
            ['value' => '7', 'title' => 'Sunday'],
        ];

        $amount = ['name' => 'amount', 'title' => 'Amount Required', 'value' => null];

        return [
            [
                'type' => 'total_points',
                'title' => 'Total number of points',
                'type_data' => [$amount],
            ],
            [
                'type' => 'time_points',
                'title' => 'Total number of points for particular time',
                'type_data' => [
                    $amount,
                    [
                        'type' => 'select',
                        'name' => 'based_on',
                        'title' => 'Based on',
                        'options' => [
                            ['value' => 'week_days', 'title' => 'Week days'],
                            ['value' => 'date_range', 'title' => 'Date range'],
                        ],
                    ],
                    [
                        'type' => 'select',
                        'parent' => 'based_on',
                        'parent_value' => 'week_days',
                        'title' => 'From Week Day',
                        'name' => 'from_week_day',
                        'options' => $daysOption,
                    ],
                    [
                        'type' => 'select',
                        'parent' => 'based_on',
                        'parent_value' => 'week_days',
                        'title' => 'To Week Day',
                        'name' => 'to_week_day',
                        'options' => $daysOption,
                    ],
                    [
                        'type' => 'input',
                        'name' => 'from_date',
                        'title' => 'From Date',
                        'parent' => 'based_on',
                        'parent_value' => 'date_range',
                        'value' => null,
                    ],
                    [
                        'type' => 'input',
                        'name' => 'to_date',
                        'title' => 'To Date',
                        'parent' => 'based_on',
                        'parent_value' => 'date_range',
                        'value' => null,
                    ],
                ],
            ],
            [
                'type' => 'total_sales',
                'title' => 'Total number of sales',
                'type_data' => [$amount],
            ],
            [
                'type' => 'total_sales_by_product',
                'title' => 'Total number of sales by product',
                'type_data' => [
                    $amount,
                    [
                        'type' => 'select',
                        'title' => 'Product',
                        'name' => 'product',
                    ],
                ],
            ],
            [
                'type' => 'total_sales_by_dealer',
                'title' => 'Total number of sales by dealer',
                'type_data' => [$amount],
            ],
            [
                'type' => 'total_quizzes',
                'title' => 'Completed amount of quizes',
                'type_data' => [$amount],
            ],
            [
                'type' => 'total_surveys',
                'title' => 'Completed amount of surveys',
                'type_data' => [$amount],
            ],
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function achievements()
    {
        return $this->hasMany(AchievedLevel::class);
    }

    /**
     * @param GetListRequest $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getList(GetListRequest $request)
    {
        return $this->_getListByChannel($request, [], ['title', 'description'], !!$request->simple_paginate);
    }

    /**
     * @param CreateRequest $request
     * @return mixed
     */
    public function add(CreateRequest $request)
    {
        $allowedFields = [
            'channel_id',
            'title',
            'description',
            'type',
            'type_data',
        ];
        return $this->create($request->only($allowedFields));
    }

    /**
     * @param UpdateRequest $request
     * @return mixed
     */
    public function resave(UpdateRequest $request)
    {
        return $this->_resave($request, ['title','description','type', 'type_data']);
    }

    /**
     * @param QuickEditRequest $request
     * @return mixed
     */
    public function quickEdit(QuickEditRequest $request)
    {
        return $this->_resave($request, ['points','use_weight_points']);
    }

    /**
     * @param DeleteRequest $request
     * @return bool
     */
    public function remove(DeleteRequest $request)
    {
        $levels = $this->whereIn('id', $request->id)->get();

        if (! $levels->all()) {
            return false;
        }

        $this->destroy($request->id);

        return $levels;
    }

    /**
     * @param Request $request
     * @param array $allowedFields
     * @return mixed
     */
    private function _resave(Request $request, array $allowedFields)
    {
        $level = $this->find($request->id);

        foreach($allowedFields as $key) {
            $level->$key = $request->$key;
        }

        $level->save();
        return $level;
    }
}
