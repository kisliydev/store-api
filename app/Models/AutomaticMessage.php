<?php

namespace App\Models;

use App\Http\Requests\AutomaticMessage\GetListRequest;
use App\Http\Requests\AutomaticMessage\CreateRequest;
use App\Http\Requests\AutomaticMessage\UpdateRequest;
use App\Http\Requests\AutomaticMessage\DeleteRequest;

class AutomaticMessage extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'channel_id',
        'type',
        'title',
        'description',
    ];

    /**
     * @return array
     */
    public function getTypesList()
    {
        return [
            'new_quiz',
            'new_survey',
            'campaign_started',
            'campaign_sale_started',
            'campaign_sale_finished',
            'campaign_finished',
        ];
    }

    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }
    /**
     * @return array
     */
    public function getTypesSchema()
    {
        return [
            [
                'type'  => 'new_quiz',
                'title' => 'New quiz is released',
            ],
            [
                'type'  => 'new_survey',
                'title' => 'New survey is released',
            ],
            [
                'type'  => 'campaign_started',
                'title' => 'Triggered when new campaign started',
            ],
            [
                'type'  => 'campaign_sale_started',
                'title' => 'Triggered when sale started',
            ],
            [
                'type'  => 'campaign_sale_finished',
                'title' => 'Triggered when sale finished',
            ],
            [
                'type'  => 'campaign_finished',
                'title' => 'Triggered when campaign finished',
            ]
        ];
    }

    /**
     * @param string $campaignStatus
     * @param int $channelID
     * @return mixed
     */
    public function getCampaignRelativeMessages(string $campaignStatus, int $channelID)
    {
        switch ($campaignStatus) {
            case 'pre_active':
                $type = 'campaign_started';
                break;
            case 'active':
                $type = 'campaign_sale_started';
                break;
            case 'post_active':
                $type = 'campaign_sale_finished';
                break;
            case 'finished':
                $type = 'campaign_finished';
                break;
            default:
                return [];
        }

        return $this->where('type', $type)
            ->where('channel_id', $channelID)
            ->get();
    }

    /**
     * @param int $channelId
     * @param string $academyItemType
     * @return mixed
     */
    public function getAcademyRelativeMessages(int $channelId, string $academyItemType)
    {
        return $this->where('channel_id', $channelId)
            ->where('type', "new_{$academyItemType}")
            ->get();
    }

    /**
     * @param GetListRequest $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function getList(GetListRequest $request)
    {
        return $this->_getListByChannel($request, [], ['title', 'description'], !!$request->simple_paginate);
    }

    /**
     * @param CreateRequest $request
     * @return mixed
     */
    public function add(CreateRequest $request)
    {
        return $this->create($request->only($this->getFillable()));
    }

    /**
     * @param UpdateRequest $request
     * @return mixed
     */
    public function resave(UpdateRequest $request)
    {
        $message = $this->find($request->id);

        foreach(['title','description','type'] as $key) {
            $message->$key = $request->$key;
        }

        $message->save();
        return $message;
    }

    /**
     * @param DeleteRequest $request
     * @return bool
     */
    public function remove(DeleteRequest $request)
    {
        $messages = $this->whereIn('id', $request->id)->get();

        if (! $messages->all()) {
            return false;
        }

        $this->destroy($request->id);

        return $messages;
    }
}
