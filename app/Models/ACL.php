<?php

namespace App\Models;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\ACL\ACLUpdateRequest;
use App\Http\Requests\ACL\ACLGetRoleRequest;
use App\Http\Requests\ACL\ACLUpdateUserRequest;
use App\Http\Requests\ACL\ACLRestoreUserRequest;

class ACL extends Model
{
    /**
     * @param bool $without_super_admin
     * @return array
     */
    public function getRoles($without_super_admin = false)
    {
        $roles = $this->_roles;

        if ($without_super_admin) {
            unset($roles[array_search('super_admin', $roles)]);
        }

        return $roles;
    }

    /**
     * @param string|null $role
     * @return array|mixed
     */
    public function getDefaults(string $role = null)
    {
        if (! $role) {
            return $this->_flattenPermissions();
        }

        $permissions = [];

        foreach ($this->_flattenPermissions() as $permission => $roles) {
            if ((is_array($roles) && in_array($role, $roles)) || 'all' == $roles) {
                $permissions[] = $permission;
            }
        }

        return $permissions;
    }

    /**
     * @return array
     */
    public function getRegisteredPermissions()
    {
        return array_keys($this->getDefaults());
    }

    /**
     * @return mixed
     */
    public function getAll()
    {
        return Role::whereIn('name', $this->getRoles())->with('permissions')->get();
    }

    /**
     * @param \App\Http\Requests\ACL\ACLGetRoleRequest $request
     * @return mixed
     */
    public function getRole(ACLGetRoleRequest $request)
    {
        return Role::where('name', $request->role_name)->with('permissions')->get();
    }

    /**
     * @param \App\Http\Requests\ACL\ACLUpdateRequest $request
     * @return mixed
     */
    public function resave(ACLUpdateRequest $request)
    {
        foreach ($this->getRoles(true) as $role_name) {
            $permissions = array_intersect($this->getRegisteredPermissions(), array_unique($request->permissions[$role_name]));
            $this->_resave($role_name, $permissions);
        }

        $this->_flushCache();

        return $this->getAll();
    }

    /**
     * @return bool
     */
    public function restore()
    {
        $this->_flushCache();

        foreach ($this->_flattenPermissions() as $permission => $roles) {
            Permission::firstOrCreate(['name' => $permission]);
        }

        foreach ($this->getRoles() as $role_name) {
            $this->_resave($role_name, $this->getDefaults($role_name));
        }

        return $this->getAll();
    }

    /**
     * @param \App\Http\Requests\ACL\ACLUpdateUserRequest $request
     * @return mixed
     */
    public function updateUser(ACLUpdateUserRequest $request)
    {
        return $this->manageUser($request->user_id, $request->user_role, $request->permissions);
    }

    /**
     * @param \App\Http\Requests\ACL\ACLRestoreUserRequest $request
     * @return mixed
     */
    public function restoreUser(ACLRestoreUserRequest $request)
    {
        return $this->manageUser($request->user_id, $request->user_role);
    }

    /**
     * @return void
     */
    public function setup()
    {
        $this->_flushCache();
        foreach ($this->_roles as $role) {
            $$role = Role::firstOrCreate(['name' => $role]);
        }

        $temp = [];
        foreach ($this->_flattenPermissions() as $permission => $roles) {
            Permission::firstOrCreate(['name' => $permission]);

            if ('all' == $roles) {
                $roles = $this->_roles;
            }

            foreach ($roles as $role) {
                if (empty($temp[$role])) {
                    $temp[$role] = [];
                }

                $temp[$role][] = $permission;
            }
        }

        foreach($temp as $role => $permissions) {
            $$role->syncPermissions($permissions);
        }
    }

    /**
     * @param int $user_id
     * @param string $role
     * @param array $permissions
     * @return mixed
     */
    public function manageUser(int $user_id, string $role, array $permissions = [])
    {
        $user = User::find($user_id);

        if (strcmp($user->role, $role) !== 0) {
            $user->removeRole($user->role);
            $user->assignRole($role);
            $user->role = $role;
            $user->syncPermissions($this->getDefaults($role));
        }

        if (strcmp($user->role, 'super_admin') !== 0) {
            DeniedPermission::where('user_id', $user_id)->delete();

            $requested = $permissions ? array_unique($permissions) : $this->getDefaults($role);
            $role_permissions = $this->getDefaults($user->role);
            $denied = array_diff($role_permissions, $requested);
            $extra = array_intersect($this->getRegisteredPermissions(), array_diff($requested, $denied)) ?? [];

            $user->syncPermissions($extra);

            if ($denied) {
                $data = [];
                foreach ($denied as $permission) {
                    $data[] = [
                        'user_id' => $user_id,
                        'permission' => $permission,
                    ];
                }
                DeniedPermission::insert($data);
            }
        }

        $permissions = $user->getAllPermissions()->pluck('name');
        $cache = app()['cache']->tags(["user_acl:{$user->id}"]);
        $cache->flush();
        $cache->put("role:{$this->id}", $user->role, config('cache.lifetime'));
        $cache->put("acl:{$this->id}", $permissions, config('cache.lifetime'));

        return $permissions;
    }

    /**
     * @return array
     */
    private function _flattenPermissions() {
        $permissions = [];
        foreach($this->_permissions as $catPermissions) {
            $permissions = array_merge($permissions, $catPermissions);
        }
        return $permissions;
    }

    /**
     * @param string $role_name
     * @param array $permissions
     */
    private function _resave(string $role_name, array $permissions)
    {
        $role = Role::where('name', $role_name)->first();
        $role->syncPermissions($permissions);
    }

    /**
     * @return void
     */
    private function _flushCache()
    {
        $cache = app()['cache'];
        $cache->forget('spatie.permission.cache');
        $cache->tags(["acl"])->flush();
    }

    /**
     * @var array
     */
    private $_roles = [
        'super_admin',
        'internal_admin',
        'brand_admin',
        'global_manager',
        'brand_manager',
        'app_user',
        'hq',
    ];

    /**
     * @var array
     */
    private $_permissions = [

        'sap' => [
            'view_settings'       => ['super_admin'],
            'edit_settings'       => ['super_admin'],
            'view_permissions'    => ['super_admin', 'internal_admin'],
            'edit_permissions'    => ['super_admin'],
        ],

        'general' => [
            'view_all_users'      => ['super_admin', 'internal_admin'],
            'view_users'          => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
            'edit_users'          => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
            'view_brands'         => ['super_admin', 'internal_admin', 'brand_admin', 'global_manager', 'brand_manager'],
            'edit_brands'         => ['super_admin', 'internal_admin'],
            'view_country_brands' => ['super_admin', 'internal_admin', 'brand_admin', 'global_manager', 'brand_manager'],
            'edit_country_brands' => ['super_admin', 'internal_admin'],
            'view_channels'       => ['super_admin', 'internal_admin', 'brand_admin', 'global_manager', 'brand_manager'],
            'edit_channels'       => ['super_admin', 'internal_admin'],
            'view_profile'        => ['super_admin', 'internal_admin', 'brand_admin', 'global_manager', 'brand_manager'],
            'edit_profile'        => ['super_admin', 'internal_admin', 'brand_admin', 'global_manager', 'brand_manager'],
            'view_media'          => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
            'edit_media'          => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
        ],

        'channel' => [
            'view_channel_dashboard'  => ['super_admin', 'internal_admin', 'brand_admin', 'global_manager', 'brand_manager'],
            'view_dealer_dashboard'   => ['super_admin', 'internal_admin', 'brand_admin', 'global_manager', 'brand_manager'],
            'view_user_dashboard'     => ['super_admin', 'internal_admin', 'brand_admin', 'global_manager', 'brand_manager'],
            'view_campaigns'          => ['super_admin', 'internal_admin', 'brand_admin', 'global_manager', 'brand_manager'],
            'edit_campaigns'          => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
            'view_messages'           => ['super_admin', 'internal_admin', 'brand_admin', 'global_manager', 'brand_manager'],
            'edit_messages'           => ['super_admin', 'internal_admin', 'brand_admin', 'global_manager', 'brand_manager'],
            'view_messages_settings'  => ['super_admin', 'internal_admin', 'brand_admin', 'global_manager', 'brand_manager'],
            'edit_messages_settings'  => ['super_admin', 'internal_admin', 'brand_admin', 'global_manager', 'brand_manager'],
            'view_points'             => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
            'edit_points'             => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
            'view_channel_statistics' => ['super_admin', 'internal_admin', 'brand_admin', 'global_manager', 'brand_manager'],
        ],

        'units' => [
            'view_leagues'    => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
            'edit_leagues'    => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
            'view_districts'  => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
            'edit_districts'  => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
            'view_dealers'    => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
            'edit_dealers'    => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
            'view_positions'  => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
            'edit_positions'  => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
            'view_hq_users'   => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
            'edit_hq_users'   => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
        ],

        'brand_academy' => [
            'view_products'           => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
            'edit_products'           => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
            'view_quizzes'            => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
            'edit_quizzes'            => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
            'view_surveys'            => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
            'edit_surveys'            => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
            'view_product_assortment' => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
            'edit_product_assortment' => ['super_admin', 'internal_admin', 'brand_admin', 'brand_manager'],
        ],

        /** APP **/
        'mobile_app' => [
            'create_messages'       => 'all',
            'app_view_welcome'      => 'all',
            'app_view_main_screen'  => 'all',
            'app_view_rules_screen' => 'all',
            'app_contest_screen'    => 'all',

            'app_view_academy'      => 'all',
            'app_view_quizzes'      => 'all',
            'app_view_surveys'      => 'all',
            'app_view_brand_items'  => 'all',

            'app_view_app_user_list' => ['super_admin', 'internal_admin', 'brand_admin', 'global_manager', 'brand_manager'],

            'app_manage_sales'            => 'all',
            'app_view_product_assortment' => 'all',
            'app_view_messages_threads'   => 'all',
            'app_view_results'            => 'all',
            'app_view_profile'            => 'all',
            'app_view_leagues'            => 'all',
        ]
    ];
}
