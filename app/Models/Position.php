<?php

namespace App\Models;

use App\Http\Requests\Position\PositionGetAllRequest;
use App\Http\Requests\Position\PositionSaveRequest;
use App\Http\Requests\Position\PositionUpdateRequest;
use App\Http\Requests\Position\PositionGetRequest;
use App\Http\Requests\Position\PositionBulkActionRequest;

/**
 * Class Position
 *
 * @package App\Models
 */
class Position extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'channel_id',
        'created_at',
        'weight_points',
        'is_manager',
    ];

    /**
     * @param \App\Http\Requests\Position\PositionGetAllRequest $request
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function getList(PositionGetAllRequest $request)
    {
        return $this->_getListByChannel($request, [], ['name']);
    }

    /**
     * @param \App\Http\Requests\Position\PositionGetRequest $request
     * @return mixed
     */
    public function get(PositionGetRequest $request)
    {
        return $this->find($request->id);
    }

    /**
     * @param \App\Http\Requests\Position\PositionSaveRequest $request
     * @return bool
     */
    public function add(PositionSaveRequest $request)
    {
        $data = $request->validated();

        return Channel::find($data['channel_id']) ? $this->create($data) : false;
    }

    /**
     * @param \App\Http\Requests\Position\PositionUpdateRequest $request
     * @return mixed
     */
    public function resave(PositionUpdateRequest $request)
    {
        $position = $this->find($request->id);
        if ($position) {
            $position->name = $request->name;
            $position->weight_points = $request->weight_points;
            $position->is_manager = $request->is_manager ?? false;
            $position->save();
        }

        return $position;
    }

    /**
     * @param \App\Http\Requests\Position\PositionGetRequest $request
     * @return mixed
     */
    public function remove(PositionGetRequest $request)
    {
        $position = $this->find($request->id);
        if ($position) {
            $this->destroy($request->id);
        }

        return $position;
    }

    /**
     * @param \App\Http\Requests\Position\PositionBulkActionRequest $request
     * @return bool
     */
    public function bulkAction(PositionBulkActionRequest $request) {
        $positions = $this->whereIn('id', $request->id)->get();

        if (! $positions->all()) {
            return false;
        }

        switch($request->action) {
            case 'delete':
                $this->destroy($request->id);
                break;
            default:
                return false;
        }

        return $positions;
    }
}
