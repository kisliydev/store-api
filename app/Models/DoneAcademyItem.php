<?php

namespace App\Models;

use App\Http\Requests\UserAnswer\UserAnswerGetListRequest;

class DoneAcademyItem extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'item_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param \App\Http\Requests\UserAnswer\UserAnswerGetListRequest $request
     * @param \App\Models\AcademyItem $academyItem
     * @param \App\Models\User $user
     * @return \Illuminate\Contracts\Pagination\Paginator|\Illuminate\Database\Eloquent\Builder
     */
    public function getList(UserAnswerGetListRequest $request, AcademyItem $academyItem, User $user)
    {
        $ai = $academyItem->getTable();
        $dai = $this->getTable();
        $u = $user->getTable();
        $query = $this->select(
                "{$dai}.user_id",
                "{$dai}.item_id",
                "{$dai}.created_at",
                "{$ai}.title as item_title",
                "{$u}.first_name as user_first_name",
                "{$u}.last_name as user_last_name",
                "{$u}.email as user_email"
            )
            ->join($ai, function ($join) use ($request, $dai, $ai) {
                $join->on("{$ai}.id", '=', "{$dai}.item_id")
                    ->where("{$ai}.type", '=', 'survey')
                    ->where("{$ai}.channel_id", '=', $request->channel_id);
            })
            ->leftJoin($u, "{$u}.id", '=', "{$dai}.user_id");

        return $this->_getList($query, $request, [], []);
    }

    /**
     * @param int $id
     * @param bool $count
     * @return mixed
     */
    public function getRespondents(int $id, bool $count = false)
    {
        $data = $this->select('users.id', 'users.email', 'users.first_name', 'users.last_name')
            ->join('users', 'user_id', '=', 'users.id')
            ->where('item_id', $id)
            ->get();

        return $count ? $data->count() : $data;
    }
}
