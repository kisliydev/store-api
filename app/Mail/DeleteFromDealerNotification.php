<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\User;
use App\Models\Dealer;

class DeleteFromDealerNotification extends Mailable
{
    use Queueable;

    public $user;
    public $dealer;
    public $fromEmail;
    public $brand;

    /**
     * DeleteFromDealerNotification constructor.
     *
     * @param \App\Models\User $user
     * @param \App\Models\User|null $from
     * @param \App\Models\Dealer $dealer
     * @param string $brand
     */
    public function __construct(User $user, User $from = null, Dealer $dealer, string $brand)
    {
        $this->user   = $user;
        $this->dealer = $dealer;
        $this->from   = $from ? $from->toArray() : null;
        $this->brand  = $brand;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->from) {
            return $this->from($this->from['email'], $this->from['full_name'])->view('emails.delete_from_dealer_notification');
        }

        return $this->view('emails.delete_from_dealer_notification');
    }
}
