<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailVerification extends Mailable
{
    use Queueable;

    /**
     * @var \App\Models\User
     */
    public $user;

    /**
     * @var string
     */
    public $brand;

    /**
     * @var string
     */
    public $brandUrl;

    /**
     * EmailVerification constructor.
     *
     * @param \App\Models\User $user
     * @param string $brand
     * @param string $brandUrl
     */
    public function __construct(User $user, string $brand, string $brandUrl)
    {
        $this->user     = $user;
        $this->brand    = $brand;
        $this->brandUrl = $brandUrl;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.verification');
    }
}
