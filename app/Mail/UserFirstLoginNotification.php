<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Models\User;

class UserFirstLoginNotification extends Mailable
{
    use Queueable;

    public $user;
    public $fromEmail;
    public $recipient;
    public $channelName;
    public $brand;

    /**
     * UserFirstLoginNotification constructor.
     *
     * @param \App\Models\User $recipient
     * @param \App\Models\User|null $from
     * @param array $data
     * @param string $brand
     */
    public function __construct(User $recipient, User $from = null, array $data = null, string $brand = null)
    {
        $this->recipient   = $recipient;
        $this->from        = $from ? $from->toArray() : null;
        $this->user        = $data['user'] ?? null;
        $this->channelName = $data['channelName'] ?? null;
        $this->brand       = $brand;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->from) {
            return $this->from($this->from['email'], $this->from['full_name'])->view('emails.user_first_login');
        }

        return $this->view('emails.user_first_login');
    }
}
