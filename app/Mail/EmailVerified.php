<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailVerified extends Mailable
{
    use Queueable;

    public $user;
    public $brand;

    /**
     * EmailVerified constructor.
     * @param User $user
     * @param string $brand
     */
    public function __construct(User $user, string $brand)
    {
        $this->user  = $user;
        $this->brand = $brand;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.verified');
    }
}
