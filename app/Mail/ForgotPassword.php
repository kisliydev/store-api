<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForgotPassword extends Mailable
{
    use Queueable;

    public $user;
    public $data;
    public $isMobile;
    public $brand;

    /**
     * ForgotPassword constructor.
     * @param User $user
     * @param array|null $data
     * @param bool $isMobile
     * @param string|null $brand
     */
    public function __construct(User $user, array $data = null, bool $isMobile = false, string $brand = null)
    {
        $this->user     = $user;
        $this->data     = $data;
        $this->isMobile = $isMobile;
        $this->brand    = $brand;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.forgot_password' . ($this->isMobile ? '_mobile' : ''));
    }
}
