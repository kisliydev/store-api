<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OpenThreadNotification extends Mailable
{
    use Queueable;

    public $user;
    public $fromEmail;
    public $brand;

    /**
     * OpenThreadNotification constructor.
     *
     * @param \App\Models\User $user
     * @param \App\Models\User|null $from
     * @param null $data
     * @param string $brand
     */
    public function __construct(User $user, User $from = null, $data = null, string $brand = null)
    {
        $this->user  = $user;
        $this->from  = $from ? $from->toArray() : null;
        $this->brand = $brand;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->from) {
            return $this->from($this->from['email'], $this->from['full_name'])->view('emails.open_thread_notification');
        }

        return $this->view('emails.open_thread_notification');
    }
}
