<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class LevelDataTypeRule implements Rule
{
    private $message = 'The \'type_data\' field has wrong format.';

    private $request;

    /**
     * LevelDataTypeRule constructor.
     *
     * @param $request
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        try {
            $value = json_decode($value, true);

            switch ($this->request->type) {
                case 'total_points':
                    return $this->isAmountPresent($value);
                case 'time_points':
                    return $this->isAmountPresent($value) && $this->isBasedOnPresent($value) && $this->isBasedOnDataPresent($value);
                case 'total_sales':
                    return $this->isAmountPresent($value);
                case 'total_sales_by_product':
                    return $this->isAmountPresent($value) && $this->isProductIdPresent($value);
                case 'total_sales_by_dealer':
                    return $this->isAmountPresent($value);
                case 'total_quizzes':
                    return $this->isAmountPresent($value);
                case 'total_surveys':
                    return $this->isAmountPresent($value);
                default:
                    return false;
            }
        } catch (\Exception $e) {

        }

        return false;
    }

    /**
     * @param array $value
     * @return bool
     */
    private function isAmountPresent(array $value): bool
    {
        if (array_key_exists('amount', $value) && is_int($value['amount'])) {
            return true;
        }

        $this->message .= ' Integer \'amount\' value not present.';

        return false;
    }

    /**
     * @param array $value
     * @return bool
     */
    private function isProductIdPresent(array $value): bool
    {
        if (array_key_exists('product_id', $value) && is_int($value['product_id'])) {
            return true;
        }

        $this->message .= ' Integer \'product_id\' value not present.';

        return false;
    }

    /**
     * @param array $value
     * @return bool
     */
    private function isBasedOnPresent(array $value): bool
    {
        if (array_key_exists('based_on', $value) && in_array($value['based_on'], ['week_days', 'date_range'])) {
            return true;
        }

        $this->message .= ' String(week_days|date_range) \'based_on\' value not present.';

        return false;
    }

    /**
     * @param array $value
     * @return bool
     */
    private function isBasedOnDataPresent(array $value): bool
    {
        if (! $this->isBasedOnPresent($value)) {
            return false;
        }

        if (in_array('week_days', $value)) {
            return $this->isWeekDaysPresent($value);
        } elseif (in_array('date_range', $value)) {
            return $this->isDateRangePresent($value);
        }

        $this->message .= ' Integer \'based_on\' children value not present.';

        return false;
    }

    /**
     * @param array $value
     * @return bool
     */
    private function isWeekDaysPresent(array $value): bool
    {
        $days = [1, 2, 3, 4, 5, 6, 7];

        if (array_key_exists('from_week_day', $value)
            && in_array($value['from_week_day'], $days)
            && array_key_exists('to_week_day', $value)
            && in_array($value['to_week_day'], $days)) {
            return true;
        }

        $this->message .= ' Integer \'from_week_day\', \'to_week_day\' values not present or wrong.';

        return false;
    }

    /**
     * @param array $value
     * @return bool
     */
    private function isDateRangePresent(array $value): bool
    {
        if (array_key_exists('from_date', $value)
            && $this->isDateFormatValid($value['from_date'])
            && array_key_exists('to_date', $value)
            && $this->isDateFormatValid($value['to_date'])) {
            return true;
        }

        $this->message .= ' Date(Y-m-d H:i:s) \'from_date\', \'to_date\' values not present or wrong.';

        return false;
    }

    /**
     * @param string $date
     * @return bool
     */
    private function isDateFormatValid(string $date): bool
    {
        $newDate = \DateTime::createFromFormat('Y-m-d H:i:s', $date);

        return $newDate instanceof \DateTime && ! array_sum($newDate->getLastErrors());
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
