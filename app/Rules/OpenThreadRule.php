<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Thread;

class OpenThreadRule implements Rule
{
    /**
     * @var object
     */
    private $_request;

    /**
     * @var string
     */
    private $_message;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->_request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {

        $thread = Thread::find($value);

        if (!$thread) {
            $this->_message = 'Wrong :attribute value.';
            return false;
        } elseif (
            'scheduled' === $thread->status ||
            ('closed' === $thread->status && $this->_request->from == 'admin')
        ) {
            $this->_message = "You can't post new message into the {$thread->status} thread.";
        }

        return empty($this->_message);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->_message;
    }
}
