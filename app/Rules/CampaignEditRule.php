<?php

namespace App\Rules;

use App\Traits\CampaignValidator;
use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;
use App\Http\Requests\Request;
use App\Models\Campaign;

class CampaignEditRule implements Rule
{
    use CampaignValidator;
    /**
     * @var bool
     */
    private $_campaignOverlapped = true;

    /**
     * @var bool
     */
    private $_pointInThePast = true;

    /**
     * @var string
     */
    private $_pointName = '';

    /**
     * CampaignPeriodRule constructor.
     *
     * @param \App\Http\Requests\Request $request
     */
    public function __construct(Request $request)
    {
        if(!empty($request->id) ) {
            $campaign = Campaign::find($request->id);
            /**
             * due to the fact that validator checks incoming request not in the order of rules definition,
             * we let the validator pass this rule successfully in order to check the request
             * with the other rules (specially, 'exists' rule )
             */
            if (empty($campaign)) {
                $this->_campaignOverlapped = false;
                $this->_pointInThePast = false;
                return;
            }
        } else {
            return;
        }

        /**
         * Here we check whether the user set next date point (regarding to the current campaign's status) in the past.
         * So that we can avoid wrong logic in campaign's work.
         */
        $now = Carbon::now();
        switch($campaign->status) {
            case 'draft':
            case 'scheduled':
                $this->_pointInThePast = $now->gte($request->started_at);
                $this->_pointName = 'started_at';
                break;
            case 'pre_active':
                $this->_pointInThePast = $now->gte($request->sale_started_at);
                $this->_pointName = 'sale_started_at';
                break;
            case 'active':
                $this->_pointInThePast = $now->gte($request->sale_finished_at);
                $this->_pointName = 'sale_finished_at';
                break;
            case 'post_active':
                $this->_pointInThePast = $now->gte($request->finished_at);
                $this->_pointName = 'finished_at';
                break;
            case 'archived':
            default:
                break;
        }

        if ($this->_pointInThePast) {
            return;
        }

        $campaigns = Campaign::where('status', '!=', 'draft')->where($this->getOverlappedCampaignQueryClosure($request))
            ->where('channel_id', $campaign->channel_id)
            ->where('id', '<>', $campaign->id);

        $this->_campaignOverlapped = $campaigns->get()->count();
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return ! $this->_pointInThePast && ! $this->_campaignOverlapped;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if ($this->_pointInThePast) {
            return "Campaign's {$this->_pointName} date point mustn't be set in the past";
        }

        return "Campaigns periods mustn't be overlapped";
    }
}
