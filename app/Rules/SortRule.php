<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class SortRule implements Rule
{
    private $message = 'The sort field is invalid.';

    private $allowedFields = [];

    /**
     * Filter constructor.
     *
     * @param array $allowedFields
     */
    public function __construct(array $allowedFields)
    {
        $this->allowedFields = $allowedFields;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $sorts = explode(',', $value);

        foreach ($sorts as $sortCol) {
            $sortCol = ltrim($sortCol, '-');

            if (! in_array($sortCol, $this->allowedFields)) {
                $this->message = "The '{$sortCol}' field is not allowed";

                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
