<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Brand;
use App\Models\Channel;

class ChannelBelongsToBrandRule implements Rule
{
    private $_brand;
    private $_channel;
    private $_request;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->_request = $request;

        if ($this->_request->skip_brand_id) {
            return;
        }


        $this->_brand = Brand::with('channels')->find($request->brand_id);
        if (!empty($request->channel_id)) {
            $this->_channel = Channel::find($request->channel_id);
        } else {
            $this->_channel = Channel::where('invite_code', $request->invite_code)->first();
        }

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ($this->_request->skip_brand_id) {
            return true;
        }

        if ($this->_brand && $this->_channel) {
            return in_array($this->_channel->id, $this->_brand->channels->pluck('id')->toArray());
        }

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "Can't assign to the channel";
    }
}
