<?php

namespace App\Rules;

use App\Http\Requests\Request;
use Illuminate\Contracts\Validation\Rule;
use App\Models\User;

class UniqueEmail implements Rule
{
    /**
     * @var \App\Http\Requests\Request 
     * @var \App\Http\Requests\Request
     */
    private $_request;

    /**
     * UniqueEmail constructor.
     *
     * @param \App\Http\Requests\Request $request
     */
    public function __construct(Request $request)
    {
        $this->_request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        /**
         * There is no need to check if the email is unique
         * in case if user registers through a social network
         * @see User::register()
         */
        return empty($this->_request->social_id) ? !User::where('email', $value)->withTrashed()->count() : true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Such account already exists or was temporary suspended. Please try to sign in or contact the system administrator.';
    }
}
