<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Http\Requests\Request;
use App\Helpers\ValidationHelper;

class HEXColoredString implements Rule
{
    /**
     * @var object
     */
    private $_request;

    /**
     * @var object
     */
    private $_helper;

    /**
     * HEXcoloredArray constructor.
     *
     * @param \App\Http\Requests\Request $request
     * @param \App\Helpers\ValidationHelper $helper
     */
    public function __construct(Request $request, ValidationHelper $helper)
    {
        $this->_request = $request;
        $this->_helper = $helper;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->_helper->validateHEXcolor($value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be valid color string in HEX format.';
    }
}
