<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Http\Requests\Request;
use App\Models\Campaign;
use App\Traits\CampaignValidator;

class CampaignSaveRule implements Rule
{
    use CampaignValidator;

    /**
     * @var bool
     */
    private $_campaignOverlapped = true;

    /**
     * CampaignPeriodRule constructor.
     *
     * @param \App\Http\Requests\Request $request
     */
    public function __construct(Request $request)
    {
        /*
         * skip the following handling
         * in order to let other validation generate "required"-error
         */
        if (empty($request->started_at) || empty($request->finished_at)) {
            $this->_campaignOverlapped = false;
            return true;
        }

        $campaigns = Campaign::where('status', '!=', 'draft')->where($this->getOverlappedCampaignQueryClosure($request))
            ->where('channel_id', $request->channel_id);
        $this->_campaignOverlapped = $campaigns->get()->count();
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return ! $this->_campaignOverlapped;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "Campaigns periods mustn't be overlapped";
    }
}
