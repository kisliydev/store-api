<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Brand;

class BrandUniqueSlugRule implements Rule
{
    /**
     * @var array|\Illuminate\Http\Request|string
     */
    private $_brandId;

    /**
     * @var array
     */
    private $_deniedAliases = [
        'api',
        'socket',
        'admin',
        'api-socket',
    ];

    /**
     * @var string
     */
    private $_slug;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->_brandId = abs(intval(request('id')));
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $this->_slug = $value;
        return !empty($value) && !empty($this->_brandId) && $this->_isAllowed($value) && $this->_isUniqueSlug($value);
    }

    /**
     * @param string $slug
     * @return bool
     */
    private function _isAllowed(string $slug)
    {
        return ! in_array($slug, $this->_deniedAliases);
    }

    /**
     * @param string $slug
     * @return bool
     */
    private function _isUniqueSlug(string $slug)
    {
       return !Brand::where('id', '<>', $this->_brandId)
           ->whereRaw("JSON_EXTRACT(settings, \"$.slug\") = '{$slug}'")
           ->count();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('validation.unique', ['attribute' => $this->_slug]);
    }
}
