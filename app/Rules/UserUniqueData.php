<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\User;

class UserUniqueData implements Rule
{
    private $_request;
    private $_user;

    /**
     * UserUniqueData constructor.
     *
     * @param $request
     */
    public function __construct($request)
    {
        $this->_request = $request;
        $this->_user = User::find($request->id);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return strcmp($this->_user->$attribute, $value) == 0 || ! User::where('id', '<>', $this->_user->id)->where($attribute, '=', $value)->first();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute must be unique.';
    }
}
