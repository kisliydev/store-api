<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Http\Requests\Request;
use Carbon\Carbon;
use Mockery\Exception;
use Illuminate\Validation\Concerns\ValidatesAttributes;

class BrandAcademyStartedAtRule implements Rule
{
    use ValidatesAttributes;

    /**
     * @var \App\Http\Requests\Request
     */
    private $_request;

    /**
     * @var array
     */
    private $_statuses = [
        'draft',
        'scheduled',
        'published',
    ];

    /**
     * @var string
     */
    private $_message = '';

    /**
     * BrandAcademyStartedAtRule constructor.
     *
     * @param \App\Http\Requests\Request $request
     */
    public function __construct(Request $request)
    {
        $this->_request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        /**
         * Just skip the further validation in order to
         * validate the request by another rule
         * @see \App\Http\Requests\BrandAcademy\BrandAcademyQuickEditRequest::rules
         */
        if (empty($this->_request->status) || ! in_array($this->_request->status, $this->_statuses)) {
            return true;
        }

        $allowed = false;

        if (! $this->validateDateFormat($attribute, $value, ['Y-m-d H:i:s'])) {
            $this->_message = __('validation.date_format', ['attribute' => $attribute, 'format' => 'Y-m-d H:i:s']);
            return false;
        }

        switch ($this->_request->status) {
            case 'scheduled':
                $now           = Carbon::now();
                try {
                    $itemStartedAt = Carbon::parse($value);
                } catch (Exception $e) {
                    $this->_message = 'Wrong data';
                    break;
                }

                $this->_message = __('validation.after_or_equal', ['date' => $now->toDateTimeString()]);
                $allowed = $itemStartedAt->gte($now);
                break;
            case 'draft':
            case 'published':
                $allowed = true;
                break;
        }

        return $allowed;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->_message;
    }
}
