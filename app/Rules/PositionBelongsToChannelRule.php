<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Position;
use App\Http\Requests\Request;

class PositionBelongsToChannelRule implements Rule
{
    private $_request;

    /**
     * DealerBelongsToChannelRule constructor.
     *
     * @param \App\Http\Requests\Request $request
     */
    public function __construct(Request $request)
    {
        $this->_request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return !! Position::where('channel_id', $this->_request->channel_id)
            ->where('id', $this->_request->position_id)
            ->first();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute is invalid.';
    }
}
