<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class JsonIntegersArrayRule implements Rule
{
    private $message = 'The field is not array.';

    /**
     * Create a new rule instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $value = is_string($value) ? json_decode($value) : null;

        if (! is_array($value)) {
            $this->message = "The {$attribute} field is not an array of integers.";

            return false;
        }

        foreach ($value as $item) {
            if (! is_int($item)) {
                $this->message = "The {$attribute} field is not an array of integers. Wrong item: '{$item}'";

                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
