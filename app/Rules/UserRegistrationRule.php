<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Channel;
use App\Models\Dealer;
use App\Models\Position;

class UserRegistrationRule implements Rule
{
    /**
     * @var bool|string
     */
    private $_attributes;

    /**
     * UserRegistrationRule constructor.
     *
     * @param $request
     * @param \App\Models\Channel $channel
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\Position $position
     */
    public function __construct($request, Channel $channel, Dealer $dealer, Position $position)
    {
        $c = $channel->getTable();
        $d = $dealer->getTable();
        $p = $position->getTable();

        $data = $channel->select(
                "{$c}.id as channel_id",
                "{$d}.id as dealer_id",
                "{$p}.id as position_id"
            )
            ->leftJoin($d, function($join) use ($d, $c, $request) {
                $join->on("{$d}.channel_id", '=', "{$c}.id")
                    ->where("{$d}.id", '=', $request->dealer_id);
            })->leftJoin($p, function($join) use ($p, $c, $request) {
                $join->on("{$p}.channel_id", '=', "{$c}.id")
                    ->where("{$p}.id", '=', $request->position_id);
            })
            ->where("{$c}.id", '=', $request->channel_id)
            ->first();

        $data = $data ? $data->toArray() : [];

        $diff = array_diff(['channel_id', 'dealer_id', 'position_id'], array_keys(array_filter($data)));
        $this->_attributes = implode(', ', $diff) ?? false;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return empty($this->_attributes);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return sprintf('Following fields are wrong: %s', $this->_attributes);
    }
}
