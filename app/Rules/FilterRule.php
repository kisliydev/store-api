<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class FilterRule implements Rule
{
    private $message = 'The filter field is invalid.';

    private $allowedFields = [];

    /**
     * Filter constructor.
     *
     * @param array $allowedFields
     */
    public function __construct(array $allowedFields)
    {
        $this->allowedFields = $allowedFields;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $filters = explode(',', $value);

        foreach ($filters as $filter) {
            $filter = explode(':', $filter);

            if (count($filter) !== 2) {
                return false;
            }

            if (! in_array($filter[0], $this->allowedFields)) {
                $this->message = "The '{$filter[0]}' field is not allowed";

                return false;
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
