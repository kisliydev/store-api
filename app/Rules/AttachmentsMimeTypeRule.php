<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Symfony\Component\HttpFoundation\File\MimeType\ExtensionGuesser;
use Illuminate\Validation\Concerns\ValidatesAttributes;

class AttachmentsMimeTypeRule implements Rule
{
    use ValidatesAttributes;

    /**
     * @var array
     */
    private $_extensions;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {

        $this->_extensions = $this->_getAllowedExtensions();
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (! $this->isValidFileInstance($value)) {
            return false;
        }

        if ($this->shouldBlockPhpUpload($value, $this->_extensions)) {
            return false;
        }

        return $value->getPath() !== '' && in_array($this->_guessExtension($value->getClientMimeType()), $this->_extensions);
    }

    /**
     * @return array
     */
    private function _getAllowedExtensions()
    {
        return array_merge(
            config('media.mimes.video'),
            config('media.mimes.document'),
            config('media.mimes.image')
        );
    }

    /**
     * @param string $mimeType
     * @return string
     */
    private function _guessExtension(string $mimeType)
    {
        $guesser = ExtensionGuesser::getInstance();
        return $guesser->guess($mimeType);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Wrong file type. Only ' . implode(', ', $this->_extensions) . ' are allowed.';
    }
}
