<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ArrayRule implements Rule
{
    private $message = 'The field is not array.';

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (is_array($value)) {
            return true;
        }

        $this->message = "The {$attribute} field is not array.";

        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
