<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Http\Requests\Request;
use App\Models\Campaign;
use App\Helpers\ValidationHelper;
use Illuminate\Validation\Concerns\ValidatesAttributes;

class AppLayoutFieldsFormat implements Rule
{
    use ValidatesAttributes;

    /**
     * The numeric related validation rules.
     * @uses in order to implement a compatibility with ValidatesAttributes trait
     * @var array
     */
    protected $numericRules = ['Numeric', 'Integer'];

    /**
     * @var object
     */
    private $_request;

    /**
     * @var object
     */
    private $_campaign;

    /**
     * @var object
     */
    private $_helper;

    /**
     * @var string
     */
    private $_message;

    /**
     * AppLayoutFieldsFormat constructor.
     *
     * @param \App\Http\Requests\Request $request
     * @param \App\Models\Campaign $campaign
     * @param \App\Helpers\ValidationHelper $helper
     */
    public function __construct(Request $request, Campaign $campaign, ValidationHelper $helper)
    {
        $this->_request = $request;
        $this->_campaign = $campaign;
        $this->_helper = $helper;
    }

    /**
     * @param string $attribute
     * @param mixed $data
     * @return bool
     */
    public function passes($attribute, $data)
    {
        try {
            $this->_validate($data);
        } catch (\Exception $exception) {
            $this->_message = $exception->getMessage();
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->_message;
    }

    /**
     * @uses in order to implement a compatibility with the ValidatesAttributes trait
     * @param $attribute
     * @param $rules
     * @return bool
     */
    public function hasRule($attribute, $rules)
    {
        return true;
    }

    /**
     * @param array $data
     * @throws \Exception
     */
    private function _validate(array $data) {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $this->_validate($value);
            } elseif (empty($value)) {
                continue;
            } elseif ($this->_campaign->isColorOption($key)) {
                $this->_validateColor($key, $value);
            } elseif ($this->_campaign->isImageOption($key)) {
                $this->_validateImage($key, $value);
            } elseif ($this->_isMediaImageOption($key) || $this->_campaign->isNumericOption($key)) {
                $this->_validateInteger($key, $value);
            } elseif($this->_isDate($key)) {
                $this->_validateDate($key, $value);
            } elseif ($this->_campaign->isStringOption($key)) {
                $this->_validateString($key, $value);
            } elseif (! array_key_exists($key, array_keys($this->_campaign->getDefaultAppLayout()))) {
                throw new \Exception("Non-existed key {$key}.");
            }
        }
    }

    /**
     * @param $key
     * @return bool
     */
    private function _isDate($key)
    {
        return $this->_campaign->isDate($key);
    }

    /**
     * @param $key
     * @param $value
     * @throws \Exception
     */
    private function _validateDate($key, $value)
    {
        if (!$this->validateDateFormat($key, $value, ['Y-m-d H:i:s'])) {
            throw new \Exception("{$key} field must be valid date in format Y-m-d H:i:s.");
        }
    }

    /**
     * @param string $key
     * @return bool
     */
    private function _isMediaImageOption(string $key) {
        $key = $this->_campaign->getImageOption($key);
        return $key && $this->_campaign->isImageOption($key);
    }

    /**
     * @param string $key
     * @param $value
     * @throws \Exception
     */
    private function _validateInteger(string $key, $value) {
        if (!$this->validateInteger($key, $value)) {
            throw new \Exception("{$key} field must be valid integer.");
        }
    }

    /**
     * @param string $key
     * @param $value
     * @throws \Exception
     */
    private function _validateString(string $key, $value) {
        if (!$this->validateString($key, $value)) {
            throw new \Exception("{$key} field must be a string.");
        }
    }

    /**
     * @param string $key
     * @param $value
     * @throws \Exception
     */
    private function _validateImage(string $key, $value) {
        if (
            !$this->validateMimes($key, $value, ['jpeg','jpg','png','gif'])
            || !$this->validateMax($key, $value, [1000])
        ) {
            throw new \Exception("{$key} field must valid image under 1MB size.");
        }
    }

    /**
     * @param string $key
     * @param $value
     * @throws \Exception
     */
    private function _validateColor(string $key, $value) {
        if (! $this->_helper->validateColor($value)) {
            throw new \Exception("{$key} field must be valid hex-, rgb- or rgba-color.");
        }
    }
}
