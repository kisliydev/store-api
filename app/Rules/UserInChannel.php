<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\User;

class UserInChannel implements Rule
{
    /**
     * @var object
     */
    private $_user;

    /**
     * @var object
     */
    private  $_request;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->_user = User::find($request->id);
        $this->_request = $request;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $this->_user->inChannel($this->_request->channel_id);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "User isn't bind to the currently given channel.";
    }
}
