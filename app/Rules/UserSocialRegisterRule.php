<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\User;

class UserSocialRegisterRule implements Rule
{

    private $_user;

    /**
     * UserSocialRegisterRule constructor.
     *
     * @param $request
     */
    public function __construct($request)
    {
        if (in_array($request->social_vendor, ['facebook', 'google'])) {
            $this->_user = User::where($request->social_vendor.'_credentials', $request->social_id)->withTrashed()->first();
        }
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return ! $this->_user;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Such account already exists or was temporary suspended. Please try to sign in or contact the system administrator.';
    }
}
