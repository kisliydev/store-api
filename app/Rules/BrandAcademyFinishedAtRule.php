<?php

namespace App\Rules;

use App\Models\AcademyItem;
use Illuminate\Contracts\Validation\Rule;
use App\Http\Requests\Request;
use Carbon\Carbon;
use Illuminate\Validation\Concerns\ValidatesAttributes;

class BrandAcademyFinishedAtRule implements Rule
{
    use ValidatesAttributes;

    /**
     * @var \App\Http\Requests\Request
     */
    private $_request;

    private $_item;

    /**
     * @var array
     */
    private $_statuses = [
        'draft',
        'scheduled',
        'published',
    ];

    /**
     * @var string
     */
    private $_message = '';

    /**
     * BrandAcademyFinishedAtRule constructor.
     *
     * @param \App\Http\Requests\Request $request
     */
    public function __construct(Request $request)
    {
        $this->_request = $request;
        $this->_item    = AcademyItem::find($request->id);

    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        /**
         * Just skip the further validation in order to
         * validate the request by another rule
         * @see \App\Http\Requests\BrandAcademy\BrandAcademyQuickEditRequest::rules
         */
        if (empty($this->_item) || empty($this->_request->status) || ! in_array($this->_request->status, $this->_statuses)) {
            return true;
        }

        if (! $this->validateDateFormat($attribute, $value, ['Y-m-d H:i:s'])) {
            $this->_message = __('validation.date_format', ['attribute' => $attribute, 'format' => 'Y-m-d H:i:s']);
            return false;
        }

        $allowed = false;
        $itemFinishedAt = Carbon::parse($value);

        switch ($this->_request->status) {
            case 'draft':
                $allowed = true;
                break;
            case 'scheduled':
                $itemStartedAt  = Carbon::parse($this->_request->startedt_at);
                $this->_message = __('validation.after', ['date' => $itemStartedAt->toDateTimeString()]);
                $allowed        = $itemFinishedAt->gt($itemStartedAt);
                break;
            case 'published':
                $itemStartedAt  = Carbon::parse($this->_item->startedt_at);
                $now            = Carbon::now();

                /* finish date can't be set in the past */
                $allowed = $itemFinishedAt->gt($now);
                if (! $allowed) {
                    $this->_message = __('validation.after', ['date' => $now->toDateTimeString()]);
                    break;
                }

                /* finish date can't be set earlier than the start date */
                $this->_message = __('validation.after', ['date' => $itemStartedAt->toDateTimeString()]);
                $allowed = $itemFinishedAt->gt($itemStartedAt);
                break;
        }

        return $allowed;

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->_message;
    }
}
