<?php

namespace App\Jobs;

use App\Models\Channel;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;
use App\Mail\Mailhog;
use Illuminate\Support\Facades\Mail;

class OneSignalPushNotifications implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $usersIds;

    private $push;

    private $caller;

    /**
     * OneSignalPushNotifications constructor.
     *
     * @param array $usersIds
     * @param array $push
     * @param string $callerClass
     */
    public function __construct(array $usersIds, array $push, string $callerClass)
    {
        $this->usersIds = $usersIds;
        $this->push     = $push;
        $this->caller   = $callerClass;
    }

    /**
     * @return bool
     */
    private function isDataValid() : bool
    {
        return array_key_exists('subject', $this->push)
            && array_key_exists('body', $this->push)
            && array_key_exists('channel_id', $this->push);
    }

    /**
     * @param \App\Models\User $user
     * @param \App\Models\Channel $channel
     * @return mixed
     */
    public function handle(User $user, Channel $channel)
    {
        if (! $this->isDataValid()) {
            return Log::error("[{$this->caller}]OneSignalPushNotifications doesn't have proper data.");
        }

        Log::info("[{$this->caller}]Trying to send OneSignalPush (channel: {$this->push['channel_id']}) to users: ".implode(',', $this->usersIds));

        $this->_preparePush();

        $channelData = $channel->getPushSettings($this->push);
        $channelData = $channelData ? $channelData->toArray() : [];

        $onesignalBransSettings = [
            'app_id'       => $channelData['country_brand']['brand']['settings']['onesignal_app_id'] ?? null,
            'rest_api_key' => $channelData['country_brand']['brand']['settings']['onesignal_rest_api_key'] ?? null,
        ];

        if (config('mail.intercept')) {

            if ($this->isPushSettingsValid($onesignalBransSettings)) {
                $this->setPushSettings($onesignalBransSettings);
                $this->sendPushToMailhog($user);
            } else {
                Log::error("[{$this->caller}]OneSignalPushNotifications doesn't have push credentials. Channel: {$this->push['channel_id']}");
            }

        } else {
            $apps = array_merge([null], config('onesignal.universal_apps'));
            foreach ( $apps as $app) {
                if ($app) {
                    $onesignalSettings = [
                        'app_id'       => config(sprintf('onesignal.credentials.%s.app_id', $app)),
                        'rest_api_key' => config(sprintf('onesignal.credentials.%s.api_key', $app)),
                    ];
                } else {
                    $onesignalSettings = $onesignalBransSettings;
                }

                if ($this->isPushSettingsValid($onesignalSettings)) {
                    $this->setPushSettings($onesignalSettings);
                    $user->sendPushNotification($this->usersIds, $this->push, $app);
                } else {
                    $str = $app ? " for the {$app} app" : '';
                    Log::error("[{$this->caller}]OneSignalPushNotifications doesn't have push credentials{$str}. Channel: {$this->push['channel_id']}");
                }
            }
        }
    }

    /**
     * @return void
     */
    private function _preparePush()
    {
        foreach(['subject', 'body'] as $key) {
            $this->push[$key] = strip_tags($this->push[$key]);
        }
    }

    /**
     * @param \App\Models\User $user
     */
    private function sendPushToMailhog(User $user)
    {
        foreach ($this->usersIds as $userId) {
            $userData = $user->where('id', $userId)->first();
            if ($userData) {
                Mail::to('one@signal.net')->send(new Mailhog([
                    'title' => "Email: {$userData->email}",
                    'message' => "TITLE: {$this->push['subject']} BODY: {$this->push['body']}",
                    'subject' => "PUSH (user: $userId)",
                ]));
            }
        }
    }

    /**
     * @param array $settings
     */
    private function setPushSettings(array $settings)
    {
        Config::set('services.onesignal.app_id', $settings['app_id']);
        Config::set('services.onesignal.rest_api_key', $settings['rest_api_key']);
    }

    /**
     * @param array $settings
     * @return bool
     */
    private function isPushSettingsValid(array $settings) : bool
    {
        return !empty($settings['app_id']) && !empty($settings['rest_api_key']);
    }
}
