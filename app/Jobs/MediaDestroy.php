<?php

namespace App\Jobs;

use App\Models\Media;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class MediaDestroy implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $data;

    /**
     * MediaDestroy constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if (! $this->isDataValid()) {
            return Log::error("MediaDestroy don't have proper data.");
        }

        $media = Media::where('path', $this->data['path'])->first();

        if (! $media) {
            Log::info("MediaDestroy deletion of image: {$this->data['path']}");
            Storage::delete($this->data['path']);

            if (!empty($this->data['thumbnails'])) {
                try {
                    Log::info("MediaDestroy deletion of thumbnails: {$this->data['thumbnails']}");
                    $thumbnails = json_decode($this->data['thumbnails'], true);
                    if (is_array($thumbnails)) {
                        foreach($thumbnails as $thumbnail) {
                            Storage::delete($thumbnail);
                        }
                    }
                } catch (\Exception $e) {
                    Log::error("MediaDestroy skipped deletion of thumbnails: {$this->data['thumbnails']},\n {$e->getMessage()}");
                }
            }
        } else {
            Log::info("MediaDestroy skipped deletion of image: {$this->data['path']}");
        }
    }

    /**
     * @return bool
     */
    private function isDataValid(): bool
    {
        return array_key_exists('path', $this->data);
    }
}
