<?php

namespace App\Jobs;

use App\Models\AchievedLevel;
use App\Models\Channel;
use App\Notifications\OneSignalNotification;
use App\Traits\Shortcodes;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use App\Models\Level;
use App\Models\Thread;
use App\Models\PointsStatistic;
use Carbon\Carbon;
use App\Traits\SocketNotifications;
use App\Traits\Notifications;

class LevelsNotifications implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, SocketNotifications, Notifications, Shortcodes;

    private $data;

    private $thread;

    private $pointsStatistic;

    private $caller;

    /**
     * LevelsNotifications constructor.
     *
     * @param array $data
     * @param string $callerClass
     */
    public function __construct(array $data, string $callerClass)
    {
        $this->data = $data;
        $this->caller = $callerClass;
    }

    /**
     * @param Level $levels
     * @param Thread $thread
     * @param PointsStatistic $pointsStatistic
     * @return mixed
     */
    public function handle(
        Level $levels,
        Thread $thread,
        PointsStatistic $pointsStatistic
    ) {
        if (! $this->isPointsDataValid()) {
            return Log::error("[{$this->caller}]LevelsNotifications don't have proper data.");
        }

        $this->thread = $thread;
        $this->pointsStatistic = $pointsStatistic;

        /* Get only those levels that weren't achieved by the user */
        $levelItems = $levels->where("channel_id", $this->data['channel_id'])
            ->get();

        $levelItems->each(function($level) {
            $levelArr = $level->toArray();
            $levelArr['id'] = abs(intval($levelArr['id']));

            if (AchievedLevel::where('user_id', $this->data['user_id'])->where('level_id', $levelArr['id'])->first()) {
                return;
            }

            switch ($levelArr['type']) {
                case 'total_points':
                    $isLevelAchieved = $this->amountNotification($levelArr, []);
                    break;
                case 'time_points':
                    $isLevelAchieved = $this->timePointsHandler($levelArr);
                    break;
                case 'total_sales':
                    $isLevelAchieved = $this->amountNotification(
                        $levelArr,
                        [['object_type', '=', 'product']]
                    );
                    break;
                case 'total_sales_by_product':
                    $isLevelAchieved = $this->amountNotification(
                        $levelArr,
                        [['object_type', '=', 'product']],
                        [['object_id', '=', $this->data['object_id']]]
                    );
                    break;
                case 'total_sales_by_dealer':
                    $isLevelAchieved = $this->amountNotification(
                        $levelArr,
                        [['object_type', '=', 'product']],
                        [['object_id', '=', $this->data['object_id']]]
                    );
                    break;
                case 'total_quizzes':
                    $isLevelAchieved = $this->amountNotification(
                        $levelArr,
                        [['object_type', '=', 'quiz']]
                    );
                    break;
                case 'total_surveys':
                    $isLevelAchieved = $this->amountNotification(
                        $levelArr,
                        [['object_type', '=', 'survey']]
                    );
                    break;
                default:
                    $isLevelAchieved = false;
                    break;
            }

            if ($isLevelAchieved && $level->points) {
                $this->pointsStatistic->addStatItem(
                    $level,
                    $this->data['user_id'],
                    'level',
                    $this->data['channel_id']
                );
            }
        });
    }

    /**
     * @param array $level
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function timePointsHandler(array $level)
    {
        $levelData = json_decode($level['type_data'], true);

        if(
            $this->isTimePointsDataValid($levelData)
            && ($this->isCurrentDayInAllowedRange($levelData) || $this->isCurrentDateInAllowedRange($levelData))
        ) {
            return $this->amountNotification($level, []);
        }

        Log::info("[{$this->caller}]Skip AutomaticMessages(level: {$level['type']}) for user_id: {$this->data['user_id']}");
        return false;

    }

    /**
     * @param array $levelData
     * @return bool
     */
    private function isCurrentDateInAllowedRange(array $levelData): bool
    {
        $currentTimestamp = Carbon::now(config('app.timezone'))->timestamp;
        $from = Carbon::createFromFormat('Y-m-d H:i:s', $levelData['from_date'], config('app.timezone'))->timestamp;
        $to   = Carbon::createFromFormat('Y-m-d H:i:s', $levelData['to_date'], config('app.timezone'))->timestamp;

        return $levelData['based_on'] == 'date_range'
            && $currentTimestamp >= $from
            && $currentTimestamp <= $to;
    }

    /**
     * @param array $levelData
     * @return bool
     */
    private function isCurrentDayInAllowedRange(array $levelData): bool
    {
        $currentDay = date('N', Carbon::now(config('app.timezone'))->timestamp);

        return $levelData['based_on'] == 'week_days'
            && $currentDay >= $levelData['from_week_day']
            && $currentDay <= $levelData['to_week_day'];
    }

    /**
     * @param array|null $levelData
     * @return bool
     */
    private function isTimePointsDataValid(array $levelData = null)
    {
        return $this->isAmountPresent($levelData) && array_key_exists('based_on', $levelData);
    }

    /**
     * @param array $level
     * @param array $pointTypes
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function amountNotification(array $level, array $pointTypes)
    {
        if (!$this->_userEarnedEnough($level, $pointTypes)) {
            Log::info("[{$this->caller}]Skip AutomaticMessages(level: {$level['type']}) for user_id: {$this->data['user_id']}");
            return false;
        }

        return $this->_notifyUser($level);

    }

    /**
     * @param null $args
     * @param array $level
     * @return string
     */
    public function getAmountPointsShortcodeContent($args = null, array $level)
    {
        $types = ['total_points', 'time_points'];
        return $this->_getLevelRequiredAmount($types, $level);
    }

    /**
     * @param null $args
     * @param array $level
     * @return string
     */
    public function getAmountSalesShortcodeContent($args = null, array $level)
    {
        $types = ['total_sales', 'total_sales_by_product', 'total_sales_by_dealer'];
        return $this->_getLevelRequiredAmount($types, $level);
    }

    /**
     * @param null $args
     * @param array $level
     * @return mixed|string
     */
    public function getLevelTitleShortcodeContent($args = null, array $level)
    {
        return $level['title'] ?? '';
    }

    /**
     * @param array $acceptTypes
     * @param array $level
     * @return string
     */
    private function _getLevelRequiredAmount(array $acceptTypes, array $level)
    {
        if (!in_array($level['type'], $acceptTypes)) {
            return '';
        }

        $data = json_decode($level['type_data'], true);

        return empty($data['amount']) ? '' : $data['amount'];
    }

    /**
     * @param array $level
     * @return bool
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function _notifyUser(array $level)
    {
        $settings = $this->_getChannelSettings($this->data['channel_id']);
        $subject  = $this->parseShortcodes($level['title'], $level);
        $content  = $this->parseShortcodes($level['description'], $level);

        $channelName = Channel::find($this->data['channel_id'])->name;

        AchievedLevel::create([
            'user_id'  => $this->data['user_id'],
            'level_id' => $level['id'],
        ]);

        if ($settings && $settings->reply_from && $settings->reply_from->all()) {
            Log::info("[{$this->caller}]Created thread by Level(level: {$level['type']}) for user_id: {$this->data['user_id']}");

            $newThread = $this->thread->_createEntity((object) [
                'channel_id'      => $this->data['channel_id'],
                'recipients'      => "user_id:{$this->data['user_id']}",
                'type'            => 'level',
                'subject'         => $subject,
                'message_content' => $content,
                'data'            => json_encode(['level' => $level['id'], 'user_id' => $this->data['user_id']]),
            ], 'closed', $settings->reply_from->first()->id, true);

            $newThread->sentStatistics()->createMany([
                ['sent_to' => $this->data['user_id']]
            ]);

            /* notify via socket */
            $this->notify([
                'type'       => 'new_thread',
                'channel_id' => $newThread->channel_id,
                'item_id'    => $newThread->id,
            ]);
        }

        $data = [
            'thread_id' => $newThread->id ?? '',
            'channel_id' => $this->data['channel_id'],
            'channel_name' => $channelName,
            'type' => OneSignalNotification::NOTIFICATION_TYPE_LEVEL_MESSAGE
        ];

        dispatch((new OneSignalPushNotifications(
            [
                $this->data['user_id']
            ],
            [
                'subject'    => $channelName.', '.$subject,
                'body'       => $content,
                'channel_id' => $this->data['channel_id'],
                'data'       => $data
            ],
            get_class($this)
        ))->onQueue(config('queue.tube.push-notifications')));

        return true;
    }

    /**
     * @param array $level
     * @param array $pointTypes
     * @param array $conditions
     * @return bool
     */
    private function _userEarnedEnough(array $level, array $pointTypes, array $conditions = []) : bool
    {
        $levelData = json_decode($level['type_data'], true);
        $query = $this->pointsStatistic->where('user_id', $this->data['user_id'])
            ->where($conditions)
            ->where(function ($query) use ($pointTypes) {
                foreach ($pointTypes as $type) {
                    $query->orWhere([$type]);
                }
            });

        $pointsTypes     = ['total_points', 'time_points'];
        $aggregatedField = in_array($level['type'], $pointsTypes) ? 'earned_points' : 'units_number';

        return $this->isAmountPresent($levelData) && abs(intval($query->sum($aggregatedField))) >= $levelData['amount'];
    }

    /**
     * @param array|null $value
     * @return bool
     */
    private function isAmountPresent(array $value = null): bool
    {
        return is_array($value) && array_key_exists('amount', $value) && is_int($value['amount']);
    }

    /**
     * @return bool
     */
    private function isPointsDataValid(): bool
    {
        return is_array($this->data)
            && array_key_exists('channel_id', $this->data)
            && array_key_exists('user_id', $this->data)
            && array_key_exists('object_type', $this->data)
            && array_key_exists('object_id', $this->data);
    }
}
