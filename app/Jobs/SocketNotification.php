<?php

namespace App\Jobs;

use App\Models\PointsStatistic;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Transformers\Messages\ThreadsListTransformer;
use App\Transformers\Messages\AdminThreadTransformer;
use App\Transformers\Messages\UsersThreadInSocketTransformer;
use App\Models\Thread;
use App\Models\Message;
use App\Socket\MessagePusher;
use Illuminate\Support\Facades\Log;

class SocketNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $_data = [];
    private $_model;
    private $_transformer;

    /**
     * SocketNotification constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->_data = $data;
    }

    /**
     * @param MessagePusher $pusher
     * @param ThreadsListTransformer $transformer
     */
    public function handle(MessagePusher $pusher, ThreadsListTransformer $transformer)
    {
        $class = self::class;

        $this->_model = app()->make(('new_thread' == $this->_data['type'] ? Thread::class : Message::class));
        $this->_transformer = $transformer;

        Log::info("[$class] Try to send: " . json_encode($this->_data, JSON_PRETTY_PRINT));

        try {
            $pusher->sendToServer($this->_handledData());
            Log::info("[$class] Success");
        } catch (\Exception $e) {
            Log::error("[$class] Sending failure: {$e->getMessage()}");
        }
    }

    /**
     * @return array
     * @throws \Exception
     */
    private function _handledData()
    {
        $messages = [];

        if ('channel_dashboard' != $this->_data['recipients_type']) {
            $sendData = $this->_getMessageData();
            if ($this->_data['type'] == 'new_thread') {
                $sendData = array_add($sendData, 'unread_by_admin_messages', 1);
            }

            if (! $sendData) {
                throw new \Exception("Can't get message data");
            }
        }

        switch($this->_data['recipients_type']) {
            case 'admin':
                $messages[] = [
                    'alias' => "Channel:{$this->_data['channel_id']}",
                    'type'  => $this->_data['type'],
                    'data'  => $sendData
                ];
                break;

            case 'user':
                $threadIDkey = 'new_thread' == $this->_data['type'] ? 'id' : 'thread_id';
                $recipients = 'delete_thread' == $this->_data['type']
                    ? $this->_data['recipients']
                    : Thread::find($sendData[$threadIDkey])->getRecipientsIDs();
                foreach($recipients as $id) {
                    $messages[] = [
                        'alias' => "Channel:{$this->_data['channel_id']},User:{$id}",
                        'type'  => $this->_data['type'],
                        'data'  => $sendData
                    ];
                }
                break;

            case 'channel_dashboard':
                $messages[] = [
                    'alias' => "Channel:{$this->_data['channel_id']},Dashboard",
                    'type'  => $this->_data['type'],
                    'data'  => PointsStatistic::find($this->_data['item_id'])->toArray()
                ];
                break;
            default:
                throw new \Exception("Can't get message recipients");
                break;
        }

        if (! $messages) {
            throw new \Exception("Unknown error");
        }

        return $messages;

    }

    /**
     * @return bool|null
     */
    private function _getMessageData()
    {
        $relations = [
            'lastMessage' => $this->_model->lastThreadMessageClosure($this->_data['channel_id'])
        ];
        switch($this->_data['type']) {
            case 'new_thread':
                if ('admin' == $this->_data['recipients_type']) {
                    $relations[] = 'unreadByAdminMessages';
                    $itemTransformerClass = AdminThreadTransformer::class;
                } else {
                    $itemTransformerClass = UsersThreadInSocketTransformer::class;
                }

                $data = $this->_model->with($relations)
                    ->where('id', $this->_data['item_id'])
                    ->get();

                $data = $this->_transformer->chain($data)
                    ->transformer(app()->make($itemTransformerClass))
                    ->endOfChain()
                    ->first();
                break;
            case 'new_message':
                $data = $this->_model->with([
                        'attachments.media',
                        'author' => $this->_model->getAuthorClosure()
                    ])
                    ->where('id', $this->_data['item_id'])
                    ->get();

                $data = $this->_transformer->transform($data)->first();
                break;
            case 'delete_thread':
                $data = collect([
                    'thread_id' => array_map(function ($i) { return (int) $i; }, $this->_data['thread_id']),
                    'channel_id' => (int) $this->_data['channel_id'],
                ]);
                break;
            case 'delete_message':
                $data = collect([
                    'message_id' => array_map(function ($i) { return (int) $i; }, $this->_data['message_id']),
                    'channel_id' => (int) $this->_data['channel_id'],
                    'thread_id' => (int) $this->_data['thread_id'],
                ]);
                break;
            default:
                return false;
        }

        return $data ? $data->toArray() : null;
    }
}
