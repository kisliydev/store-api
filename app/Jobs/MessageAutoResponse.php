<?php

namespace App\Jobs;

use App\Models\Thread;
use App\Notifications\OneSignalNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Models\Message;
use App\Traits\SocketNotifications;

class MessageAutoResponse implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, SocketNotifications;

    private $_data;

    /**
     * MessageAutoResponse constructor.
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->_data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $message = app()->call(Message::class . '@add',[$this->_data]);

        $this->notify([
            'type'       => 'new_message',
            'channel_id' => $message->thread->channel_id,
            'item_id'    => $message->thread->id,
        ]);

        $thread = Thread::find($this->_data['thread_id']);
        (new Thread)->sendPushNotification($thread->getRecipientsIDs(),$thread, $message->content, OneSignalNotification::NOTIFICATION_TYPE_AUTOREPLY_MESSAGE);
    }
}
