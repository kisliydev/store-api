<?php

namespace App\Jobs;

use App\Models\Channel;
use App\Models\League;
use App\Notifications\OneSignalNotification;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use App\Models\PointsStatistic;
use App\Models\UsersDealers;
use App\Models\Dealer;
use App\Models\District;
use App\Models\ManualPoint;
use App\Models\Thread;
use App\Models\UserChannelStatus;
use App\Traits\SocketNotifications;

class ManualPointsUpdateStatistics implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, SocketNotifications;

    private $_point;

    private $_caller;

    private $_pointsStatistic;

    private $_dealer;

    private $_userDealers;

    private $_channelStatus;

    private $_district;

    private $_author;

    private $_userIDs;

    private $_league;

    /**
     * ManualPointsUpdateStatistics constructor.
     *
     * @param \App\Models\ManualPoint $point
     * @param string $callerClass
     * @param int $authorID
     */
    public function __construct(ManualPoint $point, string $callerClass, int $authorID)
    {
        $this->_point = $point;
        $this->_caller = $callerClass;
        $this->_author = $authorID;
    }

    /**
     * @param \App\Models\UsersDealers $userDealers
     * @param \App\Models\Dealer $dealer
     * @param \App\Models\League $league
     * @param \App\Models\District $district
     * @param \App\Models\Thread $thread
     * @param \App\Models\PointsStatistic $pointsStatistic
     * @param \App\Models\UserChannelStatus $channelStatus
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function handle(UsersDealers $userDealers, Dealer $dealer, League $league, District $district, Thread $thread, PointsStatistic $pointsStatistic, UserChannelStatus $channelStatus)
    {
        $this->_dealer = $dealer;
        $this->_userDealers = $userDealers;
        $this->_district = $district;
        $this->_league = $league;
        $this->_pointsStatistic = $pointsStatistic;
        $this->_channelStatus = $channelStatus;

        $channelName = Channel::find($this->_point->channel_id)->name;

        if (! $this->_addPointStatistic()) {
            return;
        }

        Log::info("[{$this->_caller}]Created thread by ManualPoint(id: {$this->_point->id}) for recipients:\n{$this->_point->recipients}");

        $newThread = $thread->_createEntity((object) [
            'channel_id' => $this->_point->channel_id,
            'recipients' => $this->_point->recipients,
            'type' => 'manual',
            'subject' => $this->_point->title,
            'message_content' => $this->_point->description,
            'data' => json_encode(['manual_point' => $this->_point->id]),
        ], 'closed', $this->_author, true);

        $newThread->sentStatistics()->createMany(array_map(function($userID) {
            return ['sent_to' => $userID];
        }, $this->_userIDs));


        $data = [
            'thread_id' => $newThread->id ?? '',
            'channel_id' => $this->_point->channel_id,
            'channel_name' => $channelName,
            'type' => OneSignalNotification::NOTIFICATION_TYPE_MANUAL_POINT
        ];

        dispatch((new OneSignalPushNotifications(
            $this->_userIDs,
            [
                'subject' =>  $channelName.', '.$this->_point->title,
                'body' => $this->_point->description,
                'channel_id' => $this->_point->channel_id,
                'data' => $data
            ],
            get_class($this)
        ))->onQueue(config('queue.tube.push-notifications')));

        $this->notify([
            'type' => 'new_thread',
            'channel_id' => $newThread->channel_id,
            'item_id' => $newThread->id,
        ]);
    }

    /**
     * @return bool
     */
    private function _addPointStatistic() {
        $recipients = $this->_getRecipients();

        $d   = $this->_dealer->getTable();
        $dis = $this->_district->getTable();
        $l   = $this->_league->getTable();
        $ud  = $this->_userDealers->getTable();
        $ucs = $this->_channelStatus->getTable();

        if (isset($recipients['all'])) {
            $ids = $this->_channelStatus
                ->select("{$ucs}.user_id as id")
                ->where("{$ucs}.status", "active")
                ->where("{$ucs}.channel_id", $this->_point->channel_id)
                ->get()
                ->pluck('id')
                ->toArray();
        } else {
            $ids = $recipients['user_id'] ?? [];

            if (! empty($recipients['dealer_id'])) {
                $userDealers = $this->_channelStatus
                    ->select("{$ucs}.user_id as id")
                    ->join($d, "{$d}.channel_id", '=', "{$ucs}.channel_id")
                    ->join($ud, function($join) use ($d, $ud, $ucs, $recipients){
                        $join->on("{$ud}.dealer_id", '=', "{$d}.id")
                            ->on("{$ud}.user_id", '=', "{$ucs}.user_id")
                            ->whereIn("{$ud}.dealer_id", $recipients['dealer_id']);
                    })
                    ->where("{$ucs}.status", "active")
                    ->where("{$ucs}.channel_id", $this->_point->channel_id)
                    ->get()
                    ->pluck('id')
                    ->toArray();
                $ids = array_merge($ids, $userDealers);
            }

            if (! empty($recipients['district_id'])) {
                $userDistricts = $this->_channelStatus
                    ->select("{$ucs}.user_id as id")
                    ->join($dis, function($join) use ($dis, $ucs, $recipients) {
                        $join->on("{$dis}.channel_id", '=', "{$ucs}.channel_id")
                            ->whereIn("{$dis}.id", $recipients['district_id']);
                    })
                    ->join($d, "{$d}.district_id", '=', "{$dis}.id")
                    ->join($ud, function($join) use ($d, $ud, $ucs){
                        $join->on("{$ud}.dealer_id", '=', "{$d}.id")
                            ->on("{$ud}.user_id", '=', "{$ucs}.user_id");
                    })
                    ->where("{$ucs}.status", "active")
                    ->where("{$ucs}.channel_id", $this->_point->channel_id)
                    ->get()
                    ->pluck('id')
                    ->toArray();

                $ids = array_merge($ids, $userDistricts);
            }

            if (! empty($recipients['league_id'])) {
                $userLeagues = $this->_channelStatus
                    ->select("{$ucs}.user_id as id")
                    ->join($l, function($join) use ($l, $ucs, $recipients) {
                        $join->on("{$l}.channel_id", '=', "{$ucs}.channel_id")
                            ->whereIn("{$l}.id", $recipients['league_id']);
                    })
                    ->join($d, "{$d}.league_id", '=', "{$l}.id")
                    ->join($ud, function($join) use ($d, $ud, $ucs){
                        $join->on("{$ud}.dealer_id", '=', "{$d}.id")
                            ->on("{$ud}.user_id", '=', "{$ucs}.user_id");
                    })
                    ->where("{$ucs}.status", "active")
                    ->where("{$ucs}.channel_id", $this->_point->channel_id)
                    ->get()
                    ->pluck('id')
                    ->toArray();

                $ids = array_merge($ids, $userLeagues);
            }

            $ids = array_filter(array_unique(array_map('intval', $ids)));
        }

        if ($ids) {
            foreach($ids as $userId) {
                $this->_pointsStatistic->addStatItem($this->_point, $userId, 'manual', $this->_point->channel_id);
            }

            $this->_userIDs = $ids;

            return true;
        }

        return false;
    }

    /**
     * @return array
     */
    private function _getRecipients()
    {
        $recipients = [];

        $data = explode(',', $this->_point->recipients);

        foreach($data as $item) {
            list($key, $id) = explode(':',$item);

            if (empty($recipients[$key])) {
                $recipients[$key] = [];
            }

            $recipients[$key][] = trim(abs(intval($id)));
        }

        return $recipients;
    }
}
