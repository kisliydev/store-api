<?php

namespace App\Helpers;

use App\Models\AcademyItem;
use App\Models\Thread;
use App\Models\UserChannelStatus;
use App\Notifications\OneSignalNotification;
use App\Traits\Shortcodes;
use App\Transformers\Messages\UserIdTransformer;
use App\Jobs\OneSignalPushNotifications;
use App\Models\AutomaticMessage;
use App\Traits\SocketNotifications;
use App\Traits\Notifications;

class NotificationsHelper
{
    use SocketNotifications;
    use Shortcodes;
    use Notifications;

    /**
     * @var array
     */
    private $_data = [];

    /**
     * @param \App\Models\Thread $thread
     * @param \App\Models\UserChannelStatus $userChannelStatus
     * @param \App\Transformers\Messages\UserIdTransformer $transformer
     * @param \App\Models\AutomaticMessage $message
     * @param array|null $data
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function notifyAll(
        Thread $thread,
        UserChannelStatus $userChannelStatus,
        UserIdTransformer $transformer,
        AutomaticMessage $message,
        array $data = null
    ) {
        $this->_data = $data;

        $subject = $this->parseShortcodes($message->title);
        $content = $this->parseShortcodes($message->description);

        $threadAuthor = $this->_setMessageAuthorID($message->channel_id);

        if ($threadAuthor) {
            $newThread = $thread->_createEntity((object) [
                'channel_id'      => $message->channel_id,
                'recipients'      => 'all:0',
                'type'            => 'auto_level',
                'subject'         => $subject,
                'message_content' => $content,
                'data'            => json_encode(['message' => $message->id, 'type' => $message->type]),
            ], 'closed', $threadAuthor, true);
        }

        $data = [
                'thread_id' => $newThread->id ?? '',
                'channel_id' => $message->channel_id,
                'channel_name' => $message->channel->name,
                'type' => OneSignalNotification::NOTIFICATION_TYPE_AUTOMATIC_MESSAGE
            ];

        dispatch((new OneSignalPushNotifications(
            $userChannelStatus->getChannelUserIds($message->channel_id, $transformer),
            [
                'subject'    => $message->channel->name.', '.$subject,
                'body'       => $content,
                'channel_id' => $message->channel_id,
                'data'       => $data
            ],
            get_class($this)
        ))->onQueue(config('queue.tube.push-notifications')));

        $this->notify([
            'type'       => 'new_thread',
            'channel_id' => $newThread->channel_id,
            'item_id'    => $newThread->id,
        ]);
    }

    /**
     * callback for %%quiz_points%% shortcode
     * @return string
     */
    public function getQuizPointsShortcodeContent()
    {
        return empty($this->_data['quiz_id']) ? '' : $this->_getAcademyItemData($this->_data['quiz_id'], 'points');
    }

    /**
     * callback for %%quiz_title%% shortcode
     * @return string
     */
    public function getQuizTitleShortcodeContent()
    {
        return empty($this->_data['quiz_id']) ? '' : $this->_getAcademyItemData($this->_data['quiz_id'], 'title');
    }

    /**
     * callback for %%survey_points%% shortcode
     * @return string
     */
    public function getSurveyPointsShortcodeContent()
    {
        return empty($this->_data['survey_id']) ? '' : $this->_getAcademyItemData($this->_data['survey_id'], 'points');
    }

    /**
     * callback for %%survey_title%% shortcode
     * @return string
     */
    public function getSurveyTitleShortcodeContent()
    {
        return empty($this->_data['survey_id']) ? '' : $this->_getAcademyItemData($this->_data['survey_id'], 'title');
    }

    /**
     * @param int $id
     * @param string $key
     * @return string
     */
    private function _getAcademyItemData(int $id, string $key)
    {
        return AcademyItem::find($id)->$key ?? '';
    }
}
