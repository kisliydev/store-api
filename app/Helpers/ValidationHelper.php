<?php

namespace App\Helpers;

class ValidationHelper
{
    /**
     * @param string $string
     * @return false|int
     */
    public function validateHEXcolor(string $string)
    {
        return preg_match("/#(([a-f0-9]{3})|([a-f0-9]{6})|([a-f0-9]{8}))/i", $string);
    }

    /**
     * @param string $string
     * @return false|int
     */
    public function validateRGBAcolor(string $string)
    {
        return preg_match("/rgba\((\s*\d+\s*,){3}\s*(([0]*\.[\d]+)|[1])\s*\)/i", $string);
    }

    public function validateRGBcolor(string $string) {
        return preg_match("/rgb\((\s*\d+\s*,){2}(\s*\d+\s*)\)/i", $string);
    }

    /**
     * @param string $string
     * @return bool
     */
    public function validateColor(string $string)
    {
        return $this->validateHEXcolor($string)
            || $this->validateRGBcolor($string)
            || $this->validateRGBAcolor($string);
    }
}
