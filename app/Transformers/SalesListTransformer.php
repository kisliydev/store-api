<?php

namespace App\Transformers;

use \Illuminate\Support\Collection;
class SalesListTransformer extends CollectionTransformer
{
    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
        $data = [];
        $collection->each(function ($item) use (&$data) {
            $date = $item['day'];
            if (! array_key_exists($date, $data)) {
                $data[$date] = [];
            }
            unset($item['day']);
            $data[$date][] = $item;
        });
        return collect($data);
    }
}
