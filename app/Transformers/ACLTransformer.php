<?php

namespace App\Transformers;

use \Illuminate\Support\Collection;
use App\Models\ACL;

class ACLTransformer extends CollectionTransformer
{
    /**
     * @var
     */
    private $_acl;

    /**
     * ACLTransformer constructor.
     *
     * @param \App\Models\ACL $acl
     */
    public function __construct(ACL $acl)
    {
        $this->_acl = $acl;
    }

    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
        $defaultPermissions = $this->_acl->getRegisteredPermissions();

         return $collection->map(function ($item)  use ($defaultPermissions) {
            $permissionsList = [];

            $item->permissions->each(function ($permission) use (&$permissionsList) {
                $permissionsList[$permission->name] = true;
            });

            $bannedPermissions = array_diff($defaultPermissions, array_keys($permissionsList));
            if ($bannedPermissions) {
                $bannedPermissions = array_map(function() {
                    return false;
                },array_flip($bannedPermissions));
                $permissionsList = array_merge($permissionsList, $bannedPermissions);
            }

            return [
                'name' => $item->name,
                'permissions' => $permissionsList,
            ];
        });
    }
}
