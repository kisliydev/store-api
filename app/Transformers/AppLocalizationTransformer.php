<?php

namespace App\Transformers;

use \Illuminate\Support\Collection;
use App\Traits\ChannelMetaShortcodes;

class AppLocalizationTransformer extends CollectionTransformer
{
    use ChannelMetaShortcodes;

    /**
     * @var object
     */
    private $_channel;

    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
        $this->_channel = $collection->first();

        return $collection->map(function ($item) {
            return [
                'app_localization' => is_array($item['app_localization']) ? $this->parseShortcodesRecursive($item['app_localization']) : $item['app_localization']
            ];
        });
    }

    /**
     * Callback for %%users_channels%% shortcode
     * @return string
     */
    public function getUsersChannelsShortcodeContent()
    {
        $brandID  = intval($this->_channel->countryBrand->brand_id);
        $channels = auth()->user()->getChannels($brandID)->pluck('name')->toArray();
        return implode(', ', $channels);
    }
}
