<?php

namespace App\Transformers;

use \Illuminate\Support\Collection;

class ChannelListTransformer extends CollectionTransformer
{
    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
         return $collection->map(function ($item) {
             $unreadCount = $item->unreadMessages->pluck('count');
             $item->unread_messages_count = isset($unreadCount[0]) ? $unreadCount[0] : 0;
             unset($item->unreadMessages);
             return $item;
        });
    }
}