<?php

namespace App\Transformers;

use App\Models\Channel;
use \Illuminate\Support\Collection;
use App\Models\Media;
use App\Traits\ChannelMetaShortcodes;

class AppLayoutTransformer extends CollectionTransformer
{
    use ChannelMetaShortcodes;

    /**
     * @var object \App\Models\Campaign
     */
    private $_campaign;

    /**
     * @var object \App\Models\Channel
     */
    private $_channel;

    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
        $this->_campaign = $collection->first();
        $this->_channel  = Channel::find($this->_campaign->channel_id);

        return $collection->map(function ($item) {
            $data = $this->_addMedia($item['app_layout']);
            $data['eula'] = [
                'content' => preg_replace('/\n|[\s]{2,}/', '', $this->parseShortcodes(view('terms_and_conditions')->render()))
            ];
            return ['app_layout' => $data, 'status' => $item['status']];
        });
    }

    /**
     * @param $option
     * @param string|null $key
     * @return array
     */
    private function _addMedia($option, string $key = null) {
        if (is_array($option)) {
            foreach($option as $optionKey => $value) {
                $option[$optionKey] = $this->_addMedia($value, $optionKey);
            }
        } elseif ($this->_campaign->isImageOption($key)) {
           $option = is_numeric($option) ? Media::find($option) : false;
        }
        return $option;
    }
}
