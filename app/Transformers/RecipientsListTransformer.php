<?php

namespace App\Transformers;

use App\Models\Dealer;
use App\Models\District;
use App\Models\User;
use \Illuminate\Support\Collection;
use App\Models\Delaer;

class RecipientsListTransformer extends CollectionTransformer
{
    /**
     * @var array
     */
    private $_storage = [
        'dealers' => [],
        'users' => [],
        'districts' => []
    ];

    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
        return $collection->map(function ($item) {
            $item->recipients = $this->_parseRecipients($item->recipients);
            return $item;
        });
    }

    /**
     * @param string $recipients
     * @return array
     */
    private function _parseRecipients(string $recipients)
    {
        $data = [
            'dealers' => [],
            'users' => [],
            'districts' => []
        ];

        if (empty($recipients)) {
            return $data;
        } elseif (preg_match("/all\:0/", $recipients)) {
            return ['all' => true];
        }

        foreach(explode(',', $recipients) as $raw) {
            list($type, $id) = explode(':', trim($raw));
            switch ($type) {
                case 'dealer_id':
                    array_push($data['dealers'], $this->_getDealerName($id));
                    break;
                case 'district_id':
                    array_push($data['districts'], $this->_getDistrictName($id));
                    break;
                case 'user_id':
                    array_push($data['users'], $this->_getUsertName($id));
                    break;
            }
        }

        return array_map(function($list) {
            return array_filter($list);
        }, $data);
    }

    /**
     * @param int $id
     * @return string
     */
    private function _getDealerName(int $id)
    {
        if (! array_key_exists($id, $this->_storage['dealers'])) {
            $this->_storage['dealers'][$id] = Dealer::find($id)->name ?? '';
        }

        return $this->_storage['dealers'][$id];
    }

    /**
     * @param int $id
     * @return string
     */
    private function _getDistrictName(int $id)
    {
        if (! array_key_exists($id, $this->_storage['districts'])) {
            $this->_storage['districts'][$id] = District::find($id)->name ?? '';
        }

        return $this->_storage['districts'][$id];
    }

    /**
     * @param int $id
     * @return string
     */
    private function _getUsertName(int $id)
    {
        if (! array_key_exists($id, $this->_storage['users'])) {
            $this->_storage['users'][$id] = User::find($id)->full_name ?? '';
        }

        return $this->_storage['users'][$id];
    }
}
