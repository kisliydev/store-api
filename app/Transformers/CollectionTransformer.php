<?php

namespace App\Transformers;

use App\Models\Media;
use App\Models\User;
use \Illuminate\Support\Collection;

abstract class CollectionTransformer implements CollectionTransformerInterface
{
    private $collection = null;

    /**
     * Init common collection transform it
     *
     * @param \Illuminate\Support\Collection $collection
     * @return \App\Transformers\CollectionTransformer
     */
    public function chain(Collection $collection): CollectionTransformer
    {
        $this->collection = $this->transform($collection);

        return $this;
    }

    /**
     * Transform common collection by new transformer
     *
     * @param \App\Transformers\CollectionTransformer $transformer
     * @return \App\Transformers\CollectionTransformer
     */
    public function transformer(CollectionTransformer $transformer): CollectionTransformer
    {
        $this->collection = $transformer->transform($this->collection);

        return $this;
    }

    /**
     * Return transformed common collection
     *
     * @return \Illuminate\Support\Collection
     */
    public function endOfChain(): Collection
    {
        return $this->collection;
    }

    /**
     * @param $item
     * @param string $mediaSlug
     * @return mixed
     */
    protected function getMedia($item, string $mediaSlug)
    {
        $media = new Media();
        $mediaId = "{$mediaSlug}_id";
        $fields = ["{$mediaSlug}_id", "{$mediaSlug}_path", "thumbnails"];
        if (empty($item->$mediaId)) {
            $item->$mediaSlug = null;
            foreach( $fields as $field) {
                unset($item->$field);
            }
        } else {
            $item->$mediaSlug = [];
            foreach($fields as $field) {
                switch ($field) {
                    case "{$mediaSlug}_id":
                        if ($item->$field) {
                            $item->$mediaSlug['id'] = $item->$field;
                        }
                        break;
                    case "{$mediaSlug}_path":
                        if ($item->$field) {
                            $item->$mediaSlug['path'] = $media->getMediaUrl($item->$field);
                        }
                        break;
                    case "thumbnails":
                        if ($item->$field) {
                            $item->$mediaSlug['thumbnails'] = $this->_getThumbnails($item->$field, $media);
                        }
                        break;
                }
                unset($item->$field);
            }
        }
        return $item;
    }

    /**
     * @param string $thumbnails
     * @param \App\Models\Media $media
     * @return array|null|string
     */
    private function _getThumbnails(string $thumbnails, Media $media) {
        try {
            $images = json_decode($thumbnails, true);
            $thumbnails = [];
            foreach($images as $key => $image) {
                $thumbnails[$key] = $media->getMediaUrl($image);
            }
            return $thumbnails;
        } catch(\Exception $exception) {
            return null;
        }
    }

    /**
     * @param User $item
     */
    protected function removeACL(User $item)
    {
        $item->setHidden(['role', 'acl']);
    }
}
