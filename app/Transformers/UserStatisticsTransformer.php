<?php

namespace App\Transformers;

use \Illuminate\Support\Collection;

class UserStatisticsTransformer extends CollectionTransformer
{
    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
         return $collection->map(function ($item) {
             return $this->getMedia($item, 'avatar');
        });
    }
}