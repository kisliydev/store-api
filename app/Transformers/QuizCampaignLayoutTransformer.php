<?php

namespace App\Transformers;

use Carbon\Carbon;
use Illuminate\Support\Collection;

class QuizCampaignLayoutTransformer extends CollectionTransformer
{
    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
        $campaignLayout = $collection->first();
        $activeCampaignLayout = collect([
            'brand_main_color' => $campaignLayout['brand_main_color'],
            'brand_secondary_color' => $campaignLayout['brand_secondary_color'],
            'logo_height' => $campaignLayout['logo_height'],
            'brand_logo_image' => $campaignLayout['brand_logo_image']['path'],
        ]);

        return $activeCampaignLayout;
    }
}
