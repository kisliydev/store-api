<?php

namespace App\Transformers;

use \Illuminate\Support\Collection;

class BrandAcademyListTransformer extends CollectionTransformer
{
    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
         return $collection->map(function ($item) {
             $item->questions_count = $item->questions->count() ?? 0;
             unset($item->questions);
             return $item;
        });
    }
}
