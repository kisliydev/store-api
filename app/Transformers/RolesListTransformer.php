<?php

namespace App\Transformers;

use \Illuminate\Support\Collection;

class RolesListTransformer extends CollectionTransformer
{
    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
         return $collection->map(function ($item) {

            return [
                'role' => $item->name,
                'name' => $this->_getHumanReadableName($item->name),
            ];
        });
    }

    /**
     * @param string $name
     * @return string
     */
    private function _getHumanReadableName(string $name)
    {
        return ucwords(preg_replace("/[-_]+/", " ", $name));
    }
}
