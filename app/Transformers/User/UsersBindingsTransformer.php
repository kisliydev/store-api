<?php

namespace App\Transformers\User;

use App\Transformers\CollectionTransformer;
use \Illuminate\Support\Collection;

class UsersBindingsTransformer extends CollectionTransformer
{
    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
        if (isset($collection->first()->brand)) {
            $brands = $collection->map(function ($item) {
                $brand = $item->brand;
                unset($brand->settings, $brand->created_at, $brand->updated_at);
                if (isset($brand->logo)) {
                    unset($brand->logo->created_at, $brand->logo->updated_at);
                }

                return $brand;
            });
            $brand = current($brands->uniqueStrict('name')->values()->all());
            $countries = $collection->map(function ($item) {
                unset($item->brand, $item->created_at, $item->updated_at);

                return $item;
            });
            $brand->countries = $countries;
            $brand->available_countries = collect($countries)->pluck('id')->all();

            return collect([$brand]);
        }
        $brands = $collection->map(function ($brand) use ($collection) {
            $brand->available_brands = $collection->pluck('id')->all();

            return $brand;
        });

        return $brands;
    }
}
