<?php

namespace App\Transformers\User;

use App\Transformers\CollectionTransformer;
use \Illuminate\Support\Collection;

class ChannelUsersTransformer extends CollectionTransformer
{
    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
        return $collection->map(function ($item) {
            if ($item->countryBrand && $item->countryBrand->brand && $item->countryBrand->brand->brandUsers) {
                return $item->countryBrand->brand->brandUsers;
            }

            return [];
        });
    }
}