<?php

namespace App\Transformers\User;

use App\Transformers\CollectionTransformer;
use \Illuminate\Support\Collection;
use \Illuminate\Pagination\LengthAwarePaginator;
use App\Models\User;

class HeadQuorterTransformer extends CollectionTransformer
{
    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
        return $collection->map(function ($user) {
            return $this->getEmailPassword($user);
        });
    }

    /**
     * @param \Illuminate\Pagination\LengthAwarePaginator $paginated
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function transformPaginate(LengthAwarePaginator $paginated)
    {
        $paginated->getCollection()->transform(function($user) {
            return $this->getEmailPassword($user);
        });

        return $paginated;
    }

    /**
     * @param \App\Models\User $user
     * @return array
     */
    public function getEmailPassword(User $user)
    {
        return [
            'id'        => $user->id,
            'full_name' => $user->full_name,
            'email'     => $user->email,
            'password'  => $user->getPasswordFrom($user->email),
        ];
    }
}
