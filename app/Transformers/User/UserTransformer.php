<?php

namespace App\Transformers\User;

use App\Transformers\CollectionTransformer;
use Carbon\Carbon;
use \Illuminate\Support\Collection;

class UserTransformer extends CollectionTransformer
{
    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
        return $collection->map(function ($item) {
            if (starts_with($item['email'], 'missing_')) {
                unset($item['email']);
            }

            $item['server_time'] = Carbon::now()->format('Y-m-d\TH:i:s.uP T');

            foreach(['dealer', 'position'] as $key) {
                if (empty($item[$key])) {
                    continue;
                }

                $keyId = "{$key}_id";

                $item[$key]->id = $item[$key]->$keyId;
                unset($item[$key]->$keyId);
                unset($item[$key]->created_at);
                unset($item[$key]->updated_at);
                unset($item[$key]->channel_id);
                unset($item[$key]->deleted_at);
                unset($item[$key]->user_id);
            }

            return $item;
        });
    }
}
