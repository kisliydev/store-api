<?php

namespace App\Transformers\User;

use App\Transformers\CollectionTransformer;
use \Illuminate\Support\Collection;

class UsersListTransformer extends CollectionTransformer
{
    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
        return $collection->map(function ($item) {

            unset($item->updated_at);
            unset($item->deleted_at);

            if($item->channels->count()) {
                $item->channels->map(function($channel) {
                    unset($channel->pivot);
                });
            }
            return $item;
        });
    }
}
