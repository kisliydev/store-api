<?php

namespace App\Transformers\Messages;

use App\Transformers\CollectionTransformer;
use \Illuminate\Support\Collection;
use App\Models\User;

class MessagesSettingsPopulateEmailsTransformer extends CollectionTransformer
{
    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
        $user = app()->make(User::class);

        return $collection->map(function ($item) use ($user) {

            if ($item->send_to) {
                $item->send_to = $this->getEmailsForIds($item->send_to, $user);
            }
            if ($item->reply_from) {
                $item->reply_from = $this->getEmailsForIds($item->reply_from, $user);
            }
            if ($item->auto_reply_from) {
                $item->auto_reply_from = $this->getEmailsForIds($item->auto_reply_from, $user);
            }

            return $item;
        });
    }

    /**
     * @param $ids
     * @param \App\Models\User $user
     * @return \Illuminate\Support\Collection
     */
    private function getEmailsForIds($ids, User $user)
    {
        $ids = $this->_absIntArr($ids);
        return $ids ? $user->removeAppends(
            $user->select('id', 'email','first_name', 'last_name')
                ->whereIn('id', $ids)
                ->get(),
            ['full_name']
        ) : false;
    }

    /**
     * @param $arr
     * @return array
     */
    private function _absIntArr($arr)
    {
        return array_filter(array_map(function($item) {
            return abs(intval($item));
        }, (array)$arr));

    }
}
