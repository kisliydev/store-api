<?php

namespace App\Transformers\Messages;

use App\Transformers\CollectionTransformer;
use \Illuminate\Support\Collection;

class MessagesSettingsDecodeTransformer extends CollectionTransformer
{
    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
        return $collection->map(function ($item) {
            if ($item['messages_settings']) {
                return is_array($item['messages_settings']) ? (object)$item['messages_settings'] : json_decode($item['messages_settings']);
            }

            return $item;
        });
    }
}
