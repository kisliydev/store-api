<?php

namespace App\Transformers\Messages;

use App\Transformers\CollectionTransformer;
use \Illuminate\Support\Collection;

class PlayerIdTransformer extends CollectionTransformer
{
    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
        return $collection->map(function ($item) {
            return $item['player_id'] ?? null;
        });
    }
}
