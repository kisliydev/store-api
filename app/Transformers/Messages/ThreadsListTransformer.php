<?php

namespace App\Transformers\Messages;

use App\Transformers\CollectionTransformer;
use \Illuminate\Support\Collection;
use App\Models\User;

class ThreadsListTransformer extends CollectionTransformer
{
    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
        return $collection->map(function ($item) {
            if(!empty($item->author) && $item->author instanceof User) {
                $this->removeACL($item->author);
            }
            if(!empty($item->lastMessage->author) && $item->lastMessage->author instanceof User) {
                $this->removeACL($item->lastMessage->author);
            }
            return $item;
        });
    }
}
