<?php

namespace App\Transformers\Messages;

use App\Transformers\CollectionTransformer;
use \Illuminate\Support\Collection;

class UsersThreadTransformer extends CollectionTransformer
{
    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
         return $collection->map(function ($item) {
             $item->unread_messages_count = $item->unreadByUserMessages->count();
             unset($item->unreadByUserMessages);
             return $item;
        });
    }
}