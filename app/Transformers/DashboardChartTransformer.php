<?php

namespace App\Transformers;

use Carbon\Carbon;
use Illuminate\Support\Collection;

class DashboardChartTransformer extends CollectionTransformer
{
    private $dateFrom;
    private $dateTo;

    /**
     * @param string $dateFrom
     * @param string $dateTo
     * @return \App\Transformers\DashboardChartTransformer
     */
    public function setDates(string $dateFrom, string $dateTo): DashboardChartTransformer
    {
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;

        return $this;
    }

    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
        $dateFrom = Carbon::parse($this->dateFrom);
        $dateTo = Carbon::parse($this->dateTo);
        while($dateFrom->lessThanOrEqualTo($dateTo)) {
            $data = $collection->where('date', $dateFrom->toDateString())->first();
            if (!$data) {
                $collection->push([
                    'cum_others' => 0,
                    'cum_sold_products' => 0,
                    'cum_total_count' => 0,
                    'date' => $dateFrom->toDateString(),
                    'others' => 0,
                    'passed_items' => 0,
                    'sold_products' => null,
                    'total_count' => 0,
                ]);
            }
            $dateFrom = $dateFrom->addDay();
        }
        $collection = $collection->sortBy('date')->values();

        return $collection;
    }
}