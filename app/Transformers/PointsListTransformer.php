<?php

namespace App\Transformers;

use \Illuminate\Support\Collection;
use App\Models\AcademyItem;
use App\Models\Product;
use App\Models\Level;
use App\Models\ManualPoint;
use Illuminate\Support\Facades\DB;

class PointsListTransformer extends CollectionTransformer
{
    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
         return $collection->map(function ($item) {
             if ($item->user) {
                 $item->user->withAppends(false);
             }

             $item->object = app()['cache']->remember("{$item->object_type}_{$item->object_id}", 60, function() use ($item) {

                 if (empty($item->object_id)) {
                     return null;
                 }

                 switch ($item->object_type) {
                     case 'quiz':
                     case 'survey':
                         return AcademyItem::where('id', $item->object_id)
                             ->where('type', $item->object_type)
                             ->select('id', DB::raw('title as name'))
                             ->first();
                     case 'level':
                         return Level::where('id', $item->object_id)
                             ->select('id', DB::raw('title as name'))
                             ->first();
                     case 'product':
                         return Product::where('id', $item->object_id)
                             ->select('id', 'name')
                             ->first();
                     case 'manual':
                         return ManualPoint::where('id', $item->object_id)
                             ->select('id', DB::raw('title as name'))
                             ->first();
                     default:
                         return null;
                 }

             });
            return $item;
        });
    }
}
