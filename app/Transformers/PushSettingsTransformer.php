<?php

namespace App\Transformers;

use \Illuminate\Support\Collection;

class PushSettingsTransformer extends CollectionTransformer
{
    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {
        return $collection->map(function ($item) {
            $data = [
                'id' => $item->id,
                'brand_name' => $item->name
            ];

            foreach($item->settings_keys as $key) {
                $data[$key] = $item->settings[$key] ?? '';
            }

            return $data;
        });
    }
}
