<?php

namespace App\Transformers;

use \Illuminate\Support\Collection;

class ChannelStatisticsTypeTransformer extends CollectionTransformer
{
    /**
     * @param \Illuminate\Support\Collection $collection
     * @return \Illuminate\Support\Collection
     */
    public function transform(Collection $collection): Collection
    {

        $inCollectionTypes = $collection->pluck('object_type')->toArray();
        $defaultTypes = ['product', 'quiz', 'survey', 'level', 'manual'];
        $notInCollection = array_diff($defaultTypes, $inCollectionTypes);
        if ($notInCollection) {
            foreach($notInCollection as $type) {
                $collection->push((object)[
                    'object_type' => $type,
                    'amount' => 0,
                    'points' => 0,
                    'percentage' => 0,
                ]);
            }
        }
        return $collection;
    }
}