<?php

namespace App\Transformers;

use \Illuminate\Support\Collection;

interface CollectionTransformerInterface
{
    public function transform(Collection $collection): Collection;

    public function chain(Collection $collection): CollectionTransformer;

    public function transformer(CollectionTransformer $transformer): CollectionTransformer;

    public function endOfChain(): Collection;
}
