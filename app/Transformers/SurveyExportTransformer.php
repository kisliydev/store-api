<?php

namespace App\Transformers;

use Illuminate\Support\Collection;

class SurveyExportTransformer extends CollectionTransformer
{
    public function transform(Collection $collection): Collection
    {
        $title = $collection->get('survey_results')->first()->title;
        $questions = collect();
        $answers = collect();
        $users = $collection['users'];
        $summary = collect();
        $collection['survey_results']->map(function ($item) use ($questions, $answers, $users, $summary) {
            $item->questions->map(function ($question) use ($questions, $answers, $users, $summary) {
                // Summary sheet
                if (!$summary->has($question->id)) {
                    $summary->put($question->id, collect());
                }
                $summary[$question->id] = collect(['question' => [$question->text]]);
                if ($question->type == 'text_input') {
                    $summary[$question->id]->put('total', [$question->userAnswers->count()]);
                }
                $usersCount = $users->count();
                $question->answers->map(function ($answer) use ($question, $summary, $usersCount) {
                    if (!$summary[$answer->question_id]->has('answers')
                        && !$summary[$answer->question_id]->has('responses')
                        && !$summary[$answer->question_id]->has('total')
                    ) {
                        $summary[$answer->question_id]->put('answers', collect());
                        $summary[$answer->question_id]->put('responses', collect());
                        $summary[$answer->question_id]->put('total', collect());
                    }
                    $summary[$answer->question_id]['answers']->push($answer->text);
                    $usersAnswersCount = $question->userAnswers->where('text', $answer->text)->count();
                    $responses = $usersCount == 0 ? '0%' : sprintf('%d%%', round($usersAnswersCount / $usersCount * 100, 2));
                    $summary[$answer->question_id]['responses']->push($responses);
                    $summary[$answer->question_id]['total']->push($usersAnswersCount);
                });
                // Check for "Other" answer option
                if ($question->data['show_alternative'] == true) {
                    $customAnswers = $question->userAnswers->whereNotIn('text', $question->answers->pluck('text')->all());
                    if ($customAnswers->isNotEmpty()) {
                        $summary[$question->id]['answers']->push('Other');
                        $customAnswersCount = $customAnswers->count();
                        $responses = $customAnswersCount == 0 ? '0%' : sprintf('%d%%', round($customAnswersCount / $usersCount * 100, 2));
                        $summary[$question->id]['responses']->push($responses);
                        $summary[$question->id]['total']->push($customAnswersCount);
                    }
                }

                // Personal answers sheet
                $questions->push($question->text);
                $userAnswers = $question->userAnswers->groupBy('user_id');
                $users->map(function ($item) use ($answers, $userAnswers) {
                    $userKey = $item->email === null
                        ? sprintf('%s %s', $item->first_name, $item->last_name)
                        : $item->email;
                    if (!$answers->has($userKey)) {
                        $answers->put($userKey, collect());
                        $answers[$userKey]->push($userKey);
                    }
                    if (!$userAnswers->get($item->id)) {
                        $answers[$userKey]->push(null);
                        return false;
                    }
                    if ($userAnswers[$item->id]->count() > 1) {
                        $answers[$userKey]->push($userAnswers[$item->id]->implode('text', '; '));
                        return false;
                    }
                    $answers[$userKey]->push($userAnswers[$item->id]->first()->text);
                });
            });
        });

        return collect(compact('title', 'questions', 'answers', 'users', 'summary'));
    }
}
