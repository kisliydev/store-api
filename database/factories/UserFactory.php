<?php

use Faker\Generator as Faker;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\User::class, function (Faker $faker) {
    static $password;
    static $verified;
    static $email;
    static $status;
    static $firstName;
    static $lastName;
    static $phone;

    return [
        'first_name' => $firstName ?? $faker->firstName,
        'last_name' => $lastName ?? $faker->lastName,
        'email' => $email ?? $faker->unique()->safeEmail,
        'phone' => $phone ?? $faker->unique()->e164PhoneNumber,
        'password' => $password ?: $password = bcrypt('secret'),
        'verified' => $verified ?? false,
        'status' => $status ?? 'active',
        'email_token' => str_random(10),
    ];
});

$factory->define(Spatie\Permission\Models\Role::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});

$factory->define(Spatie\Permission\Models\Permission::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
    ];
});

$factory->define(App\Models\Country::class, function (Faker $faker) {
    static $code;

    return [
        'name' => $faker->country,
        'code' => $code,
    ];
});

$factory->define(App\Models\Brand::class, function (Faker $faker) {
    static $logo_id;
    static $settings;
    static $legal_name;
    static $owner;
    static $description;

    $defaultSettings = [
        'app_name'               => "StoreDev",
        'app_url_apple'          => "https://itunes.apple.com",
        'app_url_google'         => "https://play.google.com",
        "onesignal_app_id"       => "6bf95b9d-21ee-473e-a59a-3ea6a9d4265b",
        "onesignal_rest_api_key" => "ZDg2M2Q5Y2EtMmUzYy00N2VhLWFjMWYtOWFjZTQ1YWJjNWU2",
        'slug'                   => $faker->word,
    ];

    return [
        'name'        => $faker->catchPhrase,
        'legal_name'  => $legal_name ?? $faker->company,
        'owner'       => $owner ?? $faker->name,
        'logo_id'     => $logo_id,
        'description' => $description ?? $faker->sentence,
        'settings'    => $settings ?? $defaultSettings,
    ];
});

$factory->define(App\Models\CountryBrand::class, function (Faker $faker) {
    static $name;
    static $brand_id;

    return [
        'name' => $name ?? $faker->country,
        'brand_id' => $brand_id,
    ];
});

$factory->define(App\Models\Channel::class, function (Faker $faker) {
    static $country_brand_id;
    static $invite_code;
    static $messages_settings;
    static $app_settings;
    static $app_localization;
    static $store_name;
    static $store_legal_name;
    static $store_owner;

    $channel = new App\Models\Channel();

    return [
        'name'              => $faker->catchPhrase,
        'country_brand_id'  => $country_brand_id ?? 1,
        'invite_code'       => $invite_code ?? $channel->getInviteCode(),
        'messages_settings' => $messages_settings ?? $channel->getDefaultMessagesSettings(),
        'app_settings'      => $app_settings ?? $channel->getDefaultAppSettings(),
        'app_localization'  => $app_localization ?? $channel->getDefaultAppLocalization(),
        'store_name'        => $store_name ?? $faker->company,
        'store_legal_name'  => $store_legal_name ?? $faker->company,
        'store_owner'       => $store_owner ?? $faker->name,
    ];
});

$factory->define(App\Models\UserChannelStatus::class, function () {
    static $user_id;
    static $channel_id;
    static $status;

    return [
        'user_id'    => $user_id,
        'channel_id' => $channel_id,
        'status'     => $status ?? 'active',
    ];
});

$factory->define(App\Models\District::class, function (Faker $faker) {
    static $channel_id;
    static $logo_id;

    return [
        'name'       => $faker->company,
        'channel_id' => $channel_id,
        'logo_id'    => $logo_id,
    ];
});

$factory->define(App\Models\League::class, function (Faker $faker) {
    static $channel_id;
    static $logo_id;

    return [
        'name'       => $faker->company,
        'channel_id' => $channel_id,
        'logo_id'    => $logo_id,
    ];
});

$factory->define(App\Models\Dealer::class, function (Faker $faker) {
    static $channel_id;
    static $district_id;
    static $logo_id;
    static $weight_points;

    return [
        'name' => $faker->company,
        'channel_id' => $channel_id,
        'district_id' => $district_id ?? 0,
        'logo_id' => $logo_id ?? 0,
        'weight_points' => $weight_points ?? $faker->optional($weight = 0.7, $default = 1.0)->randomFloat(1, 0, 99.9),
        'location' => $faker->address,
    ];
});

$factory->define(App\Models\UsersDealers::class, function () {
    static $user_id;
    static $dealer_id;

    return [
        'user_id' => $user_id,
        'dealer_id' => $dealer_id,
    ];
});

$factory->define(App\Models\UserPosition::class, function () {
    static $user_id;
    static $position_id;

    return [
        'user_id' => $user_id,
        'position_id' => $position_id,
    ];
});

$factory->define(App\Models\Position::class, function (Faker $faker) {
    static $channel_id;
    static $is_manager;

    return [
        'name' => $faker->jobTitle,
        'channel_id' => $channel_id,
        'weight_points' => $weight_points ?? $faker->optional($weight = 0.7, $default = 1.0)->randomFloat(1, 0, 99.9),
        'is_manager' => !! $is_manager,
    ];
});

$factory->define(App\Models\PointsStatistic::class, function (Faker $faker) {
    static $channel_id;
    static $user_id;
    static $object_id;
    static $earned_points;
    static $units_number;
    static $object_type;
    static $event_id;

    $types = ['product', 'quiz', 'survey', 'level', 'manual'];

    return [
        'user_id' => $user_id ?? 1,
        'channel_id' => $channel_id ?? 1,
        'object_type' => $object_type ?? $types[$faker->numberBetween(0,3)],
        'object_id' => $object_id ?? $faker->numberBetween(0,10000),
        'earned_points' => $earned_points ?? $faker->numberBetween(100, 1000),
        'units_number' => $units_number ?? 1,
        'event_id' => $event_id ?? null,
    ];
});

$factory->define(App\Models\Product::class, function (Faker $faker) {
    static $channel_id;
    static $points;

    return [
        'name' => $faker->text( 50),
        'channel_id' => $channel_id ?? 1,
        'text_before_amount' => $faker->text( 50),
        'text_after_amount' => $faker->text(50),
        'default_amount' => $faker->numberBetween(0,10),
        'points' => $points ?? $faker->numberBetween(100, 1000),
        'use_weight_points' => $faker->boolean(50),
        'order' => $faker->numberBetween(0, 10),
    ];
});

$factory->define(App\Models\AcademyItem::class, function (Faker $faker) {
    static $channel_id;
    static $type;
    static $thumbnail_id;
    static $background_id;
    static $started_at;
    static $finished_at;
    static $status;
    static $points;

    return [
        'channel_id' => $channel_id ?? 1,
        'type' => $type ?? 'quiz',
        'title' => $faker->text( 50),
        'description' => $faker->text(),
        'short_description' => $faker->sentence(),
        'complete_message' => $faker->sentence(),
        'fail_message' => $faker->sentence(),
        'start_button_text' => $faker->text(5),
        'video_url' => $faker->url(),
        'use_weight_points' => $faker->boolean(50),
        'points' => $points ?? $faker->numberBetween(100, 1000),
        'thumbnail_id' => $thumbnail_id ?? 0,
        'background_id' => $background_id ?? 0,
        'started_at' => $started_at ?? date("Y-m-d H:i:s", strtotime('tomorrow')),
        'finished_at' => $finished_at ?? date("Y-m-d H:i:s", strtotime('next month')),
        'status' => $status ?? 'draft',
    ];
});

$factory->define(App\Models\Question::class, function(Faker $faker) {
    static $item_id;
    static $type;
    static $data;
    return [
        'item_id' => $item_id,
        'type' => $type ?? 'single_choice',
        'text' => trim($faker->sentence, '.') . '?',
        'order' => $faker->numberBetween(0, 10),
        'data' => $data ?? [],
    ];
});

$factory->define(App\Models\Answer::class, function (Faker $faker) {
    static $question_id;
    static $image_id;

    return [
        'question_id' => $question_id,
        'text' => $faker->sentence,
        'order' => $faker->numberBetween(0, 10),
        'correct' => $faker->boolean(25),
        'image_id' => $image_id ?? 0,
    ];
});

$factory->define(App\Models\BrandUser::class, function() {
    static $brand_id;
    static $user_id;
    static $type;

    return [
        'brand_id' => $brand_id,
        'user_id' => $user_id,
        'brand_type' => $type,
    ];
});

$factory->define(App\Models\ToDoItem::class, function(Faker $faker) {
    static $text;
    static $order;

    return [
        'text' => $text ?? $faker->sentence(),
        'order' => $order ?? $faker->numberBetween(0,10),
    ];
});

$factory->define(App\Models\Media::class, function(Faker $faker) {
    static $channel_id;
    static $type;
    static $path;

    $path = $path ?? $faker->image();
    /** In case if $path is not set and faker's images service isn't available **/
    if (! $path) {
        $path = storage_path('dummy-files/image.jpg');
    }

    return [
        'channel_id' => $channel_id ?? null,
        'type' => $type ?? 'attachments',
        'path' => $path,
    ];
});

$factory->define(App\Models\ManualPoint::class, function(Faker $faker) {
    static $channel_id;
    static $title;
    static $description;
    static $points;
    static $recipients;

    return [
        'channel_id' => $channel_id ?? 1,
        'title' => $title ?? $faker->text( 200),
        'description' => $description ?? $faker->sentence,
        'points' => $points ?? $faker->numberBetween(100,1000),
        'recipients' => $recipients ?? 'all:0',
    ];
});

$factory->define(App\Models\Localization::class, function(Faker $faker) {
    static $name;
    static $translations;

    $channel = new App\Models\Channel();

    return [
        'name' => $name ?? $faker->country,
        'translations' => $translations ?? $channel->getDefaultAppLocalization(),
    ];
});

$factory->define(App\Models\Language::class, function(Faker $faker) {
    static $name;
    static $local;

    return [
        'name' => $name ?? $faker->country,
        'local' => $local,
    ];
});

$factory->define(App\Models\PlayerId::class, function() {
    static $playerId;
    static $statusID;

    return [
        'player_id' => $playerId,
        'status_id' => $statusID,
    ];
});


$factory->define(App\Models\UserAnswer::class, function (Faker $faker) {
    static $user_id;
    static $question_id;

    return [
        'user_id' => $user_id,
        'question_id' => $question_id,
        'text' => $faker->sentence,
    ];
});

$factory->define(App\Models\ProductAssortmentCategory::class, function(Faker $faker) {
    static $channel_id;
    static $title;
    static $order;

    return [
        'channel_id' => $channel_id ?? 1,
        'name' => $title ?? $faker->sentence,
        'order' => $order ?? $faker->numberBetween(0,10),
    ];
});

$factory->define(App\Models\ProductAssortment::class, function(Faker $faker) {
    static $channel_id;
    static $category_id;
    static $title;
    static $description;
    static $link;
    static $sku;
    static $thumbnail_id;
    static $order;

    return [
        'channel_id' => $channel_id ?? 1,
        'category_id' => $category_id ?? null,
        'name' => $title ?? $faker->sentence,
        'link' => $link ?? $faker->url,
        'sku' => $sku ?? $faker->word,
        'thumbnail_id' => $thumbnail_id ?? null,
        'description' => $description ?? $faker->paragraph,
        'order' => $order ?? $faker->numberBetween(0,10),
    ];
});

$factory->define(App\Models\AppStatistic::class, function() {
    static $event;
    static $user_id;
    static $channel_id;
    static $data;

    return [
        'event' => $event ?? 'sale',
        'user_id' => $user_id,
        'channel_id' => $channel_id,
        'data' => $data ?? '',
    ];
});

$factory->define(App\Models\Campaign::class, function(Faker $faker) {
    static $channel_id;
    static $name;
    static $started_at;
    static $sale_started_at;
    static $sale_finished_at;
    static $finished_at;
    static $status;
    static $app_layout;

    $now = Carbon::now();
    $campaign = new App\Models\Campaign();

    return [
        'channel_id'       => $channel_id ?? 1,
        'name'             => $name ?? $faker->company,
        'started_at'       => $started_at ?? $now->addMinute(1)->toDateTimeString(),
        'sale_started_at'  => $sale_started_at ?? $now->addMinute(2)->toDateTimeString(),
        'sale_finished_at' => $sale_finished_at ?? $now->addDays(30)->toDateTimeString(),
        'finished_at'      => $finished_at ?? $now->addMinute(40)->toDateTimeString(),
        'status'           => $status ?? 'scheduled',
        'app_layout'       => $app_layout ?? $campaign->getDefaultAppLayout(),
    ];
});

$factory->define(App\Models\AutomaticMessage::class, function(Faker $faker) {
    static $channel_id;
    static $title;
    static $description;
    static $type;

    $types = app()->make(App\Models\AutomaticMessage::class)->getTypesList();
    return [
        'channel_id'  => $channel_id ?? 1,
        'title'       => $title ?? $faker->sentence,
        'description' => $description ?? $faker->paragraph,
        'type'        => $type ?? $faker->randomElement($types),
    ];
});

$factory->define(App\Models\Level::class, function(Faker $faker) {
    static $channel_id;
    static $title;
    static $description;
    static $type;
    static $type_data;
    static $points;
    static $use_weight_points;

    return [
        'channel_id'        => $channel_id ?? 1,
        'title'             => $title ?? $faker->sentence,
        'description'       => $description ?? $faker->paragraph,
        'type'              => $type ?? 'total_points',
        'type_data'         => $type_data ?? '{"amount":5}',
        'points'            => $points ?? 0,
        'use_weight_points' => !! $use_weight_points,
    ];
});

$factory->define(App\Models\AppStatistic::class, function(Faker $faker) {
    static $channel_id;
    static $user_id;
    static $event;
    static $data;

    return [
        'channel_id' => $channel_id ?? 1,
        'user_id'    => $user_id ?? 1,
        'event'      => $event ?? 'sale',
        'data'       => $data ?? null,
    ];
});

$factory->define(\App\Models\Thread::class, function(Faker $faker) {
    static $channel_id;
    static $author_id;
    static $parent_id;
    static $status;
    static $type;
    static $publish_at;
    static $subject;
    static $data;

    return [
        'channel_id'   => $channel_id ?? 1,
        'author_id'    => $author_id ?? 1,
        'parent_id'    => $parent_id ?? null,
        'status'       => $status ?? 'open',
        'type'         => $type ?? 'from_admin',
        'publish_at'   => $publish_at ?? null,
        'subject'      => $subject ?? $faker->sentence,
        'data'         => $data ?? null,
    ];
});
