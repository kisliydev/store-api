<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Recipient::class, function (Faker $faker) {
    static $thread_id;
    static $object_id;
    static $object_type;
    return [
        'thread_id' => $thread_id ?? 1,
        'object_id' => $object_id ?? 1,
        'object_type' => $object_type ?? 'user',
    ];
});
