<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Thread::class, function (Faker $faker) {
    static $channel_id;
    static $author_id;
    static $status;
    static $type;
    static $parent_id;

    return [
        'channel_id' => $channel_id ?? 1,
        'author_id' => $author_id ?? 1,
        'subject' => $faker->sentence,
        'type' => $type ?? 'from_user',
        'status' => $status ?? ($faker->boolean(60) ? 'open' : 'closed'),
        'parent_id' => $parent_id ?? null,
    ];
});
