<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Message::class, function (Faker $faker) {
    static $thread_id;
    static $author_id;
    static $content;
    return [
        'thread_id' => $thread_id ?? 1,
        'author_id' => $author_id ?? 1,
        'content' => $content ?? $faker->sentence,
    ];
});
